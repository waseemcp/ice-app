//
//  AppDelegate.m
//  ICE
//
//  Created by LandToSky on 11/10/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "DetailVC.h"
#import <IQKeyboardManager.h>
#import "MyActivityVC.h"
#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocompleteQuery.h>

#import "IcedDoneAlertView.h"
#import <TwitterKit/TwitterKit.h>
#import "GoogleSignIn/GoogleSignIn.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface AppDelegate ()
@end

float locDistance = -1.0;
CLLocationDistance meters;
@implementation AppDelegate

{
    CLLocation *currentLocation ;
    CLLocation *lastLocationFnded;
   
 
}
@synthesize updateloctimer,imagesArrayCopy,strFollowerCount,strFollowingCount;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.ViewMain = 0;
    
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];

    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Home"
     object:self];
    [GIDSignIn sharedInstance].clientID = @"700843244032-hkkef6gr6426789q2umsrlk723dkou6t.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].scopes = @[ @"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/plus.me",@"https://www.googleapis.com/auth/plus.circles.write",@"https://www.googleapis.com/auth/plus.stream.write" ,@"https://www.googleapis.com/auth/userinfo.profile" ];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    NSDictionary *userdata = [commonUtils getUserData];
    if (userdata) {
        commonUtils.userData = userdata;
        commonUtils.sessionToken = [NSUserDefaults.standardUserDefaults valueForKey:@"session_token"];
    }

    [self updateLocationManager];
    
    [[IQKeyboardManager sharedManager] setEnable:true];

    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:false];
    [[IQKeyboardManager sharedManager] setShouldShowTextFieldPlaceholder:false];
    
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    
    // Register Push Notification
  
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
      [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    if(launchOptions != nil && [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
        NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        appController.apnsMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"info"];
        [commonUtils setUserDefault:@"apns_message_arrived" withFormat:@"1"];
        NSDictionary *aps = userInfo[@"aps"];
        NSDictionary *apsData = aps[@"data"];
        if (apsData[@"is_edited"]) {
            NSString *iceId = [NSString stringWithFormat:@"%@",apsData[@"ice_id"]];
            commonUtils.noti_ice_id = iceId;
            commonUtils.is_Notification = YES;
            if (commonUtils.sessionToken) {
                //        dispatch_async(dispatch_get_main_queue(), ^{
                if (commonUtils.is_Notification) {
                    
                    
                    
                    //   commonUtils.is_Notification = NO;
                    
                    [self getIceDetail];
                    
                    //Do checking here.
                    
                }
                
                
                
                //        });
            }
        }
        else{
            NSString *iceId = [NSString stringWithFormat:@"%@",apsData[@"ice_id"]];
            commonUtils.noti_ice_id = iceId;
            commonUtils.is_Notification = YES;
            if (commonUtils.sessionToken) {
                //        dispatch_async(dispatch_get_main_queue(), ^{
                if (commonUtils.is_Notification) {
                    
                    
                    [self getIceDetail];
                }
            }
        }
   

    }
    
   //    [navigationController performSelector:@selector(pushNewViewFromTableCell:) withObject:view afterDelay:1.0]
    
    // First VC whether exists default user in app
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UINavigationController *rootNav = [storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    
    
//    [self.window setRootViewController:rootNav];
//    [self.window makeKeyAndVisible];
    
    
     [self performSelectorInBackground:@selector(startTenMinutesTimer) withObject:nil];
     [HNKGooglePlacesAutocompleteQuery setupSharedQueryWithAPIKey: @"AIzaSyA2RSi7u6lAFI8H9LC3NhnscRc44lgV5Jg"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyA2RSi7u6lAFI8H9LC3NhnscRc44lgV5Jg"];
    [GMSServices provideAPIKey:@"AIzaSyA2RSi7u6lAFI8H9LC3NhnscRc44lgV5Jg"];
  [[Twitter sharedInstance] startWithConsumerKey:@"hREAfPQSd8UTvS8NNumZVIcti" consumerSecret:@"H2VafqyGt0Gzy1tX4qd6RsjgNpMtrTt21j3s3GlD8e7bbkXISS"];
  
       return YES;
}



-(void)startTenMinutesTimer
{
    updateloctimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(callingServiceFetchData) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:updateloctimer forMode:NSRunLoopCommonModes];
}
-(void)callingServiceFetchData
{
    [NSThread sleepForTimeInterval:0.3];
    [self performSelectorInBackground:@selector(callService) withObject:nil];
}
-(void) callService

{
    
    [self UpdatingLocation];
    //     [self performSelectorInBackground:@selector(alertSoundPlay) withObject:nil];
    
}
-(void)UpdatingLocation
{
    
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = 10;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    [self.locationManager startUpdatingLocation];
    
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    if ([commonUtils is24hourFormat]) {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"time"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }else {
        [[NSUserDefaults standardUserDefaults] setValue:@"0" forKey:@"time"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    
    NSDictionary *userdata = commonUtils.userData;
    if (userdata) {
        [commonUtils saveUserdata:userdata];
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        if ([oneEvent.alertBody isEqualToString:@"Call"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            
        }else  if ([oneEvent.alertBody isEqualToString:@"CallLive"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            
        }else  if ([oneEvent.alertBody isEqualToString:@"CallComming"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            
        }    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
 
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
 
    if (commonUtils.sessionToken) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Home"
         object:self];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"live"
         object:self];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"comingsoon"
         object:self];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"upcoming"
         object:self];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"AddIceTime"
         object:self];
        
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
   [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSDictionary *userdata = commonUtils.userData;
    if (userdata) {
        [commonUtils saveUserdata:userdata];
    }
    
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = [app scheduledLocalNotifications];
    for (int i=0; i<[eventArray count]; i++)
    {
        UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
        
        if ([oneEvent.alertBody isEqualToString:@"Call"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            
        }else  if ([oneEvent.alertBody isEqualToString:@"CallLive"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            
        }else  if ([oneEvent.alertBody isEqualToString:@"CallComming"])
        {
            //Cancelling local notification
            [app cancelLocalNotification:oneEvent];
            
        }
    }
    
    
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - FB Sign up

//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//    return [[FBSDKApplicationDelegate sharedInstance] application:application
//                                                          openURL:url
//                                                sourceApplication:sourceApplication
//                                                       annotation:annotation
//            ];
//}

#ifdef __IPHONE_9_0 
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary *)options
{
  [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url
                                         sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
      [[Twitter sharedInstance] application:application openURL:url options:options];
[[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
   return YES;
}
#else
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                                                   openURL:url sourceApplication:sourceApplication
                                                annotation:annotation];   return YES;
}
#endif

#pragma mark - Remote Notification
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    NSString* newToken = [[[NSString stringWithFormat:@"%@",deviceToken]
                           stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [commonUtils setUserDefault:@"user_apns_id" withFormat:newToken];
    NSLog(@"My saved token is: %@", [commonUtils getUserDefault:@"user_apns_id"]);
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    [commonUtils setUserDefault:@"user_apns_id" withFormat:@""];
//      NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    NSLog(@"userInfouserInfo ===> %@",userInfo);
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Home"
     object:self];
    appController.apnsMessage = [[NSMutableDictionary alloc] init];
    appController.apnsMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"info"];
//    NSLog(@"APNS Info Fetched : %@", userInfo);
//    NSLog(@"My Received Message : %@", appController.apnsMessage);
    [commonUtils setUserDefault:@"apns_message_arrived" withFormat:@"1"];
    NSDictionary *aps = userInfo[@"aps"];
//    NSString *alertTitle = aps[@"alert"]
    NSDictionary *apsData = aps[@"data"];
    if (apsData[@"is_edited"]) {
     
        NSString *iceId = [NSString stringWithFormat:@"%@",apsData[@"ice_id"]];
        commonUtils.noti_ice_id = iceId;
        commonUtils.is_Notification = YES;
        if (commonUtils.sessionToken) {
            //        dispatch_async(dispatch_get_main_queue(), ^{
            if (commonUtils.is_Notification) {
                UIApplicationState state = [[UIApplication sharedApplication] applicationState];
                if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
                {
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"live"
                     object:self];
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"comingsoon"
                     object:self];
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"upcoming"
                     object:self];
                    
                    commonUtils.is_Notification = NO;
                    
                    [self getIceDetail];
                    
                    //Do checking here.
                }
            }
            
            
            
            //        });
        }
    }
    else{
    
        NSString *iceId = [NSString stringWithFormat:@"%@",apsData[@"ice_id"]];
        commonUtils.noti_ice_id = iceId;
        commonUtils.is_Notification = YES;
        if (commonUtils.sessionToken) {
            //        dispatch_async(dispatch_get_main_queue(), ^{
            if (commonUtils.is_Notification) {
                UIApplicationState state = [[UIApplication sharedApplication] applicationState];
                if (state == UIApplicationStateBackground || state == UIApplicationStateInactive)
                {
                    
                    commonUtils.is_Notification = NO;
                    
                    [self getIceDetail];
                    
                    //Do checking here.
                }else{
                    
                    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Notification Received" message:[[userInfo objectForKey:@"aps"] objectForKey:@"alert"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                    [alert show];
                    
                }
            }
        }
    }


    [application setApplicationIconBadgeNumber:[[[userInfo objectForKey:@"aps"] objectForKey:@"badge"] intValue]];
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    //Do Your Code.................Enjoy!!!!
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    
}



#pragma mark - CLLocationManagerDelegate
- (void)updateLocationManager {
    //    if([commonUtils getUserDefault:@"flag_location_query_enabled"] != nil && [[commonUtils getUserDefault:@"flag_location_query_enabled"] isEqualToString:@"1"]) {
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    //[_locationManager setDistanceFilter:804.17f]; // Distance Filter as 0.5 mile (1 mile = 1609.34m)
    //locationManager.distanceFilter=kCLDistanceFilterNone;
    _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [_locationManager startUpdatingLocation];
    
    // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
    //    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
    //        [_locationManager requestWhenInUseAuthorization];
    //    }
    
    if(IS_OS_8_OR_LATER) {
        [_locationManager requestAlwaysAuthorization];
    }
    //        [_locationManager startMonitoringSignificantLocationChanges];
    //        [_locationManager startUpdatingLocation];
    //    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
//      NSLog(@"didFailWithError: %@", error.localizedDescription);
    [self alertForLocationNotFound];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
   // NSLog(@"didUpdateToLocation: %@", [locations lastObject]);
   currentLocation = [locations lastObject];
    if (currentLocation != nil) {
        BOOL locationChanged = NO;
        if(![commonUtils getUserDefault:@"currentLatitude"] || ![commonUtils getUserDefault:@"currentLongitude"]) {
            locationChanged = YES;
        } else if(![[commonUtils getUserDefault:@"currentLatitude"] isEqualToString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]] || ![[commonUtils getUserDefault:@"currentLongitude"] isEqualToString:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]]) {
            locationChanged = YES;
        }
        if(locationChanged) {
            [commonUtils setUserDefault:@"currentLatitude" withFormat:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude]];
            [commonUtils setUserDefault:@"currentLongitude" withFormat:[NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude]];
            //            [commonUtils setUserDefault:@"barksUpdate" withFormat:@"1"];
        }
    }
    [self getAddressFromCoordinates];
    
    
    NSString * speed = [NSString stringWithFormat:@"%f",currentLocation.speed];
    if([speed doubleValue]<=0)
    {
        speed =@"0";
    }
    NSString *adresis = [NSString stringWithFormat:@"Address not Found"];
    CLGeocoder* geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        CLPlacemark *placemark = placemarks[0];
        if(currentLocation != nil)
        {
            NSString *latitude  =   [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
            NSString *longitude =  [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        
            if(lastLocationFnded!=nil)  {
                meters = [currentLocation distanceFromLocation:lastLocationFnded];
                locDistance = meters;
            }
            
          //  NSLog(@"meters %f",meters);
         
            if(commonUtils.sessionToken!=nil)
            {
                lastLocationFnded=currentLocation;
                [self updateLatLngApi:latitude :longitude];
            }
            
            
        };
        
    }];

     //[_locationManager stopUpdatingLocation];
    //    [self updateUserLocation];
}

- (void)updateUserLocation {  //for update user's coordinate automatically
    NSString *msg = [NSString stringWithFormat:@"%@:%@", [commonUtils getUserDefault:@"currentLatitude"], [commonUtils getUserDefault:@"currentLongitude"]];
  //  [commonUtils showAlert:@"Location Updated" withMessage:msg];
}



#pragma mark - Log Out
-(void) logOut
{
    // Remove data from singleton (where all my app data is stored)
    if([[commonUtils getUserDefault:@"logged_out"] isEqualToString:@"1"]) {
        [commonUtils removeUserDefault:@"logged_out"];
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        [FBSDKAccessToken setCurrentAccessToken:nil];
        
    }
    
    // Show login screen
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    UINavigationController *nav = [storyboard instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    
    [self.window setRootViewController:nav];
    [self.window makeKeyAndVisible];
    
    
}


#pragma mark - AppDelegate shareAppDelegate
+(AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
-(void)getAddressFromCoordinates{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                    //  NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];

             
                      [commonUtils setUserDefault:@"location" withFormat:[NSString stringWithFormat:@"%@",locatedAt]];
                  }
                  else {
                    //  NSLog(@"Could not locate");
                  }
              }
     ];
}
-(void)alertForLocationNotFound {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"HOLD UP..." message:@"Before you continue, ICE needs access to your location. Turn on location services in your device settings" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Go To Settings", nil];
    [alert show];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == 1) {
        CGFloat systemVersion = [[UIDevice currentDevice].systemVersion floatValue];
        if (systemVersion < 10) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy&path=LOCATION"]];
        }else{
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"App-Prefs:root=Privacy&path=LOCATION"]
                                              options:[NSDictionary dictionary]
                                    completionHandler:nil];
        }
    }
    
}
+ (instancetype)sharedInstance
{
    static id sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
        AppDelegate *instance = sharedInstance;
        instance.locationManager = [CLLocationManager new];
        instance.locationManager.delegate = instance;
        instance.locationManager.desiredAccuracy = kCLLocationAccuracyBest; // you can use kCLLocationAccuracyHundredMeters to get better battery life
        instance.locationManager.pausesLocationUpdatesAutomatically = NO; // this is important
    });
    
    return sharedInstance;
}
- (void)startUpdatingLocation
{
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    if (status == kCLAuthorizationStatusDenied)
    {
//        NSLog(@"Location services are disabled in settings.");
    }
    else
    {
        // for iOS 8
        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        // for iOS 9
        if ([self.locationManager respondsToSelector:@selector(setAllowsBackgroundLocationUpdates:)])
        {
            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
        }
        
        [self.locationManager startUpdatingLocation];
    }
}
-(void)updateLatLngApi:(NSString*)lat :(NSString*)lng{
    
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            
            
            NSString *serverUrl = [NSString stringWithFormat:@"%@auto_Checkin?lat=%@&lng=%@",ServerUrl,lat,lng];
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
            // Create a mutable copy of the immutable request and add more headers
            NSMutableURLRequest *mutableRequest = [request mutableCopy];
            [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            
            // Now set our request variable with an (immutable) copy of the altered request
            request = [mutableRequest copy];
            
            // Log the output to make sure our new headers are there
           // NSLog(@"%@", request.allHTTPHeaderFields);
            
            
            NSURLResponse *response;
            
            NSError *error = nil;
            
           // NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

            if(error!=nil)
            {
               // NSLog(@"web service error:%@",error);
            }
            else
            {
                if(data !=nil)
                {
                    NSError *Jerror = nil;
                    
                    NSDictionary* json =[NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&Jerror];
                    // NSLog(@"user data is %@",json);
                    
                    if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                        if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                            [[NSNotificationCenter defaultCenter]
                             postNotificationName:@"Logout"
                             object:self];
                            
                            
                        }
                    }else if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                        dispatch_async(dispatch_get_main_queue(), ^(void){
                            //Run UI Updates
                            NSDictionary *successDic = [json[@"successData"]mutableCopy];
                         //   NSLog(@"successDic %@",successDic);
                            
                        });
                        
                    }
                    if(Jerror!=nil)
                    {
                    //    NSLog(@"json error:%@",Jerror);
                    }
                }
            }
              }];
        });
    
}
-(void)getIceDetail{
    
    //Background Thread
    dispatch_async(dispatch_get_main_queue(), ^(void){
       
    });
    
    // commonUtils.noti_ice_id = @"418";//Demo ice id for testing
    NSString *serverUrl = [NSString stringWithFormat:@"%@get_ice/%@?time_zone=%@",ServerUrl,commonUtils.noti_ice_id,commonUtils.getTimeZone];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    
    
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
    //NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
 
    if(error!=nil)
    {
//        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(data !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:data
                                 options:kNilOptions
                                 error:&Jerror];
            
            if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"Logout"
                     object:self];
                    
                    
                }
            }else if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^(void){
                    
                    NSDictionary *successDic = [json[@"successData"]mutableCopy];
                    commonUtils.alertDic = [successDic mutableCopy];
                    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                    DetailVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"DetailVC"];
                    vc.iceDetails = commonUtils.alertDic;
                    vc.comingFrom = @"alert";
                    vc.eventType = @"alert";
                    UINavigationController *navController = (UINavigationController  *)self.window.rootViewController;
                    [navController pushViewController:vc animated:YES];
                  
                    
                    
                    
                });
                
                
                
                
//                NSLog(@"live array is %@",commonUtils.liveEventsArray);
                
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^(void){
               
                });
                
                //Run UI Updates
                
                
            }
            if(Jerror!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^(void){
               
                });
                
//                NSLog(@"json error:%@",Jerror);
            }
        }
    }
    
    }];
    
    dispatch_async(dispatch_get_main_queue(), ^(void){

    });
    
    
    
}
-(void)showIceDoneAlert{
    [_iceDoneAlert show];
    if (!(commonUtils.pendingAlerts.count>0)) {
        self.visibleAlertView = nil;
    }

}
-(void)iceDone:(UIButton *)sender {
    [AppDelegate.sharedAppDelegate.iceDoneAlert close];
    
  //  NSLog(@"passed array is %@",imagesArrayCopy);
    //  NSLog(@"images array copy is %@",imagesArrayCopy);
   
    NSMutableArray *imagesArrayCopy2 = [NSMutableArray new];
    
    for (int i= 0; i<imagesArrayCopy.count; i++) {
        if ([imagesArrayCopy[i][@"image"]isKindOfClass:[UIImage class]]) {
            
            [imagesArrayCopy2 addObject:imagesArrayCopy[i][@"image"]];
         
        }
        else{
            NSData *postData;
            if ([imagesArrayCopy[i][@"image"]isKindOfClass:[NSString class]]) {
                postData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imagesArrayCopy[i][@"image"]]];
            }
            else{
                   postData    = [NSData dataWithContentsOfURL:imagesArrayCopy[i][@"image"]];
            }
      
         //   NSLog(@"post data at app delegate is %lu",(unsigned long)postData.length);
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //    NSLog(@"paths is %@",paths);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"myMove%d.mp4",i+1]];
            
            [postData writeToFile:path atomically:YES];
            NSURL *moveUrl = [NSURL fileURLWithPath:path];
            [imagesArrayCopy2 addObject:moveUrl];
       //     NSLog(@"images array copy 2 is %@",imagesArrayCopy2);
        }
        
    }
    if ([commonUtils.sharingOptions[@"facebook"]isEqualToString:@"yes"]) {
//        NSLog(@"images array copy 2 at app delegate is %@",imagesArrayCopy2);
        MyActivityVC *controller = [[MyActivityVC alloc]initWithActivityItems:imagesArrayCopy2 applicationActivities:nil];
        controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            // When completed flag is YES, user performed specific activity
          
            if (commonUtils.pendingAlerts.count > 0) {
                CustomIOSAlertView *alert = commonUtils.pendingAlerts.firstObject;
                [commonUtils.pendingAlerts removeObjectAtIndex:0];
                self.visibleAlertView = alert;
                [alert show];
                
            }
            else{
                self.visibleAlertView = nil;
                
    
            }
            
            
        };
        // and present it
        
       [ROOTVIEW presentViewController:controller animated:YES completion:^{
            // executes after the user selects something
           
        }];
        
//        NSLog(@"share fb");
    }
    else{

        
    }
    if (!(commonUtils.pendingAlerts.count>0)) {
          self.visibleAlertView = nil;
    }
   
}
- (NSDictionary *)parametersDictionaryFromQueryString:(NSString *)queryString {
    
    NSMutableDictionary *md = [NSMutableDictionary dictionary];
    
    NSArray *queryComponents = [queryString componentsSeparatedByString:@"&"];
    
    for(NSString *s in queryComponents) {
        NSArray *pair = [s componentsSeparatedByString:@"="];
        if([pair count] != 2) continue;
        
        NSString *key = pair[0];
        NSString *value = pair[1];
        
        md[key] = value;
    }
    
    return md;
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    
    if ([[[url scheme] lowercaseString] isEqualToString:@"com.ice.dev"] == YES) {
        return NO;
    }
    
    
    
    if ([[url scheme] isEqualToString:@"iceapp"] == NO) return NO;
    
    NSDictionary *d = [self parametersDictionaryFromQueryString:[url query]];
    
    NSString *token = d[@"oauth_token"];
    NSString *verifier = d[@"oauth_verifier"];
    
    IcedDoneAlertView *vc = (IcedDoneAlertView *)[[self window] rootViewController];
    [vc setOAuthToken:token oauthVerifier:verifier];
    
    return YES;
}


- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSString *givenName = user.profile.givenName;
    NSString *familyName = user.profile.familyName;
    NSString *email = user.profile.email;
    // [START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText":
                                     [NSString stringWithFormat:@"Signed in user access token  is : %@",
                                      user.authentication.accessToken]};
    commonUtils.gAccessToken = [NSString stringWithFormat:@"%@",user.authentication.accessToken];
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}


- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // [START_EXCLUDE]
    NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"ToggleAuthUINotification"
     object:nil
     userInfo:statusText];
    // [END_EXCLUDE]
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    NSLog(@"notification.alertBody %@",notification.alertBody);
    
    
    if ([notification.alertBody isEqualToString:@"Call"] || [notification.alertBody isEqualToString:@"CallLive"] || [notification.alertBody isEqualToString:@"CallComming"] )
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"Home"
         object:self];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"live"
         object:self];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"comingsoon"
         object:self];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"upcoming"
         object:self];
    }
}

@end

