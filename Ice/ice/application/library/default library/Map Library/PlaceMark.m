//
//  PlaceMark.m
//  iTransitBuddy
//
//  Created by Blue Technology Solutions LLC 09/09/2008.
//  Copyright 2010 Blue Technology Solutions LLC. All rights reserved.
//

#import "PlaceMark.h"


@implementation PlaceMark

@synthesize coordinate;


-(id) initWithPlace: (Place*) p
{
	self = [super init];
	if (self != nil) {
		coordinate.latitude = p.latitude;
		coordinate.longitude = p.longitude;
		
        place = p;
        self.eventID = p.eventID;
        self.time = p.time;
        self.address = p.address;
        self.isMain = p.isMain;
        self.date = p.date;
        self.weekday = p.weekday;
        self.user_photo_url = p.user_photo_url;
        self.isLiveEvent = p.isLiveEvent;
        self.eventMnth = p.eventMnth;
        self.eventDate = p.eventDate;
        
	}
	return self;
}

- (NSString *)subtitle
{
	return nil;
}
- (NSString *)title
{
	return place.title;
}


@end
