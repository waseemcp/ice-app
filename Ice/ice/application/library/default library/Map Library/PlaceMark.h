//
//  PlaceMark.h
//  iTransitBuddy
//
//  Created by Blue Technology Solutions LLC 09/09/2008.
//  Copyright 2010 Blue Technology Solutions LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Place.h"

@interface PlaceMark : NSObject <MKAnnotation> {

	CLLocationCoordinate2D coordinate;
	Place* place;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@property (nonatomic) int eventID;
//@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) NSString* time;
@property (nonatomic, retain) NSString* address;


@property (nonatomic) int isMain;
@property (nonatomic) NSString *date;
@property (nonatomic) NSString *weekday;
@property (nonatomic) NSString *eventDate;
@property (nonatomic) NSString *eventMnth;
@property (nonatomic) NSString *user_photo_url;
@property (nonatomic) BOOL isLiveEvent;

- (id) initWithPlace: (Place *) p;
- (NSString *)title;
- (NSString *)subtitle;

@end
