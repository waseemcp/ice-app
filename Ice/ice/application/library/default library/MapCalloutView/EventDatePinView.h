//
//  EventDataPinView.h
//  ICE
//
//  Created by LandToSky on 11/18/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDatePinView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *eventIv;

@property (weak, nonatomic) IBOutlet UILabel *weekdayLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *monthLbl;

@end
