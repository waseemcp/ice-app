//
//  IcedDoneAlertView.m
//  ICE
//
//  Created by LandToSky on 1/14/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "IcedDoneAlertView.h"
#import <TwitterKit/TwitterKit.h>
#import <Accounts/Accounts.h>
#define CLIEND_ID @"98223363200-tteph5459h1qve2js1o3vb4jqeegcghg.apps.googleusercontent.com" //add your Google Plus ClientID here

#define GOOGLE_PLUS_CLIEND_ID @"440607175691-4bhfdefg7sbkrrjk3mp9t5dc15upiet0.apps.googleusercontent.com"


static NSString * const kClientId = CLIEND_ID;


@interface IcedDoneAlertView()
{
    
    IBOutletCollection(UIImageView) NSArray *images;
    IBOutletCollection(UIButton) NSArray *buttons;
}

@end

@implementation IcedDoneAlertView
@synthesize btnChecked;
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    commonUtils.sharingOptions = [NSMutableDictionary new];
    [commonUtils.sharingOptions setValue:@"no" forKey:@"facebook"];
    [commonUtils.sharingOptions setValue:@"no" forKey:@"twitter"];
    [commonUtils.sharingOptions setValue:@"no" forKey:@"google"];
         NSLog(@"sharing options are %@",commonUtils.sharingOptions);
    [self setup];
  //  [self setPreviousValues];
    
}


- (void)setup
{
    btnChecked = [[NSMutableArray alloc] initWithCapacity:3];
    for (int i = 0; i < 3; i++) {
        [btnChecked addObject:@(NO)];
    }
    
    for (UIButton* btn in buttons) {
        btn.tag = [buttons indexOfObject:btn];
    }
  //  commonUtils.sharingOptions = [[NSMutableDictionary alloc] init];
}

- (IBAction)onButtons:(UIButton*)sender
{
    NSInteger i = sender.tag;
    BOOL checked = ![btnChecked[i] boolValue];
    btnChecked[i] = @(checked);

      NSLog(@"sharing options are %@",commonUtils.sharingOptions);
    switch (i) {
        case 0:
            if (checked) {
                [commonUtils.sharingOptions setValue:@"yes" forKey:@"facebook"];
                
                [(UIImageView*)images[i] setImage:[UIImage imageNamed:@"facebook-on"]];
                  NSLog(@"sharing options are %@",commonUtils.sharingOptions);
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"facebookSelected"
                 object:self];
            }
            else {
                [commonUtils.sharingOptions setValue:@"no" forKey:@"facebook"];
                [(UIImageView*)images[i] setImage:[UIImage imageNamed:@"facebook-off"]];
            }
            break;
            
        case 1:
            if (checked) {
             
                [commonUtils.sharingOptions setValue:@"yes" forKey:@"twitter"];
               [(UIImageView*)images[i] setImage:[UIImage imageNamed:@"twitter-on"]];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"twitterSelected"
                 object:self];
            }
            else {
                [commonUtils.sharingOptions setValue:@"no" forKey:@"twitter"];
               [(UIImageView*)images[i] setImage:[UIImage imageNamed:@"twitter-off"]];
            }
            break;
            
        case 2:
            if (checked) {
              //  [self launchGooglePlus];
          
                [commonUtils.sharingOptions setValue:@"yes" forKey:@"google"];
                [(UIImageView*)images[i] setImage:[UIImage imageNamed:@"google-on"]];
                [[NSNotificationCenter defaultCenter]
                 postNotificationName:@"googleSelected"
                 object:self];
            }
            else {
                [commonUtils.sharingOptions setValue:@"no" forKey:@"google"];
                [(UIImageView*)images[i] setImage:[UIImage imageNamed:@"google-off"]];
            }
            break;
            
        default:
            break;
    }
}
//-(void)launchGooglePlus{
//    GPPSignIn *signIn = [GPPSignIn sharedInstance];
//    signIn.shouldFetchGooglePlusUser = YES;
//    //signIn.shouldFetchGoogleUserEmail = YES;  // Uncomment to get the user's email
//    
//    // You previously set kClientId in the "Initialize the Google+ client" step
//    signIn.clientID = kClientId;
//    
//    // Uncomment one of these two statements for the scope you chose in the previous step
//    signIn.scopes = @[ kGTLAuthScopePlusLogin ];  // "https://www.googleapis.com/auth/plus.login" scope
//    //signIn.scopes = @[ @"profile" ];            // "profile" scope
//    
//    // Optional: declare signIn.actions, see "app activities"
//    signIn.delegate = self;
//}
//
//- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
//                   error: (NSError *) error
//{
//    NSLog(@"Received error %@ and auth object %@",error, auth);
//    if (error) {
//        // Do some error handling here.
//    } else {
//        NSLog(@"%@ %@",[GPPSignIn sharedInstance].userEmail, [GPPSignIn sharedInstance].userID);
////        [self refreshInterfaceBasedOnSignIn];
//    }
//}
-(void)launchTwitter{
   // [AppDelegate.sharedAppDelegate.iceDoneAlert close];
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {

                    if (session) {
                        NSLog(@"signed in as %@", [session userName]);
                        NSLog(@"session token is %@",[session authToken]);
                    } else {
                        [commonUtils showAlert:@"Error!" withMessage:@"Please login in the twitter app to share"];
                        NSLog(@"error: %@", [error localizedDescription]);
                    }
                }];
//    if ([[Twitter sharedInstance]sessionStore]) {
//        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
//        
//        TWTRSession *lastSession = store.session;
//        NSLog(@"signed in as %@", [lastSession userName]);
//        NSLog(@"session token is %@",[lastSession authToken]);
//        //        TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
//        //
//        //        [client requestEmailForCurrentUser:^(NSString *email, NSError *error) {
//        //            if (email) {
//        //                NSLog(@"signed in as %@", email);
//        //            } else {
//        //                NSLog(@"error: %@", [error localizedDescription]);
//        //            }
//        //
//        //        }];
//    }
//    
//    else{
//        [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
//            if (session) {
//                NSLog(@"signed in as %@", [session userName]);
//                NSLog(@"session token is %@",[session authToken]);
//            } else {
//                NSLog(@"error: %@", [error localizedDescription]);
//            }
//        }];
//    }

}
-(void)setPreviousValues{
    if (commonUtils.sharingOptions.count>0) {
       btnChecked = [[NSMutableArray alloc] initWithCapacity:3];
        for (int i = 0; i < 3; i++) {
            [btnChecked addObject:@(NO)];
        }

    }
    NSLog(@"sharing options are %@",commonUtils.sharingOptions);
    if ([[commonUtils.sharingOptions valueForKey:@"facebook"]isEqualToString:@"yes"]) {
         [(UIImageView*)images[0] setImage:[UIImage imageNamed:@"facebook-on"]];
         [btnChecked replaceObjectAtIndex:0 withObject:@(YES)];
    }
    else if([[commonUtils.sharingOptions valueForKey:@"facebook"]isEqualToString:@"no"]){
          [(UIImageView*)images[0] setImage:[UIImage imageNamed:@"facebook-off"]];
         [btnChecked replaceObjectAtIndex:0 withObject:@(NO)];
    }
    if ([[commonUtils.sharingOptions valueForKey:@"twitter"]isEqualToString:@"yes"]) {
           [(UIImageView*)images[1] setImage:[UIImage imageNamed:@"twitter-on"]];
         [btnChecked replaceObjectAtIndex:1 withObject:@(YES)];
    }
    else if([[commonUtils.sharingOptions valueForKey:@"twitter"]isEqualToString:@"no"]){
         [(UIImageView*)images[1] setImage:[UIImage imageNamed:@"twitter-off"]];
         [btnChecked replaceObjectAtIndex:1 withObject:@(NO)];
    }
    if ([[commonUtils.sharingOptions valueForKey:@"google"]isEqualToString:@"yes"]) {
        
        [(UIImageView*)images[2] setImage:[UIImage imageNamed:@"google-on"]];
        [btnChecked replaceObjectAtIndex:2 withObject:@(YES)];
    }
    else if([[commonUtils.sharingOptions valueForKey:@"google"]isEqualToString:@"no"]){
          [(UIImageView*)images[2] setImage:[UIImage imageNamed:@"google-off"]];
         [btnChecked replaceObjectAtIndex:2 withObject:@(NO)];
    }
    
    
}

@end
