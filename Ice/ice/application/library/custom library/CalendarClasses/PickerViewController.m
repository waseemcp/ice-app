//
//  PickerViewController.m
//  IgorBizi@mail.ru
//
//  Created by IgorBizi@mail.ru on 5/8/15.
//  Copyright (c) 2015 IgorBizi@mail.ru. All rights reserved.
//

#import "PickerViewController.h"
#import "UILabel+UIDatePickerLabels.h"
#import "UIViewController+PresentViewControllerOverCurrentContext.h"


typedef enum {
    // * Show year for only prev/next year, not for current year
    // * "dd MMM, hh:mm a" or "yyyy dd MMM, hh:mm a"
    DateFormatDateWithFlexibleYearAndTime = 1,
    // * "dd MMM yyyy"
    DateFormatDate,
    // * Time(if dates the same) or Date With Flexible Year
    // * "hh:mm a" or "dd MMM" or "yyyy dd MMM"
    DateFormatTimeOrDateWithFlexibleYear,
    //! Date format used to interact with server
    DateFormatForBackend,
    //! Date format used to interact with server with no time value
    DateFormatForBackendNoTime,
    //! Date With Flexible Year
    // * "dd MMM" or "yyyy dd MMM"
    DateFormatDateWithFlexibleYear,
    //! Time only
    // * "hh:mm a"
    DateFormatTime,
    //! Date only with slashes
    // * MM/dd/yyyy
    DateFormatFacebook
    
} DateFormat;


@interface PickerViewController () <UIScrollViewDelegate>
{
    // Land Custom
     IBOutletCollection(UIButton) NSMutableArray *tabBarBtns;
     IBOutlet UILabel *underLbl;
    
    // ScrollView
    IBOutlet UIScrollView *tabScrollView;
    IBOutletCollection(UIView) NSMutableArray *pageView;
    IBOutlet UILabel *startsTabLbl;
    IBOutlet UILabel *endsTabLbl;
    int ppageIndex ;
    int choosepageIndex ;
    IBOutlet UIDatePicker *startsDatePicker, *endsDatePicker;
    IBOutlet UILabel *startsResLbl, *endsResLbl;
    BOOL shouldNotChangeEndDate;
    
}
// * Save selected item with CustomPickerType to property
@property (nonatomic) NSUInteger indexOfSelectedRowInPicker;

@property (nonatomic, strong) NSDateFormatter *dateFormatter, *monthDayYearDateFormatter, *hourMinDateFormatter;
@end


@implementation PickerViewController 
@synthesize previousStartTime,shouldPickStartTime,previousEndTime;

#pragma mark - Getters/Setters

// Land Custom
- (void)initUI
{
    tabScrollView.delegate = self;
    
    /* Adjust Frame Layout */
    CGRect frame = CGRectZero;
    frame = tabScrollView.frame;
    for (int i = 0; i < 2; i++)
    {
        
        frame.origin.x = [[UIScreen mainScreen]bounds].size.width * i;
        frame.origin.y = 0;
        [pageView[i] setFrame:frame];
        
    }
    
    [tabScrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width*2, tabScrollView.frame.size.height)];
    [tabScrollView setPagingEnabled:YES];
    

    // DatePicker type set
    startsDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
    startsDatePicker.minuteInterval = 1;
    startsDatePicker.date = [NSDate date];
  //  startsDatePicker.minimumDate = [NSDate date];
    NSLog(@"commonUtils.previousStartTime %@",commonUtils.previousStartTime);
    if (commonUtils.shouldPickPreviousStartTime) {
        if (commonUtils.previousStartTime) {
            [startsDatePicker setDate:commonUtils.previousStartTime];
        }
        
    }

    [startsDatePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    endsDatePicker.datePickerMode = UIDatePickerModeDateAndTime;
    endsDatePicker.minuteInterval = 1;
    endsDatePicker.date = [NSDate date];
    if (commonUtils.shouldPickPreviousEndTime) {
        if (commonUtils.previousEndTime) {
            [endsDatePicker setDate:commonUtils.previousEndTime];
        }
        
    }
   
    [endsDatePicker addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    // Start & End Res Label
    [startsResLbl setText:[self.dateFormatter stringFromDate:[NSDate date]]];
    [endsResLbl setText:[self.dateFormatter stringFromDate:[NSDate date]]];
}

- (void) initData
{
    self.monthDayYearDateFormatter  = [[NSDateFormatter alloc] init];
    [self.monthDayYearDateFormatter setDateFormat:@"MMM d, yyyy"];
    
    self.hourMinDateFormatter = [[NSDateFormatter alloc] init];
    if ([commonUtils is24hourFormat]) {
        
        [self.hourMinDateFormatter setDateFormat:@"HH:mm"];
    }else {
        [self.hourMinDateFormatter setDateFormat:@"hh:mm a"];
    }
}

#pragma mark - onTabBar Button
- (IBAction)onTabBarButton:(UIButton*)sender
{
    NSInteger index = [tabBarBtns indexOfObject:sender];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * index, 0)];
                         
                     }];
}



#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != tabScrollView) return;
    
    CGRect frame = underLbl.frame;
    frame.origin.x = scrollView.contentOffset.x / 2;
    underLbl.frame = frame;
    
    /* Get Current Page Number */
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    if (page == 0) {
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:@"startCalendar"
//         object:self];

        [underLbl setBackgroundColor:appController.appGreenColor];
        [startsTabLbl setTextColor:appController.darkFontColor];
        [endsTabLbl setTextColor:appController.lightFontColor];
        
        
        
        NSString *endssMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:endsDatePicker.date];
        NSString *endssHourMinStr = [self.hourMinDateFormatter stringFromDate:endsDatePicker.date];

        if (ppageIndex == 0) {
            choosepageIndex = -1;
  
        }
        
        
        if (ppageIndex > 0 ) {
            ppageIndex --;
        }
        
    } else if (page == 1){
//        [[NSNotificationCenter defaultCenter]
//         postNotificationName:@"endCalendar"
//         object:self];

        
        NSString *startsMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:startsDatePicker.date];
        NSString *startsHourMinStr = [self.hourMinDateFormatter stringFromDate:startsDatePicker.date];

        
        
        [underLbl setBackgroundColor:appController.appRedColor];
        [startsTabLbl setTextColor:appController.lightFontColor];
        [endsTabLbl setTextColor:appController.darkFontColor];
        
        if (ppageIndex == 0) {
            choosepageIndex = -1;

        }
        
        if (ppageIndex > 0 ) {
            ppageIndex --;
        }
    }
    
    [scrollView setScrollEnabled:YES];
}



- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        if ([commonUtils is24hourFormat]) {
            [_dateFormatter setDateFormat:@"MMM dd, yyyy HH:mm"];
        }else {
            [_dateFormatter setDateFormat:@"MMM dd, yyyy hh:mm a"];
            
        }

    }
    return _dateFormatter;
}

- (NSDateFormatter *)dateFormatterWithFormat:(DateFormat)dateFormat withPickedDate:(NSDate *)pickedDate
{
    switch (dateFormat)
    {
        case DateFormatDateWithFlexibleYearAndTime:
        {
            if (!pickedDate) {
                NSLog(@"Error: dateFormatterWithFormat:DateFormatDateWithFlexibleYearAndTime required pickedDate");
                return self.dateFormatter;
            }
            
            // * Show year for only prev/next year, not for current year
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *pickedDateComponents = [calendar components:NSCalendarUnitYear fromDate:pickedDate];
            NSDateComponents *currentDateComponents = [calendar components:NSCalendarUnitYear fromDate:[NSDate date]];
            NSUInteger pickedYear = [pickedDateComponents year];
            NSUInteger currentYear = [currentDateComponents year];
            if (pickedYear == currentYear && self.enableShortDates) {
                if ([commonUtils is24hourFormat]) {
                    [self.dateFormatter setDateFormat:@"dd MMM, HH:mm"];
                }else {
                    [self.dateFormatter setDateFormat:@"dd MMM, hh:mm a"];
                }
            } else {
                if ([commonUtils is24hourFormat]) {
                    [self.dateFormatter setDateFormat:@"yyyy dd MMM, HH:mm"];
                }else {
                    [self.dateFormatter setDateFormat:@"yyyy dd MMM, hh:mm a"];
                }
            }
        } break;
            
        case DateFormatTimeOrDateWithFlexibleYear:
        {
            if (!pickedDate) {
                NSLog(@"Error: dateFormatterWithFormat:DateFormatDateWithFlexibleYearAndTime required pickedDate");
                return self.dateFormatter;
            }
            
            // * Show Hours and Minutes - if it of current day; if not - show Day and Month if it of current year; if not - show Year, Day, Month
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSCalendarUnit units = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
            NSDateComponents *pickedDateComponents = [calendar components:units fromDate:pickedDate];
            NSDateComponents *currentDateComponents = [calendar components:units fromDate:[NSDate date]];
            NSUInteger pickedDay = [pickedDateComponents day];
            NSUInteger currentDay = [currentDateComponents day];
            NSUInteger pickedYear = [pickedDateComponents year];
            NSUInteger currentYear = [currentDateComponents year];
            if (pickedDay == currentDay) {
                if ([commonUtils is24hourFormat]) {
                    [self.dateFormatter setDateFormat:@"HH:mm"];
                }else {
                    [self.dateFormatter setDateFormat:@"hh:mm a"];
                }
            } else
                if (pickedYear == currentYear)
                {
                    [self.dateFormatter setDateFormat:@"dd MMM"];
                } else {
                    [self.dateFormatter setDateFormat:@"yyyy, dd MMM"];
                }
        } break;
            
        case DateFormatDateWithFlexibleYear:
        {
            if (!pickedDate) {
                NSLog(@"Error: dateFormatterWithFormat:DateFormatDateWithFlexibleYearAndTime required pickedDate");
                return self.dateFormatter;
            }
            
            // * Show Day and Month if it of current year; if not - show Year, Day, Month
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSCalendarUnit units = NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay;
            NSDateComponents *pickedDateComponents = [calendar components:units fromDate:pickedDate];
            NSDateComponents *currentDateComponents = [calendar components:units fromDate:[NSDate date]];
            NSUInteger pickedYear = [pickedDateComponents year];
            NSUInteger currentYear = [currentDateComponents year];
            if (pickedYear == currentYear && self.enableShortDates)
            {
                [self.dateFormatter setDateFormat:@"dd MMM"];
            } else {
                [self.dateFormatter setDateFormat:@"yyyy, dd MMM"];
            }
        } break;
            
        case DateFormatTime:
        {
            if ([commonUtils is24hourFormat]) {
                [self.dateFormatter setDateFormat:@"HH:mm"];
            }else {
                [self.dateFormatter setDateFormat:@"hh:mm a"];
            }
        } break;
            
        case DateFormatDate:
        {
            [self.dateFormatter setDateFormat:@"dd MMM yyyy"];
        } break;
            
        case DateFormatForBackend:
        {
            if ([commonUtils is24hourFormat]) {
                [self.dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            }else {
                [self.dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            }
        } break;
            
        case DateFormatForBackendNoTime:
        {
            [self.dateFormatter setDateFormat:@"yyyy-MM-dd"];
        } break;
            
        case DateFormatFacebook:
        {
            [self.dateFormatter setDateFormat:@"MM/dd/yyyy"];
        } break;
            
        default:
            break;
    }
    
    return self.dateFormatter;
}


#pragma mark - LifeCycle


- (instancetype)initFromNib
{
    return [self initWithNibName:@"PickerViewController" bundle:nil];
}


- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup
{
    self.view.backgroundColor = [UIColor clearColor];
    
    self.enableShortDates = YES;
    
    // Land
    [self initUI];
    [self initData];
}


#pragma mark - Getters/Setters


- (void)setPickerType:(PickerType)pickerType
{
    
            self.datePicker.datePickerMode = UIDatePickerModeDateAndTime;
//            self.datePicker.minimumDate = [NSDate date];
            // Land Set Minute Interval
            self.datePicker.minuteInterval = 1;
            self.datePicker.date = [NSDate date];
}

- (void)setInitialDate:(NSDate *)date
{
    if (date) {
        self.datePicker.date = date;
    }
}

- (void)setMinimalDate:(NSDate *)date
{
    if (date) {
        self.datePicker.minimumDate = date;
    }
}
- (void)setPageIndex:(StartsEndsCalendarIndex) pageIndex
{
    // If pageIndex is setting, scroll to correspond page
    if (pageIndex != 0) {
        ppageIndex = 2;
    }else {
        ppageIndex = 0;
    }
    
    choosepageIndex = pageIndex;
    [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * pageIndex, 0)];
}

#pragma mark - Events

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    
    NSLog(@"Res Date == %@",[self.dateFormatter stringFromDate:datePicker.date]);
    if (datePicker == startsDatePicker) {
        NSTimeInterval secondsInOneHours = 1 * 60 * 60;
        NSDate *dateOneHourAhead = [datePicker.date dateByAddingTimeInterval:secondsInOneHours];
         if (commonUtils.shouldNotChangeEndDate == NO) {
             [endsDatePicker setDate:dateOneHourAhead];
            [endsResLbl setText:[self.dateFormatter stringFromDate:dateOneHourAhead]];
         }
        [startsResLbl setText:[self.dateFormatter stringFromDate:datePicker.date]];

    } else if (datePicker == endsDatePicker) {
        [endsResLbl setText:[self.dateFormatter stringFromDate:datePicker.date]];
    }
}

- (IBAction)onStartsResLblDel:(id)sender
{
    [startsResLbl setText:@""];
}

- (IBAction)onEndsResLblDel:(id)sender
{
    [endsResLbl setText:@""];
}

- (IBAction)dismissButtonAction:(UIButton *)sender
{
    [self dismissViewControllerOverCurrentContextAnimated:YES completion:nil];
}

- (IBAction)cancelButtonAction:(UIButton *)sender
{
    
    
    [self dismissViewControllerOverCurrentContextAnimated:YES completion:nil];
}

- (IBAction)doneButtonAction:(UIButton *)sender
{

     // * Format date
//    NSDateFormatter *dateFormatter;
//    if (self.pickerType == DateAndTimePickerType)
//    {
//        dateFormatter = [self dateFormatterWithFormat:DateFormatDateWithFlexibleYearAndTime withPickedDate:self.datePicker.date];
//    }
//    
//    [self.delegate didSelectDate:self.datePicker.date formattedString:[NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:self.datePicker.date]]];
    
    
    NSString *startsMonthDayYearStr = @"";
    NSString *startsHourMinStr = @"";
    NSString *endssMonthDayYearStr= @"";
    NSString *endssHourMinStr = @"";
     NSTimeInterval secondsInOneHours = 1 * 60 * 60;
    if (choosepageIndex == 1) {
        
        
        NSDateFormatter *myFormatter = [[NSDateFormatter alloc]init];
        
        if ([commonUtils is24hourFormat]) {
            [self.hourMinDateFormatter setDateFormat:@"HH:mm"];
            [myFormatter setDateFormat:@"MMM d, yyyy HH:mm"];
            
            endssMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:endsDatePicker.date];
            endssHourMinStr = [self.hourMinDateFormatter stringFromDate:endsDatePicker.date];
            endssHourMinStr = [self formatTime:endssHourMinStr];
        }else {
            [self.hourMinDateFormatter setDateFormat:@"hh:mm a"];
            [myFormatter setDateFormat:@"MMM d, yyyy hh:mm a"];
            
            endssMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:endsDatePicker.date];
            endssHourMinStr = [self.hourMinDateFormatter stringFromDate:endsDatePicker.date];
            endssHourMinStr = [self formatTime:endssHourMinStr];
        }
        
        
        NSString *endDateNew = [myFormatter stringFromDate:endsDatePicker.date];
//        NSString *startDateNew = [myFormatter stringFromDate:startsDatePicker.date];
        
        commonUtils.previousEndTime = [myFormatter dateFromString:endDateNew];
        
        
//        commonUtils.previousEndTime = endsDatePicker.date;
        commonUtils.shouldPickPreviousEndTime = YES;
        commonUtils.shouldNotChangeEndDate = YES;
        NSLog(@"shouldnotchange value is %d",commonUtils.shouldNotChangeEndDate);
    }
    else if (choosepageIndex == 0) {
    
        
        NSDateFormatter *myFormatter = [[NSDateFormatter alloc]init];

        
        if ([commonUtils is24hourFormat]) {
            [self.hourMinDateFormatter setDateFormat:@"HH:mm"];
            startsMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:startsDatePicker.date];
            startsHourMinStr = [self.hourMinDateFormatter stringFromDate:startsDatePicker.date];
            startsHourMinStr = [self formatTime:startsHourMinStr];
            
            
            
            [myFormatter setDateFormat:@"MMM d, yyyy HH:mm"];
        }else {
            [self.hourMinDateFormatter setDateFormat:@"hh:mm a"];
            startsMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:startsDatePicker.date];
            startsHourMinStr = [self.hourMinDateFormatter stringFromDate:startsDatePicker.date];
            startsHourMinStr = [self formatTime:startsHourMinStr];
            
            
            [myFormatter setDateFormat:@"MMM d, yyyy hh:mm a"];
        }
        
//        NSString *endDateNew = [myFormatter stringFromDate:endsDatePicker.date];
        NSString *startDateNew = [myFormatter stringFromDate:startsDatePicker.date];
        
//        commonUtils.previousEndTime = [myFormatter dateFromString:endDateNew];
        commonUtils.previousStartTime = [myFormatter dateFromString:startDateNew];
        
        
        
        commonUtils.previousStartTime = startsDatePicker.date;
        NSLog(@"commonUtils.previousStartTime ==> %@",commonUtils.previousStartTime);
        commonUtils.shouldPickPreviousStartTime = YES;
        NSLog(@"shouldnotchange value is %d",commonUtils.shouldNotChangeEndDate);
        if (commonUtils.shouldNotChangeEndDate == NO) {
            NSDate *timeOneHourAheadFromNow = [startsDatePicker.date dateByAddingTimeInterval:secondsInOneHours];
            endssMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:timeOneHourAheadFromNow];
            endssHourMinStr = [self.hourMinDateFormatter stringFromDate:timeOneHourAheadFromNow];
            endssHourMinStr = [self formatTime:endssHourMinStr];
            NSLog(@"shouldnotchange value is %d",commonUtils.shouldNotChangeEndDate);
            [endsDatePicker setDate:timeOneHourAheadFromNow];
        }
        
    }else {
        
//        commonUtils.previousEndTime = endsDatePicker.date;
        
        NSDateFormatter *myFormatter = [[NSDateFormatter alloc]init];
        
        if ([commonUtils is24hourFormat]) {
            [self.hourMinDateFormatter setDateFormat:@"HH:mm"];
            endssMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:endsDatePicker.date];
            endssHourMinStr = [self.hourMinDateFormatter stringFromDate:endsDatePicker.date];
            startsMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:startsDatePicker.date];
            startsHourMinStr = [self.hourMinDateFormatter stringFromDate:startsDatePicker.date];
            
            startsHourMinStr = [self formatTime:startsHourMinStr];
            endssHourMinStr = [self formatTime:endssHourMinStr];
            
            [myFormatter setDateFormat:@"MMM d, yyyy HH:mm"];
        }else {
            [self.hourMinDateFormatter setDateFormat:@"hh:mm a"];
            endssMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:endsDatePicker.date];
            endssHourMinStr = [self.hourMinDateFormatter stringFromDate:endsDatePicker.date];
            startsMonthDayYearStr = [self.monthDayYearDateFormatter stringFromDate:startsDatePicker.date];
            startsHourMinStr = [self.hourMinDateFormatter stringFromDate:startsDatePicker.date];
            
            startsHourMinStr = [self formatTime:startsHourMinStr];
            endssHourMinStr = [self formatTime:endssHourMinStr];
            [myFormatter setDateFormat:@"MMM d, yyyy hh:mm a"];
        }
        
        
        NSString *endDateNew = [myFormatter stringFromDate:endsDatePicker.date];
        NSString *startDateNew = [myFormatter stringFromDate:startsDatePicker.date];
        
        commonUtils.previousEndTime = [myFormatter dateFromString:endDateNew];
        commonUtils.previousStartTime = [myFormatter dateFromString:startDateNew];
//        endsMonthDayYearStr = [myFormatter stringFromDate:endSelected];
        
//        [myFormatter setDateFormat:@"h:mm a"];
//        endsHourMinStr = [myFormatter stringFromDate:endSelected];
        
        
        
        NSLog(@"commonUtils.previousStartTime %@",commonUtils.previousStartTime);

        commonUtils.shouldPickPreviousStartTime = YES;
        commonUtils.shouldPickPreviousEndTime = YES;
    }
    
    

    
    NSLog(@"startsMonthDayYearStr %@",startsMonthDayYearStr);
    NSLog(@"startsHourMinStr %@",startsHourMinStr);
     NSLog(@"startsDatePicker %@",startsDatePicker.date);
    
    NSLog(@"endssHourMinStr %@",endssHourMinStr);
    NSLog(@"endssMonthDayYearStr %@",endssMonthDayYearStr);
    NSLog(@"endsDatePicker %@",endsDatePicker.date);
    
    [self.delegate didSelectStartsMonthDayYear:startsMonthDayYearStr
                            withStartsHourMin:startsHourMinStr
                            withEndsMonthDayYear:endssMonthDayYearStr
                             withEndsHourMin:endssHourMinStr
                                withStartsDate:startsDatePicker.date
                                  withEndsDate:endsDatePicker.date
     ];
    
    [self dismissViewControllerOverCurrentContextAnimated:YES completion:nil];
}

- (NSString *)formatTime:(NSString *)time
{
    NSLog(@"time %@",time);
    
    
    return time;
    if (time.length > 7) {
        return time;
    }
    
    NSString *hour = [time substringWithRange:NSMakeRange(0, 2)];
    NSString *minute = [time substringWithRange:NSMakeRange(3, 2)];
    NSString *tail = @"";
    
    if ([hour integerValue] > 12) {
        tail =  @"PM";
        hour = [NSString stringWithFormat:@"%d",[hour integerValue] - 12  ];
    }else if ([hour integerValue] == 0){
        tail =  @"AM";
        hour = @"00";

    }else if ([hour integerValue] == 12){
        tail =  @"PM";

    }else {
        tail =  @"AM";
    }
    
    NSLog(@"data %@",[NSString stringWithFormat:@"%@:%@ %@", hour, minute, tail]);
    return [NSString stringWithFormat:@"%@:%@ %@", hour, minute, tail];
}

@end
