//
//  DIYCalendarCell.m
//  FSCalendar
//
//  Created by dingwenchao on 02/11/2016.
//  Copyright © 2016 Wenchao Ding. All rights reserved.
//

#import "DIYCalendarCell.h"
#import "FSCalendarExtensions.h"



@implementation DIYCalendarCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        // Today show Imageview
        UIImageView *todayShowIv = [[UIImageView alloc] init];
        [self.contentView insertSubview:todayShowIv atIndex:0];
        todayShowIv.backgroundColor = [UIColor clearColor];
        todayShowIv.layer.borderWidth = 1.0f;
        todayShowIv.layer.borderColor = RGBA(50, 74, 101, 1.0f).CGColor;
        self.todayShowIv = todayShowIv;
        

        // Select Show Layer
//        CAShapeLayer *selectionLayer = [[CAShapeLayer alloc] init];
//        selectionLayer.fillColor = RGBA(141, 201, 240, 0.3).CGColor;
//        selectionLayer.borderColor = RGBA(141, 201, 240, 1.0).CGColor;
//        selectionLayer.borderWidth = 1.0f;
//        selectionLayer.actions = @{@"hidden":[NSNull null]};
//        [self.contentView.layer insertSublayer:selectionLayer below:self.titleLabel.layer];
//        self.selectionLayer = selectionLayer;
//        self.selectionLayer.hidden = YES;
        
        self.shapeLayer.hidden = YES;
        self.backgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
        
    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.backgroundView.frame = CGRectInset(self.bounds, .5, 0);
    self.todayShowIv.frame = self.bounds;
  //  self.selectionLayer.frame = self.bounds;
 //   self.selectionLayer.path = [UIBezierPath bezierPathWithRect:self.selectionLayer.bounds].CGPath;
}

- (void)configureAppearance
{
    [super configureAppearance];
    self.eventIndicator.hidden = self.placeholder; // Hide the event indicator for placeholder cells
    if (self.placeholder) {
        self.backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0];
    } else {
         self.backgroundView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.1];
    }
}

@end
