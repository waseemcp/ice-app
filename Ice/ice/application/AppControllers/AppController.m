//
//  AppController.m
//  WebServiceSample
//
//  Created by LandtoSky on 6/14/16.
//  Copyright © 2016 LandtoSky. All rights reserved.
//

#import "AppController.h"


static AppController *_appController;

@implementation AppController

+ (AppController *)sharedInstance {
    static dispatch_once_t predicate;
    if (_appController == nil) {
        dispatch_once(&predicate, ^{
            _appController = [[AppController alloc] init];
        });
    }
    return _appController;
}

- (id)init {
    self = [super init];
    if (self) {
        
        // Utility Data
        _appMainColor = RGBA(254, 242, 91, 1.0f);
     
        _contactArray = [[NSArray alloc] init];
        
        _vAlert = [[DoAlertView alloc] init];
        _vAlert.nAnimationType = 2;  // there are 5 type of animation
        _vAlert.dRound = 7.0;
        _vAlert.bDestructive = NO;  // for destructive mode
        
        _appBackgroundColor = RGBA(259, 245, 246, 1.0f);
        
        _appBlueColor = RGBA(141, 201, 240, 1.0f);
        _appGreenColor = RGBA(163, 207, 99, 1.0f);
        _appRedColor = RGBA(254, 118, 118, 1.0f);
//        _appGrayColor = RGBA(171, 171, 171, 1.0f);
        _appGrayColor = [UIColor colorWithHex:@"#ababab" alpha:1.0f];
        _appPurpleColor = [UIColor colorWithHex:@"#c6a6dd" alpha:1.0f];
        
        _darkFontColor = [UIColor colorWithHex:@"#585858" alpha:1.0f];
        _lightFontColor = [UIColor colorWithHex:@"#ababab" alpha:1.0f];
        
        _settingTitles = @[@"NMAE:", @"LOREM", @"IPSUM", @"DOLOR", @"SIT", @"AMET"];
        
        _tempEventDatas = [[NSMutableArray alloc] init];
        _tempEventDatas = [@[
                             @{
                                 @"id" : @(0),
                                 @"title" : @"0 Angelina's Birthday Party",
                                 @"time" : @"1:00 PM - 04:00 AM PST",
                                 @"address": @"30 Eastwood Drive, Los Angeles, CA",
                                 @"location_latitude":@(40.719525),
                                 @"location_longitude": @(-73.91673),
                                 @"date" : @"24/05/16",
                                 @"weekday":@"tue",
                                 @"user_photo_url":@"http://google.com",
                                 @"isLiveEvent":@(1),
                                 @"isVideo":@(1)
                                 },
                             @{
                                 @"id" : @(1),
                                 @"title" : @"1 John's Birthday Party",
                                 @"time" : @"2:00 PM - 04:00 AM PST",
                                 @"address": @"31 Eastwood Drive, Los Angeles, CA",
                                 @"location_latitude":@(40.759525),
                                 @"location_longitude": @(-74.00673),
                                 @"date" : @"24/05/16",
                                 @"weekday":@"tue",
                                 @"user_photo_url":@"http://google.com",
                                 @"isLiveEvent":@(0),
                                 @"isVideo":@(1)
                                 },
                             @{
                                 @"id" : @(2),
                                 @"title" : @"2 Helen's Birthday Party",
                                 @"time" : @"03:00 PM - 04:00 AM PST",
                                 @"address": @"33 Eastwood Drive, Los Angeles, CA",
                                 @"location_latitude":@(40.769525),
                                 @"location_longitude": @(-74.05673),
                                 @"date" : @"24/05/16",
                                 @"weekday":@"tue",
                                 @"user_photo_url":@"http://google.com",
                                 @"isLiveEvent":@(0),
                                 @"isVideo":@(0)
                                 },
                             @{
                                 @"id" : @(3),
                                 @"title" : @"3 Bob's Birthday Party",
                                 @"time" : @"04:00 PM - 04:00 AM PST",
                                 @"address": @"34 Eastwood Drive, Los Angeles, CA",
                                 @"location_latitude":@(40.709525),
                                 @"location_longitude": @(-74.10873),
                                 @"date" : @"24/05/16",
                                 @"weekday":@"tue",
                                 @"user_photo_url":@"http://google.com",
                                 @"isLiveEvent":@(1),
                                 @"isVideo":@(0)
                                 },
                             @{
                                 @"id" : @(4),
                                 @"title" : @"4 Serge's Birthday Party",
                                 @"time" : @"05:00 PM - 04:00 AM PST",
                                 @"address": @"35 Eastwood Drive, Los Angeles, CA",
                                 @"location_latitude":@(40.689525),
                                 @"location_longitude": @(-73.98673),
                                 @"date" : @"24/05/16",
                                 @"weekday":@"tue",
                                 @"user_photo_url":@"http://google.com",
                                 @"isLiveEvent":@(0),
                                 @"isVideo":@(1)
                                 },
                             @{
                                 @"id" : @(5),
                                 @"title" : @"5 Serge's Birthday Party",
                                 @"time" : @"05:00 PM - 04:00 AM PST",
                                 @"address": @"35 Eastwood Drive, Los Angeles, CA",
                                 @"location_latitude":@(40.68),
                                 @"location_longitude": @(-73.933),
                                 @"date" : @"24/05/16",
                                 @"weekday":@"tue",
                                 @"user_photo_url":@"http://google.com",
                                 @"isLiveEvent":@(1),
                                 @"isVideo":@(0)
                                 },

                             
                             ] mutableCopy];
        
        
    
    }
    return self;
}

@end
