//
//  BaseViewController.m
//  Doo
//
//  Created by Jose on 12/19/15.
//  Copyright © 2015 simon. All rights reserved.
//

#import "BaseViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        self.isLoadingBase = NO;
           self.automaticallyAdjustsScrollViewInsets = NO;


    // Do any additional setup after loading the view.
}

- (IBAction)onBack:(id)sender{
   if (self.isLoadingBase) return;
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onMenuShow:(id)sender{    
    [self.sideMenuController showLeftViewAnimated:sender];
}



- (void)EnableMenu {
    SidePanelVC *sidePanelVC = (SidePanelVC*)self.sideMenuController;
    sidePanelVC.leftViewEnabled = true;
    sidePanelVC.leftViewDisabled = false;
    sidePanelVC.leftViewSwipeGestureDisabled = true;
    
    [sidePanelVC setRightViewEnabled:true];
    [sidePanelVC setRightViewSwipeGestureEnabled:true];
}


-(UIImage *)generateThumbImage : (NSURL *)filepath
{
    AVURLAsset* asset = [AVURLAsset URLAssetWithURL:filepath options:nil];
    AVAssetImageGenerator* imageGenerator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CGImageRef cgImage = [imageGenerator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:nil error:nil];
    UIImage* image = [UIImage imageWithCGImage:cgImage];
    
    return image;
    
//    CGImageRelease(cgImage);
    
    
    
    
//    NSURL *url = [NSURL fileURLWithPath:filepath];
//
//
//    AVAsset *asset = [AVAsset assetWithURL:url];
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    asset.imageGenerator.appliesPreferredTrackTransform = YES;
//    CMTime time = [asset duration];
//    time.value = 0;
//    Float duration = CMTimeGetSeconds([asset duration]);
//    for(Float i = 0.0; i<duration; i=i+0.1)
//    {
//        CGImageRef imgRef = [self.imageGenerator copyCGImageAtTime:CMTimeMake(i, duration) actualTime:NULL error:nil];
//        UIImage* thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
//        [thumbnailImages addObject:thumbnail];
//    }
}


- (void)Logout{
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    NSString *strKey = [[NSUserDefaults standardUserDefaults] valueForKey:@"user_apns_id"];
    
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [commonUtils setUserDefault:@"user_apns_id" withFormat:strKey];
    
    [self.navigationController popToRootViewControllerAnimated:true];
}
@end
