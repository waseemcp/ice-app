//
//  MySidePanelVC.m
//  Doo
//
//  Created by Jose on 12/16/15.
//  Copyright © 2015 simon. All rights reserved.
//

#import "SidePanelVC.h"

#import "LeftMenuVC.h"
#import "RightMenuVC.h"

@interface SidePanelVC ()
@end

@implementation SidePanelVC



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    LeftMenuVC* leftMenuVC = (LeftMenuVC*)[self.storyboard instantiateViewControllerWithIdentifier:@"LeftMenuVC"];
    RightMenuVC *rightMenuVC = (RightMenuVC*) [self.storyboard instantiateViewControllerWithIdentifier:@"RightMenuVC"];
    
    
    self.ActivitiesNavigation = [[UINavigationController alloc] initWithRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"]];
    [self.ActivitiesNavigation setNavigationBarHidden:YES];
    
    
    
    self.rootViewController = self.ActivitiesNavigation;
    self.leftViewController = leftMenuVC;
    self.rightViewController = rightMenuVC;
    
    // Left Panel Setting
    self.leftViewWidth = SCREEN_WIDTH * 330.0f/375.0f;
    self.leftViewBackgroundImage = [UIImage imageNamed:@"back-image"];
//    self.leftViewStatusBarHidden = YES;
    self.leftViewStatusBarStyle = UIStatusBarStyleLightContent;
    self.leftViewBackgroundImageInitialScale = 1.0;
    self.leftViewInititialOffsetX = 0.0;
    self.leftViewInititialScale = 1.0;
    self.leftViewCoverBlurEffect = nil;
    
    self.rootViewCoverColorForLeftView = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.8];
    self.rootViewScaleForLeftView = 455/667.0f;
    
    
    // Right Panel Setting
    self.rightViewWidth = SCREEN_WIDTH * 328.0f/375.0f;
    self.rightViewBackgroundImage = [UIImage imageNamed:@"back-image"];
//    self.rightViewStatusBarHidden = YES;
    self.rightViewStatusBarStyle = UIStatusBarStyleLightContent;
    self.rightViewBackgroundImageInitialScale = 1.0;
    self.rightViewInititialOffsetX = 0.0;
    self.rightViewInititialScale = 1.0;
    self.rightViewCoverBlurEffect = nil;
    
    self.rootViewCoverColorForRightView = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.8];
    self.rootViewScaleForRightView = 450/667.0f;
    
    self.leftViewPresentationStyle = LGSideMenuPresentationStyleScaleFromBig;
    self.rightViewPresentationStyle = LGSideMenuPresentationStyleScaleFromBig;
    
    
    
}

- (void)initData
{
    
}

//-(NSUInteger)supportedInterfaceOrientations{
//    
//    UIDevice *device = [UIDevice currentDevice];
//    
//    if(device.userInterfaceIdiom == UIUserInterfaceIdiomPad)
//    {
//        self.bounceOnCenterPanelChange = NO;
//        if (UIInterfaceOrientationIsLandscape((UIInterfaceOrientation)device.orientation))
//        {
//            //NSLog(@"Change to custom UI for landscape");
//            self.rightGapPercentage = 0.30f;
//            
//        }
//        else if (UIInterfaceOrientationIsPortrait((UIInterfaceOrientation)device.orientation))
//        {
//            //NSLog(@"Change to custom UI for portrait");
//            self.rightGapPercentage = 0.40f;
//            
//        }
//        
//        return UIInterfaceOrientationMaskAll;
//    }
//    else
//    {
//        self.rightGapPercentage = 200.0f / 320.0f;
//        return UIInterfaceOrientationMaskPortrait;
//    }
//}



//- (void)_showRightPanel:(BOOL)animated bounce:(BOOL)shouldBounce {
//    [super _showRightPanel:animated bounce:shouldBounce];
//    
//    hideStatusBar = YES;
//    [self setNeedsStatusBarAppearanceUpdate];
//    
//    [UIView animateWithDuration:0.2f animations:^{
//        [commonUtils moveView:self.centerPanelContainer
//                    withMoveX:self.centerPanelContainer.frame.origin.x
//                    withMoveY:self.view.frame.size.height*120/667];
//        
//        [commonUtils resizeFrame:self.centerPanelContainer
//                       withWidth:self.centerPanelContainer.frame.size.width
//                      withHeight:self.centerPanelContainer.frame.size.height*450/667];
//        
//    } completion:^(BOOL finished){
//        [commonUtils dropDownShadowInView:self.centerPanelContainer];
//    }];
//    
//    [self.view endEditing:YES];
//
//}
//
//- (void)_showCenterPanel:(BOOL)animated bounce:(BOOL)shouldBounce{
//    [super _showCenterPanel:animated bounce:shouldBounce];
//    hideStatusBar = NO;
//    [self setNeedsStatusBarAppearanceUpdate];
//    
//   [self.view endEditing:YES];
//    
//}
//
//- (void)_showLeftPanel:(BOOL)animated bounce:(BOOL)shouldBounce {
//    
//    // Default Font setting in Left Panel Menu
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"DefaultFontSet" object:nil];
//    
//    [super _showLeftPanel:animated bounce:shouldBounce];
//    
//    hideStatusBar = YES;
//    [self setNeedsStatusBarAppearanceUpdate];
//    
//    [UIView animateWithDuration:0.2f animations:^{
//        [commonUtils moveView:self.centerPanelContainer
//                    withMoveX:self.centerPanelContainer.frame.origin.x
//                    withMoveY:self.view.frame.size.height*108/667];
//        
//        [commonUtils resizeFrame:self.centerPanelContainer
//                       withWidth:self.centerPanelContainer.frame.size.width                      withHeight:self.centerPanelContainer.frame.size.height*450/667];
//    } completion:^(BOOL finished){
//        [commonUtils dropDownShadowInView:self.centerPanelContainer];
//    }];
//    
//    [self.view endEditing:YES];
//}
//
//- (BOOL) prefersStatusBarHidden
//{
//    return hideStatusBar;
//}
//
//- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation
//{
//    return UIStatusBarAnimationFade;
//}
//
////- (void) showLeftPanelAnimated:(BOOL)animated
////{
////    [super showLeftPanelAnimated:animated];
////}
////
////- (void) showRightPanelAnimated:(BOOL)animated
////{
////    [super showRightPanelAnimated:animated];
////}
//
// /* Remove Corner Radius to Center, Left, Right Panel */
//- (void)stylePanel:(UIView *)panel{
//};
//


@end
