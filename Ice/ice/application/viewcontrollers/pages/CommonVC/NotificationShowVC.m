//
//  NotificationShowVC.m
//  ICE
//
//  Created by LandToSky on 11/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "NotificationShowVC.h"
#import "NotificationTVCell.h"
#import "DetailVC.h"
@interface NotificationShowVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *notiTV;
    NSMutableArray *notiArray,*fullNotificationArray;
    __weak IBOutlet UILabel *notiLbl;
    int currentPageNumber;
    BOOL isPageRefresing;
    
 }


@end

@implementation NotificationShowVC
@synthesize totalNotifications;
- (void)viewDidLoad {
    [super viewDidLoad];
    notiLbl.text = totalNotifications;
    fullNotificationArray = [NSMutableArray new];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    [commonUtils setRoundedRectView:notiLbl withCornerRadius:notiLbl.frame.size.height/2];
}

- (void)initData
{
    currentPageNumber = 0;
    [self getNotfications:currentPageNumber];
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
   
}
#pragma mark - UITable View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notiArray.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"NotificationTVCell";
    
    NotificationTVCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[NotificationTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    NSLog(@"indexpath is %d",(int)indexPath.row);
    cell.titleLbl.text = [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"title"]];
    NSString *imageUrlStr =  [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"get_user"][@"photo"]];
    [cell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]
                       placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    cell.userNameLbl.text = [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"get_user"][@"first_name"]];
    BOOL is_live = [notiArray[indexPath.row][@"get_ice"][@"is_live"]boolValue];
    BOOL is_upnext = [notiArray[indexPath.row][@"get_ice"][@"is_upcoming"]boolValue];
    BOOL is_comingsoon = [notiArray[indexPath.row][@"get_ice"][@"is_comming_soon"]boolValue];
    if (is_live) {
        cell.liveLbl.hidden = NO;
        [cell.dateContainerView setBackgroundColor:appController.appBlueColor];
    }
    else if(is_upnext){
        cell.liveLbl.hidden = YES;
        [cell.dateContainerView setBackgroundColor:appController.appGreenColor];
    }
    else{
        cell.liveLbl.hidden = YES;
        [cell.dateContainerView setBackgroundColor:appController.appRedColor];
    }
    
    
    
    NSDate *today = [NSDate date];
    NSDate *todayDate = [self toLocalTime:today];
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDateFormatter *serverDateFormatter = [NSDateFormatter new];
    [serverDateFormatter setDateFormat:@"yyyy-M-dd"];
    NSDate * endDateFromApi = [startTimeformatter dateFromString:notiArray[indexPath.row][@"get_ice"][@"end_date"]];
    NSDate * startDateFromApi = [startTimeformatter dateFromString:notiArray[indexPath.row][@"get_ice"][@"start_date"]];
    
    NSString *serverStartTime = [serverDateFormatter stringFromDate:startDateFromApi];
    NSString *serverEndTime = [serverDateFormatter stringFromDate:endDateFromApi];
    if (is_upnext||is_comingsoon) {
        
        
        cell.weekdayLbl.text = [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"event_day"]];
        cell.dateLbl.text = [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"event_day_start"]];
        cell.monthLbl.text = [NSString stringWithFormat:@"%@",notiArray[indexPath.row][@"get_ice"][@"event_month"]];
    
        
        
        
        
        
        
    }
    
    else if ([serverStartTime isEqualToString:serverEndTime]) {
        cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
        cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
        cell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
 
    }
    
    else {
        
        
        
        
        
        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
        //    NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:notiArray[indexPath.row][@"get_ice"][@"end_date"]];
        // your date
        
        //    NSLog(@"server time %@",endDateFromApi);
        //    NSLog(@"server time string %@",[startTimeformatter stringFromDate:endDateFromApi]);
        
        
        //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
        
        // comparing two dates
        
        
        [startTimeformatter setDateFormat:@"yyyy-M-dd"];
        
        
        NSString *todayDateString = [startTimeformatter stringFromDate:today];
        NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
        NSDictionary *dic = notiArray[indexPath.row][@"get_ice"];
        [self setHeaderDateTime:cell :dic];
        
    }
    




    [commonUtils setRoundedRectBorderView:cell.liveLbl withBorderWidth:1.0f withBorderColor:appController.appBlueColor withBorderRadius:0.0f];
    cell.showUserProfileVCBtn.tag = indexPath.row;
    cell.showEventDetailVCBtn.tag = indexPath.row;
    [cell.showUserProfileVCBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
    [cell.showEventDetailVCBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];    
    
    return cell;
}

- (void)onShowUserProfileVC:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    commonUtils.notificationsArray = notiArray;
    [self.delegate showUserProfileVC:sender];
}

- (void)onShowEventDetailVC:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];

    commonUtils.notificationsArray = notiArray;
      commonUtils.checkincount = notiArray[sender.tag][@"get_checked_in_count"];
    NSString *notificationID = [NSString stringWithFormat:@"%@",notiArray[sender.tag][@"id"]];
    [self readNotification:notificationID];
    NSLog(@"check in count is %@",commonUtils.checkincount);
    [self.delegate showEventDetailVC:sender];
}



#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [self dismissViewControllerAnimated:NO completion:nil];
    //    CGPoint point = [sender locationInView:sender.view];
    //    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    //    if ([viewTouched isKindOfClass:[ZFTokenField class]]) {
    //        // Do nothing;
    //    } else {
    //        // respond to touch action
    //        //        [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    //        
    //    }
}

-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    
    
}
-(void)getNotfications:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^(void){
//           [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            [commonUtils showHud:self.view];
//            [self.view setUserInteractionEnabled:NO];
            [commonUtils SetWindow:false];
        });
        NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"%@get_all_notifications/%@?time_zone=%@",ServerUrl,skip,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
 
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
                    //                    icedActiviteis = successDic[@"iced_activities"];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [commonUtils hideHud];
                        [commonUtils SetWindow:true];
                        notiLbl.text = [NSString stringWithFormat:@"%@",successDic[@"un_read"]];
                     
                        [self didFinishRecordsRequest:successDic[@"notifications"] forPage:currentPageNumber];
                      
                        // [mainTV reloadData];
                    });
                    
                }
                else{
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [commonUtils hideHud];
                        [commonUtils SetWindow:true];
                        [commonUtils showAlert:@"Error" withMessage:@"Server Error"];
                    });
                }
                if(Jerror!=nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                   
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [commonUtils hideHud];
                        [commonUtils SetWindow:true];
                    });
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
        
       }];
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(notiTV.contentOffset.y >= (notiTV.contentSize.height - notiTV.bounds.size.height)) {
        
        //NSLog(@" scroll to bottom!");
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            NSInteger notficationCount = [notiLbl.text integerValue];
            isPageRefresing = YES;
            if (currentPageNumber<(notficationCount/5)) {
                currentPageNumber = currentPageNumber +1;
                [self getNotfications:currentPageNumber];
            }
            
        }
    }
    
}
-(void)didFinishRecordsRequest:(NSArray *)results forPage:(NSInteger)pageNo{
    NSLog(@"results ===> %@",results);
    if(pageNo == 0){
        fullNotificationArray = [results mutableCopy];
        
        notiArray = [results mutableCopy];
    }
    else{
        [fullNotificationArray addObject:results];
        [notiArray addObjectsFromArray:results];
    }
    isPageRefresing = NO;
    [notiTV reloadData];
}
    
-(void)setHeaderDateTime:(NotificationTVCell*)cell :(NSDictionary*)dic{
        NSDate *today = [NSDate date];
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        NSDate *todayDate = [self toLocalTime:today];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        NSDate * endDateFromApi = [startTimeformatter dateFromString:dic[@"end_date"]];
        [startTimeformatter setDateFormat:@"yyyy-M-dd"];
        NSString *todayDateString = [startTimeformatter stringFromDate:today];
        NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
        
        NSComparisonResult result;
        result = [endDateFromApi compare:today];
        if(result==NSOrderedDescending)
        {
            NSLog(@"server is large");
            if ([todayDateString isEqualToString:endDateString]) {
                cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
                cell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
                cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
              
                
            }
            else{
                cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
                cell.weekdayLbl.text= [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
                cell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
        
            }
            
            
            
            
            
            
            
        }
        
        else if(result==NSOrderedAscending){
            NSLog(@"today date  is large");
           cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            cell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
         
            
            
        }
        
        
        else{
            cell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            cell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            cell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
           
        }
    }
-(void)readNotification:(NSString*)notificationID{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:notificationID forKey:@"notification_id"];
        
        
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"read_notification"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                              if([statusis isEqualToString:@"success"]){
                                                  
                                                  [[NSNotificationCenter defaultCenter]
                                                   postNotificationName:@"Home"
                                                   object:self];
                                                  
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      
                                                      
                                                      
                                                      
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      
                                                      
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}

@end
