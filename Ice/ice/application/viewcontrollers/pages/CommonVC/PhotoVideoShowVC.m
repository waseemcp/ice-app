//
//  PhotoVideoShowVC.m
//  ICE
//
//  Created by LandToSky on 11/25/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "PhotoVideoShowVC.h"
#import "CommentTableVC.h"
#import "CommentVC1.h"
#import "InviteFriendTVCell.h"
#import <SVProgressHUD.h>
#import "TMImageZoom.h"
#import "PhotoVideoCell.h"

@interface PhotoVideoShowVC ()<UIScrollViewDelegate,ZFTokenFieldDataSource, ZFTokenFieldDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
{
    
  
    IBOutlet UILabel *currentIndexLbl;
    NSMutableArray *imagesArray,*usersArray,*usersMutableCopy,*inviteUserIDs,*imageTags,*alreadyTagged,*newTagsArray,*imagesArrayCopy;
    NSInteger currentImageIndex;
    NSMutableArray *tokens;
    UITextField *currentTextField;
    BOOL tagViewShown;
    NSMutableDictionary *mainEventDict;
  //  UIImageView *imageView;
}

@end

@implementation PhotoVideoShowVC
@synthesize collectionViewTag,comingFrom,eventType,sectionIndex;
- (void)viewDidLoad {
    [super viewDidLoad];

    [self initUI];
    [self initData];
}

- (void)initUI
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleZoomin:)
                                                 name:@"zoomin"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleZoomOut:)
                                                 name:@"zoomout"
                                               object:nil];
    [self.view bringSubviewToFront:self.topView];
    [self.view bringSubviewToFront:self.topView];

    
}
-(void)viewDidLayoutSubviews{
    //_imageCV.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}
- (void)initData
{
    tagViewShown = NO;
    
    imagesArray = [[NSMutableArray alloc]init];
    imagesArrayCopy = [NSMutableArray new];
    mainEventDict = [NSMutableDictionary new];
    
    if ([comingFrom isEqualToString:@"userprofile"]) {
        imagesArray = commonUtils.userUpcomingArray[collectionViewTag][@"get_images"];
        mainEventDict = commonUtils.userUpcomingArray[collectionViewTag];
    }
    else if([comingFrom isEqualToString:@"ownProfile"]){
        imagesArray = commonUtils.userIcePics;
        [_bottomView setHidden:YES];
    }
    else if([comingFrom isEqualToString:@"myprofilepic"]){
        imagesArray = commonUtils.myprofileImage;
        currentIndexLbl.hidden = YES;
        [_bottomView setHidden:YES];
    }
    else{
        if ([eventType isEqualToString:@"live"]) {
             imagesArray = commonUtils.liveEventsArray[collectionViewTag][@"get_images"];
            mainEventDict = commonUtils.liveEventsArray[collectionViewTag];
        }
        else if ([eventType isEqualToString:@"upcoming"]){
             imagesArray = commonUtils.upNextEventsArray[collectionViewTag][@"get_images"];
            mainEventDict = commonUtils.upNextEventsArray[collectionViewTag];
        }
        else if([eventType isEqualToString:@"comingsoon"]){
            imagesArray = commonUtils.comingSoonArray[collectionViewTag][@"get_images"];
            mainEventDict = commonUtils.comingSoonArray[collectionViewTag];
        }
        else if ([eventType isEqualToString:@"tagged"]){
            imagesArray = commonUtils.taggedData[collectionViewTag][@"get_images"];
            mainEventDict = commonUtils.taggedData[collectionViewTag];
        }
        else if ([eventType isEqualToString:@"calendar"]){
            imagesArray = commonUtils.calendarArray[sectionIndex][collectionViewTag][@"get_images"];
            mainEventDict = commonUtils.calendarArray[sectionIndex][collectionViewTag];
        }
        else if ([eventType isEqualToString:@"events"]){
            imagesArray = commonUtils.activitiesArray[collectionViewTag][@"get_images"];
               mainEventDict = commonUtils.activitiesArray[collectionViewTag];
        }
        else if ([eventType isEqualToString:@"notification"]){
            imagesArray = commonUtils.notificationsArray[collectionViewTag][@"get_ice"][@"get_images"];
            mainEventDict = commonUtils.notificationsArray[collectionViewTag];
        }
        else if ([eventType isEqualToString:@"alert"]){
            imagesArray = commonUtils.alertDic[@"get_images"];
        }
       
        [_bottomView setHidden:NO];
    }
    
    NSLog(@"mainEventDict %@",mainEventDict);
    _imageCV.delegate = self;
    _imageCV.dataSource = self;
    [_imageCV layoutIfNeeded];
    

    if([comingFrom isEqualToString:@"myprofilepic"]){
        imagesArrayCopy = [imagesArray mutableCopy];
    }
    else{
        for (int i = 0; i<imagesArray.count; i++) {
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:imagesArray[i]];
            [imagesArrayCopy addObject:dic];
        }
    }
   
  
        
    self.tokenField.delegate = self;
    self.tokenField.dataSource = self;
    [self showCurrentPage:self.currentPageNum];
    [self scrollToCurrentPage:self.currentPageNum];
    _inviteFriendTxt.delegate = self;
    [_inviteFriendTxt addTarget:self
                        action:@selector(textFieldDidChange:)
              forControlEvents:UIControlEventEditingChanged];
    _tagTV.tag = 1000;
    _tokenSV.tag = 2000;
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    LGSideMenuController *sidemenuController = self.sideMenuController;
    [sidemenuController setLeftViewSwipeGestureDisabled:YES];
    [sidemenuController setRightViewSwipeGestureDisabled:YES];
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        
    //   UIApplication.shared.isStatusBarHidden = false
        [[UIApplication sharedApplication] setStatusBarHidden:YES];
    }
     inviteUserIDs = [[NSMutableArray alloc]init];
    if ([comingFrom isEqualToString:@"userprofile"]) {
        imagesArray = commonUtils.userUpcomingArray[collectionViewTag][@"get_images"];
        [_bottomView setHidden:NO];
    }
    else if([comingFrom isEqualToString:@"ownProfile"]){
        imagesArray = commonUtils.userIcePics;
        [_bottomView setHidden:YES];
    }
    else if ([comingFrom isEqualToString:@"myprofilepic"]){
        imagesArray = commonUtils.myprofileImage;
        [_bottomView setHidden:YES];
    }
    else{
        if ([eventType isEqualToString:@"live"]) {
            imagesArray = commonUtils.liveEventsArray[collectionViewTag][@"get_images"];
        }
        else if ([eventType isEqualToString:@"upcoming"]){
            imagesArray = commonUtils.upNextEventsArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"comingsoon"]){
            imagesArray = commonUtils.comingSoonArray[collectionViewTag][@"get_images"];
        }
        else if ([eventType isEqualToString:@"tagged"]){
            imagesArray = commonUtils.taggedData[collectionViewTag][@"get_images"];
        }
        else if ([eventType isEqualToString:@"calendar"]){
            imagesArray = commonUtils.calendarArray[sectionIndex][collectionViewTag][@"get_images"];
        }
        else if ([eventType isEqualToString:@"events"]){
            imagesArray = commonUtils.activitiesArray[collectionViewTag][@"get_images"];
        }
        else if ([eventType isEqualToString:@"notification"]){
            imagesArray = commonUtils.notificationsArray[collectionViewTag][@"get_ice"][@"get_images"];
            //(@"images array of notification %@",imagesArray);
        }
        else if ([eventType isEqualToString:@"alert"]){
            imagesArray = commonUtils.alertDic[@"get_images"];
        }
        
        [_bottomView setHidden:NO];
    }
    
 
 
    [self showCurrentPage:currentImageIndex];

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];

    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    LGSideMenuController *sidemenuController = self.sideMenuController;
    [sidemenuController setLeftViewSwipeGestureDisabled:NO];
    [sidemenuController setRightViewSwipeGestureDisabled:NO];
    [commonUtils hideHud];
}
#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)rScrollView {
    if (rScrollView.tag == 1000||rScrollView.tag == 2000) {
        return;
    }
    int pageNo = round(rScrollView.contentOffset.x / rScrollView.frame.size.width);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:pageNo inSection:0];
    PhotoVideoCell *cell = (PhotoVideoCell*)[_imageCV cellForItemAtIndexPath:indexPath];
    //handle Tap...
    if (cell.scrollView.zoomScale > 1.0) {
        NSLog(@"is zooming");
        if ([comingFrom isEqualToString:@"ownProfile"]||[comingFrom isEqualToString:@"myprofilepic"]) {
            
        }
        else{
          _bottomView.hidden = YES;
        }
        _topView.hidden = YES;
        
    }
    else{
        if ([comingFrom isEqualToString:@"ownProfile"]||[comingFrom isEqualToString:@"myprofilepic"]) {
            
        }
        else{
           _bottomView.hidden = NO;
        }
        _topView.hidden = NO;
       
        NSLog(@"zoomed out");
    }
//    [currentIndexLbl setText:[NSString stringWithFormat:@"%d/%lu", pageNo, imageNames.count]];
   
    [self showCurrentPage:pageNo];
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)rscrollView{
    if (rscrollView.tag == 1000|| rscrollView.tag == 2000) {
        return;
    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)rscrollView willDecelerate:(BOOL)decelerate{
    if (rscrollView.tag ==1000||rscrollView.tag == 2000) {
        return;
    }
}


- (void)scrollToCurrentPage:(NSInteger)currentPageIndex
{

    NSInteger index= currentImageIndex;
    CGFloat pageWidth = _imageCV.frame.size.width;
    CGPoint scrollTo = CGPointMake(pageWidth * index, 0);
    [_imageCV setContentOffset:scrollTo animated:YES];
//    CGFloat x = scrollView.frame.size.width * currentPageIndex;
//    [scrollView scrollRectToVisible:CGRectMake(x, scrollView.frame.origin.y, scrollView.frame.size.width, scrollView.frame.size.height) animated:NO];
    
}

- (void)showCurrentPage:(NSInteger) currentPageIndex
{
    NSMutableAttributedString *totalNum = [[NSMutableAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"/%lu",imagesArray.count]
                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:11], NSForegroundColorAttributeName:[UIColor grayColor]}];
    NSMutableAttributedString *currentNum = [[NSMutableAttributedString alloc]
                                           initWithString:[NSString stringWithFormat:@"%lu",currentPageIndex+1]
                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}];
   
    [currentNum appendAttributedString:totalNum];
    [currentIndexLbl setAttributedText:currentNum];
    NSMutableArray *imageLikes = [[NSMutableArray alloc] init];
    NSMutableArray *imageComments =  [[NSMutableArray alloc] init];
    NSMutableArray *is_like = [NSMutableArray new];
     currentImageIndex = currentPageIndex;
    if (![comingFrom isEqualToString:@"myprofilepic"]&&![comingFrom isEqualToString:@"ownProfile"]) {

        if (currentPageIndex<imagesArray.count) {
            
       
    is_like = imagesArray[currentPageIndex][@"is_like"];
    if (is_like.count>0) {
        _likesLbl.textColor = [UIColor whiteColor];
        _likeIV.image = [UIImage imageNamed:@"card-count-icon3"];
    }
    else{
        _likesLbl.textColor = [UIColor colorWithRed:0.992 green:0.549 blue:0.537 alpha:1.000];
        _likeIV.image = [UIImage imageNamed:@"card-count-icon2"];
    }
    imageComments = imagesArray[currentPageIndex][@"comments"];
    imageLikes = imagesArray[currentPageIndex][@"likes"];

    alreadyTagged = [[NSMutableArray alloc]init];
   

    _likesLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)imageLikes.count];
    _commentsLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)imageComments.count];

    imageTags = [NSMutableArray new];
    imageTags = [imagesArray[currentPageIndex][@"get_tags"]mutableCopy];
             }
    }
     //NSLog(@"current page index is %ld",(long)currentPageIndex);
    //NSLog(@"image tags array is %@",imageTags);
    
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
   NSString *imageUrl;
     UIImageView *imageView3 = [[UIImageView alloc]init];
    if (![comingFrom isEqualToString:@"myprofilepic"]) {
        
        if ([imagesArray[currentImageIndex][@"type"]isEqualToString:@"image"]) {
            imageUrl  = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,imagesArray[currentImageIndex][@"image"]];
        }
        else{
            imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,imagesArray[currentImageIndex][@"poster"]];
        }
    }
    else{
        imageView3.image = imagesArray[currentImageIndex];
    }

    CommentVC1 *vc = [segue destinationViewController];
    
    vc.commentingImage = imageView3.image;
    vc.imageIndex = currentImageIndex;
    vc.commentOF = @"image";
    vc.imageUrl = imageUrl;
    vc.likesCount = _likesLbl.text;
    vc.commentsCount = _commentsLbl.text;
    vc.activityIndex = collectionViewTag;
    vc.comingFrom = comingFrom;

    
}

- (IBAction)imgLikeBtn:(id)sender {
   // NSLog(@"commutil user upcoming array is %@",commonUtils.userUpcomingArray);
    NSMutableArray *is_like = [NSMutableArray new];
    is_like = imagesArray[currentImageIndex][@"is_like"];
    NSMutableArray *likesPeople = [[NSMutableArray alloc]init];
    likesPeople = [imagesArray[currentImageIndex][@"likes"]mutableCopy];
    NSMutableArray *likesArray = [[NSMutableArray alloc]init];
    if (is_like.count==0) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *likesCount = [f numberFromString:_likesLbl.text];
        NSInteger likes = [likesCount integerValue];
        likes++;
        [likesArray addObject:@"1"];
    
        _likesLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
        [imagesArrayCopy[currentImageIndex]setObject:likesArray forKey:@"is_like"];
        imagesArray = [[NSMutableArray alloc]initWithArray:imagesArrayCopy];
        NSMutableDictionary *newDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:@"now",@"created_at",imagesArray[currentImageIndex][@"id"],@"image_id", nil];
        [likesPeople addObject:newDic];
        if ([comingFrom isEqualToString:@"userprofile"]) {
            commonUtils.userUpcomingArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.userUpcomingArray :likesArray :likesPeople]];
        }
        else if ([eventType isEqualToString:@"notification"]){
               NSMutableArray *changedArray = [self changeArrayToMutable:commonUtils.notificationsArray :likesArray :likesPeople];
         
            [changedArray[collectionViewTag][@"get_ice"][@"get_images"][currentImageIndex]setObject:likesPeople forKey:@"likes"];
          
            [changedArray[collectionViewTag][@"get_ice"][@"get_images"][currentImageIndex]setObject:likesArray forKey:@"is_like"];
        
            commonUtils.notificationsArray = [[NSMutableArray alloc]initWithArray:changedArray];
             imagesArray = commonUtils.notificationsArray[collectionViewTag][@"get_ice"][@"get_images"];
           // NSLog(@"notification array after like is %@",commonUtils.notificationsArray);
        }
        else if([eventType isEqualToString:@"live"]){
             commonUtils.liveEventsArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.liveEventsArray :likesArray :likesPeople]];
             imagesArray = commonUtils.liveEventsArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"upcoming"]){
            commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.upNextEventsArray :likesArray :likesPeople]];
             imagesArray = commonUtils.upNextEventsArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"comingsoon"]){
            commonUtils.comingSoonArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.comingSoonArray :likesArray :likesPeople]];
              imagesArray = commonUtils.comingSoonArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"tagged"]){
            commonUtils.taggedData = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.taggedData :likesArray :likesPeople]];
             imagesArray = commonUtils.taggedData[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"events"]){
            commonUtils.activitiesArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.activitiesArray :likesArray :likesPeople]];
              imagesArray = commonUtils.activitiesArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"calendar"]){
            commonUtils.calendarArray[sectionIndex] = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.calendarArray :likesArray :likesPeople]];
             imagesArray = commonUtils.calendarArray[sectionIndex][collectionViewTag][@"get_images"];
            
        }
        else if ([eventType isEqualToString:@"alert"]){
            [self changeDicOfAlert:likesArray :likesPeople];
        }
        _likesLbl.textColor = [UIColor whiteColor];

        _likeIV.image = [UIImage imageNamed:@"card-count-icon3"];
    
        [self imageLikeAPI:imagesArray[currentImageIndex][@"id"]];
    }
    else{
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *likesCount = [f numberFromString:_likesLbl.text];
        NSInteger likes = [likesCount integerValue];
        if (likes>0) {
            likes--;
            [likesPeople removeLastObject];
        }
        likesArray = [NSMutableArray new];
        [imagesArrayCopy[currentImageIndex]setObject:likesArray forKey:@"is_like"];
        imagesArray = [[NSMutableArray alloc]initWithArray:imagesArrayCopy];
        if ([comingFrom isEqualToString:@"userprofile"]) {
           commonUtils.userUpcomingArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.userUpcomingArray :likesArray :likesPeople]];
        }
        else if ([eventType isEqualToString:@"notification"]){
            NSMutableArray *changedArray = [self changeArrayToMutable:commonUtils.notificationsArray :likesArray :likesPeople];
            
            //   NSLog(@"changed array is %@",changedArray);
            [changedArray[collectionViewTag][@"get_ice"][@"get_images"][currentImageIndex]setObject:likesPeople forKey:@"likes"];
             [changedArray[collectionViewTag][@"get_ice"][@"get_images"][currentImageIndex]setObject:likesArray forKey:@"is_like"];
            commonUtils.notificationsArray = [[NSMutableArray alloc]initWithArray:changedArray];
             imagesArray = commonUtils.notificationsArray[collectionViewTag][@"get_ice"][@"get_images"];
        }
        else if([eventType isEqualToString:@"live"]){
            commonUtils.liveEventsArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.liveEventsArray :likesArray :likesPeople]];
             imagesArray = commonUtils.liveEventsArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"upcoming"]){
            commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.upNextEventsArray :likesArray :likesPeople]];
             imagesArray = commonUtils.upNextEventsArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"comingsoon"]){
            commonUtils.comingSoonArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.comingSoonArray :likesArray :likesPeople]];
              imagesArray = commonUtils.comingSoonArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"tagged"]){
            commonUtils.taggedData = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.taggedData :likesArray :likesPeople]];
             imagesArray = commonUtils.taggedData[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"events"]){
            commonUtils.activitiesArray = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.activitiesArray :likesArray :likesPeople]];
              imagesArray = commonUtils.activitiesArray[collectionViewTag][@"get_images"];
        }
        else if([eventType isEqualToString:@"calendar"]){
            commonUtils.calendarArray[sectionIndex] = [[NSMutableArray alloc]initWithArray:[self changeArrayToMutable:commonUtils.calendarArray :likesArray :likesPeople]];
             imagesArray = commonUtils.calendarArray[sectionIndex][collectionViewTag][@"get_images"];
        }
        else if ([eventType isEqualToString:@"alert"]){
            [self changeDicOfAlert:likesArray :likesPeople];
        }
        _likesLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
        _likesLbl.textColor = [UIColor colorWithRed:0.992 green:0.549 blue:0.537 alpha:1.000];
        _likeIV.image = [UIImage imageNamed:@"card-count-icon2"];

        [self imageUnLikeAPI:imagesArray[currentImageIndex][@"id"]];
    }
//    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//    f.numberStyle = NSNumberFormatterDecimalStyle;
//    NSNumber *likesCount = [f numberFromString:_likesLbl.text];
//    NSInteger likes = [likesCount integerValue];
//    likes++;
//    _likesLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
//   
//    [self imageLikeAPI:imagesArray[currentImageIndex][@"id"]];
    
}
-(void)imageLikeAPI:(NSString*)imageID{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:imageID forKey:@"image_id"];
        

        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_image_like"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                              if([statusis isEqualToString:@"success"]){
                                                  
                                                  
                                                  
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                                                    
                                                      //[commonUtils showAlert:@"Success" withMessage:successMessage];
                                                      
                                                      // Here we need to pass a full frame
                                                      
                                                      
                                                      
                                                      
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                           
                                                    
                                                      NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                                      f.numberStyle = NSNumberFormatterDecimalStyle;
                                                      NSNumber *likesCount = [f numberFromString:_likesLbl.text];
                                                      NSInteger likes = [likesCount integerValue];
                                                      likes--;
                                                      _likesLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                           
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                      
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}
#pragma mark - image Unlike API
-(void)imageUnLikeAPI:(NSString*)imageID{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:imageID forKey:@"image_id"];
        
        
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_image_unlike"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                              if([statusis isEqualToString:@"success"]){
                                                  
                                                  
                                                  
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      
                                                      //[commonUtils showAlert:@"Success" withMessage:successMessage];
                                                      
                                                      // Here we need to pass a full frame
                                                      
                                                      
                                                      
                                                      
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{

                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}

#pragma mark - ZFTokenField Delegate

- (CGFloat)tokenMarginInTokenInField:(ZFTokenField *)tokenField
{
    return 5;
}

- (void)tokenField:(ZFTokenField *)tokenField1 didReturnWithText:(NSString *)text
{
    [tokens addObject:text];
    [tokenField1 reloadData];
}

- (void)tokenField:(ZFTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    
    NSLog(@"data %@",tokens[index]);
    
    [tokens removeObjectAtIndex:index];

}

- (BOOL)tokenFieldShouldEndEditing:(ZFTokenField *)textField
{
    return NO;
}

- (CGFloat)widthForText:(NSString*)text font:(UIFont*)font withHeight:(CGFloat)height {
    
    CGSize constraint = CGSizeMake(2000.0f, height);
    CGSize size;
    
    CGSize boundingBox = [text boundingRectWithSize:constraint
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.width;
}
- (void)onSelectInviteFriend:(UIButton*)sender
{
    [self.view sendSubviewToBack:_tagTV];
    [inviteUserIDs addObject:usersArray[sender.tag][@"get_following"][@"id"]];
    
    [tokens addObject:usersArray[sender.tag]];
    imageTags = [NSMutableArray new];
    imageTags = tokens;
  
  
  
    newTagsArray = [tokens mutableCopy];
    [_tokenField reloadData];
    
    [self.view endEditing:YES];
    [_inviteFriendTxt setText:@""];
   // _inviteFriendTxt.userInteractionEnabled = NO;
    [_tagTV setHidden:YES];

    [self.tokenView setHidden:YES];
    [self.taggingView setHidden:YES];
    
    
    NSLog(@"usersArray %@",usersArray[sender.tag]);
    [self TagUserAPI:imagesArray[currentImageIndex][@"id"] taggedUserID:usersArray[sender.tag][@"get_following"][@"id"]];
    //After added new token, adjust frame
    [self adjustTokenView];
    
    usersArray = [NSMutableArray new];
    usersMutableCopy = [NSMutableArray new];
    [_tagTV reloadData];
    
}
- (void)tokenDeleteButtonPressed:(UIButton *)tokenButton
{
    //Land
    //    NSUInteger index = [tokenField indexOfTokenView:tokenButton.superview];
    NSUInteger index = tokenButton.tag;
    if (index != NSNotFound) {
      

            for (int i=0; i<inviteUserIDs.count; i++) {
                if ([NSString stringWithFormat:@"%@",tokens[index][@"id"]]==[NSString stringWithFormat:@"%@",inviteUserIDs[i]]) {
                    [inviteUserIDs removeObjectAtIndex:i];
                }
            }
       
        NSLog(@"data %@",tokens[index]);
        
        [self RemoveTagAPI:tokens[index][@"id"]];
        [tokens removeObjectAtIndex:index];
        [_tokenField reloadData];
       // [self adjustFrameAfterAddingInviteFriend];
    }
}

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return 25;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{
    return tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TokenView" owner:nil options:nil];
    UIView *view = nibContents[0];
    UIImageView *imageView2 = (UIImageView*)[view viewWithTag:1];
    UILabel *label = (UILabel *)[view viewWithTag:2];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    UIView *viewcross = (UIView *)[view viewWithTag:786];
    
    viewcross.hidden = true;
    NSLog(@"index wasemm ==> %@",tokens[index]);
    if ([tokens[index][@"can_edit"] boolValue]){
        viewcross.hidden = false;
    }
    if (tokens[index][@"user"][@"photo"]) {
        NSString *imageUrlString = [NSString stringWithFormat:@"%@",tokens[index][@"user"][@"photo"]];
        [imageView2 sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                     placeholderImage:[UIImage imageNamed:@"user-avatar"]
                              options:SDWebImageCacheMemoryOnly];
    }
    else {
        NSString *imageUrlString = [NSString stringWithFormat:@"%@",tokens[index][@"photo"]];
        [imageView2 sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                      placeholderImage:[UIImage imageNamed:@"user-avatar"]
                               options:SDWebImageCacheMemoryOnly];
   
    }
    if ([tokens[index][@"user"] valueForKey:@"first_name"]) {
        label.text = [tokens[index][@"user"] valueForKey:@"first_name"];
    }
    else {
         label.text = [tokens[index] valueForKey:@"first_name"];
    }
    
    
    
    
    
    
    button.tag = index;
    [button addTarget:self action:@selector(tokenDeleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    //    CGSize size = [label sizeThatFits:CGSizeMake(1000, 25)];
    CGFloat width = [self widthForText:label.text font:[UIFont systemFontOfSize:13] withHeight:25.0f];
    view.frame = CGRectMake(0, 0, width + 60, 25);
    return view;
}

- (IBAction)tagBtn:(id)sender {
      [self getICETags:[NSString stringWithFormat:@"%@",imagesArray[currentImageIndex][@"id"]]];
 

    if (!tagViewShown) {
        tagViewShown = YES;
        [self.tagTV setHidden:NO];
        [self.view bringSubviewToFront:_tagTV];
        [self.tokenView setHidden:NO];
        [self.taggingView setHidden:NO];
        CGRect contentRect = CGRectZero;
        for (UIView *view in _tokenSV.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        _tokenSV.userInteractionEnabled = YES;
        
       
        CGSize scrollableSize = CGSizeMake(0,_tokenField.frame.size.height+20);
        [_tokenSV setContentSize:scrollableSize];

    }
    else {
        tagViewShown = NO;
        [self.tagTV setHidden:YES];
        [self.view sendSubviewToBack:_tagTV];
        [self.tokenView setHidden:YES];
        [self.taggingView setHidden:YES];
    }
   
    
}

- (IBAction)videoPlayBtn:(id)sender {
  
}
-(void)getUsers{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
     
//        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,@"get_users"]]];
            
            NSString *timezone = [commonUtils getTimeZone];
            NSString *urlString = [NSString stringWithFormat:@"%@get_follow_following/%@?time_zone=%@",ServerUrl,commonUtils.userData[@"id"],timezone];

            NSLog(@"urlString %@",urlString);
             NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                   
                    usersArray=usersMutableCopy    = [[NSMutableArray alloc]init];
                    NSMutableArray *usersArrayNew = [[json valueForKey:@"successData"]  mutableCopy];
//                        usersArray = [[usersArrayNew valueForKey:@"following"] mutableCopy];
//                        usersMutableCopy = [usersArray mutableCopy];
                        
                        NSLog(@"user data is %@",usersArrayNew);
                        NSLog(@"inviteUserIDs %@",tokens);
                        
                        NSMutableArray *arrayNew = [[usersArrayNew valueForKey:@"following"] mutableCopy];
                        for (int index = 0 ; index < arrayNew.count ; index ++){
                            NSString *strID = [NSString stringWithFormat:@"%@",[arrayNew[index][@"get_following"] valueForKey:@"id"]];
                            
                            bool isfound = false;
                            for (int indexInner = 0 ; indexInner < tokens.count ; indexInner ++){
                                NSLog(@"strIDstrID  %@===%@",strID,tokens[indexInner][@"user_id"]);
                                if ( [strID isEqualToString:[NSString stringWithFormat:@"%@",tokens[indexInner][@"user_id"]]]){
                                    isfound = true;
                                    break;
                                }
                            }
                            
                            
                            if (!isfound){
                                [usersArray addObject:arrayNew[index]];
                            }
                        }
                        
                        
                        NSLog(@"user usersArray is %@",usersArray);
                        
                    usersMutableCopy = usersArray.mutableCopy;
                    [_tagTV reloadData];
                         });
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
             }];
            
               });
    }
    
}
#pragma mark - Tag Friend TableView Delegata
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return usersArray.count;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        InviteFriendTVCell *cell0 = (InviteFriendTVCell*) [tableView dequeueReusableCellWithIdentifier:@"InviteFriendTVCell"];
        
        cell0.selectFriendBtn.tag = indexPath.row;
        cell0.inviteFriendNameLbl.text = [usersArray[indexPath.row][@"get_following"] valueForKey:@"first_name"];
        
        
        NSString *imageUrlString = [NSString stringWithFormat:@"%@",[usersArray[indexPath.row][@"get_following"] valueForKey:@"photo"]];
        
        
 
        [cell0.inviteFriendImage sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                                   placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                            options:SDWebImageCacheMemoryOnly];
        
        
        
        [cell0.selectFriendBtn addTarget:self action:@selector(onSelectInviteFriend:) forControlEvents:UIControlEventTouchUpInside];
        cell = cell0;
    }     return cell;
}
#pragma mark - textfield methods
-(void)textFieldDidChange:(id)sender{
    if ([_inviteFriendTxt.text length]>0) {
        [usersArray removeAllObjects];
  
        for (int i=0; i<usersMutableCopy.count; i++) {
            // NSRange *range = ;
            NSString *object = [usersMutableCopy[i][@"get_following"] valueForKey:@"first_name"];
            
            if([object rangeOfString:_inviteFriendTxt.text options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                [usersArray addObject:usersMutableCopy[i]];
                
            }
        }
    
        
        
        
        [_tagTV reloadData];
        
    }
    else {
        usersArray = [[NSMutableArray alloc]init];
        usersArray = [usersMutableCopy mutableCopy];
       
        [_tagTV reloadData];
        
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
 
    [self.view endEditing:YES];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    currentTextField = textField;
 
    return YES;
}
-(void)TagUserAPI:(NSString*)imageID taggedUserID:(NSString*)taggedUserID{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:imageID forKey:@"image_id"];
         [_params setObject:taggedUserID forKey:@"tagged_id"];
        
        
        if (mainEventDict){
            if (mainEventDict[@"user_id"]){
                [_params setObject:mainEventDict[@"user_id"] forKey:@"owner_id"];
            }
        }
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_image_tag"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        NSLog(@"add_image_tag %@",Url);
        NSLog(@"_params %@",_params);
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSLog(@"sessionToken %@",commonUtils.sessionToken);
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                              if([statusis isEqualToString:@"success"]){

                                                      
                                                      
                                                      
                                                  
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}



-(void)RemoveTagAPI:(NSString*)taggedUserID{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:taggedUserID forKey:@"image_tagged_id"];
        
        NSLog(@"_params image_tagged_id==>  %@",_params);
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@remove_image_tag",ServerUrl];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              //                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                                  
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                      
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}



-(void)adjustTokenView{
    CGSize scrollableSize = CGSizeMake(0,_tokenField.frame.size.height+20);
    [_tokenSV setContentSize:scrollableSize];
}
-(void)getICETags:(NSString*)imageID{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        
       
        NSString *urlString = [NSString stringWithFormat:@"%@%@/%@",ServerUrl,@"get_image_tags",imageID];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
    //    NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
 
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                 [self getUsers];
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                 
                    commonUtils.userUpcomingArray = [[NSMutableArray alloc] init];
                    NSMutableDictionary *successDic = [json[@"successData"]mutableCopy];
                    //NSLog(@"%@",commonUtils.userUpcomingArray[activityIndex][@"get_user"]);
            
                        
                        NSLog(@"successDic %@",successDic);
                    imageTags = [successDic[@"image_tags"]mutableCopy];
                    tokens = imageTags;
                    [_tokenField reloadData];
                    
                       });
                    
                    
                    
                 
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
           }];
    }
    
}
#pragma Play video func
-(void)playvideomethod:(UIButton*)sender{
    NSURL *videoUrl;
    if (imagesArray[currentImageIndex][@"ice_id"]) {
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[currentImageIndex][@"image"]]];
    }
    else{
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserVideoBaseURL,imagesArray[currentImageIndex][@"image"]]];
    }
  
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}
-(NSMutableArray*)changeArrayToMutable:(NSMutableArray*)arrayToChange :(NSMutableArray*)likesArray :(NSMutableArray*)likesPeople{
    if ([eventType isEqualToString:@"notification"]) {
        NSMutableArray  *notificationsArrayCopy = [NSMutableArray new];
        for ( int i = 0; i<arrayToChange.count; i++) {
            [notificationsArrayCopy addObject:[[NSMutableDictionary alloc]initWithDictionary:arrayToChange[i]]];
        }
        for (int i =0; i<arrayToChange.count; i++) {
            NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:arrayToChange[i][@"get_ice"]];
            
            [notificationsArrayCopy[i]setObject:dic forKey:@"get_ice"];
        }
        for (int i = 0; i<arrayToChange.count; i++) {
            NSMutableArray *imagesArray2 = [[NSMutableArray alloc]initWithArray:arrayToChange[i][@"get_ice"][@"get_images"]];
            NSMutableArray *imagesArray3 = [NSMutableArray new];
            for (int j = 0; j<imagesArray2.count; j++) {
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:imagesArray2[j]];
                [imagesArray3 addObject:dic];
            }
            
            
            
            [notificationsArrayCopy[i][@"get_ice"]setObject:imagesArray3 forKey:@"get_images"];
            
        }
        
        return notificationsArrayCopy;
      

    }
    else if([eventType isEqualToString:@"calendar"]){
        NSMutableArray  *calendarArrayCopy = arrayToChange[sectionIndex];
        
        
        NSMutableArray *calendarArrayCopy2 = [NSMutableArray new];
        
        for(int index= 0 ; index < calendarArrayCopy.count ; index++){
            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:calendarArrayCopy[index]] ;
            
            [calendarArrayCopy2 addObject:dataDict];
        }
        
        
        //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
        
        
        //
        
        
        NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:calendarArrayCopy2[collectionViewTag][@"get_images"] ];
        //
        
        
        NSMutableArray *newImageArray = [NSMutableArray new];
        
        for(int index= 0 ; index < arrayGetImage.count ; index++){
            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
            
            [newImageArray addObject:dataDict];
        }
        
        
        [newImageArray[currentImageIndex] setObject:likesArray forKey:@"is_like"];
         [newImageArray[currentImageIndex] setObject:likesPeople forKey:@"likes"];
        
        
        calendarArrayCopy2[collectionViewTag][@"get_images"] = newImageArray ;
        //NSLog(@"calendar array after change %@",calendarArrayCopy2);
        return calendarArrayCopy2;
    }
    else{
        NSMutableArray  *liveEventsCopy = [arrayToChange mutableCopy];
        
        
        NSMutableArray *liveEventsCopy2 = [NSMutableArray new];
        
        for(int index= 0 ; index < liveEventsCopy.count ; index++){
            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:liveEventsCopy[index]] ;
            
            [liveEventsCopy2 addObject:dataDict];
        }
        
        
        //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
        
        
        //
        
        
        NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:liveEventsCopy2[collectionViewTag][@"get_images"] ];
        //
        
        
        NSMutableArray *newImageArray = [NSMutableArray new];
        
        for(int index= 0 ; index < arrayGetImage.count ; index++){
            NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
            
            [newImageArray addObject:dataDict];
        }
        
        
        [newImageArray[currentImageIndex] setObject:likesArray forKey:@"is_like"];
        
        [newImageArray[currentImageIndex] setObject:likesPeople forKey:@"likes"];
        liveEventsCopy2[collectionViewTag][@"get_images"] = newImageArray ;
        return liveEventsCopy2;
  
    }
 

}
-(void)changeDicOfAlert:(NSMutableArray*)likesPeople :(NSMutableArray*)likesArray{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:[commonUtils.alertDic mutableCopy]];
    
    NSMutableArray *arrayGetImage = [[NSMutableArray alloc]initWithArray:dic[@"get_images"] ];
    //
    
    
    NSMutableArray *newImageArray = [NSMutableArray new];
    
    for(int index= 0 ; index < arrayGetImage.count ; index++){
        NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:arrayGetImage[index]] ;
        
        [newImageArray addObject:dataDict];
    }
    
    [newImageArray[currentImageIndex] setObject:likesArray forKey:@"is_like"];
    [newImageArray[currentImageIndex] setObject:likesPeople forKey:@"likes"];
    [dic setObject:newImageArray forKey:@"get_images"];
    commonUtils.alertDic = [[NSMutableDictionary alloc]initWithDictionary:dic];
      imagesArray = commonUtils.alertDic[@"get_images"];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imagesArray.count;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 1.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    return size;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoVideoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PhotoVideoCell" forIndexPath:indexPath];


        if (![comingFrom isEqualToString:@"myprofilepic"]) {
            
            cell.imageView_main.userInteractionEnabled = YES;
            NSString *imageUrl;
            if ([imagesArray[indexPath.row][@"type"]isEqualToString:@"image"]) {
                
                imageUrl  = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,imagesArray[indexPath.row][@"image"]];
            }
            else{
                
                imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,imagesArray[indexPath.row][@"poster"]];
            }
            
            [cell.imageView_main sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                         placeholderImage:[UIImage imageNamed:@"image0"]
                                  options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        }
        else{
          
            cell.imageView_main.image = imagesArray[indexPath.row];
        }
  
    [cell.imageView_main setContentMode:UIViewContentModeScaleAspectFill];
    cell.imageView_main.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapGesture:)];
    cell.imageView_main.tag = indexPath.row;
    tapGesture1.numberOfTapsRequired = 1;
    
    [tapGesture1 setDelegate:self];
    
    [cell setup];
    [cell layoutIfNeeded];

    cell.btnPlay.hidden = true;
    [cell.imageView_main addGestureRecognizer:tapGesture1];
        if (![comingFrom isEqualToString:@"myprofilepic"]) {
            
            
            NSString *type = [NSString stringWithFormat:@"%@",imagesArray[indexPath.row][@"type"]];
            if ([type isEqualToString:@"image"]){
                
            }
            else{
                cell.btnPlay.hidden = false;
//                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                [cell.btnPlay addTarget:self
                           action:@selector(playvideomethod:)
                 forControlEvents:UIControlEventTouchUpInside];
//                [button setTitle:@"" forState:UIControlStateNormal];
//                [button setImage:[UIImage imageNamed:@"video-play"] forState:UIControlStateNormal];
//                button.frame = CGRectMake(0, 0, 60, 60);
                cell.btnPlay.center = self.view.center;
//                button.backgroundColor = [UIColor clearColor];
//                [cell.imageView_main addSubview:button];
//                button.center = self.view.center;
//                [button layoutIfNeeded];
//                [cell layoutIfNeeded];
                
                
//                NSLog(@"x ===> %f",button.center.x);
//                NSLog(@"y ===> %f",button.center.y);
            }
            
        }
        
 
        
    return cell;
}
-(BOOL)prefersStatusBarHidden
{
    return YES;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
//-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
//{
//    UIImageView *imageView3 = [[UIImageView alloc]init];
//    for (int i = 0; i<scrollView.subviews.count; i++) {
//        UIView *subView = scrollView.subviews[i];
//        NSLog(@"tag is %d",(int)subView.tag);
//    }
//    imageView3 = [(UIImageView*)scrollView viewWithTag:currentImageIndex+1500];
//    return imageView3;
//}
- (IBAction)backBtn:(id)sender {
    commonUtils.collectionViewCellTag = currentImageIndex;
    [self.navigationController popViewControllerAnimated:YES];
}
- (void) tapGesture: (UITapGestureRecognizer*)sender
{
    NSLog(@"tag is %d",(int)sender.view.tag);
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.view.tag inSection:0];
    PhotoVideoCell *cell = (PhotoVideoCell*)[_imageCV cellForItemAtIndexPath:indexPath];
    //handle Tap...
    if (cell.scrollView.zoomScale > 1.0) {
        NSLog(@"is zooming");
        cell.scrollView.zoomScale = 1.0;
        if ([comingFrom isEqualToString:@"ownProfile"]||[comingFrom isEqualToString:@"myprofilepic"]) {
            
        }
        else{
           
                _bottomView.hidden = NO;
           
          
        }
      
            _topView.hidden = NO;
       
   
      
       
      
    }
    else{
        if ([comingFrom isEqualToString:@"ownProfile"]||[comingFrom isEqualToString:@"myprofilepic"]) {
            
        }
        else{
            if (_bottomView.isHidden) {
                _bottomView.hidden = NO;
            }
            else{
               _bottomView.hidden = YES;
            }
            
        }
        if (_topView.isHidden) {
            _topView.hidden = NO;
        }
        else{
            _topView.hidden = YES;
        }
       
         NSLog(@"zoomed out");
    }
}
- (void) handleZoomin:(NSNotification *) notification
{
    if ([comingFrom isEqualToString:@"ownProfile"]||[comingFrom isEqualToString:@"myprofilepic"]) {
        
    }
    else{
         _bottomView.hidden = YES;
       
      
    }
   _topView.hidden = YES;
}
- (void) handleZoomOut:(NSNotification *) notification
{
    if ([comingFrom isEqualToString:@"ownProfile"]||[comingFrom isEqualToString:@"myprofilepic"]) {
        
    }
    else{
      
        _bottomView.hidden = NO;
    }
   _topView.hidden = NO;
    
}
@end
