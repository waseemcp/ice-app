//
//  CommentTableViewVC.h
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <IQKeyboardManager.h>
#import "IceTVCell.h"
#import <SVProgressHUD.h>
@interface CommentTableVC : BaseViewController

@property (nonatomic) NSInteger activityIndex,imageIndex,sectionIndex;
@property (nonatomic,strong) NSString *commentOF,*comingFrom,*iceId;
@property (nonatomic,strong) IceTVCell *iceCell;
@property (nonatomic,strong) NSMutableArray *iceDetailsArray;


@end
