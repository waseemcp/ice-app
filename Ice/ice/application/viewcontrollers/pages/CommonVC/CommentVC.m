//
//  CommentVC.m
//  ICE
//
//  Created by LandToSky on 11/23/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "CommentVC.h"
#import "CommentTVCell.h"
#import "CommentTableVC.h"
@interface CommentVC ()
{


    IBOutlet UIView *opacityView;
}

@end

@implementation CommentVC
@synthesize passedTag,cell,iceDetailsArray,iceID,comingfrom,sectionIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self EnableMenu];
}
- (void)initUI
{

    [self.view setBackgroundColor:[UIColor whiteColor]];
  
    [opacityView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];

    
    
    SidePanelVC *sidePanelVC = (SidePanelVC*)self.sideMenuController;
    sidePanelVC.leftViewEnabled = false;
    sidePanelVC.leftViewDisabled = true;
    sidePanelVC.leftViewSwipeGestureDisabled = false;
    
    [sidePanelVC setRightViewEnabled:false];
    [sidePanelVC setRightViewSwipeGestureEnabled:false];
    
    
}

- (void)initData
{


    
}


- (IBAction)onShowUserComment:(id)sender {
    
    
}



#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    
    
    if ([self.comingfrom isEqualToString:@"upnext"]){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"upcoming"
         object:self];
    }else if ([self.comingfrom isEqualToString:@"live"]){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"live"
         object:self];
    }else if ([self.comingfrom isEqualToString:@"comingsoon"]){
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"comingsoon"
         object:self];
    }else {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"detail"
         object:self];
        
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
        // Get reference to the destination view controller
        CommentTableVC *vc = [segue destinationViewController];
    
        // Pass any objects to the view controller here, like...
        vc.activityIndex = passedTag;
    vc.commentOF = @"ice";
    vc.iceCell = cell;
    vc.iceDetailsArray = iceDetailsArray;
    vc.iceId = iceID;
    vc.comingFrom = comingfrom;
    
    
}

@end
