//
//  allImagesVC.h
//  ICE
//
//  Created by MAC MINI on 04/08/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface allImagesVC : BaseViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *imgToShowView;
@property (weak, nonatomic) IBOutlet UICollectionView *eventImageCV;
@property (nonatomic,strong) NSMutableArray *iceImages;
@property (weak, nonatomic) IBOutlet UIImageView *eventIV;
@property (nonatomic) BOOL isUserImages;
- (IBAction)doneBtn:(id)sender;
- (IBAction)backBtn:(id)sender;
@end
