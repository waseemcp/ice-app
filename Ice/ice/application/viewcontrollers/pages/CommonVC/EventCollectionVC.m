//
//  EventCollectionVC.m
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventCollectionVC.h"
#import "EventImageCVCell.h"
#import <HWViewPager.h>
#import "PhotoVideoShowVC.h"

@interface EventCollectionVC ()<UICollectionViewDataSource, HWViewPagerDelegate>
{
    
    IBOutlet HWViewPager *eventPhotoCV;
    NSMutableArray *imagesArray;
}

@end

@implementation EventCollectionVC
@synthesize index,comingFrom,eventType,sectionIndex;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    eventPhotoCV.dataSource = self;
    eventPhotoCV.pagerDelegate = self;
}

- (void)initData
{
    
}

bool isClick  = false;
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.view endEditing:NO];
    [self EnableMenu];
    isClick= false;
    if ([comingFrom isEqualToString:@"userprofile"]) {
        //NSLog(@"upcoming commutil %@",commonUtils.userUpcomingArray[index]);
        imagesArray = [NSMutableArray new];
        NSLog(@"commutil array is %@",commonUtils.userUpcomingArray);
        if (commonUtils.userUpcomingArray[index][@"get_images"]) {
            imagesArray = commonUtils.userUpcomingArray[index][@"get_images"];
        }
     
    }
    else {
        if ([eventType isEqualToString:@"live"]) {
            if (commonUtils.liveEventsArray[index][@"get_images"]) {
                  imagesArray = commonUtils.liveEventsArray[index][@"get_images"];
            }
           
        }
        else if([eventType isEqualToString:@"upcoming"]){
            if (commonUtils.upNextEventsArray[index][@"get_images"]) {
              imagesArray = commonUtils.upNextEventsArray[index][@"get_images"];
            }
            
        }
        else if([eventType isEqualToString:@"comingsoon"]){
            if (commonUtils.comingSoonArray[index][@"get_images"]){
              imagesArray = commonUtils.comingSoonArray[index][@"get_images"];
            }
            
        }
        else if([eventType isEqualToString:@"tagged"]){
            if (commonUtils.taggedData[index][@"get_images"]) {
                imagesArray = commonUtils.taggedData[index][@"get_images"];
            }
          
        }
        else if([eventType isEqualToString:@"calendar"]){
            //NSLog(@"section index %ld",(long)sectionIndex);
          //  NSLog(@"index is %ld",(long)index);
            if (commonUtils.calendarArray[sectionIndex][index][@"get_images"]) {
                 imagesArray = commonUtils.calendarArray[sectionIndex][index][@"get_images"];
            }
           
        }
        else if([eventType isEqualToString:@"events"]){
            //NSLog(@"section index %ld",(long)sectionIndex);
            //  NSLog(@"index is %ld",(long)index);
            if (commonUtils.activitiesArray[index][@"get_images"]) {
                 imagesArray = commonUtils.activitiesArray[index][@"get_images"];
            }
           
        }
        else if ([eventType isEqualToString:@"notification"]){
            if (commonUtils.notificationsArray[index][@"get_ice"][@"get_images"]) {
                 imagesArray = commonUtils.notificationsArray[index][@"get_ice"][@"get_images"];
            }
           
        }
        else if ([eventType isEqualToString:@"alert"]){
            if (commonUtils.alertDic[@"get_images"]) {
              imagesArray = commonUtils.alertDic[@"get_images"];
            }
            
        }

        
      //  NSLog(@"array is %@",commonUtils.liveEventsArray[index]);
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
}
#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
 
    NSLog(@"count is %lu",(unsigned long)imagesArray.count);
return  imagesArray.count;
  
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
     NSString *imageUrl;
    
    
    UIButton *btnVideo = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    btnVideo.backgroundColor = [UIColor clearColor];
    btnVideo.center = CGPointMake(187.0, 110.0);
    [btnVideo setImage:[UIImage imageNamed:@"video-play"] forState:UIControlStateNormal];
    btnVideo.tag = indexPath.row + 1000;
    
     [btnVideo addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.videoPlayBtn.tag = indexPath.row + 1000;

    [cell.videoPlayBtn addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
    if ([imagesArray[indexPath.row][@"type"]isEqualToString:@"image"]) {
        imageUrl  = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,imagesArray[indexPath.row][@"image"]];
        NSLog(@"Waseem  ===> %@",imageUrl);
    
        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        cell.videoPlayBtn.hidden = YES;
        
        
        bool isfound = false;
        for (UIView *i in cell.subviews){
            NSLog(@"tag Inner ===>  %ld",(long)i.tag);
            if([i isKindOfClass:[UIButton class]]){
                UIButton *newLbl = (UIButton *)i;
                if(newLbl.tag >=  1000){
                    /// Write your code
                    isfound = true;
                }
            }
        }
        
        if (!isfound){
            btnVideo.hidden = true;
            [cell addSubview:btnVideo];
        }

        
        
//                UIButton *button = (UIButton *)[cell.contentView viewWithTag:indexPath.row * 1000];
//
//                button.hidden = true;
//
        
        
        
    }
    else{
        
//        UIButton *button = (UIButton *)[self.view viewWithTag:indexPath.row * -1];
//
//        button.hidden = false;
        
        imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,imagesArray[indexPath.row][@"poster"]];
        NSLog(@"Waseem Video ===> %@",imageUrl);

        [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        cell.videoPlayBtn.center = CGPointMake(187.0, 110.0);
        NSLog(@"x === %f",cell.videoPlayBtn.center.x);
        NSLog(@"y === %f",cell.videoPlayBtn.center.y);
        cell.videoPlayBtn.hidden = YES;
        
        bool isfound = false;
        for (UIView *i in cell.subviews){
            NSLog(@"tag Inner ===>  %ld",(long)i.tag);
            if([i isKindOfClass:[UIButton class]]){
                UIButton *newLbl = (UIButton *)i;
                if(newLbl.tag >= 1000){
                    /// Write your code
                    isfound = true;
                }
            }
        }
        
        if (!isfound){
            [cell addSubview:btnVideo];
        }
        
    }
    
    cell.eventImageSelectBtn.tag = indexPath.item;
    [cell.eventImageSelectBtn addTarget:self action:@selector(onSelectEventImageView:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
#pragma mark - PlayVideo
- (void)onPlayVideo:(UIButton*) sender{


    NSURL *videoUrl;
    
    videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[sender.tag - 1000 ][@"image"]]];
    
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}
- (void)onSelectEventImageView:(UIButton*) sender
{
    if(!isClick){
        isClick = true ;
        [self.view endEditing:YES];
        PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
        
        vc.currentPageNum = sender.tag;
        vc.collectionViewTag = index;
        vc.comingFrom = comingFrom;
        vc.eventType = eventType;
        vc.sectionIndex = sectionIndex;
        
        [self.navigationController pushViewController:vc animated:YES];
 
    }
}


#pragma mark - Show Comment VC
- (void) onShowCommentVC:(UIButton*) sender
{
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentVC"];
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}

#pragma mark - HWViewPagerDelegate
-(void)pagerDidSelectedPage:(NSInteger)selectedPage{
    NSLog(@"FistViewController, SelectedPage : %d",(int)selectedPage);
}
#pragma mark - VideoPlay
- (void)videoPlay:(UIButton*)sender {
    NSURL *videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[sender.tag][@"image"]]];
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}
@end
