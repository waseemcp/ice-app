//
//  FollowersVC.m
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "FollowersVC.h"
#import "FollowTVCell.h"

@interface FollowersVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *followersArray;
    UIRefreshControl *refreshControl;
}


@end

@implementation FollowersVC
@synthesize userID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"followerVC"
                                               object:nil];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor lightGrayColor];
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [mainTV addSubview:refreshControl];
    [mainTV sendSubviewToBack:refreshControl];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
     [refreshControl endRefreshing];
    [self getFollowing:true];
}

- (void)viewWillDisappear:(BOOL)animated {
    AppDelegate *appd = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
//    int count = 0;
//    for (int indexMain = 0 ; indexMain < followersArray.count ; indexMain ++){
//        if ([followersArray[indexMain][@"is_following"]boolValue]){
//            count = count + 1;
//        }
//    }
    
//    appd.strFollowerCount = [NSString stringWithFormat:@"%d",(int)followersArray.count];
    
    if (followersArray.count > 0){
        appd.strFollowerCount = [NSString stringWithFormat:@"%d",(int)followersArray.count];
    }
    
}
-(void)refreshData:(UIRefreshControl*)refreshControl{
   
    [self getFollowing:false];
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return followersArray.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FollowTVCell *cell = (FollowTVCell*) [tableView dequeueReusableCellWithIdentifier:@"FollowTVCell"];
    if (cell == nil) {
        cell = [[FollowTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"FollowTVCell"];
    }
    cell.userNameLbl.text = [NSString stringWithFormat:@"%@",followersArray[indexPath.row][@"get_follower"][@"first_name"]];
    NSLog(@"username from api %@",followersArray[indexPath.row][@"get_follower"][@"first_name"]);
    NSLog(@"cell user name is %@",cell.userNameLbl.text);
    NSString *imageUrl = [NSString stringWithFormat:@"%@",followersArray[indexPath.row][@"get_follower"][@"photo"]];
    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                   placeholderImage:[UIImage imageNamed:@"user_avatar"]
                            options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    NSMutableArray *followerArray = [[NSMutableArray alloc]initWithArray:followersArray[indexPath.row][@"get_follower"][@"follow"]];
    NSMutableArray *followingArrayFromApi = [[NSMutableArray alloc]initWithArray:followersArray[indexPath.row][@"get_follower"][@"following"]];
    NSString *iceCount = [NSString stringWithFormat:@"%@",followersArray[indexPath.row][@"get_ice_count"]];
    cell.followersCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)followerArray.count];
    cell.followingCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)followingArrayFromApi.count];
    BOOL isFollowing = [followersArray[indexPath.row][@"is_following"]boolValue];
    if (isFollowing) {
        cell.userAddImg.image = [UIImage imageNamed:@"follow-check"];
    }
    else {
        cell.userAddImg.image = [UIImage imageNamed:@"follow-plus"];
    }
    
    if ([[commonUtils.userData valueForKey:@"id"] intValue] == [commonUtils.commutilUserID intValue]){
        
        cell.userAddImg.hidden = false;
        cell.userAddBtn.hidden = false;
        
        
        cell.userAddBtn.tag = indexPath.row;
        [cell.userAddBtn addTarget:self action:@selector(followUnfollow:) forControlEvents:UIControlEventTouchUpInside];
        
    }else {
        cell.userAddImg.hidden = true;
        cell.userAddBtn.hidden = true;
        
    }
    cell.eventsCountLbl.text = iceCount;
    return cell;
}
-(void)getFollowing:(BOOL)isShowLoader{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [mainTV layoutIfNeeded];
            [refreshControl endRefreshing];
            
        });
        
        return;
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^(void){

            if (isShowLoader){
                [commonUtils showHud:self.view];
            }
        });
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"%@get_follow_following/%@?time_zone=%@",ServerUrl,commonUtils.commutilUserID,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
       [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
           if(error!=nil)
           {
               NSLog(@"web service error:%@",error);
               dispatch_async(dispatch_get_main_queue(), ^(void){
//                   [MBProgressHUD hideHUDForView:self.view animated:YES];
                   [commonUtils hideHud];
//                   [commonUtils SetWindow:true];
               });
           }
           else
           {
               if(data !=nil)
               {
                   NSError *Jerror = nil;
                   
                   NSDictionary* json =[NSJSONSerialization
                                        JSONObjectWithData:data
                                        options:kNilOptions
                                        error:&Jerror];
                   //   NSLog(@"user data is %@",json);
                   if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                       if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                           [[NSNotificationCenter defaultCenter]
                            postNotificationName:@"Logout"
                            object:self];
                           
                           
                       }
                   }else
                   if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                       
                       NSMutableDictionary *successDic = [NSMutableDictionary new];
                       followersArray = [[NSMutableArray alloc]init];
                       successDic = [json[@"successData"]mutableCopy];
                       followersArray = [successDic[@"followers"]mutableCopy];
                       NSMutableArray  *followersArrayCopy = [followersArray mutableCopy];
                       
                       
                       NSMutableArray *followersArrayCopy2 = [NSMutableArray new];
                       
                       for(int index= 0 ; index < followersArrayCopy.count ; index++){
                           NSMutableDictionary *dataDict = [NSMutableDictionary dictionaryWithDictionary:followersArrayCopy[index]] ;
                           
                           [followersArrayCopy2 addObject:dataDict];
                       }
                       
                       
                       //  NSLog(@"liveEventsCopy %@",liveEventsCopy );
                       
                       
                       //
                       
                       
                       followersArray= [[NSMutableArray alloc] initWithArray:followersArrayCopy2];
                          [mainTV reloadData];
                       dispatch_async(dispatch_get_main_queue(), ^(void){
//                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                           [commonUtils hideHud];
//                           [commonUtils SetWindow:true];
                           [self performSelector:@selector(stopRefreshAndReload) withObject:nil afterDelay:1.0];
                           //Run UI Updates
                       });
                       
                       
                       
                   }
                   if(Jerror!=nil)
                   {
                       dispatch_async(dispatch_get_main_queue(), ^(void){
//                           [MBProgressHUD hideHUDForView:self.view animated:YES];
                           [commonUtils hideHud];
//                           [commonUtils SetWindow:true];
                       });
                       // NSLog(@"json error:%@",Jerror);
                   }
               }
           }
       }];

     
    }
    
}
-(void)followUnfollow:(UIButton*)sender{
     BOOL isFollowing = [followersArray[sender.tag][@"is_following"]boolValue];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    FollowTVCell *cell = [mainTV cellForRowAtIndexPath:indexPath];
    
    
    AppDelegate *appd = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    int countMain = [appd.strFollowingCount intValue];
    
    NSLog(@"countMain %d",countMain);
    if (!isFollowing) {
        [followersArray[sender.tag] setValue:@"1" forKey:@"is_following"];
        cell.userAddImg.image = [UIImage imageNamed:@"follow-check"];
        NSString *otherUserID = [NSString stringWithFormat:@"%@",followersArray[sender.tag][@"get_follower"][@"id"]];
        NSLog(@"user id %@",otherUserID);
        countMain = countMain + 1;
        [self FollowUnfollowAPI:otherUserID AddOrRemove:@"add_follower"];
        
    }
    else{
         [followersArray[sender.tag] setValue:@"0" forKey:@"is_following"];
        cell.userAddImg.image = [UIImage imageNamed:@"follow-plus"];
        NSString *otherUserID = [NSString stringWithFormat:@"%@",followersArray[sender.tag][@"get_follower"][@"id"]];
        NSLog(@"user id %@",otherUserID);
        countMain = countMain - 1;
        [self FollowUnfollowAPI:otherUserID AddOrRemove:@"un_follower"];
    }
    
    appd.strFollowingCount = [NSString stringWithFormat:@"%d",(int)countMain];
   
}

-(void)FollowUnfollowAPI:(NSString*)followedID AddOrRemove:(NSString*)addOrRemove{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [mainTV layoutIfNeeded];
            [refreshControl endRefreshing];
            [self performSelector:@selector(showInternetError) withObject:nil afterDelay:0.5];
        });
        
        return;
    }
    else {
        //there-is-no-connection warning
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:followedID forKey:@"followed_id"];
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *serverUrl = [NSString stringWithFormat:@"%@%@",ServerUrl,addOrRemove];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:serverUrl];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        
        
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                              if([statusis isEqualToString:@"success"]){
                                                  [[NSNotificationCenter defaultCenter]
                                                   postNotificationName:@"followingVC"
                                                   object:self];
                                                  //[mainTV reloadData];
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              NSLog(@"Server not responding");
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  
                                              });
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
}
- (void) reloadTable:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"followerVC"]){
        NSLog (@"Successfully received the test notification!");
        [self getFollowing:true];
    }
    
}
-(void)stopRefreshAndReload{
    dispatch_async(dispatch_get_main_queue(), ^(void){
  
[mainTV layoutIfNeeded];
[refreshControl endRefreshing];
    [mainTV reloadData];
           });
}
-(void)showInternetError{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    });
}
@end
