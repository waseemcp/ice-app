//
//  SearchMapVC.h
//  ICE
//
//  Created by LandToSky on 1/11/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
@interface SearchMapVC : BaseViewController

-(void)ReloadData :(NSString *)SearchText;
@property (weak, nonatomic) IBOutlet UILabel *radiusLbl;
- (IBAction)sliderValueChanged:(id)sender;
@end
