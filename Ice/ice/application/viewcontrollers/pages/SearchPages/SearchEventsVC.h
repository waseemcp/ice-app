//
//  SearchEventsVC.h
//  ICE
//
//  Created by LandToSky on 1/11/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchEventsVC : UIViewController

- (void)ReloadData :(NSString *)SearchText;
@end
