//
//  locationPickerViewController.m
//  ICE
//
//  Created by Vengile on 19/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "locationPickerViewController.h"
#import <MapKit/MapKit.h>
#import "ICEVC.h"
#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocomplete.h>
#import "CLPlacemark+HNKAdditions.h"
#import "APTimeZones.h"
static NSString *const kHNKDemoSearchResultsCellIdentifier = @"HNKDemoSearchResultsCellIdentifier";
@interface locationPickerViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
 @property (strong, nonatomic) HNKGooglePlacesAutocompleteQuery *searchQuery;

@end

@implementation locationPickerViewController{
    CLLocationCoordinate2D touchMapCoordinate;
    BOOL locationPicked;
    NSString *searchedAddress;
    CLLocationManager *locationManager;
    NSString *formattedAddressOfPlace;
    BOOL shouldDeSelect;
 
   
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.delegate = self;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
    self.searchBar.searchBarStyle = UISearchBarStyleMinimal;

   // [self.view addSubview:subView];
    
    // When UISearchController presents the results view, present it in
    // this view controller, not one further up the chain.
    self.definesPresentationContext = YES;
//    [self initGmap];
//    _gmapView.delegate = self;
//     _locationPickerMap.delegate = self;
//    
//    _locationPickerMap.showsUserLocation = YES;
//
//    
//    CLLocation *previousLoc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude];
//    NSLog(@"previous loc is %f",previousLoc.coordinate.latitude);
//    if (previousLoc.coordinate.latitude != 0.000000) {
//        [self dropPreviousPin];
//        locationPicked = YES;
//    }
//    else{
//        locationPicked = NO;
//    }



}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
  
    if (!CGPointEqualToPoint(commonUtils.userPickedCord, CGPointZero)){
       
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
 
    
}
#pragma mark - Gesture Recognizer

// Gesture recognizer method

#pragma mark - Map Methods
- (void)removeAllPinsButUserLocation
{
    id userLocation = _locationPickerMap.userLocation;
    NSMutableArray *pins = [[NSMutableArray alloc] initWithArray:[_locationPickerMap annotations]];
    if ( userLocation != nil ) {
        [pins removeObject:userLocation]; // avoid removing user location off the map
    }
    
    [_locationPickerMap removeAnnotations:pins];
  
    pins = nil;
}
-(void)setInitialMapZoom
{
    NSLog(@"Did update user location");
    MKCoordinateRegion mapRegion;
    mapRegion.center = _locationPickerMap.userLocation.coordinate;
    mapRegion.span.latitudeDelta = 0.2;
    mapRegion.span.longitudeDelta = 0.2;
    
    [_locationPickerMap setRegion:mapRegion animated: YES];
}

- (IBAction)locationPickedBtn:(id)sender {
    if (!locationPicked) {
        CLLocation *pickedloc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude];
    
    if (CLLocationCoordinate2DIsValid(touchMapCoordinate)&&!(touchMapCoordinate.latitude==0)&&!(touchMapCoordinate.longitude==0)) {
          locationPicked = YES;

        [self getAddressFromGoogle];
    }
        
       
 else if(pickedloc.coordinate.latitude != 0.000000 ) {
     [self locationPickedGoBack];
    }
 else {
       [commonUtils showAlert:@"Error!" withMessage:@"Please select ICE location"];
 }
    }
    else{

        [self getAddressFromGoogle];
     
    }
}
#pragma mapview methods
-(void)getAddressFromCoordinates{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:commonUtils.pickedLocationCord.latitude longitude:commonUtils.pickedLocationCord.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                      //  NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
                      ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
                      dispatch_async(dispatch_get_main_queue(), ^(void){
                          //Run UI Updates
                         vc.locationLbl.text = locatedAt;
                          [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
                          [self.navigationController popViewControllerAnimated:YES];
                      });
                     
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
              }
     ];
}
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
   }
-(void)dropPreviousPin{
    CLLocationCoordinate2D coordinate =CLLocationCoordinate2DMake(commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude);
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
    pickedLoc.title = @"Picked Location";
    // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;

}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
  CLLocation  *currentLocation = [locations lastObject];

    NSLog(@"gmap loc is %f",_gmapView.myLocation.coordinate.latitude);
    double currentlat = currentLocation.coordinate.latitude;
    double currentlng = currentLocation.coordinate.longitude;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:currentlat
                                                            longitude:currentlng
                                                                 zoom:15];
    
    [_gmapView animateToCameraPosition:camera];
    [locationManager stopUpdatingLocation];

}

#pragma map search methods
- (IBAction)onLaunchClicked:(id)sender {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}
// Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];
    //NSLog(@"place dic %@",place);
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    NSLog(@"place lat is %f",place.coordinate.latitude);
    NSLog(@"place lng is %f",place.coordinate.longitude);
    searchedAddress = place.formattedAddress;
    
    CLLocationCoordinate2D center = place.coordinate;
   // center.latitude -= self.mapView.region.span.latitudeDelta * 0.40;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:place.coordinate.latitude
                                                            longitude:place.coordinate.longitude
                                                                 zoom:15];
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:place.coordinate];
    pickedLoc.title = place.formattedAddress;
    // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;
  commonUtils.pickedLocationCord = place.coordinate;
    [_gmapView animateToCameraPosition:camera];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)zoomInToMyLocation:(CLLocationCoordinate2D)center
{
    MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
    region.center.latitude = center.latitude;
    region.center.longitude = center.longitude;
    region.span.longitudeDelta = 0.02f;
    region.span.latitudeDelta = 0.02f;
    [self.locationPickerMap setRegion:region animated:YES];
      [self removeAllPinsButUserLocation];
    MKPointAnnotation *annot = [[MKPointAnnotation alloc] init];
    annot.coordinate = center;
    [self.locationPickerMap addAnnotation:annot];
    commonUtils.pickedLocationCord = center;
    locationPicked = NO;
    

}
-(void)locationPickedGoBack{
    NSString *locatedAt = searchedAddress;
    
    
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        //Run UI Updates
        vc.locationLbl.text = locatedAt;
        [commonUtils showAlert:@"Success" withMessage:@"ICE location is set"];
        [self.navigationController popViewControllerAnimated:YES];
         });
}
-(void)getAddressFromGoogle{
 
    //NSLog(@"App id is %@",AppUtility.deviceId);
    NSString * authenticate_url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=true/false",commonUtils.pickedLocationCord.latitude,commonUtils.pickedLocationCord.longitude];
    NSLog(@"url string : %@",authenticate_url);
    // NSLog(@"client id here is %@",_clienti);
    NSData * webData = [NSData dataWithContentsOfURL:[NSURL URLWithString:authenticate_url]];
    NSLog(@"come here check2");
    if(webData == nil)
    {
        NSLog(@"No data Found");
    
        
        return;
    }
    else
    {
        
        NSString * responseString = [[NSString alloc] initWithData:webData encoding:NSUTF8StringEncoding];
        NSLog(@"response string %@",responseString);
        NSError * error;
        NSDictionary *json =
        [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding:NSUTF8StringEncoding]
                                        options: NSJSONReadingMutableContainers
                                          error: &error];
        NSLog(@"%@",json);
        NSString *formattedAddress = [NSString stringWithFormat:@"%@",json[@"results"][0][@"formatted_address"]];
        NSLog(@"address is %@",formattedAddress);
        formattedAddressOfPlace = formattedAddress;
        [self showlocationPickedAlert];
        //dispatch_async(dispatch_get_main_queue(), ^(void){
            //Run UI Updates
        
       // });

    }
}
-(void)initGmap{
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    // Create a GMSCameraPosition that tells the map to display the
    _gmapView.myLocationEnabled = YES;
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    [locationManager startUpdatingLocation];
  
}
- (void)mapView:(GMSMapView *)mapView
didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [_gmapView clear];
    GMSMarker *pickedLoc = [GMSMarker markerWithPosition:coordinate];
    pickedLoc.title = @"Picked Location";
    // london.icon = [UIImage imageNamed:@"house"];
    pickedLoc.map = _gmapView;
    touchMapCoordinate = coordinate;
    commonUtils.pickedLocationCord = coordinate;

}
-(void)showlocationPickedAlert{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Success"
                                                                  message:@"ICE location is set"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
                                    ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];

                                    vc.locationLbl.text = formattedAddressOfPlace;
                                    [self.navigationController popViewControllerAnimated:YES];
                                    // call method whatever u need
                                }];
    
  
    
    [alert addAction:yesButton];

    
    [self presentViewController:alert animated:YES completion:nil];

}
// Handle the user's selection.
#pragma mark - UITableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.searchResults.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHNKDemoSearchResultsCellIdentifier forIndexPath:indexPath];
    
    HNKGooglePlacesAutocompletePlace *thisPlace = self.searchResults[indexPath.row];
    cell.textLabel.text = thisPlace.name;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark - UITableView Delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%d",shouldDeSelect);
    if (shouldDeSelect) {
//      [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
        // tableView deselectRow(at: indexPath, animated: true)
    }
    else{
        shouldDeSelect = YES;
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        cell.backgroundColor = [UIColor lightGrayColor];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar resignFirstResponder];
    
    HNKGooglePlacesAutocompletePlace *selectedPlace = self.searchResults[indexPath.row];
    NSString *selectedPlaceAddress = selectedPlace.name;
    [CLPlacemark hnk_placemarkFromGooglePlace: selectedPlace
                                       apiKey: self.searchQuery.apiKey
                                   completion:^(CLPlacemark *placemark, NSString *addressString, NSError *error) {
                                       if (placemark) {
                                           CLLocation *location = [[CLLocation alloc] initWithLatitude:placemark.location.coordinate.latitude
                                                                                             longitude:placemark.location.coordinate.longitude];
                                           
                                           NSString *streetAddress = [NSString stringWithFormat:@"%@",placemark.name];
                                           NSLog(@"%f  %f", location.coordinate.latitude,location.coordinate.longitude);


                                           [self.tableView setHidden: YES];
                                           NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
                                           ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];
                                           commonUtils.pickedLocationCord = placemark.location.coordinate;
                                           vc.locationLbl.text = [NSString stringWithFormat:@"%@ %@",selectedPlaceAddress,streetAddress];
                                           vc.shouldNotClear = YES;
                                           shouldDeSelect = NO;
                                            [self.navigationController popViewControllerAnimated:YES];
//                                           [self addPlacemarkAnnotationToMap:placemark addressString:addressString];
//                                           [self recenterMapToPlacemark:placemark];
                                         //  [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
                                          
                                       }else {
                                           [commonUtils showAlert:@"Error!" withMessage:@"Please check your internet connection."];
                                       }
                                   }];
    }
}

#pragma mark - UISearchBar Delegate

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if(searchText.length > 0)
    {
        [self.tableView setHidden:NO];
        
        [self.searchQuery fetchPlacesForSearchQuery: searchText
                                         completion:^(NSArray *places, NSError *error) {
                                             if (error) {
                                                 NSLog(@"ERROR: %@", error);
                                                 [self handleSearchError:error];
                                             } else {
                                                 self.searchResults = places;
                                                 [self.tableView reloadData];
                                             }
                                         }];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    searchBar.text = @"";
    [searchBar setShowsCancelButton:NO animated:YES];
    [searchBar resignFirstResponder];
    [self.tableView setHidden:YES];
}
- (void)handleSearchError:(NSError *)error
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error"
                                                                   message:error.localizedDescription
                                                            preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)locationPickerBtn:(id)sender {
}

- (IBAction)backBtn:(id)sender {
    NSInteger numberOfViewControllers = self.navigationController.viewControllers.count;
    ICEVC *vc  = self.navigationController.viewControllers[numberOfViewControllers-2];

    vc.shouldNotClear = YES;
    [self.navigationController popViewControllerAnimated:YES];

}
@end
