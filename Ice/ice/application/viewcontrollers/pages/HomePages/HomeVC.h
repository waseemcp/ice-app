//
//  HomeVC.h
//  ICE
//
//  Created by LandToSky on 11/12/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeftMenuVC.h"
#import "MyProfileEditVC.h"
@interface HomeVC : BaseViewController
 @property(nonatomic,strong)   IBOutlet UIScrollView *tabScrollView;
- (void)onShowTabView:(NSInteger) index;
@property(nonatomic) LeftMenuVC * menuVC;
@property (weak, nonatomic) IBOutlet UILabel *notificationCountLbl;

@end
