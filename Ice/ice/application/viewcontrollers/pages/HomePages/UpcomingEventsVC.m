//
//  UpcomingEventsVC.m
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "UpcomingEventsVC.h"
#import "IceTVCell.h"
#import "EventImageCVCell.h"
#import <HWViewPager.h>
#import "UserProfileVC.h"
#import "ICEVC.h"
#import "DetailVC.h"
#import "CommentVC.h"
#import "PhotoVideoShowVC.h"
#import "ICEVC.h"
#import <SVProgressHUD.h>
@interface UpcomingEventsVC ()< UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, HWViewPagerDelegate>
{
    IBOutlet UITableView *table;
    NSMutableArray *upnextArrayCopy;
    NSString *zone;
//    NSTimer *t3;
    BOOL isClick;
    BOOL isDetailShown;
    UIRefreshControl *refreshControl;
    BOOL shouldNotRefresh;
    
    BOOL isReloadAgain;
    BOOL isRefreshColelctionView ;
    NSMutableArray *oldArray;
    BOOL firstTime;
}

@end

@implementation UpcomingEventsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    oldArray = [NSMutableArray new];
    firstTime = true;
//    t3 = [NSTimer scheduledTimerWithTimeInterval: 60.0
//                                         target: self
//                                       selector:@selector(setTimeAgo:)
//                                       userInfo: nil repeats:YES];
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.backgroundColor = [UIColor whiteColor];
    refreshControl.tintColor = [UIColor lightGrayColor];

    isReloadAgain = true;
    
    [refreshControl addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [table addSubview:refreshControl];
    [table sendSubviewToBack:refreshControl];
    
    
    [self initUI];
    [self initData];
   
}

- (void)initUI
{
    table.contentInset = UIEdgeInsetsMake(0.0, 0.0, 70.0, 0.0);
    [table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
}

- (void)initData
{
    zone = @"PST";
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self EnableMenu];
    isClick = NO;
    isDetailShown = NO;
    if (shouldNotRefresh) {
        [self scrollToLatest];
    }
    [refreshControl endRefreshing];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"upcoming"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopScroll:)
                                                 name:@"stopScroll"
                                               object:nil];

    if (!shouldNotRefresh) {
        [self getDashBoard];
    }
  
    shouldNotRefresh = NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
     NSLog(@"upcoming dissapear");
}
#pragma mark - UITableView
#pragma mark - UITableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return 10;
    NSLog(@"upnext array is %@",commonUtils.upNextEventsArray);
    return commonUtils.upNextEventsArray.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger numOfSections = 0;
    if (commonUtils.upNextEventsArray.count>0)
    {
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        numOfSections                = 1;
        table.backgroundView = nil;
    }
    else
    {
       
    }
    
    return numOfSections;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    return tableCellHeight0;
   // return 87+62+([[UIScreen mainScreen]bounds].size.width);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"IceTVCell";
    IceTVCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
    NSLog(@"upnextarray here is %@",upnextArrayCopy);
    if (commonUtils.upNextEventsArray.count>0) {
        
   
     NSMutableDictionary *mainDict = [[NSMutableDictionary alloc]initWithDictionary:commonUtils.upNextEventsArray[indexPath.row]] ;
    NSMutableArray *getCommentsArray = commonUtils.upNextEventsArray[indexPath.row][@"get_comments"];
    NSMutableArray *getLikesArray = commonUtils.upNextEventsArray[indexPath.row][@"get_likes"];
  
    cell.topTitleLbl.text = commonUtils.upNextEventsArray[indexPath.row][@"title"];
    cell.topLocationLbl.text = commonUtils.upNextEventsArray[indexPath.row][@"complete_address"];
    cell.membersCountLbl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"get_members_count"]];
    cell.commentsCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)getCommentsArray.count];
    cell.likesCountLbl.text =  [NSString stringWithFormat:@"%lu",(unsigned long)getLikesArray.count];
    cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"event_day"]];
    cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"event_day_start"]];
    cell.eventMonthLBl.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"event_month"]];
    NSString *imageUrlStr =  [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"get_user"][@"photo"]];
          [cell.iceUserImg sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]];
    cell.iceUserName.text = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[indexPath.row][@"get_user"][@"first_name"]];
    
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDateFormatter *serverDateFormatter = [NSDateFormatter new];
    [serverDateFormatter setDateFormat:@"yyyy-M-dd"];
    NSDate * endDateFromApi = [startTimeformatter dateFromString:mainDict[@"end_date"]];
    NSDate * startDateFromApi = [startTimeformatter dateFromString:mainDict[@"start_date"]];
    
    NSString *serverStartTime = [serverDateFormatter stringFromDate:startDateFromApi];
    NSString *serverEndTime = [serverDateFormatter stringFromDate:endDateFromApi];
        
        NSString *eventStartTime = [NSString stringWithFormat:@"%@",mainDict[@"event_start_time"]];
        NSString *eventEndTime = [NSString stringWithFormat:@"%@",mainDict[@"event_end_time"]];
        
    if ([serverStartTime isEqualToString:serverEndTime]) {
        if ([commonUtils is24hourFormat]) {
             cell.topTimerLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
        }
        else{
           cell.topTimerLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
        }
    }
    
    else{
        NSDate* date1 = startDateFromApi;
        NSDate* date2 = endDateFromApi;
        NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
        double secondsInAnHour = 3600;
        NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
        if (hoursBetweenDates>24) {
            if ([commonUtils is24hourFormat]) {
                cell.topTimerLbl.text = [NSString stringWithFormat:@"begins %@ %@",[commonUtils convertTo24Hour:eventStartTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                cell.topTimerLbl.text = [NSString stringWithFormat:@"begins %@ %@",eventStartTime,commonUtils.getTimeZoneAbbrevation];
            }
        }
        else{
            if ([commonUtils is24hourFormat]) {
                cell.topTimerLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                cell.topTimerLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
        }
       
    }
    
      if (![commonUtils.upNextEventsArray[indexPath.row][@"timeago"] isKindOfClass:[NSNull class]] && commonUtils.upNextEventsArray[indexPath.row][@"timeago"] != nil) {
          cell.iceTimeAgo.text = [NSString stringWithFormat:@"iced %@",commonUtils.upNextEventsArray[indexPath.row][@"timeago"]];
      }
      else{
            cell.iceTimeAgo.text = @"Just Now";
      }
    if (cell == nil) {
        cell = [[IceTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    

    cell.eventImageCv.pagerDelegate = self;
    cell.eventImageCv.dataSource = self;
    cell.eventImageCv.tag = indexPath.row;
     [cell.eventImageCv layoutIfNeeded];
        if (isRefreshColelctionView) {
            [cell.eventImageCv reloadData];
        }

  
    cell.goDetailBtn.tag = indexPath.row;
    [cell.goDetailBtn addTarget:self action:@selector(onShowDetailVC:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.showCommentBtn.tag = indexPath.row;
    [cell.showCommentBtn addTarget:self action:@selector(onShowCommentVC:) forControlEvents:UIControlEventTouchUpInside];
    cell.iceLikeBtn.tag = indexPath.row;
    [cell.iceLikeBtn addTarget:self action:@selector(onLikeIce:) forControlEvents:UIControlEventTouchUpInside];
    cell.userProfileBtn.tag = indexPath.row;
    [cell.userProfileBtn addTarget:self action:@selector(onUserProfile:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.userAddEventBtn.tag = indexPath.row;
        [cell.userAddEventBtn addTarget:self action:@selector(onUserAddEvent:) forControlEvents:UIControlEventTouchUpInside];
        
        
    BOOL is_Like = [commonUtils.upNextEventsArray[indexPath.row][@"is_like_count"]boolValue];
    if (is_Like) {
        cell.likesCountLbl.textColor = [UIColor whiteColor];
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon3"];
    }
    else{
        cell.likesCountLbl.textColor = [UIColor redColor];
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon2"];
    }
   
  [cell layoutIfNeeded];
    }
    else{
        [tableView reloadData];
    }
    return cell;
   
}

#pragma mark - ColelctionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSMutableArray *getImages =commonUtils.upNextEventsArray[collectionView.tag][@"get_images"];
    return getImages.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
  
    NSMutableArray *imagesArray = commonUtils.upNextEventsArray[collectionView.tag][@"get_images"];
    
    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
    
    cell.videoPlayBtn.tag = indexPath.row;
    [cell.videoPlayBtn setTitle:[NSString stringWithFormat:@"%ld",(long)collectionView.tag] forState:UIControlStateNormal];
    [cell.videoPlayBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [cell.videoPlayBtn addTarget:self action:@selector(onPlayVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.eventImageSelectBtn.tag = indexPath.item;
    [cell.eventImageSelectBtn setTitle:[NSString stringWithFormat:@"%ld",(long)collectionView.tag] forState:UIControlStateNormal];
    [cell.eventImageSelectBtn setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
    [cell.eventImageSelectBtn addTarget:self action:@selector(onSelectEventImageView:) forControlEvents:UIControlEventTouchUpInside];
    
  
    NSString *imageUrl;
    //    NSLog(@"data of 4th cell is %@",imagesArray);
    
    NSLog(@"==========>  %@",imagesArray[indexPath.row][@"type"]);
    
    cell.videoPlayBtn.hidden = YES;
    cell.ImgViewBG.hidden = true;
    cell.ImgViewBG.alpha = 0;
    UIImageView *newImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, collectionView.frame.size.width , collectionView.frame.size.height )];
    NSLog(@"imageview height is %d",(int)newImageView.frame.size.height);
     NSLog(@"imageview width is %d",(int)newImageView.frame.size.width);
    [newImageView setContentMode:UIViewContentModeScaleAspectFill];
    if ([imagesArray[indexPath.row][@"type"]isEqualToString:@"image"]) {
        imageUrl  = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,imagesArray[indexPath.row][@"image"]];
     
   
        [newImageView sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                        placeholderImage:[UIImage imageNamed:@"image0"]
                                 options:(SDWebImageContinueInBackground) ];
        

        NSLog(@"==========>  image");
        
        
        //        cell.videoPlayBtn.hidden = YES;
        
        
        newImageView.tag= 95;
         UIImageView *previousImageView = [cell viewWithTag:95];
        if (previousImageView) {
            [previousImageView removeFromSuperview];
        }
        
        
        
        [cell addSubview:newImageView];
        [cell bringSubviewToFront:newImageView];
       // [cell bringSubviewToFront:cell.eventImageSelectBtn];
        cell.eventIv.hidden = NO;
        cell.video_player_view.hidden = YES;
        cell.mainNode = [ASDisplayNode new];
        cell.videoNode = [ASVideoNode new];
        cell.videoNode.asset = nil;
        [cell.mainNode.view removeFromSuperview];
        
        
    }
    else{
        NSLog(@"==========>  Video");
        NSURL  *videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[indexPath.row][@"image"]]];
        imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,imagesArray[indexPath.row][@"poster"]];
        NSLog(@"video url is %@",videoUrl);
        
        UIImageView *previousImageView = [cell viewWithTag:95];
        [previousImageView removeFromSuperview];
        
        cell.videoPlayBtn.hidden = NO;
        // if (![tagsArray containsObject:bothTags]) {
     
        //  [tagsArray addObject:bothTags];
        [cell.videoNode removeFromSupernode];
        [cell.mainNode.view removeFromSuperview];
        
        cell.mainNode= [ASDisplayNode new];
        //        cell.videoNode.asset = nil;
        cell.videoNode = [ASVideoNode new];
        cell.videoNode.delegate = self;
        cell.videoNode.asset = [AVAsset assetWithURL:videoUrl];
        cell.videoNode.frame = collectionView.frame;
        
        cell.videoNode.gravity = AVLayerVideoGravityResizeAspectFill;
        cell.videoNode.shouldAutoplay = NO;
        cell.videoNode.shouldAutorepeat = YES;
        //videoNode.shouldAggressivelyRecoverFromStall = YES;
        
        
        cell.videoNode.URL = [NSURL URLWithString:imageUrl];
        cell.videoNode.muted = YES;
        
        [cell.mainNode addSubnode:cell.videoNode];
        cell.eventIv.hidden = YES;
        [cell.video_player_view addSubview:cell.mainNode.view];
        UIView *videoPlayerView = [[UIView alloc]initWithFrame:collectionView.frame];
        videoPlayerView.tag = 1000;
        
        videoPlayerView.backgroundColor = [UIColor clearColor];
        
        
        
        
        
        
        
        
        
        [cell.ImgViewBG sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                          placeholderImage:[UIImage imageNamed:@"image0"]
                                   options:(SDWebImageContinueInBackground) ];
        //                cell.videoPlayBtn.hidden = YES;
        cell.video_player_view.hidden = NO;
        cell.video_player_view.backgroundColor = [UIColor clearColor];
        
    
        
        
    }
    
    
    [cell layoutIfNeeded];
    return cell;
}

#pragma mark- cell button methods
- (void) onShowDetailVC:(UIButton*) sender
{
    if (!isDetailShown) {
        isDetailShown = YES;
 
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.eventType = @"upcoming";
    vc.comingFrom = @"upcoming";
    vc.selectedTag = sender.tag;
    vc.iceDetails = commonUtils.upNextEventsArray[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
           }
}
-(void) onLikeIce:(UIButton*)sender{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    IceTVCell *cell = [table cellForRowAtIndexPath:indexPath];
    BOOL is_Like = [commonUtils.upNextEventsArray[sender.tag][@"is_like_count"]boolValue];
    if (is_Like) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *likesCount = [f numberFromString:cell.likesCountLbl.text];
        NSInteger likes = [likesCount integerValue];
        if (likes>0) {
            likes--;
        }
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon2"];
        cell.likesCountLbl.textColor = [UIColor redColor];
        cell.likesCountLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
        [upnextArrayCopy[sender.tag]setValue:@"0" forKey:@"is_like_count"];
        NSMutableArray *previousLikes = [NSMutableArray new];
        previousLikes = [upnextArrayCopy[sender.tag][@"get_likes"]mutableCopy];
        [previousLikes removeLastObject];
        [upnextArrayCopy[sender.tag]setValue:previousLikes forKey:@"get_likes"];
        commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:upnextArrayCopy copyItems:NO];
        [commonUtils unLikeICE:commonUtils.upNextEventsArray[sender.tag][@"id"] :cell];
    }
    else{
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *likesCount = [f numberFromString:cell.likesCountLbl.text];
        NSInteger likes = [likesCount integerValue];
        likes++;
        cell.likeIV.image = [UIImage imageNamed:@"card-count-icon3"];
        cell.likesCountLbl.textColor = [UIColor whiteColor];
        cell.likesCountLbl.text = [NSString stringWithFormat:@"%ld",(long)likes];
        [upnextArrayCopy[sender.tag]setValue:@"1" forKey:@"is_like_count"];
        NSMutableArray *previousLikes = [NSMutableArray new];
        previousLikes = [upnextArrayCopy[sender.tag][@"get_likes"]mutableCopy];
        NSMutableDictionary *user = [[NSMutableDictionary alloc]initWithObjectsAndKeys:commonUtils.userData[@"first_name"],@"first_name",commonUtils.userData[@"photo"],@"photo", nil];
        NSMutableDictionary *user2 = [[NSMutableDictionary alloc]initWithObjectsAndKeys:user,@"user", nil];
        [previousLikes addObject:user2];
        [upnextArrayCopy[sender.tag]setValue:previousLikes forKey:@"get_likes"];

        commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:upnextArrayCopy copyItems:NO];
        [commonUtils likeICE:commonUtils.upNextEventsArray[sender.tag][@"id"] :cell ];
    }
    
    
}
- (void) onShowCommentVC:(UIButton*) sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
    IceTVCell *cell = [table cellForRowAtIndexPath:indexPath];
    
    CommentVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentVC"];
    vc.iceDetailsArray = commonUtils.upNextEventsArray[sender.tag][@"get_comments"];
    NSString *iceID = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[sender.tag][@"id"]];
    vc.iceID = iceID;
    vc.passedTag = sender.tag;
    vc.providesPresentationContextTransitionStyle = YES;
    vc.definesPresentationContext = YES;
    vc.cell = cell;
    vc.comingfrom = @"upnext";
    [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:vc animated:YES completion:nil];
}
- (void)onSelectEventImageView:(UIButton*) sender
{
    if (!isClick) {
        
    isClick = YES;
    shouldNotRefresh = YES;
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
    NSNumber *CVTag = [f numberFromString:sender.titleLabel.text];
        
    commonUtils.collectionViewTag = [CVTag integerValue];
    vc.currentPageNum = sender.tag;
    vc.collectionViewTag = [CVTag integerValue];
    vc.eventType = @"upcoming";
    vc.comingFrom = @"upcoming";
    [self.navigationController pushViewController:vc animated:YES];
          }
}

#pragma mark - HWViewPagerDelegate
-(void)pagerDidSelectedPage:(NSInteger)selectedPage{
    //    NSLog(@"FistViewController, SelectedPage : %d",(int)selectedPage);
}
-(void)getDashBoard{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [table layoutIfNeeded];
            [refreshControl endRefreshing];
             [self performSelector:@selector(showInternetError) withObject:nil afterDelay:0.5];
        });
        
        return;
    }
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        
        
//        dispatch_async(dispatch_get_main_queue(), ^(void){
//            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        });

        NSString *serverUrl = [NSString stringWithFormat:@"%@get_up_next?time_zone=%@",ServerUrl,commonUtils.getTimeZone];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        
        UILabel *noDataLabel = (UILabel *)[table viewWithTag:-999];
        
        if (noDataLabel){
            [noDataLabel removeFromSuperview];
            noDataLabel.hidden = true;
        }
        
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

            
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
               //  NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
//                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [commonUtils hideHud];
                        NSDictionary *successDic = [json[@"successData"]mutableCopy];
                        
                        NSLog(@"success dic here is %@",successDic);
                        commonUtils.upNextEventsArray = [successDic[@"upnext"]mutableCopy];
                        upnextArrayCopy = [NSMutableArray new];
                        for (int i = 0; i<commonUtils.upNextEventsArray.count; i++) {
                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithDictionary:commonUtils.upNextEventsArray[i]];
                            [upnextArrayCopy addObject:dic];
                        }
                        if (oldArray.count == 0) {
                            oldArray = [[NSMutableArray alloc]initWithArray:upnextArrayCopy copyItems:YES];
                        }
                          [self performSelector:@selector(stopRefreshAndReload) withObject:nil afterDelay:1.0];
           
                        UILabel *noDataLabel = (UILabel *)[table viewWithTag:-999];
                        
                        if (noDataLabel){
                            [noDataLabel removeFromSuperview];
                            noDataLabel.hidden = true;
                        }
                        
                        if (commonUtils.upNextEventsArray.count==0) {
                            UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, table.bounds.size.width, table.bounds.size.height)];
                            noDataLabel.text             = @"No Upcoming ICE Available";
                            noDataLabel.textColor        = [UIColor blackColor];
                            noDataLabel.textAlignment    = NSTextAlignmentCenter;
                            noDataLabel.tag = -999;
                            table.backgroundView = noDataLabel;
                            table.separatorStyle = UITableViewCellSeparatorStyleNone;
                            
                        }else {
                            
                            UILabel *noDataLabel = (UILabel *)[table viewWithTag:-999];
                            
                            if (noDataLabel){
                                [noDataLabel removeFromSuperview];
                                noDataLabel.hidden = true;
                            }
                            
                            UIApplication *app = [UIApplication sharedApplication];
                            NSArray *eventArray = [app scheduledLocalNotifications];
                            for (int i=0; i<[eventArray count]; i++)
                            {
                                UILocalNotification* oneEvent = [eventArray objectAtIndex:i];
                                if ([oneEvent.alertBody isEqualToString:@"Call"])
                                {
                                    //Cancelling local notification
                                    [app cancelLocalNotification:oneEvent];
                                    break;
                                }
                            }
                            
                            
                            
                            NSDateFormatter *startTimeformatter = [NSDateFormatter new];
                            [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
                            NSDate * startDateFromApi = [startTimeformatter dateFromString:commonUtils.upNextEventsArray.firstObject[@"start_date"]];
                            
                            
//                            if (startDateFromApi){
//                                UILocalNotification* localNotification = [[UILocalNotification alloc] init];
//                                localNotification.fireDate = startDateFromApi;
//                                localNotification.alertBody = @"Call";
//                                localNotification.timeZone = [NSTimeZone defaultTimeZone];
//                                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
//                            }
                            
                          
                        }
                        
                    });
                    
                }
                else{

                    
                    UILabel *noDataLabel         = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, table.bounds.size.width, table.bounds.size.height)];
                    noDataLabel.text             = @"No Upcoming ICE Available";
                    noDataLabel.textColor        = [UIColor blackColor];
                    noDataLabel.textAlignment    = NSTextAlignmentCenter;
                    noDataLabel.tag = -999;
                    table.backgroundView = noDataLabel;
                    table.separatorStyle = UITableViewCellSeparatorStyleNone;
                }
                if(Jerror!=nil)
                {
                    NSLog(@"json error:%@",Jerror);
                }
            }
        }
        
          }];
//        dispatch_async(dispatch_get_main_queue(), ^(void){
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//        });
    });
   
}



- (void)onUserProfile:(UIButton*) sender{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = [NSString stringWithFormat:@"%@",commonUtils.upNextEventsArray[sender.tag][@"user_id"]];
    vc.activityIndex = sender.tag;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onUserAddEvent:(UIButton*) sender{
    
    
    NSMutableDictionary *eventdata = commonUtils.upNextEventsArray[sender.tag];
    
    NSLog(@"eventdata %@",eventdata);
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        
        [_params setObject:eventdata[@"id"] forKey:@"ice_id"];
        
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_ice_added"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        NSLog(@"addIceAdded %@",Url);
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSLog(@"sessionToken %@",commonUtils.sessionToken);
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              NSString* myString;
                                              NSLog(@"dictionary %@",dictionary);
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                          
                                                      });
                                                      
                                                  }else {
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils showAlert:@"" withMessage:[dictionary valueForKey:@"successMessage"]];
                                                          });
                                                  }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}


- (void) reloadTable:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    

    
    if ([[notification name] isEqualToString:@"upcoming"]){
        [self getDashBoard];
    }
}
- (void) stopScroll:(NSNotification *) notification
{
    [table setContentOffset:CGPointZero animated:YES];
    if ([refreshControl isRefreshing]) {
        [refreshControl endRefreshing];
        [table setContentOffset:CGPointZero animated:YES];
    }
}
-(IBAction)AddNewIce:(UIButton *)sender {
    ICEVC *newIce = [self.storyboard instantiateViewControllerWithIdentifier:@"ICEVC"];
    newIce.isshowMenu = true;
    [self.navigationController pushViewController:newIce animated:true];
}
#pragma mark - PlayVideo
- (void)onPlayVideo:(UIButton*) sender{
    NSInteger collectionTag = [sender.titleLabel.text integerValue];
    NSMutableArray *imagesArray = commonUtils.upNextEventsArray[collectionTag][@"get_images"];
    NSURL *videoUrl;
    
    videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,imagesArray[sender.tag][@"image"]]];
    
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}
//-(void)setTimeAgo:(NSTimer*)timer{
//    NSMutableArray  *liveEventsCopy3 = [commonUtils.upNextEventsArray mutableCopy];
//
//    NSMutableArray *liveEventsArrayCopy2 = [NSMutableArray new];
//     NSMutableArray *liveEventsArrayCopy4 = [NSMutableArray new];
//    for (int i = 0; i<liveEventsCopy3.count; i++) {
//        NSMutableDictionary *dic =  [NSMutableDictionary dictionaryWithDictionary:liveEventsCopy3[i]];
//        [liveEventsArrayCopy2 addObject:dic];
//         [liveEventsArrayCopy4 addObject:dic];
//    }
//    for (int i = 0; i<liveEventsArrayCopy2.count; i++) {
//        NSInteger oldTimeAgo = [self getTimeFromTimeAgo:liveEventsArrayCopy2[i][@"timeago"]];
//        NSString *timeAgo = liveEventsArrayCopy2[i][@"timeago"];
//            if ((![timeAgo isKindOfClass:[NSNull class]] && timeAgo != nil) ){
//        NSArray *splitArray = [timeAgo componentsSeparatedByString:@" "];
//        NSString *firstPartOfArray = splitArray[0];
//        NSString *timeToCheck = [firstPartOfArray substringFromIndex:[firstPartOfArray length] - 1];
//        if ([timeToCheck isEqualToString:@"s"]) {
//            [liveEventsArrayCopy2[i]setValue:[self getNewTimeAgo:oldTimeAgo] forKey:@"timeago"];
//        }
//        else if([timeToCheck isEqualToString:@"m"]){
//            NSInteger secondsOfTimeAgo = oldTimeAgo*60;
//            [liveEventsArrayCopy2[i]setValue:[self getNewTimeAgo:secondsOfTimeAgo] forKey:@"timeago"];
//        }
//        else if([timeToCheck isEqualToString:@"h"]){
//            NSInteger secondsOfTimeAgo = oldTimeAgo*60*60;
//            [liveEventsArrayCopy2[i]setValue:[self getNewTimeAgo:secondsOfTimeAgo] forKey:@"timeago"];
//
//        }
//        else if([timeToCheck isEqualToString:@"d"]){
//            NSInteger secondsOfTimeAgo = oldTimeAgo*60*60*24;
//            [liveEventsArrayCopy2[i]setValue:[self getNewTimeAgo:secondsOfTimeAgo] forKey:@"timeago"];
//
//        }
//        else if([timeToCheck isEqualToString:@"w"]){
//            NSInteger secondsOfTimeAgo = oldTimeAgo*60*60*24*7;
//            [liveEventsArrayCopy2[i]setValue:[self getNewTimeAgo:secondsOfTimeAgo] forKey:@"timeago"];
//
//        }
//
//        NSInteger oldTimeAgo2 = [self getTimeFromTimeAgo:liveEventsArrayCopy2[i][@"timeago"]];
//
//
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
//        IceTVCell *cell = [table cellForRowAtIndexPath:indexPath];
//
//        cell.iceTimeAgo.text =      [NSString stringWithFormat:@"iced %@",[commonUtils getTimeForCell:oldTimeAgo2]];
//            [liveEventsArrayCopy4[i]setValue:[commonUtils getTimeForCell:oldTimeAgo2] forKey:@"timeago"];
//    }
//
//
//    commonUtils.upNextEventsArray = [[NSMutableArray alloc]initWithArray:liveEventsArrayCopy4];
//      }
//  //  NSLog(@"after changes live events array is %@",commonUtils.liveEventsArray);
//}
-(NSInteger)getTimeFromTimeAgo:(NSString*)timeAgo{
    //    NSString *originalString = @"This is my string. #1234";
    
    // Intermediate
       if ((![timeAgo isKindOfClass:[NSNull class]] && timeAgo != nil) ) {
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:timeAgo];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    NSInteger number = [numberString integerValue];
    return number;
       }
       else{
           return 5;
       }
}
-(NSString*)getNewTimeAgo:(NSInteger)oldTimeAgo{
    NSInteger secondsOfTimeAgo = oldTimeAgo+60;
//    return [NSString stringWithFormat:@"%lds ago",(long)secondsOfTimeAgo];
    return @"Just Now 6";
}
//-(NSString*)getTimeForCell:(NSInteger)oldTimeAgo{
//    NSInteger secondsOfTimeAgo = oldTimeAgo;
//    if (secondsOfTimeAgo>59) {
//        NSInteger minutesOfTimeAgo = secondsOfTimeAgo/60;
//        
//        
//        
//        if (minutesOfTimeAgo>59) {
//            NSInteger hoursOfTimeAgo = minutesOfTimeAgo/60;
//            if (hoursOfTimeAgo>23) {
//                NSInteger daysOfTimeAgo = hoursOfTimeAgo/24;
//                if (daysOfTimeAgo>6) {
//                    NSInteger weeksOfTimeAgo = daysOfTimeAgo/7;
//                    if (weeksOfTimeAgo>3) {
//                        NSInteger monthOfTimeAgo = weeksOfTimeAgo/4;
//                        return [NSString stringWithFormat:@"%ldmo ago",(long)monthOfTimeAgo];
//                    }
//                    else{
//                        return [NSString stringWithFormat:@"%ldw ago",(long)weeksOfTimeAgo];
//                    }
//                }
//                else{
//                    return [NSString stringWithFormat:@"%ldd ago",(long)daysOfTimeAgo];
//                }
//            }
//            else{
//                return [NSString stringWithFormat:@"%ldh ago",(long)hoursOfTimeAgo];
//            }
//        }
//        else{
//            return [NSString stringWithFormat:@"%ldm ago",(long)minutesOfTimeAgo];
//        }
//    }
//    else{
//        return [NSString stringWithFormat:@"%lds ago",(long)secondsOfTimeAgo];
//        //return [NSString stringWithFormat:@"%ldm ago",(long)oldTimeAgo];
//    }
//    
//    
//}
-(void)refreshData:(UIRefreshControl *)refreshControl{
   [self performSelector:@selector(getDashBoard) withObject:nil afterDelay:0.1];
  
}
-(void)stopRefreshAndReload{
    isRefreshColelctionView = false;
    
    
    
    if (upnextArrayCopy.count == oldArray.count){
        for (int index = 0 ; index < upnextArrayCopy.count ; index++){
            
            NSMutableArray *newdict = upnextArrayCopy[index][@"get_images"];
            NSMutableArray *olddict = oldArray[index][@"get_images"];
            
            NSLog(@"olddict %@",olddict);
            NSLog(@"newDict %@",newdict);
            
            if (olddict.count == newdict.count){
                for (int indexImage = 0 ; indexImage < olddict.count ; indexImage ++ ){
                    if (olddict[indexImage][@"id"] == newdict[indexImage][@"id"])
                    {
                        
                    }else {
                        isRefreshColelctionView = true;
                        break;
                    }
                }
            }else {
                isRefreshColelctionView = true;
                break;
            }
        }
    }else {
        isRefreshColelctionView = true;
    }
    
    if (firstTime) {
        isRefreshColelctionView = true;
        firstTime = false;
    }
    
    oldArray = [[NSMutableArray alloc] initWithArray:upnextArrayCopy copyItems:true];
    dispatch_async(dispatch_get_main_queue(), ^(void){
        
        [table layoutIfNeeded];
        [refreshControl endRefreshing];
        
        
        [UIView transitionWithView:table duration:0.0 options:(UIViewAnimationOptionCurveLinear) animations:^{
            [table layoutIfNeeded];
        } completion:^(BOOL finished) {
              [table reloadData];
            
            
//            if (isReloadAgain) {
//               [self performSelector:@selector(stopRefreshAndReload) withObject:nil afterDelay:0.20];
//                isReloadAgain =false;
//            }
        }];
        
        
    });
   
}
-(void)showInternetError{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    });
}
-(void)scrollToLatest{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:commonUtils.collectionViewTag inSection:0];
    IceTVCell *cell = [table cellForRowAtIndexPath:indexPath];
    [cell.eventImageCv setPage:commonUtils.collectionViewCellTag isAnimation:YES];

    
    
}
- (void)scrollViewDidScroll:(UIScrollView *)rScrollView {
    isRefreshColelctionView = true;
}
@end
