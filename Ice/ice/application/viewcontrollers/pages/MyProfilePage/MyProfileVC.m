//
//  MyProfileVC.m
//  ICE
//
//  Created by LandToSky on 12/26/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "MyProfileVC.h"


#import "ProfileHeaderView.h"
#import "UIView+GSKLayoutHelper.h"
#import "UIViewController+KNSemiModal.h"

#import "pUpcomingTitleCell.h"
#import "pUpcomingContentCell.h"
#import "pEyeCandyTitleCell.h"
#import "pEyeCandyContentCell.h"
#import "EventImageCVCell.h"

#import "DetailVC.h"
#import "UserProfileVC.h"
#import "EventsVC.h"
#import "FollowRootVC.h"
#import "ICEVC.h"
#import "MyProfileEditVC.h"
#import "testViewController.h"
#import "allImagesVC.h"
#import "PhotoVideoShowVC.h"
#import "LeftMenuVC.h"



@interface MyProfileVC ()<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate,UICollectionViewDataSource,GSKStretchyHeaderViewStretchDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    IBOutlet UITableView *mainTV;
    
    NSInteger upComingTVCellNum;
    NSInteger eyeCandyCVCellNum;
    
    //    profile image upload
    IBOutlet UIImageView *userProfileIV;
    BOOL noCamera, isProfileImageChanged;
    UIImage *changedImage;
    MyProfileEditVC *editVc;
    NSMutableArray *icePics;
    NSMutableArray *imagesArray;
    PHImageManager *manager;
    NSMutableArray *images ;
    BOOL is_profilePic;
      LeftMenuVC * menuVC;
    // assets contains PHAsset objects.
    UIImage *image2;
    int totalImages;
    int counter;
}

@property (nonatomic, strong) IBOutlet UIView *photoPickContainerView;

@end

@implementation MyProfileVC
@synthesize userName,profileName,profileImage,iceBreaker,headerView,fullName,userImage,iceDesc;
- (void)viewDidLoad {
    [super viewDidLoad];
    is_profilePic = NO;
    counter = 0;
        editVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MyProfileEditVC"];
     [self initUI];
      [self initData];
    icePics = [NSMutableArray new];
    imagesArray = [NSMutableArray new];
    totalImages = 0;
    
}

- (void)initUI
{
    headerView = [[[NSBundle mainBundle] loadNibNamed:@"ProfileHeaderView" owner:self options:nil] firstObject];
  
            // UI UPDATE 3
    headerView.stretchDelegate = self;
    headerView.minimumContentHeight = 85;
    headerView.maximumContentHeight = 230;
    headerView.userNameLbl.text = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
        [mainTV addSubview:headerView];
    [mainTV setBackgroundColor:[UIColor colorWithHex:@"#f7f7f7" alpha:1.0f]];
    
    
    // Header View Action    
    [headerView.eventsBtn addTarget:self action:@selector(onShowEventsVC:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.followingBtn addTarget:self action:@selector(onShowFollowingVC:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.followersBtn addTarget:self action:@selector(onShowFollowersVC:) forControlEvents:UIControlEventTouchUpInside];
    
    _fullNameLbl.text = fullName;
    headerView.userProfileIv.image = userImage;
    NSString *demoImageString = @"http://139.162.37.73/iceapp/public/images/profile_pics/demo.png";
        
    
   if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"photo"] != nil )  {
        
  
    if ([commonUtils.userData[@"photo"]isEqualToString:demoImageString]) {
        headerView.imagePickView.hidden = NO;
        headerView.userProfileIv.image = [UIImage imageNamed:@"demo"];
        [headerView.imagePickBtn addTarget:self action:@selector(onShowPhotoPicker:) forControlEvents:UIControlEventTouchUpInside];
    }
    else{
        commonUtils.isDummyPic = NO;
        headerView.showProfilePicView.hidden = NO;
        [headerView.showProfilePicBtn addTarget:self action:@selector(onShowProfilePic:) forControlEvents:UIControlEventTouchUpInside];
    }
          }
    else{
        
          headerView.userProfileIv.image = [UIImage imageNamed:@"demo"];
        headerView.imagePickView.hidden = NO;
        [headerView.imagePickBtn addTarget:self action:@selector(onShowPhotoPicker:) forControlEvents:UIControlEventTouchUpInside];

        
    }
    headerView.userDescLbl.text = iceDesc;
    [self loadDataOfEditProfile];
      

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    [self getUserDetails:[NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]]];
   
    
    
}
- (void)initData
{
    
    noCamera = NO;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        noCamera = YES;
    }
    isProfileImageChanged = NO;
    changedImage = nil;
    menuVC = (LeftMenuVC*) self.sideMenuController.leftViewController;
    
   
 
    
   }

- (void)viewDidLayoutSubviews
{
    if (upComingTVCellNum == 0 && eyeCandyCVCellNum == 0){
        if (IS_IPHONE_6_OR_ABOVE) {
                    CGFloat bottomHeight = mainTV.frame.size.height - 90 - 210 - 238 +5;
                    UIView *bottomView = [self.view viewWithTag:1001];
                    [commonUtils resizeFrame:bottomView withWidth:bottomView.frame.size.width withHeight:bottomHeight];
        }

    }

}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self hideHeaderCounts];
    
    AppDelegate *appd = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    if (appd.strFollowingCount.length > 0 ){
        headerView.followingLbl.text = appd.strFollowingCount;
    }
    
    if (appd.strFollowerCount.length > 0 ){
        headerView.followersLbl.text = appd.strFollowerCount;
    }
    
    
    if (appd.strActivityCount.length > 0 ){
        headerView.eventLbl.text = appd.strActivityCount;
    }
    
}

#pragma mark - UITableView Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    static NSInteger count = 0;
    count = 2+upComingTVCellNum+ ((eyeCandyCVCellNum>0)?1:0);
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 0.0f;
    
    if (indexPath.row == 0) {
        height = (upComingTVCellNum == 0)? 210 : 60;
        
    } else if (0<indexPath.row && indexPath.row < 1+upComingTVCellNum ) {
        height = 78;
        
    } else if (indexPath.row == 1+upComingTVCellNum){
        height = (eyeCandyCVCellNum == 0) ? 238 : 75;
        
    } else if (indexPath.row == 2+upComingTVCellNum){
        if (IS_IPHONE_5) {
              height = (eyeCandyCVCellNum < 3) ? 125 : 220;
        }
        if (IS_IPHONE_6) {
            height = (eyeCandyCVCellNum < 3) ? 125 : 250;
        }
        if (IS_IPHONE_6P) {
             height = (eyeCandyCVCellNum < 3) ? 125 : 290;
        }
    }
    NSLog(@"height %f===%d",height,indexPath.row);
    return height ;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    pUpcomingTitleCell* upComingTitleCell;
    pUpcomingContentCell *upComingContentCell;
    pEyeCandyTitleCell *eyeCandyTitleCell;
    pEyeCandyContentCell *eyeCandyContentCell;
    
    
    if (indexPath.row == 0) {
        if (upComingTVCellNum == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoUpcomingEventTVCell"];
            
        } else {
            upComingTitleCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingTitleCell"];
            if (upComingTitleCell == nil) {
                upComingTitleCell = [[pUpcomingTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingTitleCell"];
            }
            [upComingTitleCell.upComingAppearanceViewAllBtn addTarget:self action:@selector(onShowRightMenuVC:) forControlEvents:UIControlEventTouchUpInside];
            cell = upComingTitleCell;
            
        }
        
    } else if (0<indexPath.row && indexPath.row < 1+upComingTVCellNum ) {
        upComingContentCell = [tableView dequeueReusableCellWithIdentifier:@"pUpcomingContentCell"];
        if (upComingContentCell == nil) {
            upComingContentCell = [[pUpcomingContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pUpcomingContentCell"];
        }
         int cellRow = indexPath.row - 1;
        upComingContentCell.showDetailBtn.tag = cellRow;
        [upComingContentCell.showDetailBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];
        [upComingContentCell.showUserProfileBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
        
       // [upComingContentCell.showDetailBtn addTarget:self action:@selector(onShowEventDetailVC:) forControlEvents:UIControlEventTouchUpInside];
        [upComingContentCell.showUserProfileBtn addTarget:self action:@selector(onShowUserProfileVC:) forControlEvents:UIControlEventTouchUpInside];
        upComingContentCell.titleLbl.text = commonUtils.userUpcomingArray[cellRow][@"title"];
        upComingContentCell.addressLbl.text = commonUtils.userUpcomingArray[cellRow][@"location"];
        upComingContentCell.userNameLbl.text = [NSString stringWithFormat:@"via %@",commonUtils.userUpcomingArray[cellRow][@"get_user"][@"first_name"]];
        NSString *imageUrl = [NSString stringWithFormat:@"%@",commonUtils.userUpcomingArray[cellRow][@"get_user"][@"photo"]];
        [upComingContentCell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                                      placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                               options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];

        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
        
        
        NSMutableDictionary *mainDict = [[NSMutableDictionary alloc]initWithDictionary:commonUtils.userUpcomingArray[cellRow]] ;
        
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        NSDateFormatter *serverDateFormatter = [NSDateFormatter new];
        [serverDateFormatter setDateFormat:@"yyyy-M-dd"];
        NSDate * endDateFromApi = [startTimeformatter dateFromString:mainDict[@"end_date"]];
        NSDate * startDateFromApi = [startTimeformatter dateFromString:mainDict[@"start_date"]];
        
        NSString *serverStartTime = [serverDateFormatter stringFromDate:startDateFromApi];
        NSString *serverEndTime = [serverDateFormatter stringFromDate:endDateFromApi];
        NSString *eventStartTime = [NSString stringWithFormat:@"%@",mainDict[@"event_start_time"]];
         NSString *eventEndTime = [NSString stringWithFormat:@"%@",mainDict[@"event_end_time"]];
        
        if ([serverStartTime isEqualToString:serverEndTime]) {
           upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
          upComingContentCell.weekdayLbl.text= [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
            if ([commonUtils is24hourFormat]) {
                 upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
           
        }

        
        else{
        
//        NSLog(@"today date is %@",todayDate);
    
        
     
        // your date
        

        
        NSComparisonResult result;
    
        
        result = [today compare:endDateFromApi]; // comparing two dates
            NSString * endDateFromApiString  = [serverDateFormatter stringFromDate:endDateFromApi];
          
            
            NSString *todayDateString = [serverDateFormatter stringFromDate:today];
        if([endDateFromApiString isEqualToString:todayDateString])
        {
            NSLog(@"today is less");
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            if ([commonUtils is24hourFormat]) {
                 upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                 upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
           
            
        }
        
        else {
            NSLog(@"server date  is less");
            upComingContentCell.monthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.weekdayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            upComingContentCell.dateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
            upComingContentCell.timeLbl.text = [NSString stringWithFormat:@"ends %@. %@",endMonth,endDate];
            
        }
        
        
       
    }
        cell = upComingContentCell;
        
    } else if (indexPath.row == 1+upComingTVCellNum){
        if (eyeCandyCVCellNum == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"NoPhotoVideoTVCell"];
        } else {
            eyeCandyTitleCell = [tableView dequeueReusableCellWithIdentifier:@"pEyeCandyTitleCell"];
            if (eyeCandyTitleCell == nil) {
                eyeCandyTitleCell = [[pEyeCandyTitleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pEyeCandyTitleCell"];
            }
            cell = eyeCandyTitleCell;
        }
        
        eyeCandyTitleCell.eyeCandyViewAllBtn.tag = indexPath.row;
        [eyeCandyTitleCell.eyeCandyViewAllBtn addTarget:self action:@selector(showAllImages:) forControlEvents:UIControlEventTouchUpInside];

    } else if (indexPath.row == 2+upComingTVCellNum){
        eyeCandyContentCell = [tableView dequeueReusableCellWithIdentifier:@"pEyeCandyContentCell"];
        if (eyeCandyContentCell == nil) {
            eyeCandyContentCell = [[pEyeCandyContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pEyeCandyContentCell"];
        }
    
        eyeCandyContentCell.eyeCandyCv.delegate = self;
        eyeCandyContentCell.eyeCandyCv.dataSource = self;
        cell = eyeCandyContentCell;
        
        
    }
    [eyeCandyContentCell.eyeCandyCv reloadData];
    return cell;
}


#pragma mark - On Other VC
- (void)onShowUserProfileVC:(UIButton*)sender
{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = commonUtils.userData[@"id"];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowEventDetailVC:(UIButton*) sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    vc.iceDetails = commonUtils.userUpcomingArray[sender.tag];
    vc.comingFrom = @"userprofile";
    vc.selectedTag = sender.tag;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowFollowingVC:(UIButton *)sender
{
    FollowRootVC *vc = (FollowRootVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"FollowRootVC"];
    vc.pageIndex = Following;
    vc.firstName = commonUtils.userData[@"first_name"];
    commonUtils.commutilUserID = [NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowFollowersVC:(UIButton *)sender
{
    FollowRootVC *vc = (FollowRootVC *) [self.storyboard instantiateViewControllerWithIdentifier:@"FollowRootVC"];
    vc.pageIndex = Followers;
     vc.firstName = commonUtils.userData[@"first_name"];
 commonUtils.commutilUserID = [NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)onShowPhotoPicker:(UIButton *)sender
{
    is_profilePic = YES;
    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];

}
- (void)onShowProfilePic:(UIButton *)sender
{
    if (commonUtils.userImage) {
        PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
        NSMutableArray *profileImageArray = [NSMutableArray new];
        [profileImageArray addObject:commonUtils.userImage];
        vc.currentPageNum = 0;
        commonUtils.myprofileImage = profileImageArray;
        vc.comingFrom = @"myprofilepic";
        [self.navigationController pushViewController:vc animated:YES];
    }
    
    
}


- (void)onShowEventsVC:(UIButton *)sender
{
    EventsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsVC"];
    commonUtils.commutilUserID = [NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onShowRightMenuVC:(UIButton*) sender
{
    [self.sideMenuController showRightViewAnimated:sender];
}

- (IBAction)onShowICEVC:(UIButton*) sender
{
    SidePanelVC *sidePanelVC = (SidePanelVC*)self.sideMenuController;
    // Goto ICEVC
    if (!sidePanelVC.ICENavigation) {
        
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ICEVC"];
        sidePanelVC.ICENavigation = [[UINavigationController alloc] initWithRootViewController:vc];
        [sidePanelVC.ICENavigation setNavigationBarHidden:YES];
    }
    
    [sidePanelVC setRootViewController:sidePanelVC.ICENavigation];
    [sidePanelVC hideLeftViewAnimated:sender];


}


#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return icePics.count+1;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGSize mElementSize = CGSizeMake(collectionView.frame.size.width/3 - 1, collectionView.frame.size.width/3-1);
    return mElementSize;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
    NSLog(@"indexpath is %ld",(long)indexPath.row);
    if (indexPath.row == 0) {
        cell.eventIv.image = [UIImage imageNamed:@"add-image"];
        cell.videoPlayBtn.hidden = YES;
    }
    else{
        if ([icePics[indexPath.row-1]isKindOfClass:[UIImage class]]) {
            
          cell.eventIv.image = icePics[indexPath.row-1];
            cell.videoPlayBtn.hidden = YES;
        }
        else{
            NSString *imageUrl;
            NSString *type = [NSString stringWithFormat:@"%@",icePics[indexPath.row-1][@"type"]];
            if ([type isEqualToString:@"image"]) {
                
            
                    cell.videoPlayBtn.hidden = YES;
               

                imageUrl = [NSString stringWithFormat:@"%@%@",ICEImageBaseURL,icePics[indexPath.row-1][@"image"]];
            }
            else{
              
                     cell.videoPlayBtn.hidden = NO;
                cell.videoPlayBtn.tag = indexPath.row-1;
                [cell.videoPlayBtn addTarget:self action:@selector(playvideomethod:) forControlEvents:UIControlEventTouchUpInside];
                imageUrl  = [NSString stringWithFormat:@"%@%@",PosterBaseURL,icePics[indexPath.row-1][@"poster"]];

            }
            [cell.eventIv sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                            placeholderImage:[UIImage imageNamed:@"image0"]
                                     options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
        }

    }

    
    return cell;
    
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    // return UIEdgeInsetsMake(0,8,0,8);  // top, left, bottom, right
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        if(self.isLoadingBase) return;
        
        [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                        KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                        KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                        KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                        }];

    }
    else {
        PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
        vc.currentPageNum = indexPath.row-1;
        commonUtils.userIcePics = icePics;
        vc.comingFrom = @"ownProfile";
        [self.navigationController pushViewController:vc animated:YES];
    }
 
    
}
#pragma mark - GSKStretchView Delegate

#pragma mark - Stretch Delegate
- (void)stretchyHeaderView:(ProfileHeaderView *)realHeaderView didChangeStretchFactor:(CGFloat)stretchFactor
{
    
    CGFloat limitedStretchFactor = MIN(1, stretchFactor);
    
    // User Profile ImageView Size Adjust
    CGSize minImageSize = CGSizeMake(55, 55);
    CGSize maxImageSize = CGSizeMake(125, 125);
    CGSize minImagePickView = CGSizeMake(55, 55);
    CGSize maxImagePickView = CGSizeMake(125, 125);
    CGSize mincamImage= CGSizeMake(15, 15);
    CGSize maxcamImage= CGSizeMake(30, 30);
    realHeaderView.userProfileIv.size = CGSizeInterpolate(limitedStretchFactor, minImageSize, maxImageSize);
    realHeaderView.imagePickView.size = CGSizeInterpolate(limitedStretchFactor, minImagePickView, maxImagePickView);
     realHeaderView.camImage.size = CGSizeInterpolate(limitedStretchFactor, mincamImage, maxcamImage);
     realHeaderView.showProfilePicView.size = CGSizeInterpolate(limitedStretchFactor, minImagePickView, maxImagePickView);
    realHeaderView.camImage.center =  CGPointMake(realHeaderView.imagePickView.frame.size.width  / 2,
                                                  realHeaderView.imagePickView.frame.size.height / 2);
    // Bottom (Following/Followers/Events) View Adjust
    CGSize minBottomViewSize = CGSizeMake(realHeaderView.size.width-55, 55);
    CGSize maxBottomViewSize = CGSizeMake(realHeaderView.size.width, 70);
    CGPoint minBottomViewOrigin = CGPointMake(55, 0);
    CGPoint maxBottomViewOrigin = CGPointMake(0, 125);
    
    
    realHeaderView.bottomView.size = CGSizeInterpolate(limitedStretchFactor, minBottomViewSize, maxBottomViewSize);
    realHeaderView.bottomView.left = CGFloatInterpolate(limitedStretchFactor, minBottomViewOrigin.x, maxBottomViewOrigin.x);
    realHeaderView.bottomView.top = CGFloatInterpolate(limitedStretchFactor, minBottomViewOrigin.y, maxBottomViewOrigin.y);
    
    // User Name View
    [realHeaderView.userNameContainerView setAlpha:limitedStretchFactor];
}

#pragma mark - Profile Photo Change

- (IBAction)onProfilePhotoChange:(id)sender {
    if(self.isLoadingBase) return;
    is_profilePic = NO;
    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];
}
- (void)onClickGallery {
    if(self.isLoadingBase) return;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.tintColor = [UIColor blackColor];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
  //  picker.videoMaximumDuration = 60;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
 //picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)showVideoPicker{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.tintColor = [UIColor blackColor];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
      picker.videoMaximumDuration = 60;
      picker.mediaTypes =[[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    //picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    [self presentViewController:picker animated:YES completion:NULL];

}
- (IBAction)onClickCamera:(id)sender {
    if(self.isLoadingBase) return;
    
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.videoMaximumDuration = 60;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    if (!is_profilePic) {
           picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    }
 
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    changedImage = info[UIImagePickerControllerEditedImage];
    if (is_profilePic) {
        headerView.userProfileIv.image = changedImage;
        commonUtils.userImage = changedImage;
        headerView.imagePickView.hidden = YES;
        commonUtils.isDummyPic = NO;
          [self dismissSemiModalView];
        [picker dismissViewControllerAnimated:NO completion:^{
            
            
            NSMutableDictionary *userDataMutable = [commonUtils.userData mutableCopy];
            
    
            if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
            
            }
            else{
                [userDataMutable setObject:@"" forKey:@"ice_breaker"];
            }
            if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"phone"] != nil ) {
                
            }
            else{
                  [userDataMutable setObject:@"" forKey:@"phone"];
            }
            [self updateProfile:userDataMutable];
            
        }];

        
    }
    else{
        imagesArray = [NSMutableArray new];
        if (changedImage) {
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:changedImage,@"image",@"image",@"IsImage", nil];
            [imagesArray addObject:dic];
            
            
            [mainTV reloadData];
            
            isProfileImageChanged = YES;
            [self dismissSemiModalView];
            
            
        }
        else{
            NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
            NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            NSData *data = [NSData dataWithContentsOfURL:videoURL];
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            [dic setObject:videoURL forKey:@"image"];
            [dic setObject:data forKey:@"videodata"];
            [dic setValue:@"video" forKey:@"IsImage"];
            [dic setValue:@"camera" forKey:@"pickedType"];
            [imagesArray addObject:dic];
            
            [mainTV reloadData];
            
            [self dismissSemiModalView];
            
        }
        [picker dismissViewControllerAnimated:NO completion:^{
            
            
        }];
        [self uploadMyPic];

    }
  }

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:NO
                               completion:^{
                                   
                                   /* ScrollView Inset Error
                                   [commonUtils moveView:self.sidePanelController.centerPanelContainer
                                               withMoveX:0
                                               withMoveY:0];
                                   
                                   [commonUtils resizeFrame:self.sidePanelController.centerPanelContainer
                                                  withWidth:SCREEN_WIDTH
                                                 withHeight:SCREEN_HEIGHT];
                                    */
                               }];
    
    [self dismissSemiModalView];
    
}

//-(void)updateUserDetails {
//
//
//    if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]]&&![[commonUtils.userData valueForKey:@"photo"]isEqualToString:@""]) {
//        NSString *imageUrlString = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
//        NSURL *url = [NSURL URLWithString:imageUrlString];
//
//        NSData *data = [NSData dataWithContentsOfURL:url];
//        [headerView.userProfileIv setImage:[UIImage imageWithData:data]];
//
//
//    }
//    else {
//       headerView.userProfileIv.image = [UIImage imageNamed:@"user-avatar"];
//
//
//    }
//    _fullNameLbl.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
//    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
//        headerView.userDescLbl.text =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
//    }
//    else {
//        headerView.userDescLbl.text = @"";
//    }
//
//}
//

    
   
- (IBAction)editBtn:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (![self.navigationController.topViewController isKindOfClass:MyProfileEditVC.class])
        {
            editVc.isProfileImageChanged = NO;
            [self.navigationController pushViewController:editVc animated:YES];
        }
    });
    
//    testViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"testVc"];
//    [self.navigationController pushViewController:vc animated:YES];
}


-(void)loadDataOfEditProfile{

        editVc.profileImage  = headerView.userProfileIv.image;
    
    editVc.userName = [[NSString stringWithFormat:@"@%@",[commonUtils.userData valueForKey:@"username"]]lowercaseString];
    
    editVc.profileName= [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
    if (![[commonUtils.userData valueForKey:@"ice_breaker"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"ice_breaker"] != nil ) {
        editVc.iceBreaker =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"ice_breaker"]];
    }
    else {
        editVc.iceBreaker = @"";
    }
    if (![[commonUtils.userData valueForKey:@"email"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"email"] != nil ) {
        editVc.email =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"email"]];
    }
    else {
        editVc.email = @"";
    }
    if (![[commonUtils.userData valueForKey:@"phone"] isKindOfClass:[NSNull class]] && [commonUtils.userData valueForKey:@"phone"] != nil ) {
        editVc.phone =  [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"phone"]];
    }
    else {
        editVc.phone = @"";
    }
}
-(void)getUserDetails:(NSString*)userID{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
//        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"%@get_profile/%@?time_zone=%@",ServerUrl,userID,timezone];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
 
        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
//                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    commonUtils.userUpcomingArray = [[NSMutableArray alloc] init];
                    NSMutableDictionary *successDic = json[@"successData"];
                    //NSLog(@"%@",commonUtils.userUpcomingArray[activityIndex][@"get_user"]);
                    
                    AppDelegate *appd = (AppDelegate *)[[UIApplication sharedApplication]delegate];

                    
                    headerView.followersLbl.text = [NSString stringWithFormat:@"%@",successDic[@"follower_count"]];
                    headerView.followingLbl.text = [NSString stringWithFormat:@"%@",successDic[@"follow_count"]];
                    headerView.eventLbl.text =  [NSString stringWithFormat:@"%@",successDic[@"activities_count"]];
                    
                    appd.strFollowingCount = headerView.followingLbl.text;
                    appd.strFollowerCount = headerView.followersLbl.text;
                    appd.strActivityCount = headerView.eventLbl.text;
                    
                    [self unHideHeaderCounts];
                    NSString *totalUserIced =  [NSString stringWithFormat:@"%@",successDic[@"ice"]];
                    NSString *totalUserAdded =  [NSString stringWithFormat:@"%@",successDic[@"added"]];
                    commonUtils.userTotalIced = [totalUserIced integerValue];
                    commonUtils.userTotalAdded = [totalUserAdded integerValue];
                    commonUtils.userUpcomingArray = [successDic[@"up_coming_events"]mutableCopy];
                    upComingTVCellNum = commonUtils.userUpcomingArray.count;
                   //   NSLog(@"commutil array is %@",commonUtils.userUpcomingArray);
                    
                    NSMutableArray *eventsImages = [NSMutableArray new];
                    eventsImages = [successDic[@"ice_pics"]mutableCopy];
                    
                    //NSLog(@"userupcoming commutil array is %@",commonUtils.userUpcomingArray);
                
                        icePics =  [[NSMutableArray alloc]init];
                        icePics =  [successDic[@"user_image"]mutableCopy];
                    for (int i = 0; i<eventsImages.count; i++) {
                        [icePics addObject:eventsImages[i]];
                    }
                  
                   
                     eyeCandyCVCellNum = icePics.count;
                    pEyeCandyContentCell *eyeCandyContentCell;
                    [eyeCandyContentCell.eyeCandyCv reloadData];
                    [mainTV reloadData];
                }
                if(Jerror!=nil)
                {
//                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
        
        }];
    }
    
}
-(void)showAllImages:(UIButton*)sender {
    allImagesVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"allImagesVC"];
    VC.iceImages = icePics;
//    VC.iceImages = true;
    [self.navigationController pushViewController:VC animated:YES];
}
-(void)uploadMyPic{
    
  
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
       
              //    NSLog(@"reminder string %@",reminderString);
      
       
        if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
            //       NSArray *invitedarray = @[@6,@7];
            
            NSError *error;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            });
          
       
            
            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
            
            //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            

            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            
            NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"upload_image"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:Url];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
            
            for (int i=0; i<imagesArray.count; i++) {
                
                
                
                
                NSString *strValue = (NSString *)imagesArray[i][@"IsImage"] ;
                
                
                if ([strValue isEqualToString:@"image"]) {
                    
                    
                    NSString *fileName = [NSString stringWithFormat:@"image[%d]",i];
                    NSData *imageData = UIImageJPEGRepresentation(imagesArray[i][@"image"], 0.5);
                    if (imageData) {
                        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:imageData];
                        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    }
                    
                }
                else{
                    // NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[i]];
                    NSData *postData = imagesArray[i][@"videodata"];
                    NSString *videoFileName = [NSString stringWithFormat:@"video[%d]",i];
                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"file.mov\"\r\n", videoFileName] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: video/mov\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:postData];
                    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    UIImage *img = [[UIImage alloc]init];
                    if ([imagesArray[i][@"pickedType"]isEqualToString:@"galery"]) {
                        
                        
                        NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[i][@"image"]];
                        AVURLAsset *asset = [AVURLAsset assetWithURL:assetURL];
                        //
                        
                        
                        
                        //
                        
                        
                        //AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row] options:nil];
                        
                        
                        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                        generate.appliesPreferredTrackTransform = YES;
                        NSError *err = NULL;
                        CMTime time = CMTimeMake(1, 60);
                        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                        
                        img = [[UIImage alloc] initWithCGImage:imgRef];
              
                    }
                    else{
                        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[i][@"image"] options:nil];
                        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                        generate.appliesPreferredTrackTransform = YES;
                        NSError *err = NULL;
                        CMTime time = CMTimeMake(1, 60);
                        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                        
                       img = [[UIImage alloc] initWithCGImage:imgRef];
                     
                        
                    }

                    // NSLog(@"image is %@",img);
                    NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
                    NSString *thumbnailName = [NSString stringWithFormat:@"thumb[%d]",i];
                    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", thumbnailName] dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                    [body appendData:imageData];
                    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                    
                    
                    
                    
                    
                    
                }
            }
        
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSString* myString;
                                                  myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                  // NSLog(@"string is %@",myString);
                                                  NSLog(@"dictionary %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                  
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                      if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                          [[NSNotificationCenter defaultCenter]
                                                           postNotificationName:@"Logout"
                                                           object:self];
                                                          
                                                          
                                                      }
                                                  }else
                                                  if([statusis isEqualToString:@"success"]){
                                                      
                                                    
                                                      
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          
//                                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                          NSMutableDictionary *images = [[NSMutableDictionary alloc]initWithDictionary:[dictionary[@"successData"]mutableCopy]];
                                                          icePics = [NSMutableArray new];
                                                            icePics = images[@"images"];
                                                          eyeCandyCVCellNum = icePics.count;
                                                          [mainTV reloadData];
                                                         [self getUserDetails:[NSString stringWithFormat:@"%@",commonUtils.userData[@"id"]]];
                                                          
                                                          
                                                          
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
//                                                          [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                      
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
//                                                  dispatch_async(dispatch_get_main_queue(), ^{
//                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
//                                                      
//                                                    
//                                                  });

                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  
//                                                  dispatch_async(dispatch_get_main_queue(), ^{
//                                                      [MBProgressHUD hideHUDForView:self.view animated:YES];
//                                                      
//                                                      
//                                                  });
                                                  NSLog(@"%@", err.localizedDescription);
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
            
            
            
        }
        
        
        
        
        else {
            [commonUtils showAlert:@"Error!" withMessage:@"Unable To Get Your Location.Please Allow The App To Get Your Location"];
        }
    }
    
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    //    NSDate *convertedDate = [converter dateFromString:PassedDate];
    //    NSString *convertedDateString = [converter stringFromDate:convertedDate];
    //    return convertedDateString;
    
}
- (void)launchGMImagePicker
{
    
    [self dismissSemiModalView];
    if(self.isLoadingBase) return;
    if (is_profilePic) {
        [self onClickGallery];
    }
    else{
        GMImagePickerController *picker = [[GMImagePickerController alloc] init];
        picker.delegate = self;
        picker.title = @"";
            picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
        picker.customDoneButtonTitle = @"Done";
        picker.customCancelButtonTitle = @"Cancel";
        picker.customNavigationBarPrompt = @"Take a new photo or select an existing one!";
        
        picker.colsInPortrait = 3;
        picker.colsInLandscape = 5;
        picker.minimumInteritemSpacing = 2.0;

        
        picker.modalPresentationStyle = UIModalPresentationPopover;
        

        
        UIPopoverPresentationController *popPC = picker.popoverPresentationController;
        popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;

        [self presentViewController:picker animated:YES completion:NULL];
 
    }
   }



#pragma mark - GMImagePickerControllerDelegate

- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSArray *)assetArray
{
    imagesArray = [NSMutableArray new];
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        
        statusBar.backgroundColor = [UIColor clearColor];
    }

    
   manager = [PHImageManager defaultManager];
   images = [NSMutableArray arrayWithCapacity:[assetArray count]];
   
    // assets contains PHAsset objects.
   
    counter = 0;
    [self recursiveFun :assetArray];
    
}
-(void)recursiveFun :(NSArray*)assetArray{
  
    PHImageRequestOptions *option = [PHImageRequestOptions new];
    option.synchronous = YES;
    PHAsset *asset =  assetArray[counter];
    if (asset.mediaType == 1) {
        [manager requestImageForAsset:asset
                           targetSize:PHImageManagerMaximumSize
                          contentMode:PHImageContentModeDefault
                              options:option
                        resultHandler:^void(UIImage *image, NSDictionary *info) {
                            //   NSLog(@"info is %@",info);
                            image2 = image;
                            NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:image2,@"image",@"image",@"IsImage", nil];
                            [imagesArray addObject:dic];
                            if (counter ==assetArray.count-1) {
                                //NSLog(@"images array is %@",imagesArray);
                                [self uploadMyPic];
                            }
                            else{
                                counter+=1;
                                [self recursiveFun:assetArray];
                            }
                            [self reloadCollectionViewAndChangeLayout];
                        }];
        
    }
    else if(asset.mediaType == 2){
        [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info) {
            NSURL *url = (NSURL *)[[(AVURLAsset *)avAsset URL] fileReferenceURL];
            
            NSLog(@"url is %@",url);
            NSLog(@"url = %@", [url absoluteString]);
            NSLog(@"url = %@", [url relativePath]);
            AVURLAsset* myAsset = (AVURLAsset*)avAsset;
            NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
            if (data) {
                //   NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[url relativePath],@"image",data,@"videodata",@"video",@"IsImage", nil];
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setObject:[url relativePath] forKey:@"image"];
                [dic setObject:data forKey:@"videodata"];
                [dic setValue:@"video" forKey:@"IsImage"];
                [dic setValue:@"galery" forKey:@"pickedType"];
                [dic setObject:url forKey:@"playurl"];
                [imagesArray addObject:dic];
                if (counter==assetArray.count-1) {
                   // NSLog(@"images array is %@",imagesArray);
                    [self uploadMyPic];
                }
                else{
                    counter+=1;
                    [self recursiveFun:assetArray];
                }

                //  NSLog(@"data here is %lu",(unsigned long)imagesArray.count);
            }
            //                                [imagesArray addObject:[url relativePath] ];
            [self reloadCollectionViewAndChangeLayout];
        }];
        [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset* avasset, AVAudioMix* audioMix, NSDictionary* info){
            AVURLAsset* myAsset = (AVURLAsset*)avasset;
            NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
            if (data) {
                
            }
        }];
        
    }

}
//Optional implementation:
-(BOOL)assetsPickerController:(GMImagePickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    if (totalImages<10) {
        return YES;
    }
    else{
        [commonUtils showAlert:@"Error!" withMessage:@"You have reached maximum images limit"];
        return NO;
    }
    
}
-(void)assetsPickerController:(GMImagePickerController *)picker didSelectAsset:(PHAsset *)asset{
    totalImages++;
}
-(void)assetsPickerController:(GMImagePickerController *)picker didDeselectAsset:(PHAsset *)asset{
    totalImages--;
}
-(void)assetsPickerControllerDidCancel:(GMImagePickerController *)picker
{
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        
        statusBar.backgroundColor = [UIColor clearColor];
    }

    NSLog(@"GMImagePicker: User pressed cancel button");
}
-(void)reloadCollectionViewAndChangeLayout{
    pEyeCandyContentCell *eyeCandyContentCell;
    [eyeCandyContentCell.eyeCandyCv reloadData];
    [mainTV reloadData];
}

-(void)playvideomethod:(UIButton*)sender{
    NSURL *videoUrl;
    if (icePics[sender.tag][@"ice_id"]) {
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ICEVideoBaseURL,icePics[sender.tag][@"image"]]];
    }
    else{
        videoUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",UserVideoBaseURL,icePics[sender.tag][@"image"]]];
    }
    
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:videoUrl];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];
    
}

-(void)updateProfile:(NSDictionary *)userData {
  
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        return;
    }
    else {
        //there-is-no-connection warning
     
        [commonUtils showHud:self.view];
        
        
        
        
        
        // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
        
        //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:userData[@"first_name"] forKey:@"full_name"];
        [_params setObject:userData[@"ice_breaker"] forKey:@"ice_breaker"];
        [_params setObject:userData[@"phone"] forKey:@"phone"];
      

      
        [_params setObject:userData[@"email"] forKey:@"email"];
        [_params setObject:userData[@"username"]forKey:@"username"];
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"pic";
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"update_profile"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(commonUtils.userImage, 0.5);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSLog(@"dictionary %@",dictionary);
                                              
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else
                                                  
                                              if([statusis isEqualToString:@"success"]){
                                                  NSDictionary *successDic = [dictionary valueForKey:@"successData"];
                                                  
                                                  
                                                  
                                                  //  NSLog(@"Old Data ====> %@", commonUtils.userData);
                                                  
                                                  [commonUtils saveUserdata:successDic];
                                                  commonUtils.userData = [commonUtils getUserData];
                                                   profileImage = commonUtils.userImage;
                                                  [[NSNotificationCenter defaultCenter]
                                                   postNotificationName:@"userImageChanged"
                                                   object:self];
                                                  
                                                  //NSLog(@"New Data ====> %@", commonUtils.userData);
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^(void){
                                                      //Run UI Updates
                                                      [commonUtils hideHud];
                                         
                                                    
                                                  menuVC.userImage.image = commonUtils.userImage;
                                                      editVc.profileImage  = headerView.userProfileIv.image;
                                                      CGSize sizeOfImage = CGSizeMake(50, 50);
                                                      menuVC.userImage.image = [self imageByCroppingImage:menuVC.userImage.image toSize:sizeOfImage];

                                                      isProfileImageChanged = NO;
                                                      commonUtils.isDummyPic = NO;
                                                      headerView.showProfilePicView.hidden = NO;
                                                      [headerView.showProfilePicBtn addTarget:self action:@selector(onShowProfilePic:) forControlEvents:UIControlEventTouchUpInside];

                                                      
                                                  });
                                 
                                                  
                                                  
                                                  
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^(void){
                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Error!" withMessage:[NSString stringWithFormat:@"%@",[dictionary valueForKey:@"errorMessage"]]];
                                                  });
                                                  
                                                  // [self updateProfile];
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              dispatch_async(dispatch_get_main_queue(), ^(void){
                                                  [commonUtils hideHud];
                                                  
                                              });
                                              NSLog(@"no data returned");
                                              
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              dispatch_async(dispatch_get_main_queue(), ^(void){
                                                  [commonUtils hideHud];
                                                  
                                              });
                                              NSLog(@"Server not responding");
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
        
        
    }
    
    
}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}
-(void)showMediaOptions{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Choose Media Type"
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Photos"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    [self launchGMImagePicker];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"Videos"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                  
                                       [self showVideoPicker];
                               
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed No, thanks button");
                                   // call method whatever u need
                               }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissSemiModalView];
    }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(IBAction)onClickGallery2:(id)sender{
    if (is_profilePic) {
        [self onClickGallery];
    }
    else{
         [self showMediaOptions];
    }
   
}

-(void)hideHeaderCounts{
    headerView.followersLbl.hidden = NO;
    headerView.followingLbl.hidden = NO;
    headerView.eventLbl.hidden = NO;
}
-(void)unHideHeaderCounts{
    headerView.followersLbl.hidden = NO;
    headerView.followingLbl.hidden = NO;
    headerView.eventLbl.hidden = NO;
}

@end
