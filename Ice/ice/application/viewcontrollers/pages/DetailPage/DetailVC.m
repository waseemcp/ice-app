//
//  DetailVC.m
//  ICE
//
//  Created by LandToSky on 11/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "DetailVC.h"
#import "CommentVC.h"
#import "CommentTVCell.h"

#import "UserProfileVC.h"
#import "PhotoVideoShowVC.h"
#import "CSGrowingTextView.h"
#import "EventCollectionVC.h"
#import "IcedUsersVC.h"
#import "DetailEditVC.h"
#import "NSDate+Compare.h"
@interface DetailVC ()<UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, CSGrowingTextViewDelegate,UIGestureRecognizerDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    
    
    // Photo / Video View
    IBOutlet UIView *photoVideoView;
    IBOutlet UIView *photoVideoContainerView;
    IBOutlet UIView *viewAllImageView;
    
    
    // Detail View
    IBOutlet UIView *detailView;
    IBOutlet UIImageView *detailExpandIv;
    IBOutlet UIView *gradientView;
  
    
    BOOL isExpand;
    BOOL isCommentLoad;
    CGFloat detailDelta; // In case detail view is expanded, increased height value
    
    
    // Comment View
    IBOutlet UIView *commentView;
    
    IBOutlet UITableView *commentTV;
    
    IBOutlet UIView *commentInputView;
    IBOutlet UIImageView *userIv;
    IBOutlet CSGrowingTextView *commentTextView;
    BOOL isKeyboardShown;
    
    CGFloat keyboardHeight;
    NSMutableArray *commentsArray;
    
    BOOL isUserComment;
    
    
    // Iced View
    IBOutlet UIView *icedView;
    
    // Liked View
    IBOutlet UIView *likedView;
    
    // Bottom View
    IBOutlet UIView *bottomView;
    
    // Delete Button View
    IBOutlet UIView *deleteBtnView;
    NSMutableArray *imagesArray;
    BOOL shouldEdit;
}


@end

@implementation DetailVC
@synthesize selectedTag,comingFrom,iceDetails,eventType,sectionTag,activitiesType;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadTable:)
                                                 name:@"detail"
                                               object:nil];
    
}

- (void)initUI
{
    
    // Add Blur Effect
    UIView *topBlurView, *bottomBlurView;
    topBlurView = [self.view viewWithTag:101];
    bottomBlurView = [self.view viewWithTag:102];
    gradientView = [self.view viewWithTag:103];

        gradientView.hidden = YES;

    [commonUtils addBlurEffectToView:topBlurView withAlpha:1.0f];
    [commonUtils addBlurEffectToView:bottomBlurView withAlpha:1.0f];
    [commonUtils overlayGradientView:gradientView];
    
    
    // Adjust Layout
    CGFloat delta = 0.0f;
    if (_isNoExistPhoto) {
        
        delta = -220.0f;
        [commonUtils resizeFrame:photoVideoView withWidth:SCREEN_WIDTH withHeight:132];
        [photoVideoContainerView setHidden:YES];
        
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:contentView.frame.size.height+delta];
        NSLog(@"ContentView H = %f", contentView.frame.size.height);
        
        [commonUtils moveView:detailView withMoveX:0 withMoveY:detailView.frame.origin.y+delta];
        [commonUtils moveView:commentView withMoveX:0 withMoveY:commentView.frame.origin.y+delta];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:icedView.frame.origin.y+delta];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:likedView.frame.origin.y+delta];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:bottomView.frame.origin.y+delta];
        
        [viewAllImageView setHidden:YES];
    }
    
    // CommentView
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)];
    gesture.delegate = self;
    [scrollView addGestureRecognizer:gesture];
  // [scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    
    
   
    // In case Detail page is shown from Right Menu page
    if (self.isShowFromRightMenuEvents) { // Show "delete button"
        [commonUtils resizeFrame:contentView withWidth:contentView.frame.size.width withHeight:1046.0f];
        [deleteBtnView setHidden:NO];
    } else {
        [commonUtils resizeFrame:contentView withWidth:contentView.frame.size.width withHeight:966.0f];
        [deleteBtnView setHidden:YES];
    }
    
    [scrollView setContentSize:contentView.frame.size];
    
    [commentTV setScrollEnabled:NO];
    
    [commentTextView.internalTextView setFont:[UIFont fontWithName:@"Lato-Italic" size:13]];
    [commentTextView.internalTextView setTextColor:[UIColor colorWithHex:@"#585858" alpha:1.0f]];
    
    [commentTextView.placeholderLabel setText: @"Tap to comment"];
    [commentTextView.placeholderLabel setFont:[UIFont fontWithName:@"Lato-Italic" size:13]];
    [commentTextView.placeholderLabel setTextColor:[UIColor colorWithHex:@"#ababab" alpha:1.0f]];
    
    [commentTextView setMinimumNumberOfLines:2];
    [commentTextView setMaximumNumberOfLines:2];
    
    [commentTextView setEnablesNewlineCharacter:YES];
    
    commentTextView.delegate = self;
    imagesArray = [[NSMutableArray alloc]init];
    userIv.image = commonUtils.userImage;


}

- (void)initData
{
    // Comments

    
    isUserComment = NO;
    isKeyboardShown = NO;    isKeyboardShown = NO;
    _detailLbl.systemURLStyle = YES;
    _detailLbl.urlLinkTapHandler = ^(KILabel *label, NSString *string, NSRange range) {
        // Open URLs
        [self attemptOpenURL:[NSURL URLWithString:string]];
    };
       imagesArray = iceDetails[@"get_images"];
    _iceTitleLbl.text = iceDetails[@"title"];
            _iceLocLbl.text = iceDetails[@"location"];
    if (![iceDetails[@"description"] isKindOfClass:[NSNull class]] && iceDetails[@"description"] != nil) {
        _detailLbl.text = iceDetails[@"description"];
    }
    else{
        _detailLbl.text = @"";
    }
    
    
            //_icedayLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_day"]];
            //_iceDateLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_day_start"]];
           // _iceMnthLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_month"]];
            _iceUserNameLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"get_user"][@"first_name"]];
            [_iceUserImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",iceDetails[@"get_user"][@"photo"]]]
                           placeholderImage:[UIImage imageNamed:@"user_avatar"]
                                    options:(SDWebImageContinueInBackground) ];
    
            _imagesCountLbl.text = [NSString stringWithFormat:@"View all %lu images",(unsigned long)imagesArray.count];
    NSDate *today = [NSDate date];
    NSDate *todayDate = [self toLocalTime:today];
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDateFormatter *serverDateFormatter = [NSDateFormatter new];
    [serverDateFormatter setDateFormat:@"yyyy-M-dd"];
    NSDate * endDateFromApi = [startTimeformatter dateFromString:iceDetails[@"end_date"]];
    NSDate * startDateFromApi = [startTimeformatter dateFromString:iceDetails[@"start_date"]];
    
    NSString *serverStartTime = [serverDateFormatter stringFromDate:startDateFromApi];
    NSString *serverEndTime = [serverDateFormatter stringFromDate:endDateFromApi];
    
    NSString *eventStartTime = [NSString stringWithFormat:@"%@",iceDetails[@"event_start_time"]];
    NSString *eventEndTime = [NSString stringWithFormat:@"%@",iceDetails[@"event_end_time"]];
    if ([eventType isEqualToString:@"upcoming"]||[eventType isEqualToString:@"comingsoon"]||[eventType isEqualToString:@"notification"]||[activitiesType isEqualToString:@"begin"]) {
        
    
        _icedayLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_day"]];
        _iceDateLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_day_start"]];
        _iceMnthLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_month"]];
        if ([serverStartTime isEqualToString:serverEndTime]) {
            if ([commonUtils is24hourFormat]) {
                _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
           

        }
        else{
            NSDate* date1 = startDateFromApi;
            NSDate* date2 = endDateFromApi;
            NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
            double secondsInAnHour = 3600;
            NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            if (hoursBetweenDates>24) {
                if ([commonUtils is24hourFormat]) {
                    _iceTImeLbl.text = [NSString stringWithFormat:@"begins %@ %@",[commonUtils convertTo24Hour:eventStartTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    _iceTImeLbl.text = [NSString stringWithFormat:@"begins %@ %@",eventStartTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
            else{
                if ([commonUtils is24hourFormat]) {
                    _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
       
            
        }
     

        


    
    }
    
  else if ([serverStartTime isEqualToString:serverEndTime]) {
      if ([endDateFromApi isEarlierThan:today]) {
          _icedayLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_day"]];
          _iceDateLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_day_start"]];
          _iceMnthLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"event_month"]];
      }
      else{
          _iceMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
          _icedayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
          _iceDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
      }
      if ([commonUtils is24hourFormat]) {
           _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
      }
      else{
         _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
      }
      
    }

    else {
        
        
        
        
        
        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
        //    NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:iceDetails[@"end_date"]];

        
        
        [startTimeformatter setDateFormat:@"yyyy-M-dd"];
        
        
        NSString *todayDateString = [startTimeformatter stringFromDate:today];
        NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
        
        
        [self setHeaderDateTime];

 
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self EnableMenu];
    shouldEdit = YES;
    
//    commentTextView.internalTextView.backgroundColor = [UIColor yellowColor];
    
    if ([[commonUtils.userData valueForKey:@"id"] intValue] == [iceDetails[@"user_id"]intValue]){
        
        
        
    }else {
        _editBtn.hidden = true;
        _editIV.hidden = true;
    }

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillAppearNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}
    
    
- (void)keyboardWillAppearNotification:(NSNotification *)notification {
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
    
    CGRect keyboardFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    isKeyboardShown = (CGRectGetMinY(keyboardFrame) < CGRectGetHeight([[UIScreen mainScreen] bounds]));
    
    [self adjustTableViewFrame:notification];
}
-(void)onKeyboardHide:(NSNotification *)notification
{
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
    [scrollView setContentOffset:bottomOffset animated:YES];
    //keyboard will hide
 
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    NSString *iceID = iceDetails[@"id"];
    isCommentLoad = false;
    
    [self getIceComments:iceID];
    
    NSMutableArray *MembersArray = [NSMutableArray new];
    MembersArray = iceDetails[@"get_members"];
    NSMutableArray *likesArray =[NSMutableArray new];
    likesArray = iceDetails[@"get_likes"];
    _likesCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)likesArray.count];
    _memberCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)MembersArray.count];
    _commentsCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)commentsArray.count];
    _likesCountLblOther.text = _likesCountLbl.text;
    _memberCountLblOther.text = _memberCountLbl.text;
    NSString *checkInCount = [NSString stringWithFormat:@"%@",iceDetails[@"get_checked_in_count"]];
    if (![checkInCount isKindOfClass:[NSNull class]] && checkInCount != nil && iceDetails[@"get_checked_in_count"]){
        _checkInLbl.text = [NSString stringWithFormat:@"%@",iceDetails[@"get_checked_in_count"]];
    }
    else if(!iceDetails[@"get_checked_in_count"]){
        NSLog(@"checked in count %@",commonUtils.checkincount);
        _checkInLbl.text = [NSString stringWithFormat:@"%@",commonUtils.checkincount];
    }
    else{
        _checkInLbl.text = @"0";
    }
    
    if (MembersArray.count==1) {
        _membersNameLbl.text = MembersArray[0][@"member_user"][@"first_name"];
    }
    else if(MembersArray.count >1){
        _membersNameLbl.text = [NSString stringWithFormat:@"%@ and %lu other people",MembersArray[0][@"member_user"][@"first_name"],(unsigned long)MembersArray.count-1];
    }
    else{
        _membersNameLbl.text = @"No one";
    }
    if (likesArray.count == 1) {
        _likesNameLbl.text = likesArray[0][@"user"][@"first_name"];
    }
    else if(likesArray.count>1){
        _likesNameLbl.text = [NSString stringWithFormat:@"%@ and %lu other people",likesArray[0][@"user"][@"first_name"],(unsigned long)likesArray.count-1];
    }
    else {
        _likesNameLbl.text = @"No one";
    }
 
   
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    [self.view endEditing:YES];
}


#pragma mark - Show Comment VC
- (IBAction)onShowCommentVC:(UIButton*) sender
{
    if (isCommentLoad){
        CommentVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentVC"];
        vc.iceDetailsArray = commentsArray;
        NSString *idToPass = [NSString stringWithFormat:@"%@",iceDetails[@"id"]];
        vc.iceID = idToPass;
        vc.providesPresentationContextTransitionStyle = YES;
        vc.definesPresentationContext = YES;
        [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self.navigationController presentViewController:vc animated:YES completion:nil];
    }
    
    
}

- (IBAction)onUserProfileVC:(UIButton*)sender
{    
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = [NSString stringWithFormat:@"%@",iceDetails[@"user_id"]];
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (IBAction)onShowAllEventPhotoVideos:(id)sender
{
    
    
    NSMutableArray *imagesArrayfromapi = [[NSMutableArray alloc]initWithArray:iceDetails[@"get_images"]];
    if (imagesArrayfromapi.count>0) {
        PhotoVideoShowVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"PhotoVideoShowVC"];
        vc.comingFrom = comingFrom;
        vc.collectionViewTag = selectedTag;
        vc.eventType = eventType;
        vc.sectionIndex = sectionTag;
        [self.navigationController pushViewController:vc animated:YES];
    }
    

    
}

- (IBAction)onShowDetails:(UIButton*)sender
{
 
    CGFloat expandHeight = 0.0f, originalHeight = 87.0f;
    
    expandHeight = [commonUtils heightForText:_detailLbl.text font:[UIFont fontWithName:@"Lato-Light" size:17.0] withinWidth: SCREEN_WIDTH - 15];
    detailDelta = expandHeight - originalHeight;
    
    
    isExpand = !isExpand;
    if (isExpand) {
        
        [detailExpandIv setImage:[UIImage imageNamed:@"arrow-up"]];
        
        [commonUtils resizeFrame:detailView withWidth:SCREEN_WIDTH withHeight:detailView.frame.size.height+detailDelta];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:contentView.frame.size.height+detailDelta];
        
        [commonUtils moveView:commentView withMoveX:0 withMoveY:commentView.frame.origin.y+detailDelta];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:icedView.frame.origin.y+detailDelta];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:likedView.frame.origin.y+detailDelta];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:bottomView.frame.origin.y+detailDelta];
        
        [gradientView setAlpha:0.0f];
        
        
    } else {

        [detailExpandIv setImage:[UIImage imageNamed:@"arrow-bottom"]];
        
        [commonUtils resizeFrame:detailView withWidth:SCREEN_WIDTH withHeight:135.0f];
        if (commentsArray.count == 0) {
            [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:800];
        }
        else if(commentsArray.count == 1){
             [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:855];
        }
        else if(commentsArray.count == 2){
            [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:910];
        }
        else{
                [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:966];
        }
    
        
        [commonUtils moveView:commentView withMoveX:0 withMoveY:commentView.frame.origin.y -detailDelta];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:icedView.frame.origin.y - detailDelta];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:likedView.frame.origin.y - detailDelta];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:bottomView.frame.origin.y- detailDelta];
        
        [gradientView setAlpha:1.0f];
        
    }
    
        [scrollView setContentSize:contentView.frame.size];
    
}
    
- (IBAction)onShowCommentVCFromBottom:(UIButton*) sender
    {
        if (isCommentLoad){
            CommentVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentVC"];
            vc.iceDetailsArray = commentsArray;
            NSString *idToPass = [NSString stringWithFormat:@"%@",iceDetails[@"id"]];
            vc.iceID = idToPass;
            vc.providesPresentationContextTransitionStyle = YES;
            vc.definesPresentationContext = YES;
            [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self.navigationController presentViewController:vc animated:YES completion:nil];
        }
    }

#pragma mark - Comment Actions

#pragma mark - UITable View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (commentsArray.count == 0) {
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:57.0f];
         [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:0];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:800];
      [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];

    }
      [scrollView setContentSize:contentView.frame.size];

 
       return commentsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static CommentTVCell *cell = nil;
    cell.commentLbl.text = commentsArray[indexPath.row][@"comment"];
    CGFloat height = [commonUtils heightForText:commentsArray[indexPath.row][@"comment"] font:[UIFont fontWithName:@"Lato" size:15.0] withinWidth: SCREEN_WIDTH - 77];
    NSLog(@"commentsHeight-- %f",height);
    return height + 37;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"CommentTVCell";
    
    CommentTVCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CommentTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    cell.commentLbl.numberOfLines = 0;
    NSString *imageUrlStr = [NSString stringWithFormat:@"%@",commentsArray[indexPath.row][@"user"][@"photo"]];
    
    NSLog(@"Indexpath %ld",(long)indexPath.row);
    NSLog(@"imageUrlStr %@",imageUrlStr);
        [cell.userIv sd_setImageWithURL:[NSURL URLWithString:imageUrlStr]
                           placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                    options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
 
    
    cell.timeLbl.text = commentsArray[indexPath.row][@"timeago"];
    [cell.commentLbl setText:commentsArray[indexPath.row][@"comment"]];
    cell.userNameLbl.text = commentsArray[indexPath.row][@"user"][@"first_name"];
    return cell;
}
- (IBAction)onSendNewComment:(id)sender {
    
    
    [commentTextView resignFirstResponder];
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmedString = [commentTextView.internalTextView.text stringByTrimmingCharactersInSet:charSet];
    if ([trimmedString isEqualToString:@""]) {
        // it's empty or contains only white spaces
        
     NSLog(@"comments Empty");
        
        return;
    }else {
        NSLog(@"comments NON-Empty");
    }
    
    
    
    isUserComment = YES;
     NSLog(@"comments array %@",commentsArray);
    if (trimmedString.length>0) {
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *commentsCount = [f numberFromString:_commentsCountLbl.text];
        NSInteger comments = [commentsCount integerValue];
        comments++;
        _commentsCountLbl.text = [NSString stringWithFormat:@"%ld",(long)comments];
    NSMutableDictionary *userDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:commonUtils.userData[@"first_name"],@"first_name",commonUtils.userData[@"photo"],@"photo", nil];
    NSMutableDictionary *commentDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:trimmedString,@"comment",@"now",@"timeago",userDic,@"user", nil];
    [commentsArray addObject:commentDic];
     NSLog(@"comments array %@",commentsArray);
    [self commentAPI:[NSString stringWithFormat:@"%@",iceDetails[@"id"]] :trimmedString];
    NSIndexPath *newCommentIndexPath = [NSIndexPath indexPathForRow:commentsArray.count-1 inSection:0];
    [commentTV beginUpdates];
    [commentTV insertRowsAtIndexPaths:@[newCommentIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [commentTV endUpdates];
    
    commentTextView.internalTextView.text = @"";
    [commonUtils resizeFrame:commentInputView withWidth:commentInputView.frame.size.width withHeight:57.0f];
    [commonUtils moveView:commentInputView withMoveX:commentInputView.frame.origin.x withMoveY:162.0f];
    
    [self commentTableViewScrollToBottom];
   if (commentsArray.count == 1){
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:112.0f];
        [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:55];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:855];
        [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];
        
    }
    else if(commentsArray.count == 2){
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:167.0f];
        [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:110];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:910];
        [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];
        
    }
    else{
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:222.0f];
        [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:165];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:966];
        [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];
        
    }
    [scrollView setContentSize:contentView.frame.size];
         }
    
}


- (void) commentTableViewScrollToBottom{
    if (commentsArray.count > 0) {

        NSIndexPath *lastIndexPath = [NSIndexPath indexPathForRow:commentsArray.count-1 inSection:0];
        [commentTV scrollToRowAtIndexPath:lastIndexPath
                         atScrollPosition:UITableViewScrollPositionBottom
                                 animated:YES];
    }
}
    

#pragma mark - Growing TextView Delegate
- (void)adjustTableViewFrame:(NSNotification *)note {
    NSLog(@"offset is %f",scrollView.contentOffset.y);
    NSLog(@"contentsize is %f",scrollView.contentSize.height);

    CGFloat y;
    if (isKeyboardShown) {
        
        if (commentsArray.count == 0){
            CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
            [scrollView setContentOffset:bottomOffset animated:YES];
            

        }else {
            y = 679 + (isExpand ? detailDelta : 0)  - ( self.view.frame.size.height - keyboardHeight - commentInputView.frame.size.height - 75);
            NSLog(@"y %f",y);
            [scrollView setContentOffset:CGPointMake(0, y) animated:YES];
            NSLog(@"offset after set is %f",scrollView.contentOffset.y);
            NSLog(@"contentsize after set %f",scrollView.contentSize.height);
        }
        
    
        [scrollView setScrollEnabled:NO];
        
    } else {
        [scrollView setScrollEnabled:YES];
    }
}
    
- (BOOL) growingTextViewShouldBeginEditing:(CSGrowingTextView *)textView
    {
        //    [self moveCommentInputViewToUp];
        [self commentTableViewScrollToBottom];
        return YES;
    }
    
    
- (void)growingTextView:(CSGrowingTextView *)growingTextView willChangeHeight:(CGFloat)height {
    
//    static CGFloat originY = 162.0f;
//
//    CGFloat y;
//    if (isKeyboardShown) {
//        [commonUtils resizeFrame:commentInputView withWidth:commentInputView.frame.size.width withHeight:height];
//        y = originY - (height - 57.0f);
//        
//        NSLog(@"Height y=== %f",y);
        [commonUtils moveView:commentTextView withMoveX:commentInputView.frame.origin.x withMoveY:15];

//    }

}
    

#pragma mark - Button Action
-(IBAction)onIcedUsers:(id)sender
{
    IcedUsersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"IcedUsersVC"];
    NSString *idToPass = [NSString stringWithFormat:@"%@",iceDetails[@"id"]];
    vc.iceID = idToPass;
    vc.isMembers = YES;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:vc];
    [navigation setNavigationBarHidden:YES];
    navigation.providesPresentationContextTransitionStyle = YES;
    navigation.definesPresentationContext = YES;
    [navigation setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
    
}

-(IBAction)onLikedUsers:(id)sender
{
    IcedUsersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"IcedUsersVC"];
    NSString *idToPass = [NSString stringWithFormat:@"%@",iceDetails[@"id"]];
    vc.iceID = idToPass;
    vc.isMembers = NO;
    UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:vc];
    [navigation setNavigationBarHidden:YES];
    navigation.providesPresentationContextTransitionStyle = YES;
    navigation.definesPresentationContext = YES;
    [navigation setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.navigationController presentViewController:navigation animated:YES completion:nil];
}


#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)rScrollView {
    if (rScrollView != commentTV) return;
    
    if (rScrollView.contentOffset.y >= rScrollView.contentSize.height - rScrollView.frame.size.height) {
        [rScrollView setContentOffset:CGPointMake(rScrollView.contentOffset.x, rScrollView.contentSize.height - rScrollView.frame.size.height)];
    }
}


#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    
    commentTextView.internalTextView.text = @"";
    [commonUtils resizeFrame:commentInputView withWidth:commentInputView.frame.size.width withHeight:57.0f];
    
    [self.view endEditing:YES];
    
}
- (void) prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender
{
    // Get reference to the destination view controller
    EventCollectionVC *vc = [segue destinationViewController];
    
    // Pass any objects to the view controller here, like...
    vc.index = selectedTag;
    vc.comingFrom = comingFrom;
    vc.eventType = eventType;
    NSLog(@"section tag is %ld",(long)sectionTag);
     NSLog(@"selected tag is %ld",(long)selectedTag);
    vc.sectionIndex = sectionTag;
  //  vc.commonUtils.userUpcomingArray = upComingArray;
    
}
-(void)commentAPI:(NSString*)ID :(NSString *)CommentString {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    
    else {
        NSError *error;
        
        // [commonUtils showHud:self.view];
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        
        [_params setObject:ID forKey:@"ice_id"];
        
        [_params setObject:CommentString forKey:@"comment"];
        
        
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_ice_comment"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              NSLog(@"dictionary %@",dictionary);
                                              
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else if([statusis isEqualToString:@"success"]){
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils hideHud];
                                                      
                                                      //[commonUtils showAlert:@"Success" withMessage:successMessage];
                                                      
                                                      // Here we need to pass a full frame
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      
                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              //[commonUtils hideHud];
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              [commonUtils hideHud];
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
    
}
- (void) reloadTable:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"detail"]){
        NSLog (@"Successfully received the test notification!");
         NSString *iceID = iceDetails[@"id"];
        isCommentLoad = false;
        [self getIceComments:iceID];
     
    }
    
}
-(void)getIceComments:(NSString*)iceID{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    
        NSString *urlString = [NSString stringWithFormat:@"%@%@/%@",ServerUrl,@"get_ice_comments",iceID];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    dispatch_async(dispatch_get_main_queue(), ^(void){

                   // commonUtils.userUpcomingArray = [[NSMutableArray alloc] init];
                    NSMutableDictionary *successDic = [json[@"successData"]mutableCopy];
                    //NSLog(@"%@",commonUtils.userUpcomingArray[activityIndex][@"get_user"]);
                    commentsArray = [NSMutableArray new];
                    commentsArray = [successDic[@"ice_comments"]mutableCopy];
                    isCommentLoad = true;
                    _commentsCountLbl.text = [NSString stringWithFormat:@"%lu",(unsigned long)commentsArray.count];
                    [self setCommentsTable];
                    
                    
                    
                    
                    
                    
                    [commentTV reloadData];
                         });
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
            }];
            
        });
    }
    
}

- (IBAction)onDetailEdit:(id)sender {
    if (shouldEdit) {
        
        shouldEdit = NO;
    DetailEditVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailEditVC"];
    vc.iceDetailsDic = iceDetails;
    [self.navigationController pushViewController:vc animated:YES];
           }
    
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;

    
}
-(void)setHeaderDateTime{
     NSDate *today = [NSDate date];
      NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    NSDate *todayDate = [self toLocalTime:today];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDate * endDateFromApi = [startTimeformatter dateFromString:iceDetails[@"end_date"]];
     [startTimeformatter setDateFormat:@"yyyy-M-dd"];
    NSString *todayDateString = [startTimeformatter stringFromDate:today];
    NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
    
    NSString *eventEndTime = [NSString stringWithFormat:@"%@",iceDetails[@"event_end_time"]];
    
    NSComparisonResult result;
    result = [endDateFromApi compare:today];
    if(result==NSOrderedDescending)
    {
        NSLog(@"server is large");
        if ([todayDateString isEqualToString:endDateString]) {
            _icedayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            _iceDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            _iceMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            if ([commonUtils is24hourFormat]) {
                  _iceTImeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                _iceTImeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
           

        }
        else{
            _iceMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            _icedayLbl.text= [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            _iceDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
//            _iceTImeLbl.text = [NSString stringWithFormat:@"ends %@. %@",endMonth,endDate];
            
            
            
            NSString *eventStartTime = iceDetails[@"event_start_time"];
            NSString *eventEndTime = iceDetails[@"event_end_time"];
            
            
            NSDate * endDateFromApi = [startTimeformatter dateFromString:iceDetails[@"end_date"]];
            NSDate * startDateFromApi = [startTimeformatter dateFromString:iceDetails[@"start_date"]];
    
            
            
            
            NSDate* date1 = startDateFromApi;
            NSDate* date2 = endDateFromApi;
            NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
            double secondsInAnHour = 3600;
            NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            if (hoursBetweenDates>24) {
                if ([commonUtils is24hourFormat]) {
                    NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
                    _iceTImeLbl.text = [NSString stringWithFormat:@"ends %@. %@",endMonth,endDate];                }
                else{
                    NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
                    _iceTImeLbl.text = [NSString stringWithFormat:@"ends %@. %@",endMonth,endDate];                }
            }
            else{
                if ([commonUtils is24hourFormat]) {
                    _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    _iceTImeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
            
            
        }
      
        
        
        
        
        
        
    }
    
    else if(result==NSOrderedAscending){
        NSLog(@"today date  is large");
        _icedayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        _iceDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        _iceMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
        _iceTImeLbl.text = [NSString stringWithFormat:@"ended at %@. %@",endMonth,endDate];
        
        
    }
    
    
    else{
        _icedayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        _iceDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        _iceMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        if ([commonUtils is24hourFormat]) {
            _iceTImeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
        }
        else{
          _iceTImeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
        }
        
        NSLog(@"Both dates are same");
    }
}
- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
//    if ([comingFrom isEqualToString:@"alert"]) {
//        commonUtils.is_Notification = NO;
//         [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SidePanelVC"] animated:YES];
//    }
//    else{
//
//    }
}
-(void)setCommentsTable{
    if (commentsArray.count == 1){
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:112.0f];
        [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:55];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:855];
        [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];
        
    }
    else if(commentsArray.count == 2){
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:167.0f];
        [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:110];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:910];
        [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];
        
    }
    else{
        [commonUtils resizeFrame:_commentInsideView
                       withWidth:SCREEN_WIDTH withHeight:222.0f];
        [commonUtils resizeFrame:commentTV withWidth:SCREEN_WIDTH withHeight:165];
        [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:966];
        [commonUtils moveView:commentInputView withMoveX:0 withMoveY:commentTV.frame.origin.y+commentTV.frame.size.height];
        [commonUtils resizeFrame:commentView withWidth:SCREEN_WIDTH withHeight:_commentInsideView.frame.size.height+32];
        [commonUtils moveView:icedView withMoveX:0 withMoveY:commentView.frame.origin.y+commentView.frame.size.height+13];
        [commonUtils moveView:likedView withMoveX:0 withMoveY:icedView.frame.origin.y+icedView.frame.size.height+18];
        [commonUtils moveView:bottomView withMoveX:0 withMoveY:likedView.frame.origin.y+likedView.frame.size
         .height+10];
        
    }
    [scrollView setContentSize:contentView.frame.size];
}
- (void)attemptOpenURL:(NSURL *)url
{
    BOOL safariCompatible = [url.scheme isEqualToString:@"http"] || [url.scheme isEqualToString:@"https"];
    
    if (safariCompatible && [[UIApplication sharedApplication] canOpenURL:url])
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
    {
        NSString *url2string = [NSString stringWithFormat:@"http://%@",url.absoluteString];
        NSURL *url2 = [NSURL URLWithString:url2string];
    [[UIApplication sharedApplication] openURL:url2];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Problem"
//                                                        message:@"The selected link cannot be opened."
//                                                       delegate:nil
//                                              cancelButtonTitle:@"Dismiss"
//                                              otherButtonTitles:nil];
//        [alert show];
    }
}
- (BOOL) gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (CGRectContainsPoint(self.detailLbl.bounds, [touch locationInView:self.detailLbl]))
        return NO;
    
    return YES;
}
@end
