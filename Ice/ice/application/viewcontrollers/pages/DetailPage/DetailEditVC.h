//
//  DetaiilEditVC.h
//  ICE
//
//  Created by LandToSky on 11/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "GMImagePickerController.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "GoogleSignIn/GoogleSignIn.h"
@interface DetailEditVC : BaseViewController<GIDSignInUIDelegate>
@property (nonatomic,strong) NSMutableDictionary *iceDetailsDic;
@property (weak, nonatomic) IBOutlet UITextField *reminderTF;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UITextField *houtMintTF;
@property (strong, nonatomic) UIPickerView *hourPicker;
@property (strong, nonatomic) NSArray *pickerElements;
@property (weak, nonatomic) IBOutlet UIButton *onDetailOutlet;
@property (weak, nonatomic) IBOutlet UIView *guestCheckView;
@property (weak, nonatomic) IBOutlet UIView *inviteIconView;
- (IBAction)onDetailBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *onInviteBtn;
- (IBAction)onInviteBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *onIcedOutlet;
@property (weak, nonatomic) IBOutlet UIView *addImageView;
@property (weak, nonatomic) IBOutlet UIView *endView;
@property (weak, nonatomic) IBOutlet UIView *startView;

@end
