//
//  LeftMenuVC.h
//  ICE
//
//  Created by LandToSky on 11/14/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeftMenuVC : UIViewController
{
    IBOutlet UIImageView *mainImage;
}
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *userFN;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@property (nonatomic, assign)  BOOL fromActivitySelector;


@end
