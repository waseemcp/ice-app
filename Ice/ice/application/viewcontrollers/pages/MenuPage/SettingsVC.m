//
//  SettingsVC.m
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingsTVCell.h"

@interface SettingsVC ()<UITableViewDataSource, UITableViewDelegate>
{
    IBOutlet UITableView *mainTV;
}

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void)initUI
{
    mainTV.delegate = self;
    mainTV.dataSource = self;
    [mainTV setScrollEnabled:NO];
    [mainTV setContentInset:UIEdgeInsetsMake(15,0,0,0)];

}

- (void)initData
{
    
}

#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return appController.settingTitles.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingsTVCell *cell = (SettingsTVCell*) [tableView dequeueReusableCellWithIdentifier:@"SettingsTVCell"];
    if (cell == nil) {
        cell = [[SettingsTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SettingsTVCell"];
    }
    
    if (indexPath.row == 0){
        [cell.nameLbl setHidden:NO];
        [cell.arrowIv setHidden:YES];
    } else {
        [cell.nameLbl setHidden:YES];
        [cell.arrowIv setHidden:NO];
    }
    
    [cell.titleLbl setText:appController.settingTitles[indexPath.row]];
    
    return cell;
}





@end
