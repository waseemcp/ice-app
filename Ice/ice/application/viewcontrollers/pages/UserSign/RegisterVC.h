//
//  RegisterVC.h
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EHPlainAlert/EHPlainAlert.h>
@interface RegisterVC : UserBaseViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *backTV;
@property (weak, nonatomic) IBOutlet UITextField *iceBrakerTF;
@property (weak, nonatomic) IBOutlet UIView *profileNameView;
@property (weak, nonatomic) IBOutlet UIView *userNameView;
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIView *confirmPswdView;
@property (weak, nonatomic) IBOutlet UILabel *profileNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *pswdLbl;
@property (weak, nonatomic) IBOutlet UILabel *confirmPswdLbl;

@end
