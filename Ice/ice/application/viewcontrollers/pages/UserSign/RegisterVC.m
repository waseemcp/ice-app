//
//  RegisterVC.m
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "RegisterVC.h"
#import "UIViewController+KNSemiModal.h"
#import <IQKeyboardManager.h>
#import "SZTextView.h" //https://github.com/glaszig/SZTextView  UITextView Placeholder

@interface RegisterVC ()<UIScrollViewDelegate, UITextFieldDelegate,UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    UITextField *currentTextField;
    IBOutlet UITextField *profileNameTF, *userNameTF, *emailTF, *pswdTF, *confirmPswdTF;
    
    //    profile image upload
    IBOutlet UIImageView *userProfileIV;
    BOOL isEditing, noCamera, isProfileImageChanged;
    UIImage *changedImage;
    UIView *redview;
    
    
    /* Ice Breaker TextView */
    
    IBOutlet SZTextView *iceBreakerTv;
}

@property (nonatomic, strong) IBOutlet UIView *photoPickContainerView;
@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
    [self resetAllBorderColors];
    _backTV.text = @"ICE Breaker";
    _backTV.textColor = [UIColor lightTextColor];
    iceBreakerTv.delegate = self;
    _backTV.delegate = self;
    _iceBrakerTF.delegate = self;
}

- (void)initUI
{
    [scrollView setContentSize:contentView.frame.size];
    [scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen)]];
    scrollView.delegate = self;
    
    profileNameTF.delegate =
    userNameTF.delegate =
    emailTF.delegate =
    pswdTF.delegate =
    confirmPswdTF.delegate = self;
    profileNameTF.autocapitalizationType = UITextAutocapitalizationTypeWords;
    
    
    UIView *cameraView = (UIView*) [self.view viewWithTag:101];
    [commonUtils setRoundedRectBorderView:cameraView withBorderWidth:1.0f withBorderColor:RGBA(255, 255, 255, 0.09) withBorderRadius:0.0f];
    
    /* Change UITextField PlaceHolder Color */
    [commonUtils changeUITextFiledPlaceHolderColor:profileNameTF withPlaceHolderText:@"Profile name" withColor:RGBA(255, 255, 255, 0.5)];
    [commonUtils changeUITextFiledPlaceHolderColor:userNameTF withPlaceHolderText:@"User name" withColor:RGBA(255, 255, 255, 0.5)];
    [commonUtils changeUITextFiledPlaceHolderColor:emailTF withPlaceHolderText:@"E mail" withColor:RGBA(255, 255, 255, 0.5)];
    [commonUtils changeUITextFiledPlaceHolderColor:pswdTF withPlaceHolderText:@"Password" withColor:RGBA(255, 255, 255, 0.5)];
    [commonUtils changeUITextFiledPlaceHolderColor:confirmPswdTF withPlaceHolderText:@"Confirm Password" withColor:RGBA(255, 255, 255, 0.5)];
     [commonUtils changeUITextFiledPlaceHolderColor:_iceBrakerTF withPlaceHolderText:@"ICE Breaker" withColor:RGBA(255, 255, 255, 0.5)];
    /* Ice Breaker TextView */
    iceBreakerTv.delegate = self;
    CGFloat inset = 1.0;
    // ios 7
    if ([UITextView instancesRespondToSelector:@selector(setTextContainerInset:)]) {
        iceBreakerTv.textContainerInset = UIEdgeInsetsMake(inset, inset, inset, inset);
    } else {
        iceBreakerTv.contentInset = UIEdgeInsetsMake(inset, inset, inset, inset);
    }

}

- (void)initData
{
    isEditing = NO;
    noCamera = NO;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        noCamera = YES;
    }
    isProfileImageChanged = NO;
    changedImage = nil;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
        [self resetAllBorderColors];
     [[IQKeyboardManager sharedManager] setEnable:false];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
     [[IQKeyboardManager sharedManager] setEnable:true];
}

- (IBAction)onSignUp:(id)sender {
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    isEditing = NO;
    [currentTextField resignFirstResponder];
    
    if (profileNameTF.text.length>0 && userNameTF.text.length>0 && emailTF.text.length>0 && pswdTF.text.length >0 && confirmPswdTF.text.length >0 && pswdTF.text.length>=6 && confirmPswdTF.text.length>=6) {
        if ([pswdTF.text isEqualToString:confirmPswdTF.text]) {
            
        
        if ([commonUtils validateEmail:emailTF.text]) {
             [self signup];
        }
        else {
            [self removeTwoRedView];
            [self resetAllBorderColors];
            [self createRedView:_userNameView.frame.origin.x :_userNameView.frame.origin.y+_userNameView.frame.size.height:_userNameView.frame.size.width :_emailView.frame.origin.y-_userNameView.frame.origin.y];

              [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Email is not valid" type:ViewAlertPanic];
           
        }
            }
        else {
            [self removeRedView];
            [self resetAllBorderColors];

            [self createTwoRedView:_emailView.frame.origin.x :_emailView.frame.origin.y+_emailView.frame.size.height:_emailView.frame.size.width :_passwordView.frame.origin.y-_emailView.frame.origin.y :-2000];
            [self createTwoRedView:_passwordView.frame.origin.x :_passwordView.frame.origin.y+_passwordView.frame.size.height:_passwordView.frame.size.width :_confirmPswdView.frame.origin.y-_passwordView.frame.origin.y :-3000];
             [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Password & Confirm Password Do Not Match" type:ViewAlertPanic];
      
        }
    }
    else if(profileNameTF.text.length==0){
        _profileNameView.layer.borderColor = [UIColor redColor].CGColor;
        _profileNameView.layer.borderWidth = 1.0;
          [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Please Provide Profile Name" type:ViewAlertPanic];
        
    }
    else if(userNameTF.text.length==0){
        [self resetAllBorderColors];
        [redview removeFromSuperview];
        redview = nil;
        [self createRedView:_profileNameView.frame.origin.x :_profileNameView.frame.origin.y+_profileNameView.frame.size.height:_profileNameLbl.frame.size.width :_userNameView.frame.origin.y-_profileNameView.frame.origin.y];
         [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Please Provide User Name" type:ViewAlertPanic];
     
    }
    else if(emailTF.text.length == 0){
         [self resetAllBorderColors];
        [self createRedView:_userNameView.frame.origin.x :_userNameView.frame.origin.y+_userNameView.frame.size.height:_userNameView.frame.size.width :_emailView.frame.origin.y-_userNameView.frame.origin.y];
        [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Please Provide Email" type:ViewAlertPanic];

    }
    else if(pswdTF.text.length == 0){
         [self resetAllBorderColors];
          [self createRedView:_emailView.frame.origin.x :_emailView.frame.origin.y+_emailView.frame.size.height:_emailView.frame.size.width :_passwordView.frame.origin.y-_emailView.frame.origin.y];
        [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Please Provide Password" type:ViewAlertPanic];
      
    }
    else if(confirmPswdTF.text.length == 0){
         [self resetAllBorderColors];
       [self createRedView:_passwordView.frame.origin.x :_passwordView.frame.origin.y+_passwordView.frame.size.height:_passwordView.frame.size.width :_confirmPswdView.frame.origin.y-_passwordView.frame.origin.y];
        [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Please Provide Confirm Password" type:ViewAlertPanic];
    
    }
    else if (pswdTF.text.length >0 && pswdTF.text.length<6){
         [self resetAllBorderColors];
         [self createRedView:_emailView.frame.origin.x :_emailView.frame.origin.y+_emailView.frame.size.height:_emailView.frame.size.width :_passwordView.frame.origin.y-_emailView.frame.origin.y];
        [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Password Must Be of Atleast 6 Chracters" type:ViewAlertPanic];

  
    }
    else if (confirmPswdTF.text.length >0 && confirmPswdTF.text.length<6){
         [self resetAllBorderColors];
        [self createRedView:_passwordView.frame.origin.x :_passwordView.frame.origin.y+_passwordView.frame.size.height:_passwordView.frame.size.width :_confirmPswdView.frame.origin.y-_passwordView.frame.origin.y];
        [EHPlainAlert showAlertWithTitle:@"Error!" message:@"Confirm Password Must Be of Atleast 6 Chracters" type:ViewAlertPanic];

       
    }
    else {
        [commonUtils showAlert:@"Warning!" withMessage:@"Please provide all information"];
    }
   
    //[self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SidePanelVC"] animated:YES];
}


#pragma mark - Profile Photo Change

- (IBAction)onProfilePhotoChange:(id)sender {
    if(self.isLoadingUserBase) return;
    
    if(isEditing) {
        [currentTextField resignFirstResponder];
        isEditing = NO;
    }
    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];
}
- (IBAction)onClickGallery:(id)sender {
    if(self.isLoadingUserBase) return;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.navigationBar.tintColor = [UIColor blackColor];
    
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (IBAction)onClickCamera:(id)sender {
    if(self.isLoadingUserBase) return;
    
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:picker animated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    changedImage = info[UIImagePickerControllerEditedImage];
    [userProfileIV setContentMode:UIViewContentModeScaleAspectFill];
    [userProfileIV setImage:changedImage];
    
    isProfileImageChanged = YES;
    [self dismissSemiModalView];
    [picker dismissViewControllerAnimated:NO completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self dismissSemiModalView];
    
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(self.isLoadingUserBase) return NO;
    currentTextField = textField;
    isEditing = YES;

    float offset = 0;
    
    if(currentTextField == profileNameTF) {
        offset = 0;
    } else if(currentTextField == userNameTF) {
        offset = 0;
    } else if(currentTextField == emailTF) {
        offset = 50;
    } else if(currentTextField == pswdTF) {
        offset = 100;
    } else if(currentTextField == confirmPswdTF) {
        offset = 150;
    }
    [scrollView setContentOffset:CGPointMake(0, offset) animated:YES];
    
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(self.isLoadingUserBase) return NO;
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    isEditing = NO;
    return [textField resignFirstResponder];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [self resetAllBorderColors];
    [self removeRedView];
    [self removeTwoRedView];
    return YES;
}
#pragma mark - TextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(self.isLoadingUserBase) return NO;
    [scrollView setContentOffset:CGPointMake(0, 250) animated:YES];
   //  [self performSelector:@selector(setCursorToBeginning:) withObject:textView afterDelay:0.01];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if(textView == iceBreakerTv){
        NSLog(@"text view text %@",textView.text);
        if ([_backTV.text isEqualToString:@"ICE Breaker"]) {
            _backTV.text = @"";
            _backTV.textColor = [UIColor lightTextColor];
            
        }
        
        
    }
    else{
        if ([text isEqualToString:@"\n"]) {
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            isEditing = NO;
            return [textView resignFirstResponder];
        }
        
        // Limit line of UITextView is 2
        NSString *newText = [textView.text stringByReplacingCharactersInRange:range withString:text];
        CGFloat textWidth = CGRectGetWidth(UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset));
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        
        CGSize boundingRect = [self sizeOfString:newText constrainedToWidth:textWidth font:textView.font];
        NSInteger numberOfLines = boundingRect.height / textView.font.lineHeight;
        
        return numberOfLines <= 2;
    }
    //    if (iceBreakerTv.text.length==0) {
    //        iceBreakerTv.text = @"ICE Breaker";
    //    }
    
    return YES;
}
-(void) textViewDidChange:(UITextView *)textView
{
    
    if(iceBreakerTv.text.length == 0){
        
        _backTV.text = @"ICE Breaker";
        [_backTV resignFirstResponder];
    }
}
- (void)textViewDidBeginEditing:(UITextView *)inView
{
   
}

- (void)setCursorToBeginning:(UITextView *)inView
{
    //you can change first parameter in NSMakeRange to wherever you want the cursor to move
    inView.selectedRange = NSMakeRange(0, 0);
}

-(CGSize)sizeOfString:(NSString *)string constrainedToWidth:(double)width font:(UIFont *)font
{
    return  [string boundingRectWithSize:CGSizeMake(width, DBL_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil].size;
}

- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor blueColor] setFill];
//    [[self placeholder] drawInRect:rect withFont:[UIFont systemFontOfSize:16]];
}

#pragma mark - ScrollView Tap
- (void) onTappedScreen {
    if (self.isLoadingUserBase) return;
    [self.view endEditing:YES];
    [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
-(void)signup {
        [self resetAllBorderColors];
    [self removeRedView];
    [self removeTwoRedView];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found!"];
    }
    else {
        //there-is-no-connection warning
    
  //  NSLog(@"%@", [NSString stringWithFormat:@"%@",[commonUtils getUserDefault:@"currentLatitude"]]);
//    if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
        if (userNameTF.text.length>0 && profileNameTF.text.length>0 && emailTF.text.length>0 && pswdTF.text.length>0 && confirmPswdTF.text.length>0) {
            [commonUtils showHud:self.view];
            
          
            NSString *deviceId = @"abcdefghijklmnopqrstuvwxyzabcdefghi";
            NSString *address,*currentLatitude,*currentLongtitude;
       
               if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
                   if ([commonUtils getUserDefault:@"location"]) {
                       address =  [commonUtils getUserDefault:@"location"];
                   }
                   else{
                        address =  @"";
                   }
                   currentLatitude = [commonUtils getUserDefault:@"currentLatitude"];
                  currentLongtitude = [commonUtils getUserDefault:@"currentLongitude"];
               }
               else {
                   address = @"";
                   currentLatitude = @"";
                   currentLongtitude = @"";
               }
            
            NSString *timeZoneOffset = [commonUtils getTimeZone];
            NSLog(@"device id is %@ address is %@ timezone seconds are %@",deviceId,address,timeZoneOffset);
            
                            // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
                
                //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
          
            [_params setObject:emailTF.text forKey:@"email"];
            [_params setObject:pswdTF.text forKey:@"password"];
              [_params setObject:[NSString stringWithFormat:@"%@",userNameTF.text] forKey:@"username"];
            [_params setObject:address forKey:@"location"];
            [_params setObject:currentLatitude forKey:@"lat"];
             [_params setObject:currentLongtitude forKey:@"lng"];
             [_params setObject:profileNameTF.text forKey:@"full_name"];
             [_params setObject:[NSString stringWithFormat:@"ios"] forKey:@"device_type"];
            if (TARGET_IPHONE_SIMULATOR) {
                [_params setObject:[NSString stringWithFormat:@"%@", @"d9381cfc906fcb487396d17dceb376b83512d7c0f365422e248e0bd2c9216906"] forKey:@"device_id"];
            }
            else{
                 if ([commonUtils getUserDefault:@"user_apns_id"]){
                     [_params setObject:[NSString stringWithFormat:@"%@", [commonUtils getUserDefault:@"user_apns_id"]] forKey:@"device_id"];
                 }else {
                        [_params setObject:@"" forKey:@"device_id"];
                 }
                
            }
            

             [_params setObject:timeZoneOffset forKey:@"timezone"];
            NSLog(@"ice breaker tv is %@",_iceBrakerTF.text);
//            if (iceBreakerTv.text.length>0) {
                 [_params setObject:_iceBrakerTF.text forKey:@"ice_breaker"];
//            }
            
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
            NSString* FileParamConstant = @"pic";
            NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"register"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:Url];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:30];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
            NSLog(@"body =====> %@",body);
            NSData *imageData = UIImageJPEGRepresentation(changedImage, 0.5);
            if (imageData) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  //NSLog(@"Server Response %@",response);
                                                  NSLog(@"dictionary Registert ===> %@",dictionary);
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                               
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  if([statusis isEqualToString:@"success"]){
                                                      commonUtils.cominfFromFB = NO;
                                                      NSDictionary *successDic = [dictionary valueForKey:@"successData"];
                                                      [commonUtils saveUserdata:successDic];
                                                      [NSUserDefaults.standardUserDefaults setValue:[NSString stringWithFormat:@"%@",[successDic valueForKey:@"session_token"]] forKey:@"session_token"];
                                                      commonUtils.userData = [commonUtils getUserData];
                                                      commonUtils.cominfFromFB = NO;
                                                      commonUtils.sessionToken = [NSUserDefaults.standardUserDefaults objectForKey:@"session_token"];

                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                              [commonUtils hideHud];
                                                          
                                                          
                                                          [commonUtils showAlert:@"Success" withMessage:@"Congratulation!You are successfully signed up"];
                                                          [self.navigationController pushViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"SidePanelVC"] animated:YES];
                                                         
                                                          
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          dispatch_async(dispatch_get_main_queue(), ^{
                                                              [commonUtils hideHud];
                                                          });
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  NSLog(@"no data returned");
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                        [commonUtils hideHud];
                                                  });
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                       [commonUtils hideHud];
                                                  });
                                                  NSLog(@"Server not responding");
                                                  //couldn't download
                                                  
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
        
        }
        
        else {
            [commonUtils showAlert:@"Warning!" withMessage:@"Please Fill All Fields"];
        }
        
       // }
    
    
    
    
//    else {
//        [commonUtils showAlert:@"Error!" withMessage:@"Unable To Get Your Location.Please Allow The App To Get Your Location"];
//    }
    }
}

-(void)resetAllBorderColors{
    
    _userNameView.layer.borderColor = [UIColor clearColor].CGColor;
    _userNameView.layer.borderWidth = 1.0;
    _emailView.layer.borderColor = [UIColor clearColor].CGColor;
    _emailView.layer.borderWidth = 1.0;
    _profileNameView.layer.borderColor = [UIColor clearColor].CGColor;
    _profileNameView.layer.borderWidth = 1.0;
    _passwordView.layer.borderColor = [UIColor clearColor].CGColor;
    _passwordView.layer.borderWidth = 1.0;
    _confirmPswdView.layer.borderColor = [UIColor clearColor].CGColor;
    _confirmPswdView.layer.borderWidth = 1.0;
   
}

-(void)createRedView :(CGFloat)withX :(CGFloat)withY :(CGFloat)withWidth :(CGFloat)withHeight{
  
  redview = [[UIView alloc]initWithFrame:CGRectMake(withX, withY, withWidth, withHeight)];
    redview.tag = -1000;
   // view.alpha = 0.2;
    redview.userInteractionEnabled = NO;
    redview.layer.backgroundColor = [UIColor clearColor].CGColor;
    redview.layer.borderColor = [UIColor redColor].CGColor;
    redview.layer.borderWidth = 1.0;
    UIView *newView = [contentView viewWithTag:-1000];
    newView.alpha = 0;
    [newView removeFromSuperview];
    [contentView addSubview:redview];
    
}
-(void)createTwoRedView :(CGFloat)withX :(CGFloat)withY :(CGFloat)withWidth :(CGFloat)withHeight :(int)withTag{
    UIView *Tworedview = [[UIView alloc]initWithFrame:CGRectMake(withX, withY, withWidth, withHeight)];
    Tworedview.userInteractionEnabled = NO;
    Tworedview.layer.backgroundColor = [UIColor clearColor].CGColor;
    Tworedview.layer.borderColor = [UIColor redColor].CGColor;
    Tworedview.layer.borderWidth = 1.0;
    Tworedview.tag = withTag;
      [contentView addSubview:Tworedview];
}
-(void)removeTwoRedView{
    UIView *newView = [contentView viewWithTag:-2000];
    newView.alpha = 0;
    [newView removeFromSuperview];
    UIView *newView2 = [contentView viewWithTag:-3000];
    newView2.alpha = 0;
    [newView2 removeFromSuperview];
}
-(void)removeRedView{
    UIView *newView = [contentView viewWithTag:-1000];
    newView.alpha = 0;
    [newView removeFromSuperview];
}
@end
