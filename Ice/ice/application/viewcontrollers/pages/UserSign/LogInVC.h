//
//  LogInVC.h
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EHPlainAlert/EHPlainAlert.h>
@interface LogInVC : UserBaseViewController
@property (weak, nonatomic) IBOutlet UIView *userNameView;
@property (weak, nonatomic) IBOutlet UIView *pswdView;

@end
