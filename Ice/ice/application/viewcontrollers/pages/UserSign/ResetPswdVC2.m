//
//  ResetPswdVC2.m
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "ResetPswdVC2.h"
#import "WelcomeVC.h"
@interface ResetPswdVC2 ()

@end

@implementation ResetPswdVC2
@synthesize userEmail;
- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self showUserData];
}


- (IBAction)resetPswdEmail:(id)sender {
    [self resetPassword];
    
}
-(void)resetPassword{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        //there-is-no-connection warning
    
        [commonUtils showHud:self.view];
    
    
    
    
    
    // NSData *data =  [NSURLConnection sendSynchronousRequest:rq returningResponse:&res error:&err];
    
    //NSLog(@"response: %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);//To print respose of server in string format,whatever type of response is
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    
    
    [_params setObject:userEmail forKey:@"email"];
    

    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ

    NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"forgetpassword"];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:Url];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];

    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data

    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    NSError *err = nil;
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *data, NSURLResponse *response, NSError *error) {
                                      if ([data length] > 0 && err == nil){
                                          NSError* error;
                                          NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                     options:kNilOptions
                                                                                                       error:&error];
                                          //NSLog(@"Server Response %@",response);
                                          NSLog(@"dictionary %@",dictionary);
                                          NSString *message = [dictionary valueForKey:@"errorMessage"];
                                          
                                          NSString *statusis = [dictionary valueForKey:@"status"];
                                          if([statusis isEqualToString:@"success"]){
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  NSString *successMessage = [dictionary valueForKey:@"successMessage"];
                                                   [commonUtils hideHud];
                                                  [self showAlertWithAction:successMessage];
                                                  
                                                  
                                              });
                                          }
                                          if(![statusis isEqualToString:@"success"]){
                                              dispatch_async(dispatch_get_main_queue(), ^{
                                                  [commonUtils hideHud];
                                                  [commonUtils showAlert:@"Error!" withMessage:message];
                                              });
                                              
                                          }
                                          
                                      }
                                      else if ([data length] == 0 && err == nil){
                                          NSLog(@"no data returned");
                                           [commonUtils hideHud];
                                          //no data, but tried
                                      }
                                      else if (err != nil)
                                      {
                                           [commonUtils hideHud];
                                          NSLog(@"Server not responding");
                                          //couldn't download
                                          
                                      }
                                      
                                      
                                      
                                  }];
    [task resume];
    
    
    }
    
    
}
-(void)showUserData
{
    
        if (![[commonUtils.userData valueForKey:@"photo"] isKindOfClass:[NSNull class]]&&![[commonUtils.userData valueForKey:@"photo"]isEqualToString:@""]&&[commonUtils.userData valueForKey:@"photo"]!=nil) {
            NSString *imageUrlString = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"photo"]];
            NSURL *url = [NSURL URLWithString:imageUrlString];
            
            NSData *data = [NSData dataWithContentsOfURL:url];
            CGSize sizeOfImage = CGSizeMake(50, 50);
            UIImage *image = [self imageByCroppingImage:[UIImage imageWithData:data] toSize:sizeOfImage];
           _userImage.image = image;
        }
        else {
            [_userImage setImage:[UIImage imageNamed:@"user-avatar"]];
        }
    if ([commonUtils.userData valueForKey:@"first_name"]) {
        _userFN.text = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"first_name"]];
        _descLbl.text = [NSString stringWithFormat:@"Hi %@. How would you like to reset your password?",_userFN.text];
    }
    
    
    
}
-(void)showAlertWithAction:(NSString*)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Email Sent" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
  
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        [self turnBackToAnOldViewController];
        //Code for OK button
    }
   
}
- (void)turnBackToAnOldViewController{
    
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        //Do not forget to import AnOldViewController.h
        if ([controller isKindOfClass:[WelcomeVC class]]) {
            
            [self.navigationController popToViewController:controller
                                                  animated:YES];
            break;
        }
    }
}
- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}

@end
