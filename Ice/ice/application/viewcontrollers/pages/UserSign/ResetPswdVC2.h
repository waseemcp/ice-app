//
//  ResetPswdVC2.h
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPswdVC2 : UserBaseViewController<UIAlertViewDelegate>
@property (atomic) NSString *userEmail;
- (IBAction)resetPswdEmail:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *userFN;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@end
