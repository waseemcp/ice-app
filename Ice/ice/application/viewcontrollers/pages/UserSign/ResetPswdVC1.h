//
//  ResetPswdVC1.h
//  ICE
//
//  Created by LandToSky on 11/11/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EHPlainAlert/EHPlainAlert.h>
@interface ResetPswdVC1 : UserBaseViewController
- (IBAction)findBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *emailView;

@end
