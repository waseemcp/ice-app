//
//  ICEVC.m
//  ICE
//
//  Created by LandToSky on 1/12/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "ICEVC.h"

#import "EventImageCVCell.h"
#import "UIViewController+KNSemiModal.h"
#import "ZFTokenField.h"

#import "CustomIOSAlertView.h"
#import "SaveGroupAlertView.h"
#import "IcedDoneAlertView.h"
#import "PickerViewController.h"
#import "HomeVC.h"
#import "InviteFriendTVCell.h"
#import "InviteGroupsTVCell.h"
#import "locationPickerViewController.h"
#import "NSDate+Compare.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <Social/Social.h>
#import "imagesCell.h"
#import <Social/Social.h>
#import <IQKeyboardManager.h>
#import <SafariServices/SafariServices.h>
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "GMImagePickerController.h"
#import "AppDelegate.h"
#import <TwitterKit/TwitterKit.h>
#import <GooglePlaces/GooglePlaces.h>
#import "GKImagePicker.h"
#import "PhotoTweaksViewController.h"
#import <DZNPhotoPickerController/DZNPhotoEditorViewController.h>
#import <DZNPhotoPickerController/UIImagePickerController+Edit.h>
#import "AppDelegate.h"


@interface ICEVC ()<UICollectionViewDelegate, UICollectionViewDataSource,UIImagePickerControllerDelegate, UINavigationControllerDelegate,ZFTokenFieldDataSource, ZFTokenFieldDelegate, PickerViewControllerDelegate, UITextViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource,FBSDKSharingDelegate,UIGestureRecognizerDelegate,GMImagePickerControllerDelegate,UIAlertViewDelegate,GMSAutocompleteViewControllerDelegate,PhotoTweaksViewControllerDelegate>
{
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIView *contentView;
    UITextField *currentTextField;
    
    // Top View
    BOOL isPrivate;
    IBOutlet UITextField *iceTitleTxt;
    IBOutlet UIButton *btn;
    IBOutlet UIImageView *iceCheckImg;
    IBOutlet UIButton *iceCheckBtn;
    IBOutlet UIImageView *imgViewMenu;
    
    // Event Photo & Video CollectionView
    IBOutlet UICollectionView *photoCv;
    NSMutableArray *eventDatas;
    
    // Starts and Ends Date Label;
    IBOutlet UILabel *startsDateLbl;
    IBOutlet UILabel *startsTimeLbl;
    IBOutlet UILabel *endsDateLbl;
    IBOutlet UILabel *endsTimeLbl;
    
    NSTimeInterval durationInMinutes;
    // Add More Photo
    BOOL isEditing, noCamera, isProfileImageChanged;
    UIImage *changedImage;
    IBOutlet UIView *addImageView;
    IBOutlet UIView *addImageViewNew;
    IBOutlet UITextField *inviteFriendTxt;
    IBOutlet UITableView *inviteFriendsTV;
    
    BOOL isSharingShown;
    // Invite Friend Tag List View
    IBOutlet UIView *inviteFriendsView;
    CGFloat increaseInviteTokenViewH;
    IBOutlet ZFTokenField *tokenField;
    NSMutableArray *tokens;
    BOOL isGuestCanInvite,addApiCalled;

    
    // Detail
    IBOutlet UIView *detailView;
    BOOL isDetailEditing,isStartDate,isEndDate;
    IBOutlet UIView *detailContainerView;
    CGFloat keyboardHeight;
    IBOutlet UITextView *detailTxt;
    NSMutableArray *imagesArray;
    // Reminder
    IBOutlet UIView *reminderView;
    
    // Save Group As AlertView
    CustomIOSAlertView *saveGroupAlertView;
    
    // Iced Done Alert View
    CustomIOSAlertView *icedDoneAlertView;
    CustomIOSAlertView *visibleAlertView;
    IcedDoneAlertView *customAlertView;
    NSMutableArray *usersArray,*usersMutableCopy,*userGroupsMutableCopy;
   
    NSString *pickedAddress;
    NSString *userID;
    NSDateFormatter *dateformatter;
    NSString *iceStartTime ,*iceEndTime;
    CLLocationCoordinate2D *emptyCord;
    BOOL m_postingInProgress;
    NSMutableArray *btncheckedArray,*imagesArrayCopy,*invitedGroups,*userGroups,*inviteGroupIDs,*inviteUserIDs,*videosArrayData;
    UIImage *imageToUpload;
    SaveGroupAlertView *saveGroupAlert;
    int totalVideos;
    BOOL isUsersShown;
    
    BOOL isImagedone;
    
    BOOL shouldShowMaxVideoAlert;
    int totalImages;
    float totalSize,recentSize;
 
    NSString *ice_id;
    BOOL can_share_twitter;
    
    
    NSDate *startSelected , *start;
    NSDate *endSelected , *end;
    UIView *transparentView;
    BOOL endTimeNotValid;
    NSString *shortAddress;
    
}
@property (nonatomic, strong) IBOutlet UIView *photoPickContainerView;
@property (nonatomic, strong) GKImagePicker *imagePicker;
@property (nonatomic, strong) UIPopoverController *popoverController;
@end

@implementation ICEVC
@synthesize pickerElements,hourPicker,shouldNotClear;
- (void)viewDidLoad {
    [super viewDidLoad];
        [GIDSignIn sharedInstance].uiDelegate = self;
    [self setInitialStartAndEndTime];

   detailTxt.text = @"";
    can_share_twitter = YES;
    commonUtils.shouldNotChangeEndDate = NO;
    [self initUI];
    [self initData];
    self.pendingAlertViews = [NSMutableArray new];
}

- (void)initUI
{
    
   // [self.view bringSubviewToFront:inviteFriendsView];
    [scrollView setContentSize:contentView.frame.size];

    [commonUtils setRoundedRectBorderView:[self.view viewWithTag:101] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#f8f8f8" alpha:1.0f] withBorderRadius:0.0f];
    [commonUtils setRoundedRectBorderView:[self.view viewWithTag:102] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#f8f8f8" alpha:1.0f] withBorderRadius:0.0f];
    [commonUtils setRoundedRectBorderView:[self.view viewWithTag:103] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#f8f8f8" alpha:1.0f] withBorderRadius:0.0f];
    [commonUtils setRoundedRectBorderButton:[self.view viewWithTag:104] withBorderWidth:1.0f withBorderColor:[UIColor colorWithHex:@"#c6a6dd" alpha:1.0f] withBorderRadius:0.0f];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ChangeTime:)
                                                 name:@"AddIceTime"
                                               object:nil];
    
    
    iceTitleTxt.delegate = self;
    
    tokenField.dataSource = self;
    tokenField.delegate = self;
    tokenField.textField.enabled = NO;
    _houtMintTF.delegate = self;
    inviteFriendTxt.userInteractionEnabled = NO;
    inviteFriendTxt.delegate = self;
    _reminderTF.delegate = self;
    _imageToChooseView.userInteractionEnabled = YES;
    _imgCollectionView.userInteractionEnabled = YES;
    [inviteFriendsTV setHidden:YES];
    inviteFriendsTV.delegate = self;
    inviteFriendsTV.dataSource = self;
    
    detailTxt.delegate = self;
    
   [self.view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTappedScreen:)]];
    
    increaseInviteTokenViewH = 0.0f;
    imagesArray = [[NSMutableArray alloc]init];
    usersArray = [[NSMutableArray alloc]init];
    videosArrayData = [NSMutableArray new];
    usersMutableCopy=userGroupsMutableCopy = [[NSMutableArray alloc]init];
    [inviteFriendTxt addTarget:self
                         action:@selector(textFieldDidChange:)
               forControlEvents:UIControlEventEditingChanged];
    _imgCollectionView.pagingEnabled = YES;
    
    imagesArrayCopy = [[NSMutableArray alloc]init];
    pickerElements = @[@"Minutes", @"Hours"];
    self.hourPicker = [[UIPickerView alloc] init];
    self.hourPicker.delegate = self;
    self.hourPicker.dataSource = self;
    self.hourPicker.showsSelectionIndicator = YES;
    _houtMintTF.inputView = self.hourPicker;
    
    commonUtils.shouldPickPreviousStartTime = NO;
    commonUtils.shouldPickPreviousEndTime = NO;
    
}

- (void)keyboardWillShow:(NSNotification *)notification {
    keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
}


- (void) ChangeTime:(NSNotification *) notification{
    NSLog(@"Calllllllllllllllll=================>");
    
    NSString *strValue = [[NSUserDefaults standardUserDefaults] valueForKey:@"time"];
    
    NSString *startTimeDateString = [NSString stringWithFormat:@"%@ %@",startsDateLbl.text,startsTimeLbl.text];
    NSString *endTimeDateString =  [NSString stringWithFormat:@"%@ %@",endsDateLbl.text,endsTimeLbl.text];
    
    NSLog(@"startTimeDateString %@",startTimeDateString);
    NSLog(@"endTimeDateString %@",endTimeDateString);
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc]init];
    
  
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormat setLocale:locale];
    

    if ([strValue isEqualToString:@"1"]){
    
        startTimeDateString = [startTimeDateString stringByReplacingOccurrencesOfString:@" AM"
                                             withString:@""];
        startTimeDateString = [startTimeDateString stringByReplacingOccurrencesOfString:@" PM"
                                                                             withString:@""];
        endTimeDateString = [endTimeDateString stringByReplacingOccurrencesOfString:@" PM"
                                             withString:@""];
        endTimeDateString = [endTimeDateString stringByReplacingOccurrencesOfString:@" AM"
                                                                         withString:@""];
        
        [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
    }else {
        
        [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
    }
    
    
    NSDate *dateStart = [dateFormat dateFromString:startTimeDateString];
    NSDate *dateEnd = [dateFormat dateFromString:endTimeDateString];
    
    
    [dateFormat setDateFormat:@"MMM d, yyyy"];
    
    startsDateLbl.text = [dateFormat stringFromDate:dateStart];
    endsDateLbl.text = [dateFormat stringFromDate:dateEnd];
    
    if ([commonUtils is24hourFormat]) {
        [dateFormat setDateFormat:@"HH:mm"];
    }else {
        [dateFormat setDateFormat:@"hh:mm a"];
    }
    
    startsTimeLbl.text = [dateFormat stringFromDate:dateStart];
    endsTimeLbl.text = [dateFormat stringFromDate:dateEnd];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
    if (CLLocationCoordinate2DIsValid(commonUtils.pickedLocationCord)&&!(commonUtils.pickedLocationCord.latitude==0)&&!(commonUtils.pickedLocationCord.longitude==0)){
        [self checkTextfieldValue];
    }
    [detailTxt setEditable:YES];
     [self getUsers];
     [self getUserGroups];
    

   
}

- (void)initData
{
    isPrivate = NO;
    isGuestCanInvite = YES;
    totalVideos = 0;
    totalImages = 0;
    isEditing = NO;
    noCamera = NO;
    shouldNotClear = YES;
    shouldShowMaxVideoAlert = NO;
    durationInMinutes = 1 * 60 * 60;
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        noCamera = YES;
    }
    isProfileImageChanged = NO;
    changedImage = nil;
      tokens = [NSMutableArray array];
    
    isDetailEditing = NO;
 
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _reminderTF.inputAccessoryView = numberToolbar;
    invitedGroups = [[NSMutableArray alloc]init];
    userGroups = [[NSMutableArray alloc]init];
    inviteUserIDs = [[NSMutableArray alloc]init];
    inviteGroupIDs = [[NSMutableArray alloc]init];
    addApiCalled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{

    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receiveToggleAuthUINotification:)
     name:@"ToggleAuthUINotification"
     object:nil];
    iceCheckImg.alpha = 0.2;
    iceCheckBtn.enabled = NO;
    
    if (!shouldNotClear) {
        [self clearAddIceData];
        [self resetCameraView];
        [self performSelector:@selector(resetCameraView2) withObject:nil afterDelay:0.25];
    }


   if (self.isshowMenu) {
       imgViewMenu.image = [UIImage imageNamed:@"arrow-left-black"];
   }else {
       imgViewMenu.image = [UIImage imageNamed:@"menu-icon"];
   }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.navigationController setNavigationBarHidden:YES];
    });
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   
    [self.view endEditing:YES];
    _onDetailOutlet.hidden = NO;
    
}

#pragma mark - ColelctionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _imgCollectionView) {
        return   imagesArrayCopy.count;
    }
    else {
        if (imagesArray.count>0) {
            return imagesArray.count;
        }
        else {
            return 0;
        }
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _imgCollectionView) {
        imagesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"imgToChooseCell" forIndexPath:indexPath];
        cell.imgBtn.tag = indexPath.row;
    
        cell.imageToChoose.image = imagesArrayCopy[indexPath.row];
      
       
        return cell;
    }
    else {
        EventImageCVCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventImageCVCell" forIndexPath:indexPath];
        NSString *strValue = (NSString *)imagesArray[indexPath.row][@"IsImage"] ;
        
        if ([strValue isEqualToString:@"image"]) {
         
            cell.eventIv.image = imagesArray[indexPath.row][@"image"];
            cell.videoPlayBtn.hidden = YES;
        }
        else{

            dispatch_async(dispatch_get_main_queue(), ^(void){
                if ([imagesArray[indexPath.row][@"pickedType"]isEqualToString:@"galery"]) {
                    
                
                    NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[indexPath.row][@"image"]];
                    AVURLAsset *asset = [AVURLAsset assetWithURL:assetURL];
               
                        
                    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                    generate.appliesPreferredTrackTransform = YES;
                    NSError *err = NULL;
                    CMTime time = CMTimeMake(1, 60);
                    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                    
                    UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
                    cell.eventIv.image = img;
                    cell.videoPlayBtn.hidden = NO;
                }
                else{
                    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row][@"image"] options:nil];
                    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
                    generate.appliesPreferredTrackTransform = YES;
                    NSError *err = NULL;
                    CMTime time = CMTimeMake(1, 60);
                    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
                    
                    UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
                    cell.eventIv.image = img;
                    cell.videoPlayBtn.hidden = NO;

                }
             });

        }
        cell.closeBtn.tag = indexPath.row;
        cell.videoPlayBtn.tag = indexPath.row;
        [cell.closeBtn addTarget:self action:@selector(removeImage:) forControlEvents:UIControlEventTouchUpInside];
        [cell.videoPlayBtn addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
  

    
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"did select");
    if (collectionView == _imgCollectionView) {
        imageToUpload = imagesArrayCopy[indexPath.row];
        _imageToChooseView.hidden = YES;
    }
   
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 2; // This is the minimum inter item spacing, can be more
}

#pragma mark Button Action
- (IBAction)onPrivatePublic:(id)sender
{
    
    isPrivate = !isPrivate;
    UILabel *lbl = (UILabel*)[self.view viewWithTag:1001];
    UIImageView *imageView = (UIImageView*) [self.view viewWithTag:1002];
    if (isPrivate) {
        [lbl setText:@"PRIVATE"];
        [imageView setImage:[UIImage imageNamed:@"private"]];
    } else {
        [lbl setText:@"PUBLIC"];
        [imageView setImage:[UIImage imageNamed:@"public"]];
    }
  
}

- (IBAction)onInviteFriendAdd:(id)sender
{
    if (isUsersShown) {
        isUsersShown = NO;
        [inviteFriendTxt resignFirstResponder];
        
        [inviteFriendTxt setText:@""];
        inviteFriendTxt.userInteractionEnabled = NO;
        [inviteFriendsTV setHidden:YES];
        
        //After added new token, adjust frame
        [self adjustFrameAfterAddingInviteFriend];
    }
    else{
        isUsersShown = YES;
        inviteFriendTxt.userInteractionEnabled = YES;
        [inviteFriendTxt becomeFirstResponder];
        [scrollView setContentOffset:CGPointMake(0, 348) animated:YES];
    }
 
}

- (IBAction)onGuestCanInvite:(id)sender
{
    isGuestCanInvite = !isGuestCanInvite;
    UILabel *lbl = (UILabel*)[self.view viewWithTag:1003];
    UIImageView *imageView = (UIImageView*) [self.view viewWithTag:1004];
    if (isGuestCanInvite) {
        [lbl setText:@"Guest can invite friends"];
        [imageView setImage:[UIImage imageNamed:@"guest-invite-on"]];
    } else {
        [lbl setText:@"Guest can't invite friends"];
        [imageView setImage:[UIImage imageNamed:@"guest-invite-off"]];
    }
}


- (IBAction)onLocation:(id)sender
{

     [detailTxt resignFirstResponder];
      [currentTextField resignFirstResponder];
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    shouldNotClear = YES;
    [self presentViewController:acController animated:YES completion:nil];
}


#pragma mark - Invite Friend Token View Delegate
- (void)tokenDeleteButtonPressed:(UIButton *)tokenButton
{
    //Land
    //    NSUInteger index = [tokenField indexOfTokenView:tokenButton.superview];
    NSUInteger index = tokenButton.tag;
    if (index != NSNotFound) {
        if (tokens[index][@"name"]) {
            for (int i=0; i<inviteGroupIDs.count; i++) {
                if ([NSString stringWithFormat:@"%@",tokens[index][@"id"]]==[NSString stringWithFormat:@"%@",inviteGroupIDs[i]]) {
                    [inviteGroupIDs removeObjectAtIndex:i];
                }
            }
        }
        else {
            for (int i=0; i<inviteUserIDs.count; i++) {
                if ([NSString stringWithFormat:@"%@",tokens[index][@"id"]]==[NSString stringWithFormat:@"%@",inviteUserIDs[i]]) {
                    [inviteUserIDs removeObjectAtIndex:i];
                }
            }
        }
  
        [tokens removeObjectAtIndex:index];
        [tokenField reloadData];
        [self adjustFrameAfterAddingInviteFriend];
    }
}

- (CGFloat)lineHeightForTokenInField:(ZFTokenField *)tokenField
{
    return 25;
}

- (NSUInteger)numberOfTokenInField:(ZFTokenField *)tokenField
{
    return tokens.count;
}

- (UIView *)tokenField:(ZFTokenField *)tokenField viewForTokenAtIndex:(NSUInteger)index
{
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:@"TokenView" owner:nil options:nil];
    UIView *view = nibContents[0];
    UIImageView *imageView = (UIImageView*)[view viewWithTag:1];
    UILabel *label = (UILabel *)[view viewWithTag:2];
    UIButton *button = (UIButton *)[view viewWithTag:3];
    UIView *viewMain = (UIView *)[view viewWithTag:786];
    NSLog(@"index wasemm ==> %@",tokens[index]);

    if (tokens[index][@"can_edit"]){
        if ([tokens[index]boolForKey:@"can_edit"]) {
            viewMain.hidden = true;
        }
    }
    
    
    
    if ([tokens[index]valueForKey:@"photo"]) {
     NSString *imageUrlString = [NSString stringWithFormat:@"%@",[tokens[index]valueForKey:@"photo"]];
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                     placeholderImage:[UIImage imageNamed:@"user-avatar"]
                              options:SDWebImageCacheMemoryOnly];
    }
    else {
        imageView.image = [UIImage imageNamed:@"invite-groups"];
    }
    if ([tokens[index] valueForKey:@"first_name"]) {
         label.text = [tokens[index] valueForKey:@"first_name"];
    }
    else {
        label.text =  label.text = [tokens[index] valueForKey:@"name"];
    }
    
    button.tag = index;
    [button addTarget:self action:@selector(tokenDeleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat width = [self widthForText:label.text font:[UIFont systemFontOfSize:13] withHeight:25.0f];
    view.frame = CGRectMake(0, 0, width + 60, 25);
    return view;
}

#pragma mark - ZFTokenField Delegate

- (CGFloat)tokenMarginInTokenInField:(ZFTokenField *)tokenField
{
    return 5;
}

- (void)tokenField:(ZFTokenField *)tokenField1 didReturnWithText:(NSString *)text
{
    [tokens addObject:text];
    [tokenField1 reloadData];
}

- (void)tokenField:(ZFTokenField *)tokenField didRemoveTokenAtIndex:(NSUInteger)index
{
    NSLog(@"data %@",tokens[index]);
    
    [tokens removeObjectAtIndex:index];
}

- (BOOL)tokenFieldShouldEndEditing:(ZFTokenField *)textField
{
    return NO;
}

- (CGFloat)widthForText:(NSString*)text font:(UIFont*)font withHeight:(CGFloat)height {
    
    CGSize constraint = CGSizeMake(2000.0f, height);
    CGSize size;
    
    CGSize boundingBox = [text boundingRectWithSize:constraint
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:font}
                                            context:nil].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.width;
}

#pragma mark - Invite Friend TableView Delegata
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (userGroups.count>0) {
        return 2;
    }
    else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
    if(section == 0)
        return usersArray.count;
    
    else if (section == 1)
        return userGroups.count;
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if(section == 0)
        return @"Section 1";
    else
        return @"Section 2";
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 20)];
    [view setBackgroundColor:[UIColor colorWithHex:@"#f9f6fc" alpha:1.0f]];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, tableView.frame.size.width-5, 15)];
    [label setFont:[UIFont fontWithName:@"Lato-Regular" size:11]];
    [label setTextColor:[UIColor colorWithHex:@"#c6a6dd" alpha:1.0f]];
    
    NSString *sectionTitleStr;
    if (section == 0) {
        sectionTitleStr = @"PEOPLE:";
    } else {
        sectionTitleStr = @"GROUPS:";
    }
    
    [label setText:sectionTitleStr];
    [view addSubview:label];
    return view;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        InviteFriendTVCell *cell0 = (InviteFriendTVCell*) [tableView dequeueReusableCellWithIdentifier:@"InviteFriendTVCell"];
        
        cell0.selectFriendBtn.tag = indexPath.row;
        cell0.inviteFriendNameLbl.text = [usersArray[indexPath.row][@"get_follower"]valueForKey:@"first_name"];
       
       
            NSString *imageUrlString = [NSString stringWithFormat:@"%@",[usersArray[indexPath.row][@"get_follower"]valueForKey:@"photo"]];
   
               [cell0.inviteFriendImage sd_setImageWithURL:[NSURL URLWithString:imageUrlString]
                            placeholderImage:[UIImage imageNamed:@"user-avatar"]
                                     options:SDWebImageCacheMemoryOnly];
               
    
            
        [cell0.selectFriendBtn addTarget:self action:@selector(onSelectInviteFriend:) forControlEvents:UIControlEventTouchUpInside];
        cell = cell0;
    } else if (indexPath.section == 1){
        InviteGroupsTVCell* cell1 = (InviteGroupsTVCell*) [tableView dequeueReusableCellWithIdentifier:@"InviteGroupsTVCell"];
        cell1.groupName.text = userGroups[indexPath.row][@"name"];
        cell1.selectGroupBtn.tag = indexPath.row;
        cell1.groupCount.text = [NSString stringWithFormat:@"%@",userGroups[indexPath.row][@"group_count_count"]];
        [cell1.selectGroupBtn addTarget:self action:@selector(onSelectInviteGroup:) forControlEvents:UIControlEventTouchUpInside];
        cell = cell1;
    }
    
    return cell;
}


- (void)onSelectInviteFriend:(UIButton*)sender
{
    if (usersArray.count>0) {
        
  
    [inviteUserIDs addObject:usersArray[sender.tag][@"get_follower"][@"id"]];
    [tokens addObject:usersArray[sender.tag][@"get_follower"]];
    [tokenField reloadData];
    _onDetailOutlet.hidden = NO;
    [inviteFriendTxt resignFirstResponder];
    [inviteFriendTxt setText:@""];
    isUsersShown = NO;
    inviteFriendTxt.userInteractionEnabled = NO;
    [inviteFriendsTV setHidden:YES];
    
     //After added new token, adjust frame
    [self adjustFrameAfterAddingInviteFriend];
          }
}

- (void)onSelectInviteGroup:(UIButton*)sender
{
    if (userGroups.count>0) {
        
   
    [inviteGroupIDs addObject:userGroups[sender.tag][@"id"]];
    [tokens addObject:userGroups[sender.tag]];
    [tokenField reloadData];
    
    [self.view endEditing:YES];
    _onDetailOutlet.hidden = NO;
    [inviteFriendTxt setText:@""];
    inviteFriendTxt.userInteractionEnabled = NO;
    [inviteFriendsTV setHidden:YES];
    
    //After added new token, adjust frame
    [self adjustFrameAfterAddingInviteFriend];
         }
}


- (void)adjustFrameAfterAddingInviteFriend
{
    //After added new token, adjust frame
    increaseInviteTokenViewH = tokenField.frame.size.height - 62.0f;
    // - increase InviteFriendView Height
    [commonUtils resizeFrame:inviteFriendsView withWidth:SCREEN_WIDTH withHeight:(tokenField.frame.size.height + 133.0f)];
    
    // - move DetailView
    [commonUtils moveView:detailView withMoveX:0.0f withMoveY:543.0f + increaseInviteTokenViewH];
    
    // - move ReminderView
//    [commonUtils resizeFrame:reminderView withWidth:SCREEN_WIDTH withHeight:(tokenField.frame.size.height + 160.0f)];
    [commonUtils moveView:reminderView withMoveX:0.0f withMoveY:718.0f + increaseInviteTokenViewH];
    
   
    
    // - resize ContentView
    [commonUtils resizeFrame:contentView withWidth:SCREEN_WIDTH withHeight:849.0f + increaseInviteTokenViewH];
    
    [scrollView setContentSize:contentView.frame.size];
    
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
   [scrollView setContentOffset:bottomOffset animated:YES];
    
}

#pragma mark - Profile Photo Change
- (IBAction)onProfilePhotoChange:(id)sender {
    
    if(self.isLoadingBase) return;
     [detailTxt resignFirstResponder];
      [currentTextField resignFirstResponder];
    if(isEditing) {
        [currentTextField resignFirstResponder];
        isEditing = NO;
    }
    [self presentSemiView:self.photoPickContainerView withOptions:@{
                                                                    KNSemiModalOptionKeys.pushParentBack : @(NO),
                                                                    KNSemiModalOptionKeys.parentAlpha : @(0.6),
                                                                    KNSemiModalOptionKeys.animationDuration : @(0.3)
                                                                    }];
}
- (void)showVideoPicker {
    if(self.isLoadingBase) return;
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
   picker.mediaTypes =[[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
    picker.navigationBar.tintColor = [UIColor blackColor];
    picker.videoMaximumDuration = 15;
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    isImagedone = false;
    [self presentViewController:picker animated:YES completion:NULL];
}
- (IBAction)onClickCamera:(id)sender {
    if(self.isLoadingBase) return;
    
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = false;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
      picker.videoMaximumDuration = 15;
    if (totalImages<10&&totalSize<11) {
           picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
    }
    if (totalImages<10&&totalSize<11) {
        if (totalImages==10) {
             picker.mediaTypes =[[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        }
        isImagedone = false;
          [self presentViewController:picker animated:YES completion:NULL];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{

        [self dismissViewControllerAnimated:YES completion:nil];
        [self dismissSemiModalView];
             });
        [commonUtils showAlert:@"Error!" withMessage:@"You have reached maximum limit"];

    }
}
- (void)HidePickerView:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        if (shouldShowMaxVideoAlert) {
                        [self showMaxVideoAlert];
                    }
    }];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    changedImage = info[UIImagePickerControllerOriginalImage];
    shouldNotClear = YES;
    
    
__weak __typeof(self)weakSelf = self;
    if (changedImage) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self dismissSemiModalView];
        });
       
        DZNPhotoEditorViewController *editor = [[DZNPhotoEditorViewController alloc] initWithImage:changedImage];
        editor.cropMode = DZNPhotoEditorViewControllerCropModeCustom;
        
        UIImageView *newImge = [[UIImageView alloc] initWithFrame:self.view.frame];
        newImge.image = changedImage;
        
        [newImge setContentMode:UIViewContentModeScaleAspectFit];
        
        CGSize sizehere = [self aspectScaledImageSizeForImageView:newImge image:newImge.image];
        

        if (sizehere.height>=269) {
           editor.cropSize = CGSizeMake(self.view.frame.size.width, 269);
        }
        else{
            editor.cropSize = CGSizeMake(sizehere.height * 1.4, sizehere.height);
        }

        [editor setAcceptBlock:^(DZNPhotoEditorViewController *editor, NSDictionary *userInfo){
             UIImage *image = userInfo[UIImagePickerControllerEditedImage];
            
            if (!isImagedone){
                isImagedone = true;
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                [dic setValue:image forKey:@"image"];
                [dic setValue:@"image" forKey:@"IsImage"];
                
                [imagesArray addObject:dic];
            }
           
            
            totalImages++;
            
            [photoCv reloadData];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController setNavigationBarHidden:YES];
                [UIView animateWithDuration:1.0f animations:^{
                    [commonUtils resizeFrame:addImageViewNew withWidth:87.0f withHeight:addImageViewNew.frame.size.height];
                    [commonUtils moveView:addImageViewNew withMoveX:SCREEN_WIDTH - 87 withMoveY:addImageViewNew.frame.origin.y
                     ];
                }];
                [self dismissSemiModalView];
            });
            //Your implementation here
             [self.navigationController popViewControllerAnimated:YES];
        }];
        
        [editor setCancelBlock:^(DZNPhotoEditorViewController *editor){
            
            //Your implementation here
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self dismissSemiModalView];
            });
            [self.navigationController popViewControllerAnimated:YES];
            dispatch_async(dispatch_get_main_queue(), ^{
                 [self.navigationController setNavigationBarHidden:YES];
            });
            
        }];
        
        // The view controller requieres to be nested in a navigation controller
        [self.navigationController pushViewController:editor animated:NO];

    }
    else{
        
        if (totalImages<10&&totalSize<11) {
            NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
            NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
            NSURL *imageURL = [info valueForKey:UIImagePickerControllerMediaURL];
            NSData *data = [NSData dataWithContentsOfURL:imageURL];
            float videoSize = data.length;
            //convert to Megabytes
            videoSize = videoSize/(1024*1024);
            NSLog(@"%f",videoSize);
            totalSize+=videoSize;
             NSLog(@"total size is %f",totalSize);
            //   NSData *data = [NSData dataWithContentsOfURL:videoURL];
            
            NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
            if (![videoURL isKindOfClass:[NSNull class]]&&videoURL!=nil) {
                [dic setObject:videoURL forKey:@"image"];
                
                [dic setValue:@"video" forKey:@"IsImage"];
                [dic setValue:@"camera" forKey:@"pickedType"];
                [dic setValue:[self generateThumbImage:videoURL] forKey:@"posters"];

                dic[@"fileSize"] = @(videoSize);
                
                if (!isImagedone){
                    isImagedone = true;
                    [imagesArray addObject:dic];
                }
                

                totalImages++;
                shouldShowMaxVideoAlert = NO;
                [photoCv reloadData];

            }

        }
        else{
            shouldShowMaxVideoAlert = YES;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:1.0f animations:^{
                [commonUtils resizeFrame:addImageViewNew withWidth:87.0f withHeight:addImageViewNew.frame.size.height];
                [commonUtils moveView:addImageViewNew withMoveX:SCREEN_WIDTH - 87 withMoveY:addImageViewNew.frame.origin.y
                 ];
            }];
  [self dismissSemiModalView];
              });
    }
     [self performSelector:@selector(HidePickerView:) withObject:picker afterDelay:0.10];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        shouldNotClear = YES;

    }];
    dispatch_async(dispatch_get_main_queue(), ^{

    [self dismissSemiModalView];
            });
}

#pragma mark - AlertView - Save Group As
- (IBAction)onSaveGroup:(id)sender
{
    if (inviteUserIDs.count>1) {
        saveGroupAlertView = [[CustomIOSAlertView alloc] init];
        if (IS_IPHONE_5) {
            saveGroupAlertView.frame = CGRectMake(saveGroupAlertView.frame.origin.x, saveGroupAlertView.frame.origin.y, 280, saveGroupAlertView.frame.size.height);
        }
        // Add some custom content to the alert view
        [saveGroupAlertView setContainerView:[self createSaveGroupCustomView]];
    
        [saveGroupAlertView setUseMotionEffects:true];
        [saveGroupAlertView setCloseOnTouchUpOutside:YES];
        [saveGroupAlertView show];

    }
    else{
        [commonUtils showAlert:@"Error!" withMessage:@"Please Add More Members To Create Group"];
    }
    // Here we need to pass a full frame
   }

- (UIView*) createSaveGroupCustomView{
     saveGroupAlert= [[[NSBundle mainBundle] loadNibNamed:@"SaveGroupAlertView" owner:self options:nil] objectAtIndex:0];

    [saveGroupAlert.cancelBtn addTarget:self action:@selector(onSaveGroupCancel:) forControlEvents:UIControlEventTouchUpInside];
    [saveGroupAlert.saveBtn addTarget:self action:@selector(onSaveGroupSaveBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    return saveGroupAlert;
}

- (void)onSaveGroupCancel:(UIButton *)sender
{
        [saveGroupAlertView close];
}

- (void)onSaveGroupSaveBtn:(UIButton *)sender
{
    if (saveGroupAlert.groupNameTxt.text.length>0) {
         [saveGroupAlertView close];
        [self saveGroup:saveGroupAlert.groupNameTxt.text];
    }
    else{
        [commonUtils showAlert:@"Warning!" withMessage:@"Please Provide Group Name"];
    }

   
}

#pragma mark - AlertView - Iced Done
- (IBAction)onIced:(id)sender
{
    if (!addApiCalled) {
        
    
     if (CLLocationCoordinate2DIsValid(commonUtils.pickedLocationCord)&&!(commonUtils.pickedLocationCord.latitude==0)&&!(commonUtils.pickedLocationCord.longitude==0)) {
     
    userID = [NSString stringWithFormat:@"%@",[commonUtils.userData valueForKey:@"id"]];
    if (startsDateLbl.text.length>0&&endsDateLbl.text.length>0&&userID.length>0&&iceTitleTxt.text.length>0&&imagesArray.count>0) {
        NSString *startTimeDateString = [NSString stringWithFormat:@"%@ %@",startsDateLbl.text,startsTimeLbl.text];
        NSString *endTimeDateString =  [NSString stringWithFormat:@"%@ %@",endsDateLbl.text,endsTimeLbl.text];
        NSDateFormatter *dateFormat = [NSDateFormatter new];
        
        if ([commonUtils is24hourFormat]) {
            [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
        }else {
            [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
        }
        
        NSDate *mydate = [dateFormat dateFromString:startTimeDateString];
    
        [dateFormat setDateFormat:@"yyyy-M-dd HH:mm:ss"];
       iceStartTime = [dateFormat stringFromDate:mydate];
        if ([commonUtils is24hourFormat]) {
            [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
        }else {
            [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
        }
        mydate = [dateFormat dateFromString:endTimeDateString];

        [dateFormat setDateFormat:@"yyyy-M-dd HH:mm:ss"];
 
       iceEndTime = [dateFormat stringFromDate:mydate];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [commonUtils showHud:self.view];
            [commonUtils SetWindow:false];
            [self addTransparentView];
            
        });
        
        [self performSelector:@selector(getAddressFromGoogle) withObject:nil afterDelay:.5];
    }
      
    else if (imagesArray.count==0){
        [commonUtils showAlert:@"Error" withMessage:@"Please add images of ICE"];
    }

     }
    }
}

- (UIView*) createIcedDoneCustomView{
    customAlertView= [[[NSBundle mainBundle] loadNibNamed:@"IcedDoneAlertView" owner:self options:nil] objectAtIndex:0];
    
    
    
    [customAlertView.doneBtn addTarget:self action:@selector(onIcedDone:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return customAlertView;
}
- (void)onIcedDone:(UIButton *)sender
{
        can_share_twitter = YES;
 
    NSMutableArray *imagesArrayCopy2 = [NSMutableArray new];

    for (int i= 0; i<imagesArrayCopy.count; i++) {
        if ([imagesArrayCopy[i][@"image"]isKindOfClass:[UIImage class]]) {
           [imagesArrayCopy2 addObject:imagesArrayCopy[i][@"image"]];
        }
        else{
            if ([commonUtils.sharingOptions[@"twitter"]isEqualToString:@"yes"]) {
                 can_share_twitter = NO;
            }
           
        NSData *postData = [NSData dataWithContentsOfURL:imagesArrayCopy[i][@"image"]];
           NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"myMove%d.mp4",i+1]];
            
            [postData writeToFile:path atomically:YES];
            NSURL *moveUrl = [NSURL fileURLWithPath:path];
            [imagesArrayCopy2 addObject:moveUrl];
        }
     
    }
    if (imagesArrayCopy2.count>1 && [commonUtils.sharingOptions[@"twitter"]isEqualToString:@"yes"]) {
        can_share_twitter = NO;
    }
    if (can_share_twitter) {
      if ([commonUtils.sharingOptions[@"facebook"]isEqualToString:@"yes"]||[commonUtils.sharingOptions[@"twitter"]isEqualToString:@"yes"]||[commonUtils.sharingOptions[@"google"]isEqualToString:@"yes"]) {
    
    [icedDoneAlertView close];
     UIActivityViewController *controller = [[UIActivityViewController alloc]initWithActivityItems:imagesArrayCopy2 applicationActivities:nil];
    NSMutableArray *excludedArray  = [[NSMutableArray alloc] initWithObjects:
                                      UIActivityTypeCopyToPasteboard,
                                      UIActivityTypePostToWeibo,
                                      UIActivityTypeSaveToCameraRoll,
                                      UIActivityTypeCopyToPasteboard,
                                      UIActivityTypeMail,
                                      UIActivityTypeMessage,
                                      UIActivityTypeAssignToContact,
                                      
                                      nil];

    if ([commonUtils.sharingOptions[@"facebook"]isEqualToString:@"no"]) {
        [excludedArray addObject:UIActivityTypePostToFacebook];
        controller.excludedActivityTypes = excludedArray;

    }
    if ([commonUtils.sharingOptions[@"twitter"]isEqualToString:@"no"]) {
        [excludedArray addObject:UIActivityTypePostToTwitter];
        controller.excludedActivityTypes = excludedArray;
    }
    if ([commonUtils.sharingOptions[@"google"]isEqualToString:@"no"]) {
        [excludedArray addObject:@[@"com.google.GooglePlus.ShareExtension"]];
        controller.excludedActivityTypes = excludedArray;

        
    }
    NSLog(@"excluded options %@",controller.excludedActivityTypes);
        controller.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            // When completed flag is YES, user performed specific activity
            
            NSLog(@"pending alerts is %@",_pendingAlertViews);
    
            if (completed == YES) {
                if (self.isshowMenu) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else{
                    HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
                    [homeVC onShowTabView:[AppDelegate sharedAppDelegate].ViewMain];
                    [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
                    [self.sideMenuController hideLeftViewAnimated:sender];
                }

            }

        };
        // and present it
  
        [self presentViewController:controller animated:YES completion:^{
            // executes after the user selects something

        }];
       
        NSLog(@"share fb");
    }
      else{
          [icedDoneAlertView close];
          if (self.isshowMenu) {
            
              [self.navigationController popViewControllerAnimated:YES];
          }
          else{
              HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
              [homeVC onShowTabView:[AppDelegate sharedAppDelegate].ViewMain];
              [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
              [self.sideMenuController hideLeftViewAnimated:sender];
              
          }
          
      }
    }
    else {
        [commonUtils showAlert:@"Error!" withMessage:@"You can not post more than one image or any video on twitter.So please uncheck twitter"];
    }
}
#pragma mark - onStarts
- (IBAction)onStartsCalendar:(id)sendec
{
   
    isStartDate = YES;
     [detailTxt resignFirstResponder];
    [currentTextField resignFirstResponder];
    NSString *startTimeDateString = [NSString stringWithFormat:@"%@ %@",startsDateLbl.text,startsTimeLbl.text];
    NSString *endTimeDateString =  [NSString stringWithFormat:@"%@ %@",endsDateLbl.text,endsTimeLbl.text];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    
     if ([commonUtils is24hourFormat]) {
         [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
     }else {
         [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
     }
    
    
    NSLog(@"startTimeDateString %@",startTimeDateString);
    NSLog(@"endTimeDateString %@",endTimeDateString);

    NSDate *previousStartDate = [dateFormat dateFromString:startTimeDateString];
    NSDate *previousEndDate = [dateFormat dateFromString:endTimeDateString];

    commonUtils.shouldPickPreviousStartTime = YES;
    commonUtils.shouldPickPreviousEndTime = YES;
    NSLog(@"previousStartDate %@",previousStartDate);
    NSLog(@"previousEndDate %@",previousEndDate);

    commonUtils.previousStartTime = previousStartDate;
    NSLog(@"commonUtils.previousStartTime %@",commonUtils.previousStartTime);

    commonUtils.previousEndTime = previousEndDate;
    NSLog(@"previousEndTime %@",commonUtils.previousEndTime);

    PickerViewController *pickerViewController =[[PickerViewController alloc] initFromNib];
  
    pickerViewController.delegate = self;
   
    [pickerViewController setPageIndex:StartsCalendar];
 
    [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];


}
- (IBAction)onEndsCalendar:(id)sender
{
    isEndDate = YES;
     [detailTxt resignFirstResponder];
      [currentTextField resignFirstResponder];
    NSString *startTimeDateString = [NSString stringWithFormat:@"%@ %@",startsDateLbl.text,startsTimeLbl.text];
    NSString *endTimeDateString =  [NSString stringWithFormat:@"%@ %@",endsDateLbl.text,endsTimeLbl.text];
    NSDateFormatter *dateFormat = [NSDateFormatter new];

    if ([commonUtils is24hourFormat]) {
        [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
    }else {
        [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
    }
    
    
    NSDate *previousStartDate = [dateFormat dateFromString:startTimeDateString];
    NSDate *previousEndDate = [dateFormat dateFromString:endTimeDateString];

    commonUtils.shouldPickPreviousStartTime = YES;
    commonUtils.shouldPickPreviousEndTime = YES;
    
    commonUtils.previousStartTime = previousStartDate;

    commonUtils.previousEndTime = previousEndDate;

    PickerViewController *pickerViewController = [[PickerViewController alloc] initFromNib];
    pickerViewController.delegate = self;
 
    [pickerViewController setPageIndex:EndsCalendar];
    [self presentViewControllerOverCurrentContext:pickerViewController animated:YES completion:nil];
}

- (void)didSelectStartsMonthDayYear:(NSString *)startsMonthDayYearStr
                  withStartsHourMin:(NSString*)startsHourMinStr
               withEndsMonthDayYear:(NSString*)endsMonthDayYearStr
                    withEndsHourMin:(NSString*)endsHourMinStr
                     withStartsDate:(NSDate*)startstDate
                       withEndsDate:(NSDate*) endsDate{
    
    
    if (startsHourMinStr.length > 0 ) {
        startsTimeLbl.text = startsHourMinStr;
        startsDateLbl.text = startsMonthDayYearStr;
        startSelected = startstDate;

    }
    
        NSLog(@"endsHourMinStr==> %@",endsHourMinStr);
        NSLog(@"endsMonthDayYearStr==> %@",endsMonthDayYearStr);
    
    
    if ([endsHourMinStr isEqualToString:endsTimeLbl.text] && [endsMonthDayYearStr isEqualToString:endsDateLbl.text]) {
        
    }else {
        if (endsHourMinStr.length > 0 ) {
            endsTimeLbl.text = endsHourMinStr;
            endsDateLbl.text = endsMonthDayYearStr;
            endSelected = endsDate;
            
            
            durationInMinutes = [endSelected timeIntervalSinceDate:startSelected];
        }else {
            endSelected = [startSelected dateByAddingTimeInterval:durationInMinutes];
            
            endsDateLbl.text = [self ReturnStringInDate:endSelected withformat:@"MMM d, yyyy"];
            endsDateLbl.text = [self ReturnTimeInDate:endSelected];
            
        }
    }
  
    
    
    NSLog(@"endsDateLbl.text %@",endsDateLbl.text);
    NSLog(@"startsDateLbl.text %@",startsDateLbl.text);
    if (endsDateLbl.text.length > 0 && startsDateLbl.text.length > 0){
        NSLog(@"endsDateLbl.text %@",endsDateLbl.text);
        
        NSDateFormatter *myFormatter  = [[NSDateFormatter alloc] init];
        
        if ([endsMonthDayYearStr isEqualToString:@""]) {
            [myFormatter setDateFormat:@"MMM d, yyyy"];
            endsMonthDayYearStr = [myFormatter stringFromDate:endSelected];
            if ([commonUtils is24hourFormat]) {
                [myFormatter setDateFormat:@"HH:mm"];
            }else {
                [myFormatter setDateFormat:@"hh:mm a"];
            }
            
            endsHourMinStr = [myFormatter stringFromDate:endSelected];
        }
        
        
        NSLog(@"startSelected ==> %@",startSelected);
        NSLog(@"endsHourMinStr ==> %@",endsHourMinStr);
        NSLog(@"endSelected==> %@",endSelected);
        
        if ([startSelected isLaterThan:endSelected])
        {

            
            
            _endView.layer.borderColor = [UIColor redColor].CGColor;
            _endView.layer.borderWidth = 1.0;
            endTimeNotValid = YES;
            
            NSTimeInterval secondsInEightHours = 8 * 60 * 60;
            NSDate *dateEightHoursAhead = [startSelected dateByAddingTimeInterval:secondsInEightHours];
            
            
            
            NSDateFormatter *monthDayYearDateFormatter  = [[NSDateFormatter alloc] init];
            [monthDayYearDateFormatter setDateFormat:@"MMM d, yyyy"];
            
            NSDateFormatter *hourMinDateFormatter = [[NSDateFormatter alloc] init];
            
            if ([commonUtils is24hourFormat]) {
                [hourMinDateFormatter setDateFormat:@"HH:mm"];
            }else {
                [hourMinDateFormatter setDateFormat:@"hh:mm a"];
            }
            
            
            NSLog(@"monthDayYearDateFormatter %@",[monthDayYearDateFormatter stringFromDate:dateEightHoursAhead]);
            NSLog(@"hourMinDateFormatter %@",[hourMinDateFormatter stringFromDate:dateEightHoursAhead]);
            

            [endsDateLbl setText:endsMonthDayYearStr];
            [endsTimeLbl setText:endsHourMinStr];
            
        } else {
            _endView.layer.borderColor = [UIColor clearColor].CGColor;
            _endView.layer.borderWidth = 1.0;
            endTimeNotValid = NO;
            NSLog(@"endsMonthDayYearStr %@",endsMonthDayYearStr);
            NSLog(@"endsHourMinStr %@",endsHourMinStr);
            
            [endsDateLbl setText:endsMonthDayYearStr];
            [endsTimeLbl setText:endsHourMinStr];
//            endsDateLbl.text = endsMonthDayYearStr;
//            endsTimeLbl.text = endsHourMinStr;
        }
        NSLog(@"endsDateLbl.text %@",endsDateLbl.text);
    }
    
    NSLog(@"endsDateLbl.text %@",endsDateLbl.text);

    [self checkTextfieldValue];
}

- (IBAction)onDetailEdit:(id)sender
{
    
    [detailTxt setEditable:YES];
    [detailTxt becomeFirstResponder];
}

#pragma mark - TextFiled Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    currentTextField = textField;
    if(self.isLoadingBase) return NO;
    if (textField == inviteFriendTxt) {
        
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
//    if (textField == inviteFriendTxt && textField.text.length>0) {
////        isUsersShown = YES;
////        [inviteFriendsTV setHidden:NO];
//    }
    
    if (textField == iceTitleTxt) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        if (iceTitleTxt.text.length>0 ) {
            [self checkTextfieldValue];
        }
        if (iceTitleTxt.text.length == 1 && range.length >0) {
            iceCheckImg.alpha = 0.2;
            iceCheckBtn.enabled = NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 24;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(self.isLoadingBase) return NO;
    [self.view endEditing:YES];
    _onDetailOutlet.hidden = NO;
    if (textField == inviteFriendTxt) {
        isUsersShown = NO;
     
        
        [inviteFriendTxt setText:@""];
        inviteFriendTxt.userInteractionEnabled = NO;
        [inviteFriendsTV setHidden:YES];

    }
    [self checkTextfieldValue];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField == iceTitleTxt) return;
//    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
 //   [scrollView setContentOffset:bottomOffset animated:YES];
}


#pragma mark - TextView Delegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(self.isLoadingBase) return NO;
    isDetailEditing = YES;
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if(self.isLoadingBase) return NO;

    if ([text isEqualToString:@"\n"]) {
        
        isEditing = NO;
        [textView setEditable:NO];
        isDetailEditing = NO;
        return [textView resignFirstResponder];
    }
     return textView.text.length + (text.length - range.length) <= 160;
   
}
- (void)textViewDidChange:(UITextView *)textView{
    NSLog(@"textViewDidChange:");
    
    textView.scrollEnabled = NO;
    NSRange selectedRange = textView.selectedRange;
    NSString *text = textView.text;
    
    // This will give me an attributedString with the base text-style
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    NSArray *matches = [regex matchesInString:text
                                      options:0
                                        range:NSMakeRange(0, text.length)];
    
    for (NSTextCheckingResult *match in matches)
    {
        NSRange matchRange = [match rangeAtIndex:0];
        [attributedString addAttribute:NSForegroundColorAttributeName
                                 value:[UIColor blueColor]
                                 range:matchRange];
    }
    
    textView.attributedText = attributedString;
    textView.selectedRange = selectedRange;
    textView.scrollEnabled = YES;
    
}
-(void)textFieldDidChange:(id)sender{
    if ([inviteFriendTxt.text length]>0) {
        [usersArray removeAllObjects];
        [userGroups removeAllObjects];
        for (int i=0; i<usersMutableCopy.count; i++) {
            // NSRange *range = ;
            NSString *object = [usersMutableCopy[i][@"get_follower"]valueForKey:@"first_name"];
            
            if([object rangeOfString:inviteFriendTxt.text options:NSCaseInsensitiveSearch].location != NSNotFound )
            {
                if (![inviteUserIDs containsObject:usersMutableCopy[i][@"get_follower"][@"id"]]) {
                       [usersArray addObject:usersMutableCopy[i]];
                }
             
                
            }
        }
        for (int i=0; i<userGroupsMutableCopy.count; i++) {
            // NSRange *range = ;
            NSString *object = [userGroupsMutableCopy[i][@"get_follower"]valueForKey:@"name"];
            
            if([object rangeOfString:inviteFriendTxt.text options:NSCaseInsensitiveSearch].location != NSNotFound)
            {
                if (i < usersMutableCopy.count){
                    if (![inviteGroupIDs containsObject:usersMutableCopy[i][@"get_follower"][@"id"]])
                    {
                        [userGroups addObject:userGroupsMutableCopy[i]];
                    }
                }
                
                
                
            }
        }

      
        if (usersArray.count>0||userGroups.count>0) {
              [inviteFriendsTV reloadData];
            isUsersShown = YES;
            
            [inviteFriendsTV setHidden:NO];
        }
        else{
            [inviteFriendsTV reloadData];
            isUsersShown = NO;
            
            [inviteFriendsTV setHidden:YES];

        }
        
    }
    else {
        isUsersShown = NO;
        
           [inviteFriendsTV setHidden:YES];
        [inviteFriendTxt setText:@""];
     

        usersArray=userGroups = [[NSMutableArray alloc]init];
        usersArray = [usersMutableCopy mutableCopy];
        userGroups = [userGroupsMutableCopy mutableCopy];
        [inviteFriendsTV reloadData];
        
    }

}
- (void)textViewDidEndEditing:(UITextView *)textView{
    _onDetailOutlet.hidden = NO;
    CGPoint bottomOffset = CGPointMake(0, scrollView.contentSize.height - scrollView.bounds.size.height);
   // [scrollView setContentOffset:bottomOffset animated:YES];
}

#pragma mark - View TapGesture
- (void) onTappedScreen:(UITapGestureRecognizer*) sender {
    if (self.isLoadingBase) return;
    [currentTextField resignFirstResponder];
    
    CGPoint point = [sender locationInView:self.view];
   
    CGPoint guestPoint = CGPointMake(_guestCheckView.frame.origin.x, _guestCheckView.frame.origin.y);
    UIView *viewTouched = [sender.view hitTest:point withEvent:nil];
    if ([viewTouched isKindOfClass:[inviteFriendsView class]]) {
        CGPoint location = [sender locationInView:inviteFriendsView];
        
        if (location.y>(_inviteIconView.frame.origin.y)&&location.y<_guestCheckView.frame.origin.y) {
            if (!isUsersShown) {
                inviteFriendTxt.userInteractionEnabled = YES;
                [inviteFriendTxt becomeFirstResponder];
                [scrollView setContentOffset:CGPointMake(0, 348) animated:YES];
                
               
            }
            else{
                isUsersShown = NO;
                [self.view endEditing:YES];
                _onDetailOutlet.hidden = NO;
                [inviteFriendTxt setText:@""];
                inviteFriendTxt.userInteractionEnabled = NO;
                [inviteFriendsTV setHidden:YES];
            }

        }
    }
   else if ([viewTouched isKindOfClass:[detailContainerView class]]) {
        [detailTxt setEditable:YES];
        [detailTxt becomeFirstResponder];
    }
    else{
        isUsersShown = NO;
        _onDetailOutlet.hidden = NO;
        [self.view endEditing:YES];
        [inviteFriendTxt setText:@""];
        inviteFriendTxt.userInteractionEnabled = NO;
        [inviteFriendsTV setHidden:YES];
    }
    
}

-(void)addIce:(NSMutableArray*)dataArray{
    
    addApiCalled = YES;
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    [currentTextField resignFirstResponder];
    [detailTxt resignFirstResponder];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        NSString *reminderString = _reminderTF.text;
        if (_reminderTF.text.length == 0) {
            reminderString = @"0";
        }
        if ([_houtMintTF.text isEqualToString:@"hours"]) {
            int reminderTime = [reminderString integerValue];
            reminderTime = reminderTime*60;
            reminderString = [NSString stringWithFormat:@"%d",reminderTime];
        }
    NSNumber *isPrivateNumber = [NSNumber numberWithBool:!isPrivate];
        NSString *isprivateString = [NSString stringWithFormat:@"%@",isPrivateNumber];
      
    NSNumber *guestCanInvite = [NSNumber numberWithBool:isGuestCanInvite];
    if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            [self addTransparentView];
            [commonUtils showHud:self.view];
            [commonUtils SetWindow:false];
        });
        NSError *error;
        NSData *invitedGroupData =  [NSJSONSerialization dataWithJSONObject:inviteGroupIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *invitedGroupString = [[NSString alloc] initWithData:invitedGroupData encoding:NSUTF8StringEncoding];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inviteUserIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
          
            @autoreleasepool{
            NSString *currentLatitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.latitude];
            NSString *currentLongtitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.longitude];
                NSString *userLat = [NSString stringWithFormat:@"%@",[commonUtils getUserDefault:@"currentLatitude"]];
                NSString *userLng = [NSString stringWithFormat:@"%@",[commonUtils getUserDefault:@"currentLongitude"]];
                
//            NSString *deviceId = @"abcdefghijklmnopqrstuvwxyzabcdefghi";
//            NSString *address =  [commonUtils getUserDefault:@"location"];
            
            NSString *timeZoneOffset = [commonUtils getTimeZone];
            NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
            
                [_params setObject:iceStartTime forKey:@"start_date"];
            [_params setObject:iceEndTime forKey:@"end_date"];
            [_params setObject:userID forKey:@"user_id"];
            [_params setObject:iceTitleTxt.text forKey:@"title"];
            [_params setObject:detailTxt.text forKey:@"description"];
            [_params setObject:pickedAddress forKey:@"location"];
            [_params setObject:currentLatitude forKey:@"lat"];
             [_params setObject:invitedGroupString forKey:@"group_ids"];
            [_params setObject:currentLongtitude forKey:@"lng"];
            [_params setObject:isprivateString forKey:@"is_public"];
            [_params setObject:guestCanInvite forKey:@"can_invite"];
                [_params setObject:jsonString forKey:@"invited_ids"];
        [_params setObject:timeZoneOffset forKey:@"timezone"];
         [_params setObject:reminderString forKey:@"reminder_time"];
                [_params setObject:userLat forKey:@"currentLat"];
                [_params setObject:userLng forKey:@"currentLng"];
         [_params setObject:shortAddress forKey:@"complete_address"];
        NSLog(@"param ===> %@",_params);
            // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
            NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
            
            // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
            NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"add_ice"];
            // the server url to which the image (or the media) is uploaded. Use your server url here
            NSURL* requestURL = [NSURL URLWithString:Url];
            
            // create request
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
            [request setHTTPShouldHandleCookies:NO];
            [request setTimeoutInterval:500];
            [request setHTTPMethod:@"POST"];
            
            // set Content-Type in HTTP header
            NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
            [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
            [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
             [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
            // post body
            NSMutableData *body = [NSMutableData data];
            
            // add params (all params are strings)
            for (NSString *param in _params) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            // add image data
                
                
                NSMutableArray *posterArray = [NSMutableArray new];
                
        for (int i=0; i<dataArray.count; i++) {
            
            NSString *strValue = (NSString *)dataArray[i][@"IsImage"] ;
            if ([strValue isEqualToString:@"image"]) {
                
                    NSString *fileName = [NSString stringWithFormat:@"files[%d]",i];
                    NSData *imageData = UIImageJPEGRepresentation(dataArray[i][@"image"], 0.5);
                    if (imageData) {
                        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", fileName] dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                        [body appendData:imageData];
                    
                        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                        
                    }
           
                }
            else{
               // NSURL *assetURL = [NSURL fileURLWithPath:imagesArray[i]];
                NSData *postData = [NSData dataWithContentsOfURL:dataArray[i][@"image"]];
                [posterArray addObject:dataArray[i][@"posters"]];
                NSLog(@"post data length is %lu ==>",(unsigned long)postData.length);
                NSString *videoFileName = [NSString stringWithFormat:@"files[%d]",i];
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"file.mov\"\r\n", videoFileName] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: video/mov\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                
                [body appendData:postData];
          
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                
                
             
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                EventImageCVCell *cell = (EventImageCVCell *)[photoCv cellForItemAtIndexPath:indexPath];
                UIImage *img = cell.eventIv.image;
                NSLog(@"image is %@",img);
                if (img) {
                    
                
                NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
               
                                NSString *thumbnailName = [NSString stringWithFormat:@"thumbs[%d]",i];
                                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", thumbnailName] dataUsingEncoding:NSUTF8StringEncoding]];
                                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                                [body appendData:imageData];
                                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
                }
            }
        }

                
                
                
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            
            // set URL
            [request setURL:requestURL];
            NSError *err = nil;
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                    completionHandler:
                                          ^(NSData *data, NSURLResponse *response, NSError *error) {
                                              if ([data length] > 0 && err == nil){
                                                  NSError* error;
                                                  NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:kNilOptions
                                                                                                               error:&error];
                                                  NSString* myString;
                                                  myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                                  NSString *message = [dictionary valueForKey:@"errorMessage"];
                                                 
                                                  NSString *statusis = [dictionary valueForKey:@"status"];
                                                  
                                                  if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                      if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                          [[NSNotificationCenter defaultCenter]
                                                           postNotificationName:@"Logout"
                                                           object:self];
                                                          
                                                          
                                                      }
                                                  }else   if([statusis isEqualToString:@"success"]){
                                                 
                                                      NSString *successMessage = [dictionary valueForKey:@"successMessage"];
                                                      ice_id = [NSString stringWithFormat:@"%@",dictionary[@"ice_id"]];
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self clearAddIceData];
                                                          [self resetCameraView];
                                                          [self removeTransparentView];
                                                          [commonUtils hideHud];
                                                          [commonUtils SetWindow:true];
                                                             imagesArrayCopy = dataArray;
                                                          // Here we need to pass a full frame
                                                       icedDoneAlertView   = [[CustomIOSAlertView alloc] init];
                                                      
                                                          // Add some custom content to the alert view
                                                          [icedDoneAlertView setContainerView:[self createIcedDoneCustomView]];
                                                          
                                                          [icedDoneAlertView setUseMotionEffects:true];
                                                          [icedDoneAlertView setCloseOnTouchUpOutside:YES];
                                                      isSharingShown = YES;
                                                             visibleAlertView = icedDoneAlertView;
                                                              [icedDoneAlertView show];
                                                              addApiCalled = NO;
                                                      });
                                                  }
                                                  if(![statusis isEqualToString:@"success"]){
                                                      dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self removeTransparentView];
                                                          [self clearAddIceData];
                                                          [self resetCameraView];
                                                          [commonUtils hideHud];
                                                          [commonUtils SetWindow:true];
                                                          addApiCalled = NO;
                                                          [commonUtils showAlert:@"Error!" withMessage:message];
                                                      });
                                                      
                                                  }
                                                  
                                              }
                                              else if ([data length] == 0 && err == nil){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self removeTransparentView];
                                                      [self clearAddIceData];
                                                      [self resetCameraView];
                                                      [commonUtils hideHud];
                                                      [commonUtils SetWindow:true];
                                                  });
                                                  //no data, but tried
                                              }
                                              else if (err != nil)
                                              {
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                          [self removeTransparentView];
                                                      [self clearAddIceData];
                                                      [self resetCameraView];
                                                      [commonUtils hideHud];
                                                      [commonUtils SetWindow:true];
                                                  });
                                            
                                                  NSLog(@"%@", err.localizedDescription);
                                              }
                                              
                                              
                                              
                                          }];
            [task resume];
            }
              });
        
    }
    else {
        [commonUtils showAlert:@"Error!" withMessage:@"Unable To Get Your Location.Please Allow The App To Get Your Location"];
    }
    }
    
}
-(void)getUsers{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
        return;
    }
    else {
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
      
      
            NSString *urlString = [NSString stringWithFormat:@"%@get_follow_following/%@?time_zone=%ld",ServerUrl,commonUtils.userData[@"id"],timezone];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    NSLog(@"%@", request.allHTTPHeaderFields);
  
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
  
    if(error!=nil)
    {
        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(data !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:data
                                 options:kNilOptions
                                 error:&Jerror];
            if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"Logout"
                     object:self];
                    
                    
                }
            }else if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
               
                    NSMutableDictionary *successDic = [[json valueForKey:@"successData"] mutableCopy];
                usersArray = [[successDic valueForKey:@"followers"] mutableCopy];
              
                [tokenField reloadData];
                usersMutableCopy = usersArray.mutableCopy;
                     });
            }
            else{
                dispatch_async(dispatch_get_main_queue(), ^{
//                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [commonUtils hideHud];
                    [commonUtils SetWindow:true];
                });
            }
            if(Jerror!=nil)
            {
               
               // NSLog(@"json error:%@",Jerror);
            }
        }
    }
            
           }];
            
              });
    }
    
}
#pragma Play video func
-(void)playVideo:(UIButton*)sender{
    NSURL *assetURL;
    if ([imagesArray[sender.tag][@"pickedType"]isEqualToString:@"galery"]) {
        assetURL =  [NSURL fileURLWithPath:imagesArray[sender.tag][@"image"]];
    }
    else{
        assetURL = imagesArray[sender.tag][@"image"];
    }
    NSLog(@"url is %@",assetURL);
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc]init];
    playerViewController.player = [[AVPlayer alloc]initWithURL:assetURL];
    [self presentViewController:playerViewController animated:YES completion:nil];
    playerViewController.view.frame = self.view.frame;
    [playerViewController.player play];

}
#pragma Remove Image from CollectionView
- (void) removeImage:(UIButton *) sender {
      NSString *strValue = (NSString *)imagesArray[sender.tag][@"IsImage"] ;
    float fileSize = [imagesArray[sender.tag][@"fileSize"]floatValue];
    totalSize-=fileSize;
    [imagesArray removeObjectAtIndex:sender.tag];
  
    if (imagesArray.count == 0) {
        [self checkTextfieldValue];
         [self performSelector:@selector(resetCameraView2) withObject:nil afterDelay:0.25];
    }
    
    if ([strValue isEqualToString:@"image"]) {
        totalImages--;
    }
    else{
        totalImages--;
    }
    [photoCv reloadData];
    [_imgCollectionView reloadData];
    NSLog(@"Tag : %ld", (long)sender.tag);
    
}

-(void)cancelNumberPad{
    [detailTxt resignFirstResponder];
    [_reminderTF resignFirstResponder];
    //_phoneTF.text = @"";
}

-(void)doneWithNumberPad{
     [detailTxt resignFirstResponder];
    [_reminderTF resignFirstResponder];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Clicked! %d, %d", (int)buttonIndex, (int)[alertView tag]);
   
}
- (IBAction)cancelImageUploadBtn:(id)sender {
    _imageToChooseView.hidden = YES;
}

-(void)getUserGroups {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
        return;
    }
    else {
      
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,@"get_groups"]]];
    // Create a mutable copy of the immutable request and add more headers


    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    NSLog(@"%@", request.allHTTPHeaderFields);
    
    
    NSURLResponse *response;
    
    NSError *error = nil;
    
            [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
  
    if(error!=nil)
    {
        NSLog(@"web service error:%@",error);
    }
    else
    {
        if(data !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:data
                                 options:kNilOptions
                                 error:&Jerror];
           // NSLog(@"user data is %@",json);
            
            if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"Logout"
                     object:self];
                    
                    
                }
            }else if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
              
                userGroups = [json[@"successData"]mutableCopy];
                userGroupsMutableCopy = [userGroups mutableCopy];
                [inviteFriendsTV reloadData];
                      });
                //[tokenField reloadData];
            }
            else{
             
            }
            if(Jerror!=nil)
            {
             
                NSLog(@"json error:%@",Jerror);
            }
        }
    }
            
          }];
            
        });

    }
}
-(void)saveGroup:(NSString *)groupName{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }

    else {
        NSError *error;
        NSData *invitedGroupData =  [NSJSONSerialization dataWithJSONObject:inviteGroupIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *invitedGroupString = [[NSString alloc] initWithData:invitedGroupData encoding:NSUTF8StringEncoding];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inviteUserIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [commonUtils showHud:self.view];
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];

        [_params setObject:invitedGroupString forKey:@"group_ids"];

        [_params setObject:jsonString forKey:@"user_ids"];
        [_params setObject:groupName forKey:@"name"];
    
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
     
        NSString *Url = [NSString stringWithFormat:@"%@%@",ServerUrl,@"create_group"];
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:Url];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        [request addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [request addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
   
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        [request setURL:requestURL];
        NSError *err = nil;
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                                completionHandler:
                                      ^(NSData *data, NSURLResponse *response, NSError *error) {
                                          if ([data length] > 0 && err == nil){
                                              NSError* error;
                                              NSDictionary* dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                         options:kNilOptions
                                                                                                           error:&error];
                                              //NSLog(@"Server Response %@",response);
                                              NSString* myString;
                                              myString = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
                                              // NSLog(@"string is %@",myString);
                                              NSLog(@"dictionary %@",dictionary);
                                              NSString *message = [dictionary valueForKey:@"errorMessage"];
                                              
                                              NSString *statusis = [dictionary valueForKey:@"status"];
                                              
                                              if ([[dictionary valueForKey:@"status"]isEqualToString:@"error"]){
                                                  if ([[dictionary valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                                                      [[NSNotificationCenter defaultCenter]
                                                       postNotificationName:@"Logout"
                                                       object:self];
                                                      
                                                      
                                                  }
                                              }else if([statusis isEqualToString:@"success"]){
                                                  
                                                  NSString *successMessage = [dictionary valueForKey:@"successMessage"];
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Success" withMessage:successMessage];
                                                      [self getUserGroups];
                                                      // Here we need to pass a full frame
                                                      
                                                      
                                                      
                                                      
                                                  });
                                              }
                                              if(![statusis isEqualToString:@"success"]){
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [commonUtils hideHud];
                                                      [commonUtils showAlert:@"Error!" withMessage:message];
                                                  });
                                                  
                                              }
                                              
                                          }
                                          else if ([data length] == 0 && err == nil){
                                              NSLog(@"no data returned");
                                              [commonUtils hideHud];
                                              //no data, but tried
                                          }
                                          else if (err != nil)
                                          {
                                              
                                              [commonUtils hideHud];
                                              NSLog(@"%@", err.localizedDescription);
                                              //couldn't download
                                              
                                          }
                                          
                                          
                                          
                                      }];
        [task resume];
    }
    
        
    }


    
#pragma picker methods
#pragma mark - UIPickerViewDataSource

// #3
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == hourPicker) {
        return 1;
    }
    
    return 0;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == hourPicker) {
        return [pickerElements count];
    }
    
    return 0;
}

#pragma mark - UIPickerViewDelegate

// #5
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView == hourPicker) {
        return pickerElements[row];
    }
    
    return nil;
}

// #6
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == hourPicker) {
        _houtMintTF.text = pickerElements[row];
    }
}


- (IBAction)onMenuShowforChild:(id)sender{
    if (self.isshowMenu) {
        shouldNotClear = NO;
        [self.navigationController popViewControllerAnimated:true];
    }else {
        shouldNotClear = NO;
        [self.sideMenuController showLeftViewAnimated:sender];
    }
}


- (IBAction)pickerBtn:(id)sender {
   
        [_houtMintTF becomeFirstResponder];
    
}
- (NSData *)generatePostDataForData:(NSData *)uploadData
{
    // Generate the post header:
    NSString *post = [NSString stringWithCString:"--AaB03x\r\nContent-Disposition: form-data; name=\"upload[file]\"; filename=\"somefile\"\r\nContent-Type: video/mov\r\nContent-Transfer-Encoding: binary\r\n\r\n" encoding:NSASCIIStringEncoding];
    
    // Get the post header int ASCII format:
    NSData *postHeaderData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    // Generate the mutable data variable:
    NSMutableData *postData = [[NSMutableData alloc] initWithLength:[postHeaderData length] ];
    [postData setData:postHeaderData];
    
    // Add the image:
    [postData appendData: uploadData];
    
    // Add the closing boundry:
    [postData appendData: [@"\r\n--AaB03x--" dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES]];
    
    // Return the post data:
    return postData;
}
-(void)uploadVideo{
    NSString *reminderString = _reminderTF.text;
    if ([_houtMintTF.text isEqualToString:@"hours"]) {
        int reminderTime = [reminderString integerValue];
        reminderTime = reminderTime*60;
        reminderString = [NSString stringWithFormat:@"%d",reminderTime];
    }
    //    NSLog(@"reminder string %@",reminderString);
    NSNumber *isPrivateNumber = [NSNumber numberWithBool:!isPrivate];
    NSString *isprivateString = [NSString stringWithFormat:@"%@",isPrivateNumber];
    
    NSNumber *guestCanInvite = [NSNumber numberWithBool:isGuestCanInvite];
    if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
        //       NSArray *invitedarray = @[@6,@7];
        
        NSError *error;
        NSData *invitedGroupData =  [NSJSONSerialization dataWithJSONObject:inviteGroupIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *invitedGroupString = [[NSString alloc] initWithData:invitedGroupData encoding:NSUTF8StringEncoding];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inviteUserIDs options:NSJSONWritingPrettyPrinted error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [commonUtils showHud:self.view];
        
        NSString *currentLatitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.latitude];
        NSString *currentLongtitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.longitude];
        
        NSString *deviceId = @"abcdefghijklmnopqrstuvwxyzabcdefghi";
        NSString *address =  [commonUtils getUserDefault:@"location"];
        
        NSString *timeZoneOffset = [commonUtils getTimeZone];
        NSLog(@"device id is %@ address is %@ timezone seconds are %@",deviceId,address,timeZoneOffset);

    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    
    [_params setObject:iceStartTime forKey:@"start_date"];
    [_params setObject:iceEndTime forKey:@"end_date"];
    [_params setObject:userID forKey:@"user_id"];
    [_params setObject:iceTitleTxt.text forKey:@"title"];
    [_params setObject:detailTxt.text forKey:@"description"];
    [_params setObject:pickedAddress forKey:@"location"];
    [_params setObject:currentLatitude forKey:@"lat"];
    [_params setObject:invitedGroupString forKey:@"group_ids"];
    [_params setObject:currentLongtitude forKey:@"lng"];
    [_params setObject:isprivateString forKey:@"is_public"];
    [_params setObject:guestCanInvite forKey:@"can_invite"];
    [_params setObject:jsonString forKey:@"invited_ids"];
    [_params setObject:timeZoneOffset forKey:@"timezone"];
    [_params setObject:reminderString forKey:@"reminder_time"];
}
}


- (void)launchGMImagePicker
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
 
    [self dismissSemiModalView];
         });
      if(self.isLoadingBase) return;
    GMImagePickerController *picker = [[GMImagePickerController alloc] init];
    picker.delegate = self;
    picker.title = @"";
    recentSize = 0;
    picker.customDoneButtonTitle = @"Done";
    picker.customCancelButtonTitle = @"Cancel";
    picker.customNavigationBarPrompt = @"Select Photo or Video";
   
    picker.colsInPortrait = 3;
    picker.colsInLandscape = 5;
    picker.minimumInteritemSpacing = 2.0;
  
    picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
    //    picker.allowsMultipleSelection = NO;
    //    picker.confirmSingleSelection = YES;
    //    picker.confirmSingleSelectionPrompt = @"Do you want to select the image you have chosen?";
    
    //    picker.showCameraButton = YES;
    //    picker.autoSelectCameraImages = YES;
    
    picker.modalPresentationStyle = UIModalPresentationPopover;
    
    //    picker.mediaTypes = @[@(PHAssetMediaTypeImage)];
    
    //    picker.pickerBackgroundColor = [UIColor blackColor];
    //    picker.pickerTextColor = [UIColor whiteColor];
    //    picker.toolbarBarTintColor = [UIColor darkGrayColor];
    //    picker.toolbarTextColor = [UIColor whiteColor];
    //    picker.toolbarTintColor = [UIColor redColor];
    //    picker.navigationBarBackgroundColor = [UIColor blackColor];
    //    picker.navigationBarTextColor = [UIColor whiteColor];
    //    picker.navigationBarTintColor = [UIColor redColor];
    //    picker.pickerFontName = @"Verdana";
    //    picker.pickerBoldFontName = @"Verdana-Bold";
    //    picker.pickerFontNormalSize = 14.f;
    //    picker.pickerFontHeaderSize = 17.0f;
    //    picker.pickerStatusBarStyle = UIStatusBarStyleLightContent;
    //    picker.useCustomFontForNavigationBar = YES;
    
    UIPopoverPresentationController *popPC = picker.popoverPresentationController;
    popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    //    popPC.sourceView = _gmImagePickerButton;
    //    popPC.sourceRect = _gmImagePickerButton.bounds;
    //    popPC.backgroundColor = [UIColor blackColor];
    
    //[self showViewController:picker sender:nil];
    [self presentViewController:picker animated:YES completion:NULL];
}



#pragma mark - GMImagePickerControllerDelegate

- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSArray *)assetArray
{
    dispatch_async(dispatch_get_main_queue(), ^(void){

    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        
        statusBar.backgroundColor = [UIColor clearColor];
    }
     });

    shouldNotClear = YES;
    
    PHImageManager *manager = [PHImageManager defaultManager];
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:[assetArray count]];
    PHImageRequestOptions *option = [PHImageRequestOptions new];
    option.synchronous = YES;

    // assets contains PHAsset objects.
    __block UIImage *ima;
    
    for (PHAsset *asset in assetArray) {
        // Do something with the asset
        if (asset.mediaType == 1) {
            [manager requestImageForAsset:asset
                               targetSize:PHImageManagerMaximumSize
                              contentMode:PHImageContentModeDefault
                                  options:option
                            resultHandler:^void(UIImage *image, NSDictionary *info) {
                                //   NSLog(@"info is %@",info);
                                [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
                                    ima = image;
                                    float imageSize = imageData.length;
                                    //convert to Megabytes
                                    imageSize = imageSize/(1024*1024);
                                    NSLog(@"%f",imageSize);
                                    
                                    NSLog(@"total size is %f",totalSize);
                                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:ima,@"image",@"image",@"IsImage", nil];
                                    dic[@"fileSize"] = @(imageSize);
                                    [imagesArray addObject:dic];
                                    [self reloadCollectionViewAndChangeLayout];
                                    
                                }];

                            }];
            
        }
        else if(asset.mediaType == 2){
            [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:nil resultHandler:^(AVAsset *avAsset, AVAudioMix *audioMix, NSDictionary *info) {
             
               
               
                NSURL *url = (NSURL *)[[(AVURLAsset *)avAsset URL] fileReferenceURL];
                             
                NSLog(@"url is %@",url);
                NSLog(@"url = %@", [url absoluteString]);
                NSLog(@"url = %@", [url relativePath]);
                AVURLAsset* myAsset = (AVURLAsset*)avAsset;
                NSData * data = [NSData dataWithContentsOfFile:myAsset.URL.relativePath];
                if (data) {
                 //   NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[url relativePath],@"image",data,@"videodata",@"video",@"IsImage", nil];
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
                    [dic setObject:[url relativePath] forKey:@"image"];
                     [dic setObject:data forKey:@"videodata"];
                     [dic setValue:@"video" forKey:@"IsImage"];
                    [dic setValue:@"galery" forKey:@"pickedType"];
                    [dic setObject:url forKey:@"playurl"];
                    float imageSize = data.length;
                    //convert to Megabytes
                    imageSize = imageSize/(1024*1024);
                    NSLog(@"%f",imageSize);
                     dic[@"fileSize"] = @(imageSize);
                    [imagesArray addObject:dic];
                  //  NSLog(@"data here is %lu",(unsigned long)imagesArray.count);
                }
//                                [imagesArray addObject:[url relativePath] ];
                  [self reloadCollectionViewAndChangeLayout];
                   
            }];
            
        }
        
        
    }
 //   NSLog(@"images array is %@",images);
    
}
-(BOOL)assetsPickerController:(GMImagePickerController *)picker shouldSelectAsset:(PHAsset *)asset{
    float __block previousTotal;
    
    if (totalImages<10&&totalSize<11) {
        
        
        
       
        PHImageRequestOptions * options = [[PHImageRequestOptions alloc] init];
        options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
        options.resizeMode = PHImageRequestOptionsResizeModeExact;
        
        options.synchronous = YES; //Set this to NO if is needed
        
        [[PHImageManager defaultManager] requestImageDataForAsset:asset
                                                          options:options
                                                    resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info)
         {
             //(CGFloat)imageData.length returns size in bytes
             NSLog(@"%@",[NSString stringWithFormat:@"%.2lf MB", ((CGFloat)imageData.length)/1024/1024]);
                         float imageSize = imageData.length;
                         //convert to Megabytes
                         imageSize = imageSize/(1024*1024);
                         NSLog(@"%f",imageSize);
                         previousTotal = totalSize;
                         previousTotal+=imageSize;
         }];

        if (previousTotal<11) {
            return YES;
        }
        else{
              [commonUtils showAlert:@"Error!" withMessage:@"You have reached maximum data limit"];
           return NO;
        }
        
    }
    else{
        [commonUtils showAlert:@"Error!" withMessage:@"You have reached maximum image limit"];
        return NO;
    }

}
-(void)assetsPickerController:(GMImagePickerController *)picker didSelectAsset:(PHAsset *)asset{
    [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
        float imageSize = imageData.length;
        //convert to Megabytes
        imageSize = imageSize/(1024*1024);
        NSLog(@"%f",imageSize);
        totalSize+= imageSize;
        recentSize+=imageSize;
        NSLog(@"total size is %f",totalSize);
    }];
    
    totalImages++;
}
-(void)assetsPickerController:(GMImagePickerController *)picker didDeselectAsset:(PHAsset *)asset{
    [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
        float imageSize = imageData.length;
        //convert to Megabytes
        imageSize = imageSize/(1024*1024);
        NSLog(@"%f",imageSize);
        totalSize-= imageSize;
    }];
    totalImages--;
}
//Optional implementation:
-(void)assetsPickerControllerDidCancel:(GMImagePickerController *)picker
{
 [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];

    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {

        statusBar.backgroundColor = [UIColor whiteColor];
    }
    totalSize-=recentSize;
    shouldNotClear = YES;
    NSLog(@"GMImagePicker: User pressed cancel button");
}
-(void)reloadCollectionViewAndChangeLayout{
//    dispatch_async(dispatch_get_main_queue(), ^(void){
//        [commonUtils resizeFrame:addImageViewNew withWidth:87.0f withHeight:addImageViewNew.frame.size.height];
//        [commonUtils moveView:addImageViewNew withMoveX:SCREEN_WIDTH - 87 withMoveY:addImageViewNew.frame.origin.y
//         ];
    [photoCv reloadData];
   // [UIView animateWithDuration:1.0f animations:^{
  
    [self performSelector:@selector(ChangeFrame) withObject:nil afterDelay:0.5];
   // }];
//           });

}


-(void)ChangeFrame{
    NSLog(@"imageview frame before %f",addImageViewNew.frame.origin.x);
    NSLog(@"imageview frame before %f",addImageViewNew.frame.size.width);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
    dispatch_async(dispatch_get_main_queue(), ^{
       // [UIView animateWithDuration:1.0f animations:^{
            [commonUtils resizeFrame:addImageViewNew withWidth:87.0f withHeight:addImageViewNew.frame.size.height];
            [commonUtils moveView:addImageViewNew withMoveX:SCREEN_WIDTH - 87 withMoveY:addImageViewNew.frame.origin.y
             ];
      //  }];
//        addImageViewNew.frame = CGRectMake(150, 0, 87, addImageViewNew.frame.size.height);
//      [addImageViewNew setNeedsDisplay];
//        [_photoVideoView setNeedsDisplay];
       //  [addImageViewNew setNeedsLayout];
    });
   });
    NSLog(@"imageview frame after %f",addImageViewNew.frame.origin.x);
    NSLog(@"imageview frame after %f",addImageViewNew.frame.size.width);
}

-(void)getAddressFromGoogle{
      [currentTextField resignFirstResponder];
    
                if ([commonUtils getUserDefault:@"currentLatitude"] && [commonUtils getUserDefault:@"currentLongitude"]  ) {
                   
                    if ([self checkTimeValidation]) {
                        
                        [self addIce:imagesArray];
                    }
                   
                    
                }
                else{
                    [commonUtils showAlert:@"Error!" withMessage:@"Please Turn On Location Services To Proceed"];
                }
    

}
-(void)clearAddIceData{
   dispatch_async(dispatch_get_main_queue(), ^{
       inviteUserIDs = [NSMutableArray new];
       invitedGroups = [NSMutableArray new];
       _endView.layer.borderColor = [UIColor clearColor].CGColor;
       _endView.layer.borderWidth = 1.0;
    startsDateLbl.text = @"";
    endsDateLbl.text = @"";
    startsTimeLbl.text = @"__";
    endsTimeLbl.text = @"__";
    
    
    tokens = [[NSMutableArray alloc] init];
    [tokenField reloadData];
    self.locationLbl.text = @"\n__";
    iceTitleTxt.text = @"";
    self.reminderTF.text = @"";
    detailTxt.text = @"";
       totalImages = 0;
    imagesArray = [[NSMutableArray alloc] init];
    commonUtils.pickedLocationCord = CLLocationCoordinate2DMake(0, 0);
    [photoCv reloadData];
    [self.imgCollectionView reloadData];
    addApiCalled = NO;
       totalSize = 0;
    commonUtils.userPickedCord = CGPointZero;
       commonUtils.shouldPickPreviousStartTime = NO;
       commonUtils.shouldPickPreviousEndTime = NO;
       
       [self setInitialStartAndEndTime];
           });
    
    
}
-(void)showSuccessAlertAndGoBack{
 
    
    
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Success"
                                                                  message:@"Images & Videos Are Uploading"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    [self resetCameraView];

                                    if (self.isshowMenu) {
                                        [self addIce:imagesArray];
                                        [self.navigationController popViewControllerAnimated:YES];
                                    }
                                    else{
                                        [self addIce:imagesArray];
                                        HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
                                        [homeVC onShowTabView:[AppDelegate sharedAppDelegate].ViewMain];
                                        [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
                                        //     [self.sideMenuController hideLeftViewAnimated:sender];
                                        
                                        
                                    }

                                    
                                    
                                }];
    [alert addAction:yesButton];

    
    [self presentViewController:alert animated:YES completion:nil];

}
-(UIImage*)getThumbnailFromArray:(NSMutableArray*)Array index:(NSInteger)index{
  
        

    if ([Array[index][@"pickedType"]isEqualToString:@"galery"]) {
        
        
        NSURL *assetURL = [NSURL fileURLWithPath:Array[index][@"image"]];
        AVURLAsset *asset = [AVURLAsset assetWithURL:assetURL];
        
        //AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:imagesArray[indexPath.row] options:nil];
        
        
        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generate.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
        
        UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
        return img;
  
    }
    else{
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:Array[index][@"image"] options:nil];
        AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
        generate.appliesPreferredTrackTransform = YES;
        NSError *err = NULL;
        CMTime time = CMTimeMake(1, 60);
        CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
        
       UIImage *img = [[UIImage alloc] initWithCGImage:imgRef];
        return img;

        
    }
    

}
- (void) enableStartCalendar:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    isStartDate = YES;
    
}
- (void) enableEndCalendar:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    isEndDate = YES;
   
    
}

- (void) locationPicked:(NSNotification *) notification
{
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    [self checkTextfieldValue];
    
    
}

-(void)showMediaOptions{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Choose Media Type"
                                                                  message:@""
                                                           preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"Photos"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                    if (totalImages<10&&totalSize<11) {
                                        PHAuthorizationStatus status = [PHPhotoLibrary authorizationStatus];
                                        
                                        if (status == PHAuthorizationStatusAuthorized) {
                                          //    [self launchGMImagePicker];
                                            [self openGallery];
                                            // Access has been granted.
                                        }
                                        
                                        else if (status == PHAuthorizationStatusDenied) {
                                            // Access has been denied.
                                            [commonUtils showAlert:@"Error!" withMessage:@"Go to settings and allow the app to acess photos"];
                                        }
                                        
                                        else if (status == PHAuthorizationStatusNotDetermined) {
                                            
                                            // Access has not been determined.
                                            [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
                                                
                                                if (status == PHAuthorizationStatusAuthorized) {
                                                    // Access has been granted.
                                                      [self openGallery];
                                                }
                                                
                                                else {
                                                          [commonUtils showAlert:@"Error!" withMessage:@"Go to settings and allow the app to acess photos"];
                                                    // Access has been denied.
                                                }
                                            }];  
                                        }
                                        
                                        else if (status == PHAuthorizationStatusRestricted) {
                                            // Restricted access - normally won't happen.
                                        }
                                        
                                        
                                        
                                        
                                    }
                                    else{
                                        [self dismissViewControllerAnimated:YES completion:nil];
                                        dispatch_async(dispatch_get_main_queue(), ^(void){
                                              [self dismissSemiModalView];
                                        });
                                      
                                        [commonUtils showAlert:@"Error!" withMessage:@"You have reached maximum image limit"];

                                    }
                                }];
    
    UIAlertAction* noButton = [UIAlertAction actionWithTitle:@"Videos"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction * action)
                               {
                                   if (totalImages<10&&totalSize<11) {
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       dispatch_async(dispatch_get_main_queue(), ^(void){
   
                                       [self dismissSemiModalView];
                                           });
                                     [self showVideoPicker];
                                   }
                                   else{
                                       [self dismissViewControllerAnimated:YES completion:nil];
                                       
                                       [self dismissSemiModalView];
                                       [self showMaxVideoAlert];
                                   }
                                   /** What we write here???????? **/
                                   NSLog(@"you pressed No, thanks button");
                                   // call method whatever u need
                               }];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self dismissSemiModalView];
    }];
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
     [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];

}
-(IBAction)onClickGallery:(id)sender{
    [self showMediaOptions];
}
-(void)showMaxVideoAlert{
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error!"
                                                                  message:@"You have reached maximum video limit!"
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action)
                                {
                                   
                                }];

    [alert addAction:yesButton];
     [self presentViewController:alert animated:YES completion:nil];
}
-(NSMutableAttributedString*)decorateTags:(NSString *)stringWithTags{
    
    
    NSError *error = nil;
    
    //For "Vijay #Apple Dev"
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"#(\\w+)" options:0 error:&error];
    
    //For "Vijay @Apple Dev"
    //NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"@(\\w+)" options:0 error:&error];
    
    NSArray *matches = [regex matchesInString:stringWithTags options:0 range:NSMakeRange(0, stringWithTags.length)];
    NSMutableAttributedString *attString=[[NSMutableAttributedString alloc] initWithString:stringWithTags];
    
    NSInteger stringLength=[stringWithTags length];
    
    for (NSTextCheckingResult *match in matches) {
        
        NSRange wordRange = [match rangeAtIndex:1];
        
        NSString* word = [stringWithTags substringWithRange:wordRange];
        
        //Set Font
        UIFont *font=[UIFont fontWithName:@"Helvetica-Bold" size:15.0f];
        [attString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, stringLength)];
        
        
        //Set Background Color
        UIColor *backgroundColor=[UIColor orangeColor];
        [attString addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:wordRange];
        
        //Set Foreground Color
        UIColor *foregroundColor=[UIColor blueColor];
        [attString addAttribute:NSForegroundColorAttributeName value:foregroundColor range:wordRange];
        
        NSLog(@"Found tag %@", word);
        
    }
    
    // Set up your text field or label to show up the result
    
    //    yourTextField.attributedText = attString;
    //
    //    yourLabel.attributedText = attString;
    
    return attString;
}
-(void)checkTextfieldValue{
    if (startsDateLbl.text.length>0) {
        NSLog(@"start is present %@",startsDateLbl.text);
    }
    if ((CLLocationCoordinate2DIsValid(commonUtils.pickedLocationCord)&&!(commonUtils.pickedLocationCord.latitude==0)&&!(commonUtils.pickedLocationCord.longitude==0))){
        NSLog(@"location is present");
    }
    if (imagesArray.count>0){
          NSLog(@"images are present");
    }
    if (startsDateLbl.text.length>0&& endsDateLbl.text.length>0 && iceTitleTxt.text.length>0 && (CLLocationCoordinate2DIsValid(commonUtils.pickedLocationCord)&&!(commonUtils.pickedLocationCord.latitude==0)&&!(commonUtils.pickedLocationCord.longitude==0))&&imagesArray.count>0&&!endTimeNotValid) {
        iceCheckImg.alpha = 1.0;
        iceCheckBtn.enabled = YES;
    }
    else{
        iceCheckImg.alpha = 0.2;
        iceCheckBtn.enabled = NO;
    }

}
- (IBAction)onDetailBtn:(id)sender {
    _onDetailOutlet.hidden = YES;
    [detailTxt setEditable:YES];
    [detailTxt becomeFirstResponder];
    
    
}


-(void)resetCameraView2{

    dispatch_async(dispatch_get_main_queue(), ^{
        addImageViewNew.frame = CGRectMake(0, 0, self.view.frame.size.width, addImageViewNew.frame.size.height);
 });
    

}
-(void)resetCameraView{
 
    [self resetCameraView2];
//    NSLog(@"imageview frame %f",addImageView.frame.origin.y);
//   NSLog(@"imageview frame %f",addImageView.frame.origin.x);
//    NSLog(@"imageview frame %f",addImageView.frame.size.width);
//    NSLog(@"imageview frame %f",addImageView.frame.size.height);
//    NSLog(@"imageview frameyyyyyy %f",btn.frame.origin.x);
//    NSLog(@"imageview fasdasdrame %f",btn.frame.origin.y);
//    NSLog(@"imageview frameyyyyyy %f",btn.frame.size.width);
//    NSLog(@"imageview fasdasdrame %f",btn.frame.size.height);
    
    
//    dispatch_async(dispatch_get_main_queue(), ^{
  
        
//        if (addImageView.frame.origin.x == SCREEN_WIDTH-87) {
//        addImageView.center = CGPointMake(self.view.center.x, addImageView.center.y);
//                        [commonUtils moveView:addImageViewNew withMoveX:00 withMoveY:addImageViewNew.frame.origin.y
//                         ];
    
//    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
//
//    addImageView.frame = app.frame;
//        }

       
      
//   });
 
}
-(void)iceDone:(UIButton *)sender {
    [icedDoneAlertView close];
    
    if (self.isshowMenu) {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        HomeVC *homeVC = [[((SidePanelVC*)self.sideMenuController).ActivitiesNavigation viewControllers] firstObject];
        [homeVC onShowTabView:[AppDelegate sharedAppDelegate].ViewMain];
        [self.sideMenuController setRootViewController:((SidePanelVC*)self.sideMenuController).ActivitiesNavigation];
        [self.sideMenuController hideLeftViewAnimated:sender];
 
    }
    if ([[commonUtils.sharingOptions valueForKey:@"facebook"]isEqualToString:@"yes"]||[[commonUtils.sharingOptions valueForKey:@"twitter"]isEqualToString:@"yes"]){
        [commonUtils shareEvent:ice_id];
    }
    
}
- (void) showTwitter:(NSNotification *) notification
{
    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    NSArray *accounts = [accountStore accountsWithAccountType:accountType];
    ACAccount *twitterAccount = [accounts lastObject];
    if (!twitterAccount) {
        
        
        if (![[Twitter sharedInstance]sessionStore]) {
            [icedDoneAlertView close];
            [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                
                if (session) {
                    NSLog(@"signed in as %@", [session userName]);
                    NSLog(@"session token is %@",[session authToken]);
                    
                    commonUtils.tAccessToken = [session authToken];
                    commonUtils.tAuthSecret = [session authTokenSecret];
                    
                } else {
                    [commonUtils showAlert:@"Error!" withMessage:@"Please login in the twitter app to share"];
                    NSLog(@"error: %@", [error localizedDescription]);
                }
            }];
            
            
        }
        else{
            TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
            
            TWTRSession *lastSession = store.session;
            NSLog(@"signed in as %@", [lastSession userName]);
            NSLog(@"session token is %@",[lastSession authToken]);
            commonUtils.tAccessToken = [lastSession authToken];
            commonUtils.tAuthSecret = [lastSession authTokenSecret];
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            
            [client requestEmailForCurrentUser:^(NSString *email, NSError *error) {
                if (email) {
                    NSLog(@"signed in as %@", email);
                } else {
                    NSLog(@"error: %@", [error localizedDescription]);
                }
                
            }];
        }
        
    }
    
}



- (void) showGoogle:(NSNotification *) notification{
    if (![[GIDSignIn sharedInstance]hasAuthInKeychain]) {
        [icedDoneAlertView close];
    }
    

     NSArray *currentScopes = [GIDSignIn sharedInstance].scopes;
    NSLog(@"current scopes are %@",currentScopes);
    [[GIDSignIn sharedInstance] signIn];
    
    
}
-(void)showSharingAgain{
    icedDoneAlertView = [[CustomIOSAlertView alloc]init];
    
    // Add some custom content to the alert view
    [icedDoneAlertView setContainerView:[self createIcedDoneCustomView]];
    
    [icedDoneAlertView setUseMotionEffects:true];
    [icedDoneAlertView setCloseOnTouchUpOutside:YES];
    
    visibleAlertView = icedDoneAlertView;
    [icedDoneAlertView show];
}
- (void)toggleAuthUI {
    if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
    }
}
- (void) receiveToggleAuthUINotification:(NSNotification *) notification {
    if ([notification.name isEqualToString:@"ToggleAuthUINotification"]) {
        [self toggleAuthUI];
        NSLog(@"user info is %@",notification.userInfo);
        
    }
}
-(void)showFacebook:(NSNotification*)notification{
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
    
    if([[FBSDKAccessToken currentAccessToken]hasGranted:@"publish_actions"]){
        NSLog(@"token is %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        commonUtils.fAccessToken = [[FBSDKAccessToken currentAccessToken]tokenString];
       // NSLog(@"saved token is %@",commonUtils.fAccessToken);
        
    }else{
        [icedDoneAlertView close];
        [login
         logInWithPublishPermissions: @[@"publish_actions"]
         fromViewController:self
         handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
             if ([result.declinedPermissions containsObject:@"publish_actions"]) {
                 // TODO: do not request permissions again immediately. Consider providing a NUX
                 // describing  why the app want this permission.
             } else {
                 // ...
             }
             if (error) {
                 
                 NSLog(@"Process error");
             } else if (result.isCancelled) {
                 [commonUtils hideHud];
                 NSLog(@"Cancelled");
             } else {
                 NSLog(@"Logged in with token : %@", result.token.tokenString);
                 commonUtils.fAccessToken = result.token.tokenString;
             }
         }];
    }
}
-(BOOL)checkTimeValidation{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [commonUtils showHud:self.view];
        [commonUtils SetWindow:false];
    });
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    [currentTextField resignFirstResponder];
    [detailTxt resignFirstResponder];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
        
        return false;
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [commonUtils showHud:self.view];
        [commonUtils SetWindow:false];
    });
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.latitude];
    NSString *currentLongtitude = [NSString stringWithFormat:@"%f",commonUtils.pickedLocationCord.longitude];
    NSString *startTimeDateString = [NSString stringWithFormat:@"%@ %@",startsDateLbl.text,startsTimeLbl.text];
    NSString *endTimeDateString =  [NSString stringWithFormat:@"%@ %@",endsDateLbl.text,endsTimeLbl.text];
    NSDateFormatter *dateFormat = [NSDateFormatter new];
    
    if ([commonUtils is24hourFormat]) {
        startTimeDateString = [startTimeDateString stringByReplacingOccurrencesOfString:@" AM"
                                                                         withString:@""];
        startTimeDateString = [startTimeDateString stringByReplacingOccurrencesOfString:@" PM"
                                                                         withString:@""];
        
        
        [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
    }else {
        [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
    }
    
    
    NSDate *mydate = [dateFormat dateFromString:startTimeDateString];
    

    [dateFormat setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    
    iceStartTime = [dateFormat stringFromDate:mydate];
    if ([commonUtils is24hourFormat]) {
        endTimeDateString = [endTimeDateString stringByReplacingOccurrencesOfString:@" AM"
                                       withString:@""];
        endTimeDateString = [endTimeDateString stringByReplacingOccurrencesOfString:@" PM"
                                                                         withString:@""];
        
        
        [dateFormat setDateFormat:@"MMM d, yyyy HH:mm"];
    }else {
        [dateFormat setDateFormat:@"MMM d, yyyy hh:mm a"];
    }
    

    NSDate *mydate2 = [dateFormat dateFromString:endTimeDateString];
    
    [dateFormat setDateFormat:@"yyyy-M-dd HH:mm:ss"];

    
    iceEndTime = [dateFormat stringFromDate:mydate2];
    NSString *serverUrl = [NSString stringWithFormat:@"%@check/time?start_date=%@&end_date=%@&lat=%@&lng=%@",ServerUrl,iceStartTime,iceEndTime,currentLatitude,currentLongtitude];
    
    NSString *serverUrlwithoutSpace = [serverUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSLog(@"serverUrl %@",serverUrlwithoutSpace);
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrlwithoutSpace]];
    // Create a mutable copy of the immutable request and add more headers
    NSMutableURLRequest *mutableRequest = [request mutableCopy];
    [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
    [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
    
    // Now set our request variable with an (immutable) copy of the altered request
    request = [mutableRequest copy];
    
    // Log the output to make sure our new headers are there
    NSURLResponse *response;
    
    NSError *error = nil;
    
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if(error!=nil)
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [commonUtils hideHud];
            [commonUtils SetWindow:true];
            [self removeTransparentView];
    });
    }
    else
    {
        if(receivedData !=nil)
        {
            NSError *Jerror = nil;
            
            NSDictionary* json =[NSJSONSerialization
                                 JSONObjectWithData:receivedData
                                 options:kNilOptions
                                 error:&Jerror];
            if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                    [[NSNotificationCenter defaultCenter]
                     postNotificationName:@"Logout"
                     object:self];
                }
            }else if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                
                    //Run UI Updates
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [commonUtils hideHud];
                        [commonUtils SetWindow:true];
                        [self removeTransparentView];
                    });
                    int suceesData = (int)[json[@"successData"]integerValue];
                if (suceesData == 0) {
                    [commonUtils showAlert:@"Cannot ICE Event" withMessage:@"Event cannot be longer than a 30 day period"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [commonUtils hideHud];
                        [commonUtils SetWindow:true];
                        [self removeTransparentView];
                    });
                    return NO;
                }
                else if (suceesData == 1){
                    return YES;
                }
               
            }
            else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [commonUtils hideHud];
                    [commonUtils SetWindow:true];
                    [self removeTransparentView];
                });
                [commonUtils showAlert:@"Cannot ICE Event" withMessage:@"Event cannot be longer than a 30 day period"];
                return NO;
            }
            if(Jerror!=nil)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                    [commonUtils SetWindow:true];
            });
                 NSLog(@"json error:%@",Jerror);
                [commonUtils showAlert:@"Cannot ICE Event" withMessage:@"Event cannot be longer than a 30 day period"];
                return NO;
               
            }
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [commonUtils hideHud];
        [commonUtils SetWindow:true];
        [self removeTransparentView];
        
    });
    [commonUtils showAlert:@"Cannot ICE Event" withMessage:@"Event cannot be longer than a 30 day period"];
   return NO;
}
-(void)addTransparentView{
   transparentView = [[UIView alloc] init];
    transparentView.frame = contentView.frame;
    NSLog(@"frame is %f",transparentView.frame.size.height);
    transparentView.backgroundColor = [UIColor lightGrayColor];
    transparentView.alpha = 0.5;
    [scrollView addSubview:transparentView];
    [scrollView bringSubviewToFront:transparentView];
}
-(void)removeTransparentView{
    [transparentView removeFromSuperview];
}
-(void)openGallery{
    if(noCamera) {
        [commonUtils showVAlertSimple:@"Warning" body:@"Your device has no camera" duration:1.0f];
        return;
    }
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = false;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//    picker.allowsEditing = NO;
     [picker setMediaTypes:@[(NSString *)kUTTypeImage]];

 
    if (totalImages<10&&totalSize<11) {
        if (totalImages==10) {
            picker.mediaTypes =[[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        }
        isImagedone = false;
        [self presentViewController:picker animated:YES completion:NULL];
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            isImagedone = false;
            [self dismissViewControllerAnimated:YES completion:nil];
            [self dismissSemiModalView];
        });
        [commonUtils showAlert:@"Error!" withMessage:@"You have reached maximum limit"];
        
    }
}
-(void)setInitialStartAndEndTime{
    startSelected = [NSDate new];
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
//    [dateFormatter setDateFormat:@"hh"];
//    NSString *hoursNow = [dateFormatter stringFromDate:startSelected];
//    [dateFormatter setDateFormat:@"mm"];
//    NSString *minutesNow = [dateFormatter stringFromDate:startSelected];
//    NSInteger minutes = [minutesNow integerValue];
    
    NSString *startTime;
    NSString *endTime;
    NSString *startDate,*endDate;

    if ([commonUtils is24hourFormat]) {
        
        [dateFormatter setDateFormat:@"HH"];
    }else {
        [dateFormatter setDateFormat:@"hh"];
    }
    NSString *startTimeString = [NSString stringWithFormat:@"%@:00",[dateFormatter stringFromDate:startSelected]];
    if ([commonUtils is24hourFormat]) {
        startTime = startTimeString;
        
    }else {
        [dateFormatter setDateFormat:@"a"];
        NSString *amOrPmForStart = [dateFormatter stringFromDate:startSelected];
        startTime = [NSString stringWithFormat:@"%@ %@",startTimeString,amOrPmForStart];
    }
    
     [dateFormatter setDateFormat:@"MMM d, yyyy"];
    startDate = [dateFormatter stringFromDate:startSelected];
    
    if (durationInMinutes == 0) {
        durationInMinutes = 1 * 60 * 60;
    }
    
    
        endSelected = [startSelected dateByAddingTimeInterval:durationInMinutes];
    if ([commonUtils is24hourFormat]) {
        
        [dateFormatter setDateFormat:@"HH"];
    }else {
        [dateFormatter setDateFormat:@"hh"];
    }
        NSString *endTimeString = [NSString stringWithFormat:@"%@:00",[dateFormatter stringFromDate:endSelected]];
    if ([commonUtils is24hourFormat]) {
        
        
        endTime =endTimeString;

    }else {
        [dateFormatter setDateFormat:@"a"];
        NSString *amOrPmForEnd = [dateFormatter stringFromDate:endSelected];
        endTime = [NSString stringWithFormat:@"%@ %@",endTimeString,amOrPmForEnd];

    }
    [dateFormatter setDateFormat:@"MMM d, yyyy"];

        endDate = [dateFormatter stringFromDate:endSelected];
        
   // }

    startsTimeLbl.text = startTime;
    
    endsTimeLbl.text = endTime;
    startsDateLbl.text = startDate;
    endsDateLbl.text = endDate;
    NSLog(@"endsTimeLbl %@",endsTimeLbl.text);
    NSLog(@"start date is %@",startDate);
    NSLog(@"end date is %@",endDate);
    NSLog(@"start time is %@",startTime);
    NSLog(@"end time is %@",endTime);
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
   
    // Do something with the selected place.
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    commonUtils.pickedLocationCord = place.coordinate;
    shortAddress = [NSString stringWithFormat:@"%@",place.name];
    NSString *placeLat = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
     NSString *placeLng = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
   
    
    NSString *strCity = @"";
    NSString *strCountry = @"";
    
    for (GMSAddressComponent *component in place.addressComponents){
        NSLog(@"Type is %@",component.type);
        NSLog(@"name is %@",component.name);
        if ([component.type isEqualToString:@"locality"]) {
            NSLog(@"name is %@",component.name);
            strCity = component.name;
        }else if ([component.type isEqualToString:@"country"]) {
            NSLog(@"name is %@",component.name);
            strCountry = component.name;
        }else if ([component.type isEqualToString:@"administrative_area_level_2"]) {
            NSLog(@"name is %@",component.name);
            if ([strCity isEqualToString:@""]){
                strCountry = component.name;
            }
            
        }

    }
    
    
    if ([shortAddress isEqualToString:strCity]){
        shortAddress = [NSString stringWithFormat:@"%@, %@",shortAddress,strCountry];
    }else {
        shortAddress = [NSString stringWithFormat:@"%@,  %@, %@",shortAddress,strCity,strCountry];
    }
    
    
    pickedAddress = shortAddress;
    
//    shortAddress = pickedAddress;
    NSLog(@"short address is %@",shortAddress);
    _locationLbl.text = shortAddress;
    [self dismissViewControllerAnimated:YES completion:nil];
    
//     [self getPlaceAttributes:placeLat :placeLng];
    
}
// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
- (void)presentController:(UIViewController *)controller push:(BOOL)push sender:(id)sender
{
    if (_popoverController) {
        [_popoverController dismissPopoverAnimated:YES];
        _popoverController = nil;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        
        if (push) {
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
            navigationController.preferredContentSize = CGSizeMake(320.0, 520.0);
            _popoverController = [[UIPopoverController alloc] initWithContentViewController:navigationController];
        }
        else {
            _popoverController = [[UIPopoverController alloc] initWithContentViewController:controller];
        }
        
        [_popoverController presentPopoverFromBarButtonItem:sender permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        if (push) {
            [self.navigationController pushViewController:controller animated:YES];
        }
        else {
            [self presentViewController:controller animated:YES completion:NULL];
        }
    }
}
- (void)dismissController:(UIViewController *)controller
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [_popoverController dismissPopoverAnimated:YES];
    }
    else {
        if (controller.presentingViewController) {
            [controller dismissViewControllerAnimated:YES completion:NULL];
        }
        else {
            [controller.navigationController popViewControllerAnimated:YES];
        }
    }
}
-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_height sizeImage : (CGSize) sizeimg
{
    float oldheight = sizeimg.height;
    float scaleFactor = i_height / oldheight;
   
    float newWidth = sizeimg.width * scaleFactor;
    float newHeight = oldheight * scaleFactor;
    NSLog(@"new height is %f",newHeight);
     NSLog(@"new width is %f",newWidth);
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (CGSize) aspectScaledImageSizeForImageView:(UIImageView *)iv image:(UIImage *)im {
    
    float x,y;
    float a,b;
    x = iv.frame.size.width;
    y = iv.frame.size.height;
    a = im.size.width;
    b = im.size.height;
    
    if ( x == a && y == b ) {           // image fits exactly, no scaling required
        // return iv.frame.size;
    }
    else if ( x > a && y > b ) {         // image fits completely within the imageview frame
        if ( x-a > y-b ) {              // image height is limiting factor, scale by height
            a = y/b * a;
            b = y;
        } else {
            b = x/a * b;                // image width is limiting factor, scale by width
            a = x;
        }
    }
    else if ( x < a && y < b ) {        // image is wider and taller than image view
        if ( a - x > b - y ) {          // height is limiting factor, scale by height
            a = y/b * a;
            b = y;
        } else {                        // width is limiting factor, scale by width
            b = x/a * b;
            a = x;
        }
    }
    else if ( x < a && y > b ) {        // image is wider than view, scale by width
        b = x/a * b;
        a = x;
    }
    else if ( x > a && y < b ) {        // image is taller than view, scale by height
        a = y/b * a;
        b = y;
    }
    else if ( x == a ) {
        a = y/b * a;
        b = y;
    } else if ( y == b ) {
        b = x/a * b;
        a = x;
    }
    return CGSizeMake(a,b);
    
}


- (UIImage *)scaleImageToSize:(CGSize)newSize image: (UIImage *)img{
    
    CGRect scaledImageRect = CGRectZero;
    
    CGFloat aspectWidth = newSize.width / img.size.width;
    CGFloat aspectHeight = newSize.height / img.size.height;
    CGFloat aspectRatio = MIN ( aspectWidth, aspectHeight );
    
    scaledImageRect.size.width = img.size.width * aspectRatio;
    scaledImageRect.size.height = img.size.height * aspectRatio;
    scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0f;
    scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0f;
    
    UIGraphicsBeginImageContextWithOptions( newSize, NO, 0 );
    [img drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return scaledImage;
    
}
-(void)getPlaceAttributes:(NSString*)lat :(NSString*)lng{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
        });
        
        return;
    }

        
      //  http://maps.googleapis.com/maps/api/geocode/json?latlng=29.751049,-95.362133&sensor=false
        
        NSString *serverUrl = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=false",lat,lng];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:serverUrl]];
        
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
    
        
        
        request = [mutableRequest copy];
    
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            if(error!=nil)
            {
                //            NSLog(@"web service error:%@",error);
            }
            else
            {
                if(data !=nil)
                {
                    NSError *Jerror = nil;
                    
                    NSDictionary* json =[NSJSONSerialization
                                         JSONObjectWithData:data
                                         options:kNilOptions
                                         error:&Jerror];
                    
                    
                    if ([[json valueForKey:@"status"]isEqualToString:@"OK"]) {
                        NSMutableArray *results = [NSMutableArray new];
                        results = json[@"results"];
                        NSMutableDictionary *resultsDic = results[0];
                        NSMutableArray *addressComponents = [NSMutableArray new];
                        addressComponents = resultsDic[@"address_components"];
                        for (NSMutableDictionary* componentdDic in addressComponents) {
                            NSMutableArray *types = [NSMutableArray new];
                            types = componentdDic[@"types"];
                            if ([types containsObject:@"administrative_area_level_1"]) {
                                 [self dismissViewControllerAnimated:YES completion:nil];
                                NSString *stateName = [NSString stringWithFormat:@"%@",componentdDic[@"short_name"]];
                                NSLog(@"state name is %@",stateName);
                                shortAddress = [NSString stringWithFormat:@"%@,  %@",shortAddress,stateName];
                                NSLog(@"short address is %@",shortAddress);
                                _locationLbl.text = shortAddress;
                            }
                        }
          
                    }
                    else{
              
                        
                    }
                    if(Jerror!=nil)
                    {
             
                    }
                }
            }
        }];
}



- (NSString *)ReturnStringInDate:(NSDate *)chooseDate withformat:(NSString *)format{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    
    return [dateFormatter stringFromDate:chooseDate];
}

- (NSString *)ReturnTimeInDate:(NSDate *)chooseDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    if ([commonUtils is24hourFormat]) {
        [dateFormatter setDateFormat:@"HH:mm"];
    }else {
        [dateFormatter setDateFormat:@"hh:mm a"];
    }

    
    return [dateFormatter stringFromDate:chooseDate];
}

@end

