//
//  ICEVC.h
//  ICE
//
//  Created by LandToSky on 1/12/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "IcedDoneAlertView.h"
#import "CustomIOSAlertView.h"
#import "GoogleSignIn/GoogleSignIn.h"
@interface ICEVC : BaseViewController<UIPickerViewDelegate,UIPickerViewDataSource,GIDSignInUIDelegate>
@property (weak, nonatomic) IBOutlet UITextField *reminderTF;
@property (weak, nonatomic) IBOutlet UILabel *locationLbl;
@property (weak, nonatomic) IBOutlet UIView *imageToChooseView;
@property (weak, nonatomic) IBOutlet UICollectionView *imgCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *hideImgCollectionViewBtn;
@property (weak, nonatomic) IBOutlet UIButton *imgBtn;
@property (weak, nonatomic) IBOutlet UITextField *houtMintTF;
@property (strong, nonatomic) UIPickerView *hourPicker;
@property (strong, nonatomic) NSArray *pickerElements;
- (IBAction)pickerBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *onClickGalleryOutlet;

@property ( nonatomic) BOOL isshowMenu;
@property ( nonatomic) CustomIOSAlertView *visibleAlertView;
@property ( nonatomic) CustomIOSAlertView *pendingAlertView;
@property (strong, nonatomic) NSMutableArray *pendingAlertViews;
@property (weak, nonatomic) IBOutlet UIView *guestCheckView;
@property (weak, nonatomic) IBOutlet UIView *inviteIconView;
- (IBAction)onDetailBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *onDetailOutlet;
@property (nonatomic)    BOOL shouldNotClear;
@property (weak, nonatomic) IBOutlet UIView *endView;
@property (weak, nonatomic) IBOutlet UIView *photoVideoView;
@property (weak, nonatomic) IBOutlet UIView *startView;

@end
