//
//  EventsAddedVC.m
//  ICE
//
//  Created by LandToSky on 12/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventsAddedVC.h"
#import "EventsAddedTVCell.h"
#import "EventsPrivateTVCell.h"
#import "NSDate+Compare.h"
#import "UserProfileVC.h"
#import "DetailVC.h"

@interface EventsAddedVC ()<UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *addedIces;
    BOOL isPageRefresing;
    int currentPagingNumber;
    NSString *zone;
}


@end

@implementation EventsAddedVC
@synthesize userID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    zone = @"PST";
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    currentPagingNumber = 0;
    [self getAddedIced:currentPagingNumber];
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addedIces.count;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsAddedTVCell *cell = (EventsAddedTVCell*) [tableView dequeueReusableCellWithIdentifier:@"EventsAddedTVCell"];
    if (cell == nil) {
        cell = [[EventsAddedTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventsAddedTVCell"];
    }
    cell.eventTitleLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"title"]];
    cell.addressLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"location"]];
    cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_day"]];
    cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_day_start"]];
    cell.eventMnthLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_month"]];
    NSString *eventStartTime = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_start_time"]];
    NSString *eventEndTime =  [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_end_time"]];
    if ([commonUtils is24hourFormat]) {
         cell.timeLbl.text = [NSString stringWithFormat:@"%@-%@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
    }
    else{
        cell.timeLbl.text = [NSString stringWithFormat:@"%@-%@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
    }
   
    cell.userName.text = [NSString stringWithFormat:@"via %@",addedIces[indexPath.row][@"get_user"][@"first_name"]];
    cell.userProfileBtn.tag = indexPath.row;
    [cell.userProfileBtn addTarget:self action:@selector(onUserProfilePage:) forControlEvents:UIControlEventTouchUpInside];
    cell.eventDetailBtn.tag = indexPath.row;
    [cell.eventDetailBtn addTarget:self action:@selector(onEventDetailPage:) forControlEvents:UIControlEventTouchUpInside];
    NSString *imageUrl = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"get_user"][@"photo"]];
    BOOL isLive = [addedIces[indexPath.row][@"is_live"]boolValue];
    if (isLive) {
        cell.liveLbl.hidden = NO;
    }
    else{
        cell.liveLbl.hidden = YES;
    }
    NSDate *today = [NSDate date];
    NSDate *todayDate = [self toLocalTime:today];
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDateFormatter *serverDateFormatter = [NSDateFormatter new];
    [serverDateFormatter setDateFormat:@"yyyy-M-dd"];
    NSDate *endDateFromApi = [startTimeformatter dateFromString:addedIces[indexPath.row][@"end_date"]];
    NSDate *startDateFromApi = [startTimeformatter dateFromString:addedIces[indexPath.row][@"start_date"]];
    
    NSString *serverStartTime = [serverDateFormatter stringFromDate:startDateFromApi];
    NSString *serverEndTime = [serverDateFormatter stringFromDate:endDateFromApi];
   
    if (!isLive) {
        
        
        cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_day"]];
        cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_day_start"]];
        cell.eventMnthLbl.text  = [NSString stringWithFormat:@"%@",addedIces[indexPath.row][@"event_month"]];
        if ([serverStartTime isEqualToString:serverEndTime]) {
            if ([commonUtils is24hourFormat]) {
                 cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
               cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventStartTime,commonUtils.getTimeZoneAbbrevation];
            }
            
            
        }
        else if([endDateFromApi isEarlierThan:today] ){
            cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
            cell.timeLbl.text = [NSString stringWithFormat:@"ended at %@. %@",endMonth,endDate];
        }
        else{
            NSDate* date1 = startDateFromApi;
            NSDate* date2 = endDateFromApi;
            NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
            double secondsInAnHour = 3600;
            NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            if (hoursBetweenDates>24) {
                if ([commonUtils is24hourFormat]) {
                    cell.timeLbl.text = [NSString stringWithFormat:@"begins %@ %@",[commonUtils convertTo24Hour:eventStartTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    cell.timeLbl.text = [NSString stringWithFormat:@"begins %@ %@",eventStartTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
            else{
                if ([commonUtils is24hourFormat]) {
                    cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventStartTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
          
            
        }
        
        
        
        
        
        
    }
    
    else if ([serverStartTime isEqualToString:serverEndTime]) {
        cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
        cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
        cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
        if ([commonUtils is24hourFormat]) {
            cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
        }
        else{
          cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
        }
        
    }
    
    else {
        
        
        
        
        
        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
        //    NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:addedIces[indexPath.row][@"end_date"]];
        
        
        
        [startTimeformatter setDateFormat:@"yyyy-M-dd"];
        
        
        NSString *todayDateString = [startTimeformatter stringFromDate:today];
        NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
        
        NSMutableDictionary *dic = addedIces[indexPath.row];
        [self setHeaderDateTime:cell :dic];
        
        
    }

    [cell.userIV sd_setImageWithURL:[NSURL URLWithString:imageUrl]
                    placeholderImage:[UIImage imageNamed:@"user_avatar"]
                             options:(SDWebImageContinueInBackground,SDWebImageProgressiveDownload) ];
    return cell;
}

- (void) onUserProfilePage:(UIButton *)sender
{
    UserProfileVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileVC"];
    vc.otherUserID = [NSString stringWithFormat:@"%@",addedIces[sender.tag][@"get_user"][@"id"]];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)onEventDetailPage:(UIButton*)sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
    
    BOOL isLive = [addedIces[sender.tag][@"is_live"]boolValue];
    NSDate *today = [NSDate date];
    
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDate *endDateFromApi = [startTimeformatter dateFromString:addedIces[sender.tag][@"end_date"]];
    
    if (!isLive) {
        if([endDateFromApi isEarlierThan:today] ){
        }
        else{
            vc.activitiesType = @"begin";
        }
        
    }
    else{
        vc.activitiesType = @"notbegin";
    }
    
    vc.selectedTag = sender.tag;
    vc.comingFrom = @"events";
    vc.eventType = @"events";
    commonUtils.activitiesArray = addedIces;
    vc.iceDetails = addedIces[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getAddedIced:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        NSString *timezone = [commonUtils getTimeZone];
           NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
     NSString *urlString = [NSString stringWithFormat:@"%@get_activities/%@?time_zone=%@&skip=%@",ServerUrl,commonUtils.commutilUserID,timezone,skip];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
                 
                      [self didFinishRecordsRequest:successDic[@"added_activities"] forPage:currentPagingNumber];
                    
                }
                if(Jerror!=nil)
                {
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
        }];
        
    }
    
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(mainTV.contentOffset.y >= (mainTV.contentSize.height - mainTV.bounds.size.height)) {
        
        //NSLog(@" scroll to bottom!");
        if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            isPageRefresing = YES;
            if (currentPagingNumber<(commonUtils.userTotalAdded/12)) {
                currentPagingNumber = currentPagingNumber +1;
                [self getAddedIced:currentPagingNumber];
            }
            
        }
    }
    
}
-(void)didFinishRecordsRequest:(NSArray *)results forPage:(NSInteger)pageNo{
    if(pageNo == 0){
        addedIces = [results mutableCopy];
    }
    else{
        [addedIces addObjectsFromArray:results];
    }
    isPageRefresing = NO;
    [mainTV reloadData];
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    
    
}
-(void)setHeaderDateTime:(EventsAddedTVCell*)cell :(NSMutableDictionary*)iceDetails{
    NSDate *today = [NSDate date];
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    NSDate *todayDate = [self toLocalTime:today];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDate * endDateFromApi = [startTimeformatter dateFromString:iceDetails[@"end_date"]];
    [startTimeformatter setDateFormat:@"yyyy-M-dd"];
    NSString *todayDateString = [startTimeformatter stringFromDate:today];
    NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
    NSString *eventEndTime = [NSString stringWithFormat:@"%@",iceDetails[@"event_end_time"]];
    
    NSComparisonResult result;
    result = [endDateFromApi compare:today];
    if(result==NSOrderedDescending)
    {
        
        
        
        NSLog(@"server is large");
        if ([todayDateString isEqualToString:endDateString]) {
            cell.eventDayLbl.text  = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventMnthLbl.text  = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            if ([commonUtils is24hourFormat]) {
                cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
               cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
            
            
        }
        else{
            cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            cell.eventDayLbl.text= [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
            cell.timeLbl.text = [NSString stringWithFormat:@"ends %@. %@",endMonth,endDate];
        }
        
        
        
        
        
        
        
    }
    
    else if(result==NSOrderedAscending){
        NSLog(@"today date  is large");
        cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventDateLbl.text= [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventMnthLbl.text  = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
        cell.timeLbl.text = [NSString stringWithFormat:@"ended at %@. %@",endMonth,endDate];
        
        
    }
    
    
    else{
        cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        if ([commonUtils is24hourFormat]) {
            cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
        }
        else{
             cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
        }
       
        NSLog(@"Both dates are same");
    }
}

@end
