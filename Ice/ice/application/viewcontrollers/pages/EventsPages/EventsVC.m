//
//  EventsVC.m
//  ICE
//
//  Created by LandToSky on 12/27/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventsVC.h"
#import "EventsIcedVC.h"
#import "EventsAddedVC.h"

@interface EventsVC ()<UIScrollViewDelegate>
{
    EventsIcedVC *eventsIcedVC;
    EventsAddedVC *eventsAddedVC;
    
    // TabBar
    __weak IBOutlet UILabel *icedLbl;
    __weak IBOutlet UILabel *addedLbl;
    IBOutletCollection(UIButton) NSMutableArray *tabBarBtns;
    IBOutlet UILabel *underLbl;
    
    // ScrollView
    IBOutlet UIScrollView *tabScrollView;
    IBOutletCollection(UIView) NSMutableArray *pageView;
}

@end

@implementation EventsVC
@synthesize userId;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];    
}

- (void)initUI
{
    tabScrollView.delegate = self;
    
    /* Adjust Frame Layout */
    CGRect frame = CGRectZero;
    frame = tabScrollView.frame;
    for (int i = 0; i < 2; i++)
    {
        frame.origin.x = tabScrollView.frame.size.width * i;
        frame.origin.y = 0;
        [(UIView*)pageView[i] setFrame:frame];
    }
    
    [tabScrollView setContentSize:CGSizeMake(tabScrollView.frame.size.width*2, tabScrollView.frame.size.height)];
    [tabScrollView setPagingEnabled:YES];
}

- (void)initData
{
    
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSString * segueName = segue.identifier;
    if ([segueName isEqualToString: @"segue-events-iced"]) {
        eventsIcedVC.userID = userId;
        eventsIcedVC = segue.destinationViewController;
    } else if ([segueName isEqualToString:@"segue-events-added"]){
        eventsAddedVC.userID = userId;
        eventsAddedVC = segue.destinationViewController;
    }
}

#pragma mark - onTabBar Button
- (IBAction)onTabBarButton:(UIButton*)sender
{
    NSInteger index = [tabBarBtns indexOfObject:sender];
    [UIView animateWithDuration:0.3f
                     animations:^{
                         [tabScrollView setContentOffset:CGPointMake(tabScrollView.frame.size.width * index, 0)];
                         
                     }];
}
#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != tabScrollView) return;
    
    CGRect frame = underLbl.frame;
    frame.origin.x = scrollView.contentOffset.x / 2;
    underLbl.frame = frame;
    
    /* Get Current Page Number */
    CGFloat width = scrollView.frame.size.width;
    NSInteger page = (scrollView.contentOffset.x + (0.5f * width)) / width;
    
    if (page == 0) {
        [icedLbl setTextColor:appController.darkFontColor];
        [addedLbl setTextColor:appController.lightFontColor];
        
    } else if (page == 1){
        [icedLbl setTextColor:appController.lightFontColor];
        [addedLbl setTextColor:appController.darkFontColor];

    }
    
    [scrollView setScrollEnabled:YES];
}

@end
