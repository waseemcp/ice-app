//
//  EventsIcedVC.m
//  ICE
//
//  Created by LandToSky on 12/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "EventsIcedVC.h"
#import "EventsIcedTVCell.h"
#import "EventsPrivateTVCell.h"
#import "DetailVC.h"
#import "NSDate+Compare.h"
@interface EventsIcedVC () <UITableViewDelegate, UITableViewDataSource>
{
    IBOutlet UITableView *mainTV;
    NSMutableArray *icedActiviteis;
    int currentPageNumber;
    BOOL isPageRefresing;
}

@end

@implementation EventsIcedVC
@synthesize userID;
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self initData];
}

- (void) initUI
{
    mainTV.dataSource = self;
    mainTV.delegate = self;
    mainTV.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void) initData
{
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    currentPageNumber = 0;
    [self getIcedActitvities:currentPageNumber];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
  
}
#pragma mark - Table View Delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  
    int retValue = 0;
    if(icedActiviteis != nil){
        retValue = [icedActiviteis count];
    }
    return retValue;
}

- (UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventsIcedTVCell *cell = (EventsIcedTVCell*) [tableView dequeueReusableCellWithIdentifier:@"EventsIcedTVCell"];
    if (cell == nil) {
        cell = [[EventsIcedTVCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"EventsIcedTVCell"];
    }
    cell.eventTitleLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"title"]];
    cell.addressLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"location"]];
   // cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_day"]];
  //  cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_day_start"]];
  //  cell.eventMnthLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_month"]];
  //  cell.timeLbl.text = [NSString stringWithFormat:@"%@-%@ %@",icedActiviteis[indexPath.row][@"event_start_time"],icedActiviteis[indexPath.row][@"event_end_time"],commonUtils.getTimeZoneAbbrevation];
    BOOL isLive = [icedActiviteis[indexPath.row][@"is_live"]boolValue];
    if (isLive) {
        cell.liveLbl.hidden = NO;
    }
    else{
        cell.liveLbl.hidden = YES;
    }
    
    
    NSDate *today = [NSDate date];
    NSDate *todayDate = [self toLocalTime:today];
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDateFormatter *serverDateFormatter = [NSDateFormatter new];
    [serverDateFormatter setDateFormat:@"yyyy-M-dd"];
    NSDate *endDateFromApi = [startTimeformatter dateFromString:icedActiviteis[indexPath.row][@"end_date"]];
    NSDate *startDateFromApi = [startTimeformatter dateFromString:icedActiviteis[indexPath.row][@"start_date"]];
    
    NSString *serverStartTime = [serverDateFormatter stringFromDate:startDateFromApi];
    NSString *serverEndTime = [serverDateFormatter stringFromDate:endDateFromApi];
    if (!isLive) {
        
        
        cell.eventDayLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_day"]];
        cell.eventDateLbl.text = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_day_start"]];
        cell.eventMnthLbl.text  = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_month"]];
        NSString *eventStartTime = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_start_time"]];
        NSString *eventEndTime =  [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_end_time"]];
        if ([serverStartTime isEqualToString:serverEndTime]) {
            
            if (commonUtils.is24hourFormat) {
                 cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
                cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
           
            
        }
        else if([endDateFromApi isEarlierThan:today] ){
            cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
            cell.timeLbl.text = [NSString stringWithFormat:@"ended at %@. %@",endMonth,endDate];
        }
        else{
            NSString *eventStartTime = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_start_time"]];
            NSDate* date1 = startDateFromApi;
            NSDate* date2 = endDateFromApi;
            NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date1];
            double secondsInAnHour = 3600;
            NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
            if (hoursBetweenDates>24) {
                if ([commonUtils is24hourFormat]) {
                    cell.timeLbl.text = [NSString stringWithFormat:@"begins %@ %@",[commonUtils convertTo24Hour:eventStartTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    cell.timeLbl.text = [NSString stringWithFormat:@"begins %@ %@",eventStartTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
            else{
                if (commonUtils.is24hourFormat) {
                    cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
                }
                else{
                    cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
                }
            }
          
            
        }
        
        
        
        
        
        
    }
    
    else if ([serverStartTime isEqualToString:serverEndTime]) {
        cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
       cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
       cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
        NSString *eventStartTime = [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_start_time"]];
        NSString *eventEndTime =  [NSString stringWithFormat:@"%@",icedActiviteis[indexPath.row][@"event_end_time"]];
        if ([commonUtils is24hourFormat]) {
             cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",[commonUtils convertTo24Hour:eventStartTime],[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
        }
        else{
               cell.timeLbl.text = [NSString stringWithFormat:@"%@ - %@ %@",eventStartTime,eventEndTime,commonUtils.getTimeZoneAbbrevation];
        }
     
    }
    
    else {
        
        
        
        
        
        NSDate *today = [NSDate date];
        
        
        NSDate *todayDate = [self toLocalTime:today];
        //    NSLog(@"today date is %@",todayDate);
        NSDateFormatter *startTimeformatter = [NSDateFormatter new];
        [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
        
        NSDate * endDateFromApi = [startTimeformatter dateFromString:icedActiviteis[indexPath.row][@"end_date"]];
        
        
        
        [startTimeformatter setDateFormat:@"yyyy-M-dd"];
        
        
        NSString *todayDateString = [startTimeformatter stringFromDate:today];
        NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
        
        NSMutableDictionary *dic = icedActiviteis[indexPath.row];
        [self setHeaderDateTime:cell :dic];
        
        
    }
    
    
    
    cell.eventDetailBtn.tag = indexPath.row;
    [cell.eventDetailBtn addTarget:self action:@selector(onEventDetailPage:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if(mainTV.contentOffset.y >= (mainTV.contentSize.height - mainTV.bounds.size.height)) {
        
        //NSLog(@" scroll to bottom!");
       if(isPageRefresing == NO){ // no need to worry about threads because this is always on main thread.
            
            isPageRefresing = YES;
           if (currentPageNumber<(commonUtils.userTotalIced/12)) {
               currentPageNumber = currentPageNumber +1;
               [self getIcedActitvities:currentPageNumber];
           }
          
        }
    }
    
}
-(void)didFinishRecordsRequest:(NSArray *)results forPage:(NSInteger)pageNo{
    if(pageNo == 0){
        icedActiviteis = [results mutableCopy];
    }
    else{
        [icedActiviteis addObjectsFromArray:results];
    }
    isPageRefresing = NO;
    [mainTV reloadData];
}
- (void)onEventDetailPage:(UIButton*)sender
{
    
    DetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
     BOOL isLive = [icedActiviteis[sender.tag][@"is_live"]boolValue];
    NSDate *today = [NSDate date];

    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDate *endDateFromApi = [startTimeformatter dateFromString:icedActiviteis[sender.tag][@"end_date"]];

    if (!isLive) {
        if([endDateFromApi isEarlierThan:today] ){
        }
        else{
          vc.activitiesType = @"begin";
        }
        
    }
    else{
        vc.activitiesType = @"notbegin";
    }
    vc.selectedTag = sender.tag;
    vc.comingFrom = @"events";
    vc.eventType = @"events";
    commonUtils.activitiesArray = icedActiviteis;
    vc.iceDetails = icedActiviteis[sender.tag];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)getIcedActitvities:(int)currentPage{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus == NotReachable) {
        //my web-dependent code
        [commonUtils showAlert:@"Error!" withMessage:@"No Internet Connection Found"];
    }
    else {
        [commonUtils showHud:self.view];
        NSString *skip = [NSString stringWithFormat:@"%d",currentPage];
        NSString *timezone = [commonUtils getTimeZone];
        NSString *urlString = [NSString stringWithFormat:@"%@get_activities/%@?time_zone=%@&skip=%@",ServerUrl,commonUtils.commutilUserID,timezone,skip];
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        
        // Create a mutable copy of the immutable request and add more headers
        NSMutableURLRequest *mutableRequest = [request mutableCopy];
        [mutableRequest addValue:@"yXNboqr+rvELlRc7oq7lAP/tLQmV6mgmAhhbH8QAtvQ=" forHTTPHeaderField:@"app_key"];
        [mutableRequest addValue:commonUtils.sessionToken forHTTPHeaderField:@"session_token"];
        
        // Now set our request variable with an (immutable) copy of the altered request
        request = [mutableRequest copy];
        
        // Log the output to make sure our new headers are there
        NSLog(@"%@", request.allHTTPHeaderFields);
        
        
        NSURLResponse *response;
        
        NSError *error = nil;
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {

        if(error!=nil)
        {
            NSLog(@"web service error:%@",error);
        }
        else
        {
            if(data !=nil)
            {
                NSError *Jerror = nil;
                
                NSDictionary* json =[NSJSONSerialization
                                     JSONObjectWithData:data
                                     options:kNilOptions
                                     error:&Jerror];
                //   NSLog(@"user data is %@",json);
                if ([[json valueForKey:@"status"]isEqualToString:@"error"]){
                    if ([[json valueForKey:@"errorMessage"]isEqualToString:@"Session Expired"]){
                        [[NSNotificationCenter defaultCenter]
                         postNotificationName:@"Logout"
                         object:self];
                        
                        
                    }
                }else
                if ([[json valueForKey:@"status"]isEqualToString:@"success"]) {
                 
                    NSMutableDictionary *successDic = [[NSMutableDictionary alloc]init];
                    successDic = json[@"successData"];
//                    icedActiviteis = successDic[@"iced_activities"];
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        //Run UI Updates
                        [commonUtils hideHud];
                        [self didFinishRecordsRequest:successDic[@"iced_activities"] forPage:currentPageNumber];
                       // [mainTV reloadData];
                    });
                
                }
                if(Jerror!=nil)
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                          [commonUtils hideHud];
                    });
                    // NSLog(@"json error:%@",Jerror);
                }
            }
        }
         }];
        
    }
    
}
-(NSDate *) toLocalTime:(NSDate*)todayDate
{
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: todayDate];
    return [NSDate dateWithTimeInterval: seconds sinceDate: todayDate];
}
-(NSString*)getRequiredDate:(NSString*)format dateToConvert:(NSDate*)dateToConvert{
    NSDateFormatter *converter = [NSDateFormatter new];
    [converter setDateFormat:format];
    NSString *PassedDate = [converter stringFromDate:dateToConvert];
    //    [converter setDateFormat:format];
    NSLog(@"returned date %@",PassedDate);
    return PassedDate;
    
    
}
-(void)setHeaderDateTime:(EventsIcedTVCell*)cell :(NSMutableDictionary*)iceDetails{
    NSDate *today = [NSDate date];
    NSDateFormatter *startTimeformatter = [NSDateFormatter new];
    NSDate *todayDate = [self toLocalTime:today];
    [startTimeformatter setDateFormat:@"yyyy-M-dd HH:mm:ss"];
    NSDate * endDateFromApi = [startTimeformatter dateFromString:iceDetails[@"end_date"]];
    [startTimeformatter setDateFormat:@"yyyy-M-dd"];
    NSString *todayDateString = [startTimeformatter stringFromDate:today];
    NSString *endDateString = [startTimeformatter stringFromDate: endDateFromApi];
    
    NSComparisonResult result;
    result = [endDateFromApi compare:today];
    if(result==NSOrderedDescending)
    {
        
      
        
        NSLog(@"server is large");
        if ([todayDateString isEqualToString:endDateString]) {
           cell.eventDayLbl.text  = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
           cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            cell.eventMnthLbl.text  = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
            NSString *eventEndTime = [NSString stringWithFormat:@"%@",iceDetails[@"event_end_time"]];
            if ([commonUtils is24hourFormat]) {
                cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
            }
            else{
               cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
            }
            
            
        }
        else{
            cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:todayDate]uppercaseString];
            cell.eventDayLbl.text= [[self getRequiredDate:@"EEE" dateToConvert:todayDate]uppercaseString];
            cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:today]uppercaseString];
            NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
            NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
            cell.timeLbl.text = [NSString stringWithFormat:@"ends %@. %@",endMonth,endDate];
        }
        
        
        
        
        
        
        
    }
    
    else if(result==NSOrderedAscending){
        NSLog(@"today date  is large");
        cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
       cell.eventDateLbl.text= [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventMnthLbl.text  = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endDate = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        NSString *endMonth = [self getRequiredDate:@"MMM" dateToConvert:endDateFromApi];
        cell.timeLbl.text = [NSString stringWithFormat:@"ended at %@. %@",endMonth,endDate];
        
        
    }
    
    
    else{
         cell.eventDayLbl.text = [[self getRequiredDate:@"EEE" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventDateLbl.text = [[self getRequiredDate:@"d" dateToConvert:endDateFromApi]uppercaseString];
        cell.eventMnthLbl.text = [[self getRequiredDate:@"MMM" dateToConvert:endDateFromApi]uppercaseString];
        NSString *eventEndTime = [NSString stringWithFormat:@"%@",iceDetails[@"event_end_time"]];
        if ([commonUtils is24hourFormat]) {
             cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",[commonUtils convertTo24Hour:eventEndTime],commonUtils.getTimeZoneAbbrevation];
        }
        else{
            cell.timeLbl.text = [NSString stringWithFormat:@"until %@ %@",eventEndTime,commonUtils.getTimeZoneAbbrevation];
        }
       
        NSLog(@"Both dates are same");
    }
}


@end
