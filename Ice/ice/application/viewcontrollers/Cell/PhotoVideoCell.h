//
//  PhotoVideoCell.h
//  ICE
//
//  Created by MAC MINI on 30/10/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#define MAXIMUM_SCALE 2.0
#define MINIMUM_SCALE  1.0
@interface PhotoVideoCell : UICollectionViewCell<UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView_main;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIButton *btnPlay;

- (void)setup ;
@end
