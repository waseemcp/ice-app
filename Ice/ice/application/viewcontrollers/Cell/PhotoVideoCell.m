//
//  PhotoVideoCell.m
//  ICE
//
//  Created by MAC MINI on 30/10/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "PhotoVideoCell.h"

@implementation PhotoVideoCell
- (void)setup {
    
    _scrollView.delegate = self;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    
    _scrollView.frame = CGRectMake(0, 0, screenWidth, screenHeight);
    
   // _scrollView.backgroundColor = [UIColor redColor];
   // _imageView_main.backgroundColor = [UIColor orangeColor];
    UIImage *image = self.imageView_main.image;
    CGFloat ratio = CGRectGetWidth(self.scrollView.bounds) / image.size.width;
    self.imageView_main.bounds = CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), image.size.height * ratio);
    self.imageView_main.center = CGPointMake(CGRectGetMidX(self.scrollView.bounds), CGRectGetMidY(self.scrollView.bounds));
    self.imageView_main.image = image;
    self.scrollView.clipsToBounds = YES;
    self.scrollView.contentSize = self.imageView_main.bounds.size;
    self.scrollView.zoomScale = 1.0;
    self.scrollView.maximumZoomScale = 10.0;
    self.scrollView.minimumZoomScale = 1.0;

    
}




//-----------------------------------------------------------------------

#pragma mark - Custom Methods

- (void)zoomImage:(UIPinchGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded
        || gesture.state == UIGestureRecognizerStateChanged) {
        NSLog(@"gesture.scale = %f", gesture.scale);

        CGFloat currentScale = self.imageView_main.frame.size.width / self.bounds.size.width;
        CGFloat newScale = currentScale * gesture.scale;
        NSLog(@"newscale %f",newScale);
        if (newScale < MINIMUM_SCALE) {
            newScale = MINIMUM_SCALE;
        }
        if (newScale > MAXIMUM_SCALE) {
            newScale = MAXIMUM_SCALE;
        }

        CGAffineTransform transform = CGAffineTransformMakeScale(newScale, newScale);
        self.imageView_main.transform = transform;
        NSLog(@"view height is %f",self.frame.size.height);
         NSLog(@"view width is %f",self.frame.size.width);
         NSLog(@"view x is %f",self.frame.origin.x);
        NSLog(@"view y is %f",self.frame.origin.y);
         NSLog(@"imageview height is %f",self.imageView_main.frame.size.height);
        NSLog(@"imageview width is %f",self.imageView_main.frame.size.width);
        NSLog(@"imageview x is %f",self.imageView_main.frame.origin.x);
        NSLog(@"imageview y is %f",self.imageView_main.frame.origin.y);
        _scrollView.contentSize = self.imageView_main.frame.size;

    }

}
#pragma  mark  - Scrollview Delegate Method

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView_main;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
  
    
        UIView *subView = self.imageView_main;
        CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
        (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
        
        CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
        (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
        
        subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                     scrollView.contentSize.height * 0.5 + offsetY);
    if (scrollView.zoomScale>1.0) {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"zoomin"
         object:self];
    }
    else{
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"zoomout"
         object:self];
    }

}
@end
