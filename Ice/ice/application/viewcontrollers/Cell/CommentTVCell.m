//
//  CommentTVCell.m
//  ICE
//
//  Created by LandToSky on 11/23/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import "CommentTVCell.h"

@implementation CommentTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
