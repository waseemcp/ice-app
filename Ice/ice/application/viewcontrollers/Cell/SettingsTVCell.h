//
//  SettingsTVCell.h
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTVCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLbl;
@property (nonatomic, strong) IBOutlet UILabel *nameLbl;
@property (nonatomic, strong) IBOutlet UIButton *settingsBtn;
@property (nonatomic, strong) IBOutlet UIImageView *arrowIv;

@end
