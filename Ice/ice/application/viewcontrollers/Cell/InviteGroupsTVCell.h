//
//  InviteGroupsTVCell.h
//  ICE
//
//  Created by LandToSky on 1/21/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteGroupsTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectGroupBtn;
@property (weak, nonatomic) IBOutlet UILabel *groupName;
@property (weak, nonatomic) IBOutlet UILabel *groupCount;

@end
