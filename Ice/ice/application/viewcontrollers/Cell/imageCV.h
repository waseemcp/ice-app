//
//  imageCV.h
//  ICE
//
//  Created by MAC MINI on 04/08/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imageCV : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *eventIV;
@property (weak, nonatomic) IBOutlet UIButton *videoPlayBtn;

@end
