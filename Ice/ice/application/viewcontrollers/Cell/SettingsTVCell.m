//
//  SettingsTVCell.m
//  ICE
//
//  Created by LandToSky on 1/9/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "SettingsTVCell.h"

@implementation SettingsTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
     [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
