//
//  InviteFriendTVCell.h
//  ICE
//
//  Created by LandToSky on 1/21/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendTVCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *selectFriendBtn;
@property (weak, nonatomic) IBOutlet UILabel *inviteFriendNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *inviteFriendImage;

@end
