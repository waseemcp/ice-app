//
//  imagesCell.h
//  ICE
//
//  Created by MAC MINI on 27/07/2017.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imagesCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageToChoose;
@property (weak, nonatomic) IBOutlet UIButton *imgBtn;


@end
