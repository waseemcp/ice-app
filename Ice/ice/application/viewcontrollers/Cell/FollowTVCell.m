//
//  FollowTVCell.m
//  ICE
//
//  Created by LandToSky on 1/10/17.
//  Copyright © 2017 LandToSky. All rights reserved.
//

#import "FollowTVCell.h"

@implementation FollowTVCell

- (void)awakeFromNib {
    [super awakeFromNib];
     [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
