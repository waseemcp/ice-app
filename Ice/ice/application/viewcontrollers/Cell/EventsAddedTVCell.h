//
//  EventsAddedTVCell.h
//  ICE
//
//  Created by LandToSky on 12/28/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsAddedTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *liveLbl;
// Date

// Profile Image
@property (nonatomic, strong) IBOutlet UIButton* userProfileBtn;

// Event
@property (nonatomic, strong) IBOutlet UIButton *eventDetailBtn;

@property (weak, nonatomic) IBOutlet UILabel *eventDayLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventMnthLbl;
@property (weak, nonatomic) IBOutlet UILabel *eventTitleLbl;
@property (weak, nonatomic) IBOutlet UILabel *timeLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIImageView *userIV;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@end
