//
//  EventImageCVCell.h
//  ICE
//
//  Created by LandToSky on 11/13/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncDisplayKit/AsyncDisplayKit.h>

@class SPAsyncVideoView;
@interface EventImageCVCell : UICollectionViewCell<ASVideoNodeDelegate>


@property (nonatomic, strong) IBOutlet UIImageView *eventIv;
@property (nonatomic, strong) IBOutlet UIImageView *ImgViewBG;

@property (weak, nonatomic) IBOutlet UIButton *eventImageSelectBtn;


// Detail Edit Page
@property (nonatomic, strong) IBOutlet UIButton *closeBtn;

@property (weak, nonatomic) IBOutlet UIImageView *videoplayImg;
@property (weak, nonatomic) IBOutlet UIButton *videoPlayBtn;



@property (strong,nonatomic)NSURL *videoUrl;
@property (nonatomic)CGRect frame;
@property (weak,nonatomic)IBOutlet UIView *video_player_view;
@property (strong,nonatomic)  ASVideoNode *videoNode;
@property (strong,nonatomic) ASDisplayNode *mainNode;
@end
