//
//  AppDelegate.h
//  ICE
//
//  Created by LandToSky on 11/10/16.
//  Copyright © 2016 LandToSky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>
#import <GooglePlaces/GooglePlaces.h>
#import <GoogleMaps/GoogleMaps.h>
#import "IcedDoneAlertView.h"
#import "CustomIOSAlertView.h"
#import "GoogleSignIn/GoogleSignIn.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate,UNUserNotificationCenterDelegate,UIAlertViewDelegate,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, retain) CLLocationManager *locationManager;
@property (strong, nonatomic) NSDate *lastTimestamp;
@property (nonatomic) CustomIOSAlertView *iceDoneAlert;
@property (nonatomic) IcedDoneAlertView *customAlertView;
@property ( nonatomic) CustomIOSAlertView *visibleAlertView;
@property (strong, nonatomic) NSString *strFollowerCount;
@property (strong, nonatomic) NSString *strFollowingCount;
@property (strong, nonatomic) NSString *strActivityCount;
@property int ViewMain;

@property (nonatomic)NSMutableArray *imagesArrayCopy;
@property CGRect frame;
@property BOOL isSet;

- (void)updateLocationManager;
-(void) logOut;
-(void)getAddressFromCoordinates;
-(void)iceDone:(UIButton*)sender;

+(AppDelegate *)sharedAppDelegate;

@property (copy) NSTimer *updateloctimer;
@end

